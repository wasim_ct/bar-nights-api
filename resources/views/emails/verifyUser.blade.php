<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Bar Nights | Email Verification</title>
    <style type="text/css">
        body {
            font-family: 'Open Sans', sans-serif;
            margin: 0px;
            background-color:  #f3f6fb;
        }

        #box {
            background-color: #f1f1f1;
            box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
            width: 600px;
            padding: 30px;
            margin: 0px auto;
        }

        .button {
            background-image: linear-gradient(to right, #d90130, #2c48ac);
            color: white;
            margin: 8px 0px;
            border-radius: 4px;
            text-align: center;
            padding: 0px;
            border: none;
            margin-top: 20px;
            width: 100%;
            height: 45px;
            line-height: 45px;
            /*opacity: 0.9;*/
        }
        .button a{
            text-align: center;
            color: #fff;
            text-transform: uppercase;
            text-decoration: none;
            font-weight: bold;
            float: left;
            width: 100%;
        }
        .text{
            
        }
        .text .title{
            text-align: left; 
            font-size: 16px;
            color:#3a3838; 
            font-weight: bold; 
            margin: 0 0 5px 0;
        }
        .text p{
            text-align: left; font-size: 14px; color:#212121;  margin: 0 0 10px 0px; line-height: 22px
        }
    </style>
</head>

<body>
    <div id="box">
        <div class="img" style="text-align: center;">
            <img src="{{url('images/logo.png')}}" width="100px">
        </div>

        <div class="text">
            <!--<h1 class="title">Hi there</h1>-->
            <p>Welcome to <?php echo config('app.name') ?></p>
            <p>Your registered email ID is {{$user['email']}}, Please click on the below button to verify your email account.</p>
        </div>

        <div class="button">
            <a href="{{url('/user/verify',$user->verifyUser->token)}}">Click to Verify</a>
        </div>
    </div>
</body>
</html>