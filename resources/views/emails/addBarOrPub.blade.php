
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Bar Nights | Login Details</title>
    <style type="text/css">
        body {
            font-family: 'Open Sans', sans-serif;
            margin: 0px;
            background-color:  #f3f6fb;
        }

        #box {
            background-color: #f1f1f1;
            box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
            width: 600px;
            padding: 30px;
            margin: 0px auto;
        }

        .button {
            background-image: linear-gradient(to right, #d90130, #2c48ac);
            color: white;
            margin: 8px 0px;
            border-radius: 4px;
            text-align: center;
            padding: 0px;
            border: none;
            margin-top: 20px;
            width: 100%;
            height: 45px;
            line-height: 45px;
            /*opacity: 0.9;*/
        }
        .button a{
            text-align: center;
            color: #fff;
            text-transform: uppercase;
            text-decoration: none;
            font-weight: bold;
            float: left;
            width: 100%;
        }
        .text{
            
        }
        .text .title{
            text-align: left; 
            font-size: 16px;
            color:#3a3838; 
            font-weight: bold; 
            margin: 0 0 5px 0;
        }
        .text p{
            text-align: left; font-size: 14px; color:#212121;  margin: 0 0 10px 0px; line-height: 22px
        }
    </style>

</head>


<body>
    <div id="box">
        <div class="img" style="text-align: center;">
            <img src="{{url('images/logo.png')}}" width="100px"></div>
 
            <div class="text">

                <h1 class="title">Hello <?php echo $name ?></h1>

                <p>Your account has been successfully created on <?php echo config('app.name') ?>. Please login to your account and complete the profile.</p>

                <h1 class="title">Login Details:</h1>

                <p><b>Email:</b> <?php echo $email ?><br>
                <b>Password:</b> <?php echo $password ?></p>

                <p><b>Note:</b> You need to click on <b>Activate Profile</b> button in My Profile after completing the profile, so we can activate your profile.</p>

                <?php /*
                <p style="color: grey; font-size: 16px; text-align: left;margin: 0px;">
                    <a href="<?php echo $link ?>" class="">Click here to complete your Bar profile</a>
                </p>
                */ ?>

                <div class="button">
                    <a href="<?php echo $link ?>">Login Now</a>
                </div>
            </div>

    </div>
</body>

</html>