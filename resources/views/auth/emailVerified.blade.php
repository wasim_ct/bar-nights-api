<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
    body {
        font-family: 'Open Sans', sans-serif;
        margin: 0px;
    }

    #box {
        background-color: white;
        box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
        width: 600px;
        text-align: center;
        padding: 50px;
        margin: 70px auto;
    }
    </style>
</head>

<body style="background-color:  #f3f6fb;">
    <div id="box">
        <div class="check">
            <img src="{{ url('images/logo.png') }}" width="100px">
        </div>
        <h2 style="font-size: 24px;  margin-top: 40px; font-weight: bold;">Your email has been verified successfully</h2>
        <div class="text" style="text-align: center; font-size: 16px;">
            <p> You can now login to your account by app</p>
        </div>
    </div>
</body>