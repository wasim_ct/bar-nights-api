<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
    body {
        font-family: 'Open Sans', sans-serif;
        margin: 0px;
    }

    #box {
        background-color: white;
        box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
        width: 600px;
        text-align: center;
        padding: 50px;
        margin: 70px auto;
    }
    </style>
</head>

<body style="background-color:  #f3f6fb;">
    <div id="box">
        <div class="check">
            <img src="{{ url('images/logo.png') }}" width="100px">
        </div>
        <h2 style="font-size: 24px; margin-top: 40px; font-weight: bold;">
            <?php if($status == 1) echo "Thank You!"; else echo "Sorry!"; ?>
        </h2>
        <div class="text" style="text-align: center; font-size: 16px; line-height: 24px">
            <p><?php echo $message ?></p>
        </div>
    </div>
</body>