<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
    body {
        font-family: 'Open Sans', sans-serif;
        margin: 0px;
    }

    #form {
        background-color: white;
        width: 600px;
        padding: 20px;
        margin: 70px auto;
        box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
    }



    input.inputs {
        padding: 10px;
        width: 100%;
        margin-top: 8px;
        height: 40px;
        display: block;
        border-radius: 4px;

        border: 1px #d6d0d0 solid;


    }

    input.button {

        background-image: linear-gradient(to right, #d90130, #2c48ac);
        color: white;
        height: 40px;
        margin: 8px 0;
        border-radius: 4px;
        border: none;
        margin-top: 20px;
        width: 100%;
        opacity: 0.9;
        cursor: pointer;
    }
    </style>
</head>

<body style="background-color: #f3f6fb;">

    <div id="form">
        <form method="POST" action="{{ route('bar.password.reset.submit') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="userType" value="B">

            <div class="img" style="text-align: center;padding-top: 15px;">
                <img src="{{ url('public/images/logo.png') }}" width="100px"></div>
            <h3 style="color:#212121; text-align: center;   font-family: Roboto; font-size: 20px;">Bar Reset password
            </h3>
            <div id="email">
                <label style="color:  #212121; font-size: 14px; font-weight: bold;" for="email"
                    class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                <input id="email" type="email"
                    class=" inputs form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                    value="{{ $email ?? old('email') }}" placeholder="Email" required readOnly autofocus>
                @if ($errors->has('email'))
                <span class="text text-alert" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div id="new" style="margin-top: 15px;">
                <label style="color:  #212121; font-size: 14px; font-weight: bold;" for="password"
                    class="col-md-4 col-form-label text-md-right">New Password</label>
                <input id="password" type="password"
                    class="inputs form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                    placeholder="New Password" required>
                @if ($errors->has('password'))
                <span class="text text-alert" style="color:red; margin-top:5px;" role="alert">
                    <strong style="color:red">{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div id="pass" style="margin-top: 15px;">
                <label style="color:#212121; font-size: 14px;margin-top: 5px;font-weight: bold;" for="password-confirm"
                    class="col-md-4 col-form-label text-md-right">Re-enter
                    Password</label>
                <input id="password-confirm" type="password" class="form-control inputs" name="password_confirmation"
                    required placeholder="Re-enter password">
            </div>
            <input type="submit" name="submit" class="btn btn-primary button" value="Reset Password">
    </div>
    </div>
    </form>
    </div>

</body>

</html>