<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
    body {
        font-family: 'Open Sans', sans-serif;
        margin: 0px;
    }

    #box {
        background-color: white;
        box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15);
        width: 600px;
        text-align: center;
        padding: 50px;
        margin: 70px auto;
    }

    .button {
        background-image: linear-gradient(to right, #d90130, #2c48ac);
        color: white;
        padding-top: 10px;
        height: 30px;
        border-radius: 4px;
        text-decoration: none;
        border: none;
        width: 100%;
        opacity: 0.9;
        display: block;
    }
    </style>
</head>

<body style="background-color:  #f3f6fb;">
    <div id="box">
        <div class="check">
            <img src="{{ url('images/logo.png') }}" width="100px">
        </div>
        <h2 style="font-size: 24px; font-weight: bold;">Your password has been reset successfully</h2>
        <div class="text" style="text-align: center; font-size: 16px;margin-bottom:40px;">
            <p> You can now login to your account by using link given below</p>
        </div>
        <div style="width:100%;">
            <!--  -->
            <!-- <a type="button" class="btn btn-primary button" class="href=" #">Login</a> -->
            <a href="http://104.197.28.169" class="btn btn-primary button"> Login</a>
        </div>


    </div>
</body>