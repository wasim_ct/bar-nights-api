<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});*/
 
//====User Signup-Login APIs=====
Route::post('login', 'API\Mobile\UserController@login');
Route::post('logout', 'API\Mobile\UserController@logout');
Route::post('register', 'API\Mobile\UserController@register');
Route::post('registerStep2', 'API\Mobile\UserController@registerStep2');
Route::post('resendVerifyEmail', 'API\Mobile\UserController@resendVerifyEmail');
Route::post('socialMediaSignup', 'API\Mobile\UserController@socialMediaSignup');
Route::post('socialMediaLogin', 'API\Mobile\UserController@socialMediaLogin');
Route::post('forgotPassword', 'API\Mobile\UserController@forgotPassword');
Route::post('changePassword', 'API\Mobile\UserController@changePassword');
Route::post('checkCurrentPassword', 'API\Mobile\UserController@checkCurrentPassword');
Route::post('contactSupport', 'API\Mobile\UserController@contactSupport');
Route::post('uploadProfilePic', 'API\Mobile\UserController@uploadProfilePic');
Route::post('updateMyProfile', 'API\Mobile\UserController@updateMyProfile');
Route::post('updateLatLong', 'API\Mobile\UserController@updateLatLong');
Route::post('updateFirebaseToken', 'API\Mobile\UserController@updateFirebaseToken');
Route::post('getMyProfile', 'API\Mobile\UserController@getMyProfile');
Route::post('getAllUsers', 'API\Mobile\UserController@getAllUsers');
 
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('getUserProfile', 'API\Mobile\UserController@getUserProfile');
});


//====Patron APIs=====
Route::post('getPatronProfile', 'API\Mobile\PatronController@getPatronProfile');
Route::post('udpdateIncognito', 'API\Mobile\PatronController@udpdateIncognito');
Route::post('getWalletBalance', 'API\Mobile\PatronController@getWalletBalance');
Route::post('loadCityAreas', 'API\Mobile\PatronController@loadCityAreas');
Route::post('updateNotificationSetting', 'API\Mobile\PatronController@updateNotificationSetting');

//====BAR APIs=====
Route::post('getBarProfile', 'API\Mobile\BarController@getBarProfile');
Route::post('getBarSpeicalEvents', 'API\Mobile\BarController@getBarSpeicalEvents');
Route::post('getBarOffers', 'API\Mobile\BarController@getBarOffers');
Route::post('getBarPatrons', 'API\Mobile\BarController@getBarPatrons');
Route::post('getBarMultimedia', 'API\Mobile\BarController@getBarMultimedia');
Route::post('getBarReviews', 'API\Mobile\BarController@getBarReviews');
Route::post('getBarMenu', 'API\Mobile\BarController@getBarMenu');
Route::post('getBarProfileCart', 'API\Mobile\BarController@getBarProfileCart');
Route::post('getBarVisibilityStatus', 'API\Mobile\BarController@getBarVisibilityStatus');

//====Home Page APIs=====
Route::post('getHomeData', 'API\Mobile\AppController@getHomeData');
Route::post('getBarsListing', 'API\Mobile\AppController@getBarsListing');
Route::get('getCommonData', 'API\Mobile\AppController@getCommonData');
Route::post('loadActiveUsers', 'API\Mobile\AppController@loadActiveUsers');
Route::post('loadHomeData', 'API\Mobile\AppController@loadHomeData');
Route::post('loadHomePrevData', 'API\Mobile\AppController@loadHomePrevData');
Route::post('searchRecords', 'API\Mobile\AppController@searchRecords');
Route::post('searchAreaNewsFeeds', 'API\Mobile\AppController@searchAreaNewsFeeds');
Route::post('getLocalAreas', 'API\Mobile\AppController@getLocalAreas');

//====Visit APIs=====
Route::post('bookVisit', 'API\Mobile\VisitController@bookVisit');
Route::post('getUserVisits', 'API\Mobile\VisitController@getUserVisits');
Route::post('updateVisitStatus', 'API\Mobile\VisitController@updateVisitStatus');
Route::post('sendVisitInvitation', 'API\Mobile\VisitController@sendVisitInvitation');
Route::post('getVisitInvitations', 'API\Mobile\VisitController@getVisitInvitations');
Route::post('getCurrentVisit', 'API\Mobile\VisitController@getCurrentVisit');
Route::post('removeVisitInvites', 'API\Mobile\VisitController@removeVisitInvites');
Route::post('getMyInvites', 'API\Mobile\VisitController@getMyInvites');
Route::post('changeInviteStatus', 'API\Mobile\VisitController@changeInviteStatus');
Route::post('getVisitStatus', 'API\Mobile\VisitController@getVisitStatus');

//====book table apis======
Route::post('bookTable', 'API\Mobile\BookTableController@bookTable');
Route::post('getTableBooking', 'API\Mobile\BookTableController@getTableBooking');
Route::post('getBookingDetails', 'API\Mobile\BookTableController@getBookingDetails');
Route::post('updateTablePayment', 'API\Mobile\BookTableController@updateTablePayment');

//====Pre-Order APIs=====
Route::post('addToCart', 'API\Mobile\OrderController@addToCart');
Route::post('getCartItems', 'API\Mobile\OrderController@getCartItems');
Route::post('updateCartItem', 'API\Mobile\OrderController@updateCartItem');
Route::post('clearCart', 'API\Mobile\OrderController@clearCart');
Route::post('insertPreOrder', 'API\Mobile\OrderController@insertPreOrder');
Route::post('removeCartItem', 'API\Mobile\OrderController@removeCartItem');
Route::post('getPreOrders', 'API\Mobile\OrderController@getPreOrders');
Route::post('preOrderDetails', 'API\Mobile\OrderController@preOrderDetails');

//===Order APIs=====
Route::post('insertOrder', 'API\Mobile\OrderController@insertOrder');
Route::post('getOrders', 'API\Mobile\OrderController@getOrders');
Route::post('getCurrentOrders', 'API\Mobile\OrderController@getCurrentOrders');
Route::post('getRecievedGifts', 'API\Mobile\OrderController@getRecievedGifts');
Route::post('getOrderReceipt', 'API\Mobile\OrderController@getOrderReceipt');
Route::post('getMyOrders', 'API\Mobile\OrderController@getMyOrders');
Route::post('updateOrderStatus', 'API\Mobile\OrderController@updateOrderStatus');
Route::post('updateOrderPayment', 'API\Mobile\OrderController@updateOrderPayment');

//====rating api====
Route::post('submitRating', 'API\Mobile\VisitController@submitRating');

//====special events api=====
Route::post('getEvents', 'API\Mobile\SpecialEventController@getEvents');
Route::post('bookEventTicket', 'API\Mobile\SpecialEventController@bookEventTicket');
Route::post('updateEventPayment', 'API\Mobile\SpecialEventController@updateEventPayment');
Route::post('getMyBookedEvents', 'API\Mobile\SpecialEventController@getMyBookedEvents');

//====News Feed api=====
Route::post('addNewsFeed', 'API\Mobile\NewsFeedController@addNewsFeed');
Route::post('postNewsComment', 'API\Mobile\NewsFeedController@postNewsComment');
Route::post('deleteComment', 'API\Mobile\NewsFeedController@deleteComment');
Route::post('postNewsReaction', 'API\Mobile\NewsFeedController@postNewsReaction');
Route::post('getMyNewsFeed', 'API\Mobile\NewsFeedController@getMyNewsFeed');
Route::post('getTaggedNewsFeed', 'API\Mobile\NewsFeedController@getTaggedNewsFeed');
Route::post('deleteNewsFeed', 'API\Mobile\NewsFeedController@deleteNewsFeed');
Route::post('getCommentsReactions', 'API\Mobile\NewsFeedController@getCommentsReactions');
Route::post('reportNewsFeed', 'API\Mobile\NewsFeedController@reportNewsFeed');
Route::post('getNewsFeedDetail', 'API\Mobile\NewsFeedController@getNewsFeedDetail');

//====My Contacts api=====
Route::post('getAllPatrons', 'API\Mobile\MyContactsController@getAllPatrons');
Route::post('getMyContacts', 'API\Mobile\MyContactsController@getMyContacts');

//====Friends list,request and add api=====
Route::post('addFriend', 'API\Mobile\MyContactsController@addFriend');
Route::post('getRequestList', 'API\Mobile\MyContactsController@getRequestList');
Route::post('getMyFriends', 'API\Mobile\MyContactsController@getMyFriends');
Route::post('deleteFriend', 'API\Mobile\MyContactsController@deleteFriend');
Route::post('updateFriendRequestStatus', 'API\Mobile\MyContactsController@updateFriendRequestStatus');
Route::post('addToBlockList', 'API\Mobile\MyContactsController@addToBlockList');

//====Chat Media APIs=====
Route::post('uploadChatMedia', 'API\Mobile\ChatController@uploadChatMedia');
Route::post('chatNotification', 'API\Mobile\ChatController@chatNotification');
Route::post('getFriendStatus', 'API\Mobile\ChatController@getFriendStatus');

//===notification api=====
Route::post('getNotifications', 'API\Mobile\ChatController@getNotifications');
Route::post('changeNotificationStatus', 'API\Mobile\ChatController@changeNotificationStatus');

//====Top Spenders api=====
Route::post('getTopSpenders', 'API\Mobile\ToppersController@getTopSpenders');

//====Story APIs=====
Route::post('addStory', 'API\Mobile\StoryController@addStory');
Route::post('getStories', 'API\Mobile\StoryController@getStories');
Route::post('getUserStories', 'API\Mobile\StoryController@getUserStories');
Route::post('deleteStory', 'API\Mobile\StoryController@deleteStory');
Route::post('addSeenStory', 'API\Mobile\StoryController@addSeenStory');
Route::post('deleteDailyStories', 'API\Mobile\StoryController@deleteDailyStories');

/* ######################### ######################### ######################### 
						====WEB PANEL APIs STARTING====
 ######################### ######################### #########################  */

//====Bar Login apis====
Route::post('barLogin', 'API\Web\LoginController@barLogin');
Route::post('barLogout', 'API\Web\LoginController@logout');

//====Bar Profile Apis=====
Route::post('barForgotPassword', 'API\Web\BarController@forgotPassword');
Route::post('barChangePassword', 'API\Web\BarController@barChangePassword');
Route::post('getBarProfileWeb', 'API\Web\BarController@getBarProfileWeb');
Route::post('getBarProfileData', 'API\Web\BarController@getBarProfileData');
Route::post('addBarMedia', 'API\Web\BarController@addBarMedia');
Route::post('getImagesVideos', 'API\Web\BarController@getImagesVideos');
Route::post('deleteBarMedia', 'API\Web\BarController@deleteBarMedia');
Route::post('addLicenseDoc', 'API\Web\BarController@addLicenseDoc');
Route::post('deleteLicenseDoc', 'API\Web\BarController@deleteLicenseDoc');
Route::post('getLicenseList', 'API\Web\BarController@getLicenseList');
Route::post('addBarBanners', 'API\Web\BarController@addBarBanners');
Route::post('editBarProfile', 'API\Web\BarController@editBarProfile');
Route::post('getBarReview', 'API\Web\BarController@getBarReviews');
Route::post('addEditBarTiming', 'API\Web\BarController@addEditBarTiming');
Route::post('editBarTiming', 'API\Web\BarController@editBarTiming');
Route::post('editBarTaxes', 'API\Web\BarController@editBarTaxes');
Route::post('getBarRank', 'API\Web\BarController@getBarRank');
Route::post('getTopBars', 'API\Web\BarController@getTopBars');
Route::post('updateBannerOrder', 'API\Web\BarController@updateBannerOrder');

//State & City APIs
Route::post('getAllStates', 'API\Web\BarController@getAllStates');
Route::post('getAllCities', 'API\Web\BarController@getAllCities');
Route::post('getAllAreas', 'API\Web\BarController@getAllAreas');

//====book table apis for bar======
Route::post('getAllTableBooking', 'API\Web\BookTableController@getAllTableBooking');
Route::post('changeBookTableStatus', 'API\Web\BookTableController@changeBookTableStatus');
Route::post('sendBookingReminder', 'API\Web\BookTableController@sendBookingReminder'); //CRON

//====Menu Apis====
Route::post('addEditMenuItem', 'API\Web\MenuItemController@addEditMenuItem');
Route::post('getCategories', 'API\Web\MenuItemController@getCategories');
Route::post('getMenu', 'API\Web\MenuItemController@getMenu');
Route::post('getAllCategories', 'API\Web\MenuItemController@getAllCategories');
Route::post('addEditSubCategory', 'API\Web\MenuItemController@addEditSubCategory');
Route::post('getCategoryData', 'API\Web\MenuItemController@getCategoryData');
Route::post('getMenuDetail', 'API\Web\MenuItemController@getMenuDetail');
Route::post('getItemsList', 'API\Web\MenuItemController@getItemsList');

//====Visit apis===
Route::post('getVisits', 'API\Web\VisitController@getVisits');
Route::post('checkCodeExist', 'API\Web\VisitController@checkCodeExist');
Route::post('availCode', 'API\Web\VisitController@availCode');
Route::post('getAllVisits', 'API\Web\VisitController@getAllVisits');
Route::post('changeVisitStatus', 'API\Web\VisitController@changeVisitStatus');

//===Offer apis===
Route::post('addEditOffer', 'API\Web\OfferController@addEditOffer');
Route::post('addEditItems', 'API\Web\OfferController@addEditItems');
Route::post('getOffersListing', 'API\Web\OfferController@getOffersListing');
Route::post('getOffersData', 'API\Web\OfferController@getOffersData');
Route::post('getOffersItems', 'API\Web\OfferController@getOffersItems');
Route::post('getAllOffersListing', 'API\Web\OfferController@getAllOffersListing');

//===SpecialEvent apis===
Route::post('getSpecialEvents', 'API\Web\SpecialEventController@getSpecialEvents');
Route::post('getSpecialEventsData', 'API\Web\SpecialEventController@getSpecialEventsData');
Route::post('addEditSpecialEvents', 'API\Web\SpecialEventController@addEditSpecialEvents');
Route::post('getEventDetails', 'API\Web\SpecialEventController@getEventDetails');
Route::post('getSpecialEventsFees', 'API\Web\SpecialEventController@getSpecialEventsFees');
Route::post('getTicketBookingList', 'API\Web\SpecialEventController@getTicketBookingList');
Route::post('getAllSpecialEvents', 'API\Web\SpecialEventController@getAllSpecialEvents');

//====Order apis===
Route::post('getVisitOrderInfo', 'API\Web\OrderController@getVisitOrderInfo');
Route::post('getOrdersList', 'API\Web\OrderController@getOrdersList');
Route::post('getOrderInvoice', 'API\Web\OrderController@getOrderInvoice');
Route::post('getOrderKots', 'API\Web\OrderController@getOrderKots');
Route::post('editKotItems', 'API\Web\OrderController@editKotItems');
Route::post('getKotDetail', 'API\Web\OrderController@getKotDetail');
Route::post('addOrderToCart', 'API\Web\OrderController@addOrderToCart');
Route::post('getOrderCartItems', 'API\Web\OrderController@getOrderCartItems');
Route::post('createOrder', 'API\Web\OrderController@createOrder');
Route::post('changeKotStatus', 'API\Web\OrderController@changeKotStatus');
Route::post('updatePaymentStatus', 'API\Web\OrderController@updatePaymentStatus');
Route::post('changeOrderStatus', 'API\Web\OrderController@changeOrderStatus');
Route::post('updateKotServeStatus', 'API\Web\OrderController@updateKotServeStatus');

//====Common functions===
Route::post('changeStatus', 'API\Web\CommonController@changeStatus');
Route::post('getTemplates', 'API\Web\CommonController@getTemplates');
Route::post('deleteData', 'API\Web\CommonController@deleteData');

//====Graph function====//
Route::post('loadGraph', 'API\Web\ReportController@loadGraph');
Route::post('sendDailyReport', 'API\Web\ReportController@sendDailyReport');
Route::post('closeBarBusinessDay', 'API\Web\ReportController@closeBarBusinessDay'); //CRON
Route::post('loadGraphApp', 'API\Web\ReportController@loadGraphApp');

/*################Patron Management Module Route(14-11-2019) #############################*/
Route::post('addPatron', 'API\Web\PatronManagementController@addPatron');
Route::post('editPatron', 'API\Web\PatronManagementController@editPatron');
Route::post('patronList', 'API\Web\PatronManagementController@patronList');
Route::post('patronProfileInfo', 'API\Web\PatronManagementController@patronProfileInfo');
Route::post('getPatronDetails', 'API\Web\PatronManagementController@getPatronDetails');
Route::post('changeUserStatus', 'API\Web\PatronManagementController@changeUserStatus');

/*################Bar Management Module Route(15-11-2019) #############################*/
Route::post('addBarOrPub', 'API\Web\BarOrPubManagementController@addBarOrPub');
Route::post('barList', 'API\Web\BarOrPubManagementController@barList');
Route::post('getTopSpenderList', 'API\Web\PatronManagementController@getTopSpenderList');
Route::post('getTopBarList', 'API\Web\BarOrPubManagementController@getTopBarList');
//Route::post('getTopBarList', 'API\Web\BarOrPubManagementController@getTopBarList');
Route::post('rejectBarRequest', 'API\Web\BarOrPubManagementController@rejectBarRequest');
Route::post('acceptBarRequest', 'API\Web\BarOrPubManagementController@acceptBarRequest');

/*################Visit Management Module Route(18-11-2019) #############################*/
Route::post('getAllVisitList', 'API\Web\VisitManagementController@getAllVisitList');
Route::post('getVisitDetails', 'API\Web\VisitManagementController@getVisitDetails');
Route::post('editVisitDetails', 'API\Web\VisitManagementController@editVisitDetails');
Route::post('getAllVisitCode', 'API\Web\VisitManagementController@getAllVisitCode');
Route::post('getAllGeoTagging', 'API\Web\VisitManagementController@getAllGeoTagging');

/*################Cms Management Module Route(18-11-2019) #############################*/
Route::post('editCmsSetting', 'API\Web\CmsManagementController@editCmsSetting');
Route::post('getCmsSetting', 'API\Web\CmsManagementController@getCmsSetting');
Route::post('editCommissionSetting', 'API\Web\CmsManagementController@editCommissionSetting');
Route::post('getCommisionSetting', 'API\Web\CmsManagementController@getCommissionSetting');
Route::post('updateCityAreas', 'API\Web\CmsManagementController@updateCityAreas');
Route::post('getCityAreas', 'API\Web\CmsManagementController@getCityAreas');

/*###########Special Event Fee Setup Management Module Route(19-11-2019)###############*/
Route::post('addSpecialEventFee', 'API\Web\SpecialEventFeeSetupManagementController@addFee');
Route::post('editSpecialEventFee', 'API\Web\SpecialEventFeeSetupManagementController@editFee');
Route::post('specialEventFeeList', 'API\Web\SpecialEventFeeSetupManagementController@getAllList');
Route::post('getSpecialEventFeeDetails', 'API\Web\SpecialEventFeeSetupManagementController@getFeeDetails');
Route::post('deleteSpecialEventFee', 'API\Web\SpecialEventFeeSetupManagementController@deleteFee');

/*###########Admin Auth management Module Route(19-11-2019)###############*/
Route::post('adminLogin', 'API\Web\AdminAuthManagementController@login');
Route::post('adminForgotPassword', 'API\Web\AdminAuthManagementController@forgotPassword');
Route::post('adminChangePassword', 'API\Web\AdminAuthManagementController@changePassword');

/*###########Admin newsfeed Module Route###############*/
Route::post('getReportedNewsFeed', 'API\Web\AdminNewsFeedController@getReportedNewsFeed');
Route::post('getNewsFeedReporters', 'API\Web\AdminNewsFeedController@getNewsFeedReporters');
Route::post('deleteReportedNewsFeed', 'API\Web\AdminNewsFeedController@deleteReportedNewsFeed');
Route::post('ignoreReportedNewsFeed', 'API\Web\AdminNewsFeedController@ignoreReportedNewsFeed');
Route::post('deleteNewsFeedMedia', 'API\Web\AdminNewsFeedController@deleteNewsFeedMedia');

/*###########Admin Ddashboard management Module Route(20-11-2019)###############*/
Route::post('getAdminDashboardData', 'API\Web\DashboardManagementController@getAdminDashboardData');
Route::post('loadAdminGraphData', 'API\Web\DashboardManagementController@loadAdminGraphData');
Route::post('getBarDashboardData', 'API\Web\DashboardManagementController@getBarDashboardData');
Route::post('getAppSettings', 'API\Web\DashboardManagementController@getAppSettings');
Route::post('getBarDashboard', 'API\Web\BarController@getBarDashboard');
Route::post('topBarListByCityName', 'API\Web\DashboardManagementController@topBarListByCityName');
Route::post('topSpenderListByCityName', 'API\Web\DashboardManagementController@topSpenderListByCityName');




/*##########################Payment ,commission histroy route#############################*/
Route::post('adminCommissionOrderWise', 'API\Web\PaymentCommissionManagementController@adminCommissionOrderWise');
Route::post('barIncomeMonthWise', 'API\Web\PaymentCommissionManagementController@barIncomeMonthWise');
Route::post('paidAmountToBarList', 'API\Web\PaymentCommissionManagementController@paidAmountToBarList');
Route::post('barIncomeStatement', 'API\Web\PaymentCommissionManagementController@barIncomeStatement');
Route::post('barIncomeWeekWise', 'API\Web\PaymentCommissionManagementController@barIncomeWeekWise');

/*##########################Report Management Controller 02-12-2019 #############################*/
Route::post('activeUserList', 'API\Web\ReportManagementController@activeUserList');
Route::post('offerAvailedHistory', 'API\Web\ReportManagementController@offerAvailedHistory');

//=======bar calculation, CRON Job Functions=========//
Route::post('addTopBars', 'API\Web\CommonController@addTopBars');
Route::post('addTopSpenders', 'API\Web\CommonController@addTopSpenders');
Route::post('excel', 'API\Web\CommonController@excel');

/*##########################Settlement Management B/W Bar and admin  17-12-2019 #############################*/
Route::post('getBarSettlementList', 'API\Web\PaymentSettlementController@getBarSettlementList');
Route::post('paySettlementAmount', 'API\Web\PaymentSettlementController@paySettlementAmount');
Route::post('getPaymentHistory', 'API\Web\PaymentSettlementController@getPaymentHistory');
Route::post('getBarDailyRevenueReport', 'API\Web\PaymentSettlementController@getBarDailyRevenueReport');

/*##########################Bar Panel Desktop Notification 26-12-2019 #############################*/
Route::post('getPendingOrderKots', 'API\Web\OrderController@getPendingOrderKots');
/*##########################Bar Panel Notification 01-01-2020 #############################*/
Route::post('getBarNotification', 'API\Web\BarNotificationController@getBarNotification');
Route::post('deleteBarNotification', 'API\Web\BarNotificationController@deleteBarNotification');
Route::post('deleteBarAllNotification', 'API\Web\BarNotificationController@deleteBarAllNotification');

/*########################## App Download Controller  03-02-2019 #############################*/
Route::post('addOrEditAppDownload', 'API\Web\AppDownloadController@addOrEditAppDownload');

/*########################## Staff Management Controller  10-02-2019 #############################*/
Route::post('addStaff', 'API\Web\StaffManagementController@addStaff');
Route::post('editStaff', 'API\Web\StaffManagementController@editStaff');
Route::post('staffList', 'API\Web\StaffManagementController@getStaffList');
Route::post('getStaffDetail', 'API\Web\StaffManagementController@getStaffDetail');
Route::post('deleteStaff', 'API\Web\StaffManagementController@deleteStaff');

/*########################## Template Management Controller  11-02-2019 #############################*/
Route::post('addTemplate', 'API\Web\TemplateManagementController@addTemplate');
Route::post('getTemplateList', 'API\Web\TemplateManagementController@getTemplateList');
Route::post('getTemplateDetails', 'API\Web\TemplateManagementController@getTemplateDetails');

/*##########################Transaction Management Controller  12-02-2019 #############################*/
Route::post('getAllTransaction', 'API\Web\TransactionManagementController@getAllTransaction');

//====Angular Testing APIs=====
Route::post('userLogin', 'API\Mobile\TestController@userLogin');
Route::post('uploadTestImage', 'API\Mobile\TestController@uploadTestImage');
Route::get('database-backup', 'API\Mobile\TestController@backupDatabaseTables');