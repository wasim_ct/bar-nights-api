<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'Controller@index');
Route::get('/about', 'Controller@about');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Patron - Email Verification Routes === */
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('user.verify');
Route::get('password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('user.password.reset');
Route::post('passwordReset', 'API\Mobile\UserController@reset')->name('user.password.reset.submit');

// Email Update route
Route::get('/user/update-email/{token}', 'Auth\RegisterController@verifyEmailUpdate')->name('user.verify');


/* Bar - Email Verification Routes === */
Route::get('bar/password/reset/{token}/{email}', 'Auth\ResetBarPasswordController@showResetForm')->name('bar.password.reset');
Route::post('bar/passwordReset', 'Auth\ResetBarPasswordController@reset')->name('bar.password.reset.submit');