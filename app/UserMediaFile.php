<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMediaFile extends Model
{
    public $table = 'bnUserMediaFiles';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'mediaName', 'mediaType', 'mediaSize', 'moduleId', 'createdAt', 'updatedAt',
    ];
}