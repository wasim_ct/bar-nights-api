<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class HappyHourItem extends Model
{
    public $table = 'bnHappyHourItems';
    public $timestamps = false;

    protected $fillable = [
        'id', 'hhTimingId', 'itemId', 'discountRate', 'createdAt', 'updatedAt',
    ];
}