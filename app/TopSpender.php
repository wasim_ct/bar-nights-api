<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class TopSpender extends Model
{
    public $table = 'bnTopSpenders';
    public $timestamps = false;

    protected $fillable = [
        'id', 'month', 'position', 'userId', 'amountSpent','cityId', 'createdAt', 'updatedAt',
    ];
    public static function getTopSpenderList($month = '', $limit = '', $startFrom = '', $keyword='')
    {
        //get city ids by city name searching
        $cityIds = array();
         if(!empty($keyword)){
            $city = DB::table('bnCities as city')->select('city_id as cityId')->where('city.city_name','LIKE',"%{$keyword}%")->get();
            if($city && count($city)){
                foreach($city as $key=>$value){
                    array_push($cityIds,$value->cityId);
                }
            }
        }
        $query = DB::table('bnTopSpenders as t');
        $query->select('t.userId', 't.position', 't.amountSpent', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName');
        if (!empty($month)) {
            $query->where('month', '=', strtolower($month));
        }
        $query->leftJoin('bnUsers as u', 'u.id', '=', 't.userId');
        $query->leftJoin('bnPatrons as p', 'p.userId', '=', 't.userId');
        //filter by city
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'u.id');
        if(!empty($keyword)){
            $query->whereIn('t.cityId', $cityIds);
        }
        //$query->orderBy('t.amountSpent', 'desc');
        $query->orderBy('t.position');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $result = $query->get();
        return $result;
    }
}