<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    public $table = 'bnUserNotifications';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'description', 'moduleId', 'moduleType', 'createdAt', 'updatedAt',
    ];
}
