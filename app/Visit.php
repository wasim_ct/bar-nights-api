<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Visit extends Model
{
    public $table = 'bnVisits';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'visitDate', 'visitTime', 'sittingType', 'tableStandingNo', 'status', 'createdAt', 'updatedAt', 'requestId', 'requestType', 'cancelReason', 'cancelBy',
    ];

    //This function is used for return all visit list
    public static function getAllList(){
    	$result= DB::table('bnVisits as v')
        ->select('v.id','v.userId','vc.code','b.barName','v.visitDate','v.visitTime','v.sittingType','tableStandingNo','v.status')
        ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'v.id')
        ->leftJoin('bnBars as b', 'b.id', '=', 'v.barId')
        ->get();
        return $result;
    }

    public static function getVisitDetails($id){
        $result= DB::table('bnVisits as v')
        ->select('v.id','v.userId','vc.code','vc.codeAvailed','v.visitDate','v.visitTime','v.status')
        ->where('v.id',$id)
        ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'v.id')
        ->get();
        return $result;
    }

}