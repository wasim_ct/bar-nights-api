<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarItem extends Model
{
    public $table = 'bnBarItems';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'itemName', 'catId', 'itemImage', 'description', 'itemType', 'subCatId', 'status', 'price', 'isCustomized', 'createdAt', 'updatedAt',
    ];
}