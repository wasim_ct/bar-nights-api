<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class HappyHoursTiming extends Model
{
    public $table = 'bnHappyHourTiming';
    public $timestamps = false;

    protected $fillable = [
        'id', 'odId', 'barId', 'weekDay', 'appliedOnDay', 'startTime', 'endTime', 'status', 'appliedOn', 'discountType', 'free', 'buy', 'discountRate', 'createdAt', 'updatedAt',
    ];
}