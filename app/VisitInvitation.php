<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitInvitation extends Model
{
    public $table = 'bnVisitInvitations';
    public $timestamps = false;

    protected $fillable = [
        'id', 'toUserId', 'visitId', 'status', 'createdAt', 'updatedAt',
    ];
}