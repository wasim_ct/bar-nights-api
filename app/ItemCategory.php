<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    public $table = 'bnItemCategories';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'catName', 'catImage', 'catDescription', 'parentId', 'status', 'createdAt', 'updatedAt',
    ];
}