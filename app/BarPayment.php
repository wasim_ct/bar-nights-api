<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class BarPayment extends Model
{
    public $table = 'bnBarPayments';
    public $timestamps = false;

    public static function paidAmountToBarList(){
	    $result= DB::table('bnBarPayments as p')
	    ->select('p.id','p.barId','p.paidAmount',DB::raw('CASE WHEN (p.paymentStatus = 1) THEN "paid" ELSE "due" END as status'),'createdAt')
	    ->get();
	    return $result;
    }
}