<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class BlockUser extends Authenticatable
{
    public $table = 'bnBlockedUsers';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'friendId', 'updatedAt', 'createdAt',
    ];
}