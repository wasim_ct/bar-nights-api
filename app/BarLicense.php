<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarLicense extends Model
{
    public $table = 'bnBarLicenses';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'licenseTypeId', 'licenseNumber', 'fileName', 'mediaType', 'createdAt', 'updatedAt',
    ];
}