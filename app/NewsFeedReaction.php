<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsFeedReaction extends Model
{
    public $table = 'bnNewsFeedReactions';
    public $timestamps = false;

    protected $fillable = [
        'id', 'newsId', 'userId', 'reactionId', 'createdAt', 'updatedAt',
    ];
}