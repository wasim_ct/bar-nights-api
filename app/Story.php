<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    public $table = 'bnStories';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'mediaName', 'mediaType', 'jsonData', 'visibility', 'isDeleted', 'createdAt', 'updatedAt',
    ];
}