<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use commonFunction;

class Bar extends Model
{
    public $table = 'bnBars';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'type', 'barName', 'about', 'logo', 'cgst', 'sgst', 'serviceCharge', 'serviceChargeTax', 'createdAt', 'updatedAt' , 'openingStatus', 'dayOnOffStatus'
    ];

    //This function is used for return all patron list
    public static function getAllList($startFrom, $limit, $keyword, $status, $city){
        $query= DB::table('bnUsers as u');
        $query->select('u.id','u.userName','u.email','u.phone','u.status','u.createdAt', 'u.inactiveReason', 'b.barName','b.type', 'b.logo', 'b.id as barId');
        $query->leftJoin('bnBars as b', 'b.userId', '=', 'u.id');
        $query->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId');
        $query->where('u.userType','B');
        if(!empty($keyword)){
            $query->where(function($query2) use ($keyword){
                $query2->where('u.userName','LIKE',"%{$keyword}%");
                $query2->orWhere('b.barName', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.email', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.phone', 'LIKE',"%{$keyword}%");
            });
        }

        if(!empty($city)){
            $cityId = commonFunction::getCityIdByName($city);
            if($cityId){
                $query->where('ad.cityId','=',$cityId);
            }
        }

        if($status == '1' || $status == '0' || $status == '2'){
            $query->where('u.status','=',$status);
        }
        if($status == '3' || $status == '4'){
            if($status == '4')
                $query->where('u.status','=',4);
            else {
                $query->where('u.status','!=',1);
                $query->where('u.status','!=',0);
                $query->where('u.status','!=',2);
            }
        }
        $query->orderBy('u.id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $result = $query->get();
        return $result;
    }
}