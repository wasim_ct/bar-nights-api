<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderKotItem extends Model
{
    public $table = 'bnOrderKotItems';
    public $timestamps = false;

    protected $fillable = [
        'id', 'orderId', 'unitPrice', 'kotId', 'itemId', 'itemName', 'quantity', 'unitTypeId', 'unitTypeName', 'amount', 'createdAt', 'updatedAt', 'suggestions',
    ];
}