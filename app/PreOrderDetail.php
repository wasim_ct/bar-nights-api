<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrderDetail extends Model
{
    public $table = 'bnPreOrderDetails';
    public $timestamps = false;

    protected $fillable = [
        'id', 'orderId', 'itemId', 'unitPrice', 'itemName', 'quantity', 'unitTypeId', 'unitTypeName', 'amount', 'createdAt', 'updatedAt', 'suggestions'
    ];
}