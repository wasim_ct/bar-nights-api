<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    public $table = 'bnPatrons';
    public $timestamps = false;

    protected $fillable = [
        'id', 'firstName', 'lastName', 'userId', 'profilePic',
    ];
    //This function is used for return all patron list
    public static function getAllList()
    {
        $result = DB::table('bnUsers as u')
            ->select('u.id as userId', 'u.userName', 'u.email', 'u.phone', 'u.status', 'p.profilePic', 'p.firstName', 'p.lastName', DB::raw('SUM(IFNULL(t.amount , 0 )) as totalSpentAmount'), 'u.createdAt')
            ->where('u.userType', 'u')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'u.id')
            ->leftJoin('bnTransactions as t', 't.userId', '=', 'u.id')
            ->groupBy('u.id')
            ->orderBY('totalSpentAmount', 'desc')
            ->get();
        return $result;
    }
    //This function is used for return patron profile info
    public static function getProfileInfo($id)
    {
        $result = DB::table('bnUsers as u')
            ->select('u.userName', 'u.email', 'u.phone', 'u.status', 'u.inactiveReason', 'p.userId', 'p.profilePic', 'p.firstName', 'p.lastName', 'u.createdAt')
            ->where('u.id', $id)
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'u.id')
            ->get();
        return $result;
    }
}