<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarReview extends Model
{
    public $table = 'bnBarReviews';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'moduleId', 'moduleType', 'rating', 'review', 'createdAt', 'updatedAt',
    ];
}