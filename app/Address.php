<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $table = 'bnAddresses';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'cityId', 'stateId', 'countryId', 'address', 'latitude', 'longitude', 'createdAt', 'updatedAt',
    ];
}