<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public $table = 'bnWallets';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'amount', 'createdAt', 'updatedAt',
    ];
}