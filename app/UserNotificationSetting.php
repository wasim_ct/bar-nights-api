<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotificationSetting extends Model
{
    public $table = 'bnUserNotificationSettings';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'latitude', 'longitude', 'areaId', 'createdAt', 'updatedAt',
    ];
}
