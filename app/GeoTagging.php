<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class GeoTagging extends Model
{
    public $table = 'bnGeoTaggings';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'visitId', 'createdAt', 'updatedAt',
    ];

    //This function is used for get all geo tagging list
    public static function getAllGeoTagging($startFrom, $limit, $keyword){
    	$query= DB::table('bnGeoTaggings as g');
        $query->select('g.userId', 'g.barId', 'b.barName','p.firstName','p.lastName', 'u.userName', 'o.grandTotal as orderAmount','c.code as visitCode');
        $query->leftJoin('bnBars as b', 'b.id', '=', 'g.barId');
        $query->leftJoin('bnPatrons as p', 'p.userId', '=', 'g.userId');
        $query->leftJoin('bnUsers as u', 'u.id', '=', 'g.userId');
        $query->leftJoin('bnOrders as o', 'o.visitId', '=', 'g.visitId');
        $query->leftJoin('bnVisitCodes as c', 'c.visitId', '=', 'g.visitId');
        if(!empty($keyword)){
            $query->where('p.firstName','LIKE',"%{$keyword}%");
            $query->orwhere('p.lastName','LIKE',"%{$keyword}%");
            $query->orwhere('u.email','LIKE',"%{$keyword}%");
            $query->orwhere('u.phone','LIKE',"%{$keyword}%");
            $query->orWhere('b.barName', 'LIKE',"%{$keyword}%");
            $query->orWhere('c.code', 'LIKE',"%{$keyword}%");
        }
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->orderBy('g.id', 'desc');
        $query->groupBy('g.id');
        $result = $query->get();
        return $result;
    }
}