<?php

namespace App;

use Illuminate\Foundation\Auth\bar as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class BarUser extends Authenticatable
{
    public $table = 'bnUsers';
    public $timestamps = false;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userName', 'phone', 'email', 'password', 'userType', 'status', 'email_verified_at', 'phone_verified_at', 'platform',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new Notifications\BarResetPassword($token));
    }
}