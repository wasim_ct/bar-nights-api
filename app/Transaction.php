<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Transaction extends Model
{
    public $table = 'bnTransactions';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'moduleId', 'moduleType','totalPrice','paymentMode', 'amount', 'transactionId', 'responseData', 'status','paymentDate', 'description', 'createdAt', 'updatedAt', 'remark',
    ];
    //This function is used for get total amount
    public static function totalPayment(){
        $today = date("Y-m-d");
    	$result = DB::table('bnTransactions as t')
    	   ->select(DB::raw('SUM(t.amount) as totalAmount'))
           ->where(['t.paymentDate' => $today])
           ->get();
    	
        if($result){
    		return $result[0]->totalAmount;
    	}else{
    		return 0;
    	}
    }
    public static function todayTotalPayment(){
    	$result = DB::table('bnTransactions as t')
    	->select(DB::raw('SUM(t.amount) as totalAmount'))
    	->where('createdAt', '>=',date('Y-m-d'))
    	->get();
    	if($result){
    		$payment = $result[0]->totalAmount;
    		return $payment?$payment:0;
    	}else{
    		return 0;
    	}
    }
}