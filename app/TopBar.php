<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class TopBar extends Model
{
    public $table = 'bnTopBars';
    public $timestamps = false;

    protected $fillable = [
        'id', 'month', 'position', 'barId', 'commissionRate','cityId','createdAt', 'updatedAt',
    ];
    public static function getTopBarList($month = '', $limit = '', $startFrom = '', $keyword='')
    {   //get city ids by city name searching
        $cityIds = array();
         if(!empty($keyword)){
            $city = DB::table('bnCities as city')->select('city_id as cityId')->where('city.city_name','LIKE',"%{$keyword}%")->get();
            if($city && count($city)){
                foreach($city as $key=>$value){
                    array_push($cityIds,$value->cityId);
                }
            }
        }
        //tob bar query
        $query = DB::table('bnTopBars as b');
        $query->select('b.id as barId', 'bar.barName', 'bar.logo', 'b.position','address.cityId');
        if (!empty($month)) {
            $query->where('month', '=', strtolower($month));
        }
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'b.barId');
        //filter by city name
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId');
        if(!empty($keyword)){
            $query->whereIn('b.cityId', $cityIds);
        }
        $query->orderBy('b.position', 'asc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $result = $query->get();
        return $result;
    }
}