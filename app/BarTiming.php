<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarTiming extends Model
{
    public $table = 'bnBarTimings';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'weekDay', 'openingStatus', 'openingTime', 'closingTime', 'createdAt', 'updatedAt',
    ];
}