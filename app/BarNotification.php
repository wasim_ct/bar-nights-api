<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarNotification extends Model
{
    public $table = 'bnBarNotifications';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'description', 'moduleId', 'moduleType', 'createdAt', 'updatedAt',
    ];
}
