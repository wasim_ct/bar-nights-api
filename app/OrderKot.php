<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderKot extends Model
{
    public $table = 'bnOrderKots';
    public $timestamps = false;

    protected $fillable = [
        'id', 'orderId', 'status', 'serveStatus', 'friendId', 'kotNumber', 'createdAt', 'updatedAt',
    ];
}