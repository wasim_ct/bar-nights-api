<?php

namespace App;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Friend extends Authenticatable
{
    public $table = 'bnFriendLists';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'friendId', 'status', 'updatedAt', 'createdAt',
    ];

    public static function getFriendNamebyFriendId($friendId,$userId){
    	$query= DB::table('bnFriendLists as f');
        $query->select('p.firstName','p.lastName');
        $query->where(['f.friendId' => $friendId, 'f.userId' => $userId]);
        $query->leftJoin('bnPatrons as p', 'p.userId', '=', 'f.friendId');
        $result = $query->get();
        if(count($result)){
            return $result[0]->firstName." ".$result[0]->lastName;
        }else{
            return false;
        }
    }
    public static function getFriendNamebyOrderId($orderId){
    	$query= DB::table('bnOrderKots as k');
        $query->select('k.kotNumber','k.friendId','p.firstName','p.lastName');
        $query->where(['k.orderId' => $orderId]);
        $query->where('k.friendid' ,'>', 0);
        $query->leftJoin('bnPatrons as p', 'p.userId', '=', 'k.friendId');
        $result = $query->get();
        if(count($result)){
            return $result;
        }else{
            return false;
        }
    }
}