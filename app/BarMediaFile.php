<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarMediaFile extends Model
{
    public $table = 'bnBarMediaFiles';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'mediaName', 'mediaType', 'mediaSize', 'createdAt', 'updatedAt',
    ];
}