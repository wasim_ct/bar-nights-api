<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialEvent extends Model
{
    public $table = 'bnSpecialEvents';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'title', 'description', 'eventImage', 'startDate', 'endDate', 'startTime', 'endTime', 'entryType', 'entryfees', 'bookingStartDate', 'bookingendDate', 'passApplicable', 'status', 'totalTickets', 'totalStandingTickets', 'publishedOn', 'duration', 'price', 'venue', 'coverCharges', 'discountRate', 'createdAt', 'updatedAt',
    ];
}