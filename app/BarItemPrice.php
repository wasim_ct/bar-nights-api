<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarItemPrice extends Model
{
    public $table = 'bnBarItemPricing';
    public $timestamps = false;

    protected $fillable = [
        'id', 'itemId', 'unitTypeId', 'price', 'createdAt', 'updatedAt',
    ];
}