<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use App\Patron;
use App\Visit;
use App\VisitCode;
use App\GeoTagging;
use DB;
class VisitManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }

    //This function is used for get all visit list
    public function getAllVisitList(Request $request)
    {
        $list = Visit::getAllList();
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for get visit details by visit id
    public function getVisitDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $list = Visit::getVisitDetails($request->visitId);
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list[0];
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }

    //This function is used for edit visit details
    public function editVisitDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
            'visitDate' => 'required',
            'visitTime' => 'required',
            'visitCode' => 'required',
            'codeAvailable' => 'required',
            'visitCodeStatus' => 'required',

        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        //visit table param 
        $visitParam = array(
            'visitDate' => date('Y-m-d', strtotime($request->visitDate)),
            'visitTime' => $request->visitTime,
            'status' => $request->visitCodeStatus,
            'updatedAt' => date('Y-m-d H:i:s')
        );
        //visit code param
        $codeparam = array(
            'code' => $request->visitCode,
            'codeAvailed' => $request->codeAvailable,
            'updatedAt' => date('Y-m-d H:i:s')
        );
        //update bnVisits table data
        DB::table('bnVisits')->where('id', $request->visitId)->update($visitParam);
        //check code is already generated
        $isVisitcodeGenerated = DB::table('bnVisitCodes')->select('id')->where('visitId', $request->visitId)->get();
        if($isVisitcodeGenerated && count($isVisitcodeGenerated)){
            //update visit code if already generated
            DB::table('bnVisitCodes')->where('visitId', $request->visitId)->update($codeparam);
        }else{
            //insert visit code if not generated
            $codeparam['visitId'] = $request->visitId;
            DB::table('bnVisitCodes')->insert($codeparam);
        }
        $result['status'] = 1;
        $result['message'] = "Data updated successfully!";
        return response()->json($result);
    }
    //this function is used for fet all visit code
    public function getAllVisitCode(Request $request)
    {
        $list = VisitCode::getAllCode();
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for get all geo tagginh list
    public function getAllGeoTagging(Request $request)
    {
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $list = GeoTagging::getAllGeoTagging($startFrom,$limit,$request->keyword);
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['limit'] = $limit;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
}