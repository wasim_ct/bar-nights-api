<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;
use App\Transaction;

class PaymentSettlementController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //This function is used for get bar settlement list of last week
    public function getBarSettlementList(Request $request)
    {
        //Pagination setup
        $limit = 10;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $city = commonFunction::getCityIdByName($request->keyword);
        //find earlier week  start date and end date 
        $previousWeek = strtotime("-1 week +1 day");
        $startWeek = strtotime("last monday midnight",$previousWeek);
        $endWeek = strtotime("next sunday",$startWeek);
        $startWeek = date("Y-m-d",$startWeek);
        $endWeek = date("Y-m-d",$endWeek);

        //query for geting transaction list
        $query = DB::table('bnTransactions as t');
        $query->select('t.barId');
        //$query->where('t.createdAt' ,'>=', $startWeek); //will uncomment this 
        //$query->where('t.createdAt' ,'<=', $endWeek); //will uncomment this 
        $query->leftJoin('bnBars as b', 'b.id', '=', 't.barId');
        $query->leftJoin('bnAddresses as address', 'b.userId', '=', 'address.userId');
        if(!empty($request->barId)){
            $query->where(['t.barId' => $request->barId]);
        }
        if(!empty($request->keyword)){
            if(!empty($city)){
                $query->where(['address.cityId' => $city]);
            }else{
                $query->where('b.barName','LIKE',"%{$request->keyword}%");
            }
        }
        $query->limit($limit);
        $query->offset($startFrom);
        $query->groupBY('t.barId');
        $barList = $query->get();
        
        $appSetting = CommonFunction::getAppSettings(); //Get app settings

        $settlement = array();
        $barIdArray = array();
        //echo count($barList);exit;
        if (count($barList)) 
        {
            foreach($barList as $barKey => $bar)
            {
                $query= DB::table('bnTransactions as t');
                $query->select('b.barName','t.barId','t.paymentMode','t.totalPrice','t.amount','t.moduleType');
                //$query->where('t.createdAt' ,'>=', $startWeek); //will uncomment this 
                //$query->where('t.createdAt' ,'<=', $endWeek); //will uncomment this 
                $query->leftJoin('bnBars as b', 'b.id', '=', 't.barId');
                $query->where(['t.barId' => $bar->barId]);
                $list = $query->get();

                foreach($list  as $key=>$value){
                    
                    // Get Bar Position
                    $possition = CommonFunction::getTopBarPossition($value->barId);

                    //Get Bar Last due amount
                    $dueAmount = 0; //CommonFunction::getBarDueAmount($value->barId);

                    // Get the settlement Status, if already paid for this week or not
                    $settlementStatus = CommonFunction::getSettlementStatus($value->barId, $startWeek, $endWeek); 

                    if(!empty($value->barId)){
                        //get commission rate based on bar possition
                        if($appSetting && count($appSetting))
                        {
                            if($possition == 1){
                                $commissionRate = $appSetting['topper1Comm'];
                            }
                            elseif($possition == 2){
                                $commissionRate = $appSetting['topper2Comm'];
                            }
                            elseif($possition == 3){
                                $commissionRate = $appSetting['topper3Comm'];
                            }
                            else{
                                $commissionRate = $appSetting['commission'];
                            }
                            $taxCommissionRate = $appSetting['taxOnCommission'];
                        }
                        else{
                            $commissionRate = 10;
                            $taxCommissionRate = 18;
                        }

                        $totalCashAmount = 0;
                        $totalOnlineAmount = 0;
                        $totalAmount = 0;
                        $totalAmountWithTax = 0;
                        $totalOrderAmt = 0;
                        $totalEventBookingAmt = 0;
                        $totalTableBookingAmt = 0;

                        //calculate total cash,online and total sell amount
                        foreach($list as $inner_key => $item)
                        {
                            if($item->barId == $value->barId)
                            {
                                $item->paymentMode = strtolower($item->paymentMode);
                                
                                if($item->paymentMode == 'online' || $item->paymentMode == 'bnwallet'){
                                    $totalOnlineAmount = $totalOnlineAmount + $item->totalPrice;
                                } 
                                elseif($item->paymentMode == 'cash' || $item->paymentMode == 'card' || $item->paymentMode == 'wallet'){
                                    $totalCashAmount =  $totalCashAmount + $item->totalPrice;
                                }

                                $totalAmount = $totalAmount + $item->totalPrice;
                                $totalAmountWithTax = $totalAmountWithTax + $item->amount;

                                if($item->moduleType == 'order')
                                    $totalOrderAmt = $totalOrderAmt + $item->amount;
                                else if($item->moduleType == 'event')
                                    $totalEventBookingAmt = $totalEventBookingAmt + $item->amount;
                                else if($item->moduleType == 'table')
                                    $totalTableBookingAmt = $totalTableBookingAmt + $item->amount;
                            }
                        }

                        // cash commission calculation
                        if($totalCashAmount > 0){
                            $cashCommission = ($totalCashAmount*$commissionRate/100);
                        }else{
                            $cashCommission = 0;
                        }

                        //online commission calculation
                        if($totalOnlineAmount > 0){
                            $onlineCommission = ($totalOnlineAmount*$commissionRate/100);
                        }else{
                            $onlineCommission = 0;
                        }

                        //total commission calculation
                        $totalCommission = $cashCommission + $onlineCommission;
                        //calculate gst on commission amount
                        if($totalCommission > 0){
                            $gstTax = ($totalCommission * $taxCommissionRate/100);
                        }else{
                            $gstTax = 0;
                        }

                        //calculate total payable amount
                        $totalPaybaleAmount = 0;
                        if($totalOnlineAmount > 0)
                        {
                            $totalCommission = $totalCommission + $gstTax;
                            $totalPaybaleAmount = $totalOnlineAmount - $totalCommission;
                            //$totalPaybaleAmount = $totalAmount - $totalCommission;
                        }

                        //temp array create
                        $temp_array = array(
                            'barId' => $value->barId,
                            'barName' => $value->barName,
                            'position' => $possition,
                            'taxRate' => $taxCommissionRate,
                            'commissionRate' => $commissionRate,
                            'totalAmountWithTax' => round($totalAmountWithTax,2),
                            'totalAmount' => round($totalAmount,2),
                            'CashAmount' => round($totalCashAmount,2),
                            'OnlineAmount' => round($totalOnlineAmount,2),
                            'cashCommission' => round($cashCommission,2),
                            'onlineCommission' => round($onlineCommission,2),
                            'gstTax' => round($gstTax,2),
                            'totalCommission' => round($totalCommission, 2),
                            'totalPaybaleAmount' => round($totalPaybaleAmount, 2),
                            'startWeek' => $startWeek,
                            'endWeek'   => $endWeek,
                            'previousDueAmount' => round($dueAmount,2),
                            'settlementStatus' => $settlementStatus,
                            'totalOrderAmt' => round($totalOrderAmt,2),
                            'totalEventBookingAmt' => round($totalEventBookingAmt,2),
                            'totalTableBookingAmt' => round($totalTableBookingAmt,2),
                        );
                        //check bar info already settelment or not
                        if(!in_array($value->barId, $barIdArray)) {
                            array_push($settlement, $temp_array);
                            array_push($barIdArray, $value->barId);
                        }
                    }
                }
            }
            //$result = array('status' => 1, 'message' => 'Data get successfully', 'data' => $list,'settelement'=> $settlement,'setting'=>$appSetting);
            $result = array('status' => 1, 'message' => 'Data get successfully','data'=> $settlement,'limit'=> $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return json_encode($result);
    }

    //This function is used for pat amount to bar by admin
    public function paySettlementAmount(Request $request){
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'amount' => 'required',
            'paidAmount' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'paymentMode' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        //print_r($request->all());exit;

        //$startDate = date($this->entryDate, strtotime($request->startDate));
        //$endDate = date($this->entryDate ,strtotime($request->endDate));

        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $isAllreadySettled = DB::table('bnBarPayments')->select('*')
            ->where([
                'barId' => $request->barId,
                'paymentStartDate' => $startDate,
                'paymentEndDate' => $endDate
            ])->get();

        if($isAllreadySettled && count($isAllreadySettled)) {
            $result = array('status' => 0, 'message' => 'You have already settled amount for this week.'); 
        }
        else
        {
            $param = array(
                'barId' => $request->barId,
                'amount' => $request->amount,
                'paidAmount' => $request->paidAmount,
                'paymentStartDate' => $startDate,
                'paymentEndDate' => $endDate,
                'paymentMode' => $request->paymentMode,
                'commission' => $request->commission,
                'paymentFor' => 'both', //order/event
                'paymentStatus' => 1,
                'commissionPaid' => 1, 
                'remarks' => $request->remarks,
                'settlementData' => $request->settlementData,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate 
            );

            if(DB::table('bnBarPayments')->insert($param))
            {
                $due = $request->amount -  $request->paidAmount;
                
                // Get bar balance to update, after this settlement entry
                $balance = DB::table('bnBarBalances as balance')->select('*')->where(['balance.barId' => $request->barId])->get();   
                
                if($balance && count($balance))
                {
                    //$due = $due + $balance[0]->earningDue;
                    $commission = $request->commission + $balance[0]->commissionPaid;
                    $amount = $request->amount + $balance[0]->totalEarning;
                    $paidAmount = $request->paidAmount + $balance[0]->earningReceived;
                    
                    $balanceParm = array(
                        'totalEarning' => $amount,
                        'earningReceived' => $paidAmount ,
                        'earningDue' => $due,
                        'commissionPaid' => $commission,
                        'updatedAt' => $this->entryDate 
                    );
                    DB::table('bnBarBalances')->where(['barId' => $request->barId])->update($balanceParm);
                }
                else{
                    $balanceParm = array(
                        'barId' => $request->barId,
                        'totalEarning' => $request->amount,
                        'earningReceived' => $request->paidAmount,
                        'earningDue' => $due,
                        'commissionPaid' => $request->commission,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate 
                    );
                    DB::table('bnBarBalances')->insert($balanceParm);
                }
                $result = array('status' => 1, 'message' => 'Payment entry has been created sucessfully'); 
            }else{
                $result = array('status' => 0, 'message' => 'Internal server error'); 
            }
        }
        return json_encode($result);
    }

    //Funnction to get Payment history with Admin-Bar
    public function getPaymentHistory(Request $request)
    {
        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $city = commonFunction::getCityIdByName($request->keyword);
        //print_r($request->all());exit;

        $sql = "SELECT p.*, bar.barName FROM bnBarPayments as p LEFT JOIN bnBars as bar on bar.id = p.barId LEFT JOIN bnAddresses as address on address.userId = bar.userId WHERE p.id > 0";

        /*====Filter Conditions Start====*/

        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = strtolower($request->keyword);

            if(!empty($city)){
                $sql .= " AND address.cityId = $city";
            }elseif(is_numeric($keyword)){
                $sql .= " AND (p.amount = $keyword OR p.commission = $keyword)";
            }else{
                $sql .= " AND (bar.barName LIKE '%$keyword%' OR p.remarks LIKE '%$keyword%')"; 
            }
            // if (is_numeric($keyword))
            //     $sql .= " AND (p.amount = $keyword OR p.commission = $keyword)";
            // else
            //     $sql .= " AND (bar.barName LIKE '%$keyword%' OR p.remarks LIKE '%$keyword%')";
        }
        
        //If Bar ID is set
        if (isset($request->barId)) {
            $sql .= " AND p.barId = " . $request->barId;
        }
        
        $sql .= " order by p.id desc limit $startFrom, $limit";

        $history = DB::select(DB::raw($sql));
        if (!empty($history)) {
            $result = array('status' => 1, 'message' => 'Payment history get successfully', 'data' => $history, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //This function is used for get bar revenue report daily wise.
    public function getBarDailyRevenueReport(Request $request)
    {
        //set pagination 
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        if(isset($request->date)){
            $today = date('Y-m-d',strtotime($request->date));
        }else{
            $today = date('Y-m-d');
        }
        //get barList 
        $query = DB::table('bnTransactions as transaction');
        $query->select('transaction.barId', 'bar.barName');
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'transaction.barId');
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId');
        $query->where('transaction.paymentDate', '=', $today);
        $keyword = $request->keyword;
        if(!empty($keyword)){
            $city = commonFunction::getCityIdByName($keyword);
            if($city){
                $query->where('address.cityId','=',  $city);
            }
            else{
                $query->Where('bar.barName', 'LIKE',"%{$keyword}%");
            }
        }
        $query->limit($limit);
        $query->offset($startFrom);
        $query->groupBy('transaction.barId');
        $barList = $query->get();

        $list = array();
        $appSetting = CommonFunction::getAppSettings();

        if(!empty($barList) && count($barList))
        {
            foreach ($barList  as $key => $bar) {
                $revenue = DB::table('bnTransactions as transaction')
                ->select('transaction.*')
                ->where('transaction.barId',$bar->barId)
                ->where('transaction.paymentDate', '=', $today)
                ->leftJoin('bnBars as bar', 'bar.id', '=', 'transaction.barId')
                ->get();
                $possition = CommonFunction::getTopBarPossition($bar->barId);
                if(!empty($revenue))
                {
                    $totalCashAmount = 0;
                    $totalOnlineAmount = 0;
                    $totalCardAmount = 0;
                    $totalWalletAmount = 0;
                    $totalBNWalletAmount = 0;
                    $totalAmount = 0;
                    $totalAmountWithTax = 0;
                    $commissionRate = 0;
                    $taxCommissionRate = 0;

                    if($appSetting && count($appSetting))
                    {
                        if($possition == 1){
                            $commissionRate = $appSetting['topper1Comm'];
                        }elseif($possition == 2){
                            $commissionRate = $appSetting['topper2Comm'];
                        }elseif($possition == 3){
                            $commissionRate = $appSetting['topper3Comm'];
                        }else{
                            $commissionRate = $appSetting['commission'];
                        }
                        $taxCommissionRate =$appSetting['taxOnCommission'];
                    }
                    foreach ($revenue as $key => $item) {
                        if($item->barId == $bar->barId)
                        {
                            if(strtolower($item->paymentMode) == 'online'){
                                $totalOnlineAmount = $totalOnlineAmount + $item->amount;
                            }
                            elseif(strtolower($item->paymentMode) == 'cash'){
                                $totalCashAmount =  $totalCashAmount + $item->amount;
                            }
                            elseif(strtolower($item->paymentMode) == 'card'){
                                $totalCardAmount =  $totalCardAmount + $item->amount;
                            }
                            elseif(strtolower($item->paymentMode) == 'wallet'){
                                $totalWalletAmount =  $totalWalletAmount + $item->amount;
                            }
                            elseif(strtolower($item->paymentMode) == 'bnwallet'){
                                $totalBNWalletAmount =  $totalBNWalletAmount + $item->amount;
                            }
                            $totalAmount = $totalAmount + $item->totalPrice;
                            $totalAmountWithTax = $totalAmountWithTax + $item->amount;
                        }
                    }
                    //Total commission calculation
                    if( $totalAmount > 0){
                         $totalCommission = ( $totalAmount * $commissionRate/100);
                    }else{
                         $totalCommission = 0;
                    }

                    $temp_param = array(
                        'barId' => $bar->barId,
                        'barName' => $bar->barName,
                        'onlinePayment' => round($totalOnlineAmount, 2),
                        'cashPayment' => round($totalCashAmount, 2),
                        'cardPayment' => round($totalCardAmount, 2),
                        'walletPayment' => round($totalWalletAmount, 2),
                        'bnWalletPayment' => round($totalBNWalletAmount, 2),
                        'totalCommission' => round($totalCommission,2),
                        'totalAmount' => round($totalAmount, 2),
                        'totalAmountWithTax' => round($totalAmountWithTax, 2),
                        'commissionRate' => $commissionRate
                    );
                    array_push($list, $temp_param);
                }
                $result = array('status' => 1, 'message' => 'Data get successfully', 'data' => $list,'limit'=>$limit);
            }
        }else{
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return $result;
    }
}