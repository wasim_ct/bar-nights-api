<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\GeoTagging;
use App\Http\Controllers\Controller;
use App\TopBar;
use App\TopSpender;
use App\Transaction;
use App\Visit;
use App\VisitCode;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class DashboardManagementController extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }

    public function getAdminDashboardData(Request $request)
    {
        //$previous_month = strtolower(date('F-Y'));
        $previous_month = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));
        $firstDay = date("Y-m-01");
        $today = date("Y-m-d");

        // Total active patrons, in the bars
        $geoTaggedUsers = GeoTagging::select('bnGeoTaggings.id')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
            ->where(function ($query) use ($today) {
                if (!empty($today)) {
                    $query->whereRaw('date(bnGeoTaggings.createdAt) = ?', $today);
                }
            })
            ->where(['v.status' => 2]) //2 means confirmed
            ->count();

        // Total active patrons, in the bars
        $codeGeneratedUsers = DB::table('bnVisits as v')
            ->where(['v.status' => 1, 'v.visitDate' => $today])->count();
            
        $data['activePatrons'] = $geoTaggedUsers + $codeGeneratedUsers;

        // Total Android Users
        $data['totalAndroidUsers'] = DB::table('bnAppDownloads')
            ->where(['osType' => 'android'])->count();

        // Total IOS Users
        $data['totalIosUsers'] = DB::table('bnAppDownloads')
            ->where(['osType' => 'ios'])->count();
        
        // total Downloads
        //$data['totalUsers'] = DB::table('bnUsers')->where('userType', 'U')->count();
        $data['totalUsers'] = $data['totalAndroidUsers'] + $data['totalIosUsers'];

        //total bars
        $data['totalBars'] = DB::table('bnUsers')->where('userType', 'B')->count();

        //total payment and today total payment
        $todayTotalSales = Transaction::totalPayment();
        //$data['todayTotalPayment'] = Transaction::todayTotalPayment();

        // Calculate commission on Today sales
        $commission = 0;
        if ($todayTotalSales > 0) {
            $appSettings = CommonFunction::getAppSettings();

            $commission = ($todayTotalSales / 100) * $appSettings['commission'];

            $taxOnComm = 0;
            if ($appSettings['taxOnCommission'] > 0) {
                $taxOnComm = ($commission / 100) * $appSettings['taxOnCommission'];
            }

            $commission = $commission + $taxOnComm;
        }

        $data['todayTotalSales'] = number_format($todayTotalSales, 2);
        $data['todayCommission'] = number_format($commission, 2);

        //top 3 spender patron and bar or pub list
        /*$data['topThreeSpender'] = TopSpender::getTopSpenderList($previous_month, 3, 0,'');
        $data['topThreeBar'] = TopBar::getTopBarList($previous_month, 3, 0,'');*/

        // Get overall Top 3 Bars of Current Month
        $topBars = DB::table('bnTransactions as t')
            ->select('bar.id as barId', 'bar.barName', 'bar.logo', 'address.cityId', DB::raw("SUM(t.amount) as totalSales"))
            ->join('bnBars as bar', 'bar.id', '=', 't.barId')
            ->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId')
            ->where('t.paymentDate', '>=', $firstDay)
            ->where(['t.status' => 1])
            ->orderBy('t.amount', 'desc')
            ->offset(0)->limit(3)
            ->groupBy('t.barId')->get();
        
        $barToppers = array();
        if (!empty($topBars)) {
            foreach ($topBars as $key => $row) {
                $row->position = $key+1;
                $row->totalSales = number_format($row->totalSales, 2);
                $barToppers[] = $row;
            }
        }
        $data['topThreeBar'] = $barToppers;  

        // Get overall Top 3 Spenders of Current Month
        $topUsers = DB::table('bnTransactions as t')
            ->select('t.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', DB::raw("SUM(t.amount) as amountSpent"))
            ->join('bnUsers as u', 'u.id', '=', 't.userId')
            ->join('bnPatrons as p', 'p.userId', '=', 't.userId')
            ->leftJoin('bnAddresses as address', 'address.userId', '=', 'u.id')
            ->where('t.paymentDate', '>=', $firstDay)
            ->where(['t.status' => 1])
            ->orderBy('t.amount', 'desc')
            ->offset(0)->limit(3)
            ->groupBy('t.userId')->get();
        
        $topSpenders = array();
        if (!empty($topUsers)) {
            foreach ($topUsers as $key => $row) {
                $row->position = $key+1;
                $row->amountSpent = number_format($row->amountSpent, 2);
                $topSpenders[] = $row;
            }
        }
        $data['topThreeSpender'] = $topSpenders;

        //Get Report Data from the below function
        $data['graphData'] = $this->loadAdminGraphData('weekly');

        $cities = DB::table('bnCities as city')->select('city_name as cityName')
            ->orderBy('city.city_name')
            ->get()->toArray();
        $allCities = array();
        foreach ($cities as $row) {
            $allCities[] = $row->cityName;
        }
        $data['cities'] = $allCities;

        //Result Params
        $result['status'] = 1;
        $result['data'] = $data;
        //$result['day'] = $previous_month;
        $result['message'] = "Data found!";
        return response()->json($result);
    }

    // Funciton to load data in graph for Admin
    public function loadAdminGraphData($time)
    {
        $time = strtolower($time);

        // Calling data on Weekly Basis
        if ($time == 'weekly') {
            // Query to call data of Footfalls
            $sql1 = "SELECT v.visitDate, d.visitBookingDate, count(v.id) AS total
            from (select DATE(curdate()) as visitBookingDate union all
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join";

            $sql = $sql1 . ' bnVisits v on v.visitDate = d.visitBookingDate AND v.status != 1 AND v.status != 3 GROUP BY d.visitBookingDate ORDER BY d.visitBookingDate asc';
            $footFall = DB::select(DB::raw($sql));

            if (count($footFall) > 0 || $footFall != null) {
                $totalFootFall = array();
                foreach ($footFall as $row) {
                    if (!empty($row->visitDate)) {
                        $date = date('d-m-Y', strtotime($row->visitDate));
                    } else {
                        $date = date('d-m-Y', strtotime($row->visitBookingDate));
                    }

                    $totalFootFall[] = array('key' => $date, 'value' => $row->total);
                }
                $data['footFall'] = $totalFootFall;
                $status = true;
            }

            // Query to call data of Payments
            $sql1 = "SELECT t.paymentDate, d.paymentEntryDate, sum(t.amount) AS total
            from (select DATE(curdate()) as paymentEntryDate union all
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join";

            $sql = $sql1 . ' bnTransactions t on t.paymentDate = d.paymentEntryDate AND t.status = 1 GROUP BY d.paymentEntryDate ORDER BY d.paymentEntryDate asc';
            $payments = DB::select(DB::raw($sql));

            if (count($payments) > 0 || $payments != null) {
                $totalPayments = array();
                foreach ($payments as $row) {
                    if (!empty($row->paymentDate)) {
                        $date = date('d-m-Y', strtotime($row->paymentDate));
                    } else {
                        $date = date('d-m-Y', strtotime($row->paymentEntryDate));
                    }

                    if (empty($row->total)) {
                        $row->total = 0;
                    }

                    $totalPayments[] = array('key' => $date, 'value' => $row->total);
                }
                //print_r($totalPayments);exit;
                $data['payments'] = $totalPayments;
                $status = true;
            }
        }
        if (isset($status)) {
            return $data;
        }
    }

    //Funciton to get Dashboard Data for BAR
    public function getBarDashboardData(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Get Sitting type TABLE visits
        $today = date("Y-m-d");
        /*$data['tableVisits'] = Visit::select('bnVisits.*', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'o.orderId as uniqueId', 'o.paymentStatus')
            ->join('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->join('bnUsers as u', 'u.id', '=', 'bnVisits.userId')
            ->leftJoin('bnOrders as o', 'o.visitId', '=', 'bnVisits.id')
            ->where(['bnVisits.barId' => $request->barId, 'bnVisits.sittingType' => 'table', 'bnVisits.visitDate' => $today])
            ->where('bnVisits.status', '=', 2)
            //->where('bnVisits.status', '!=', 1)
            //->where('bnVisits.status', '!=', 3)
            ->orWhere('o.paymentStatus', '=', 0)
            ->get()->toArray();*/

        $tables = array();
        $sql = "SELECT v.*, p.firstName, p.lastName, p.profilePic, u.userName, o.orderId as uniqueId, o.paymentStatus FROM bnVisits v LEFT JOIN bnPatrons p on p.userId = v.userId LEFT JOIN bnUsers u on u.id = v.userId LEFT JOIN bnOrders o on o.visitId = v.id WHERE v.barId = $request->barId AND v.sittingType = 'table' AND v.visitDate = '$today' AND (v.status = 2 OR v.status = 4)";
        $tableVisits = DB::select(DB::raw($sql));
        if(!empty($tableVisits)) {
            foreach ($tableVisits as $row) {
                if($row->paymentStatus != 1) {
                    $row->paymentStatus = 0;
                    $tables[] = $row;
                }
            }
        } 
        $data['tableVisits'] = $tables;
            
        //Get Sitting type STANDING visits
        /*$data['standingVisits'] = Visit::select('bnVisits.*', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'o.orderId as uniqueId', 'o.paymentStatus')
            ->join('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->join('bnUsers as u', 'u.id', '=', 'bnVisits.userId')
            ->leftJoin('bnOrders as o', 'o.visitId', '=', 'bnVisits.id')
            ->where(['bnVisits.barId' => $request->barId, 'bnVisits.sittingType' => 'standing', 'bnVisits.visitDate' => $today])
            ->where('bnVisits.status', '=', 2)
            //->where('bnVisits.status', '!=', 1)
            //->where('bnVisits.status', '!=', 3)
            ->orWhere('o.paymentStatus', '=', 0)
            ->get()->toArray();*/
        
        $sittings = array();
        $sql = "SELECT v.*, p.firstName, p.lastName, p.profilePic, u.userName, o.orderId as uniqueId, o.paymentStatus FROM bnVisits v LEFT JOIN bnPatrons p on p.userId = v.userId LEFT JOIN bnUsers u on u.id = v.userId LEFT JOIN bnOrders o on o.visitId = v.id WHERE v.barId = $request->barId AND v.sittingType = 'standing' AND v.visitDate = '$today' AND (v.status = 2 OR o.paymentStatus = 0)";
        $standingVisits = DB::select(DB::raw($sql));
        if(!empty($standingVisits)) {
            foreach ($standingVisits as $row) {
                if($row->paymentStatus != 1) {
                    $row->paymentStatus = 0;
                    $sittings[] = $row;
                }
            }
        } 
        $data['standingVisits'] = $sittings;

        /*=====Get Dashboard Statics=====*/
        $id = $request->barId;
        $currentDay = strtolower(date('l'));
        $today = date("Y-m-d");

        //Get Active Patrons count
        $patrons = GeoTagging::select('bnGeoTaggings.id')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
            ->where(function ($query) use ($today) {
                if (!empty($today)) {
                    $query->whereRaw('date(bnGeoTaggings.createdAt) = ?', $today);
                }

            })
            ->where(['bnGeoTaggings.barId' => $id, 'v.status' => 2]) //2 means confirmed
            ->get();

        //Get Sum of total sales
        $totalSales = DB::table('bnTransactions')->where(['barId' => $id, 'paymentDate' => $today, 'status' => 1])->sum('amount');
        
        //Get Total of code generated count for today
        $visits = Visit::select('id')
            ->where(['barId' => $id, 'visitDate' => $today])
            ->get();

        //Get the Total Code Exchanged of the day (Total availed visits)
        $todayCodeExchanged = Visit::select('id')
            ->where(['barId' => $id, 'visitDate' => $today])
            ->where('bnVisits.status', '!=', 1)
            ->where('bnVisits.status', '!=', 3)
            ->get();

        //Get Bar Visiblity and Day Status
        $barInfo = Bar::select('bnBars.dayOnOffStatus', 'bnBars.openingStatus', 'bnBars.barName', 'bnBars.logo', 'u.status as barStatus')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnBars.userId')
            ->where(['bnBars.id' => $id])
            ->first();

        $data['other'] = array(
            'patrons' => number_format(count($patrons)),
            'todaySale' => number_format($totalSales),
            'codeGenerated' => number_format(count($visits)),
            'todayCodeExchanged' => number_format(count($todayCodeExchanged)),
            //'openingStatus' => $openingStatus
        );

        $data['barInfo'] = $barInfo;

        if (count($data) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'Visits get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to get App Settings
    public function getAppSettings(Request $request)
    {
        $licenseTypes = CommonFunction::licenseTypes();
        $lt = array();
        foreach ($licenseTypes as $key => $value) {
            $licenseData['id'] = $key;
            $licenseData['licName'] = $value;
            $lt[] = $licenseData;
        }
        $data['licenseType'] = $lt;

        $cats = CommonFunction::getItemCategories();
        $mainCats = array();
        foreach ($cats as $key => $value) {
            $cd['id'] = $key;
            $cd['catName'] = $value;
            $mainCats[] = $cd;
        }
        $data['itemMainCats'] = $mainCats;

        // Call all the cities
        /*$data['cities'] = DB::table('bnCities as city')->select('city_name as cityName')
            ->orderBy('city.city_name')
            ->get()->toArray();*/

        if ($data) {
            $result = array('status' => 1, 'message' => 'Settings get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }
    //filter top three bar list by city name 
    public function topBarListByCityName(Request $request)
    {
        $firstDay = date("Y-m-01");
        //get city id by city name searching
        $cityId = 0;
        if(!empty($request->keyword))
        {
            $city = DB::table('bnCities as city')->select('city_id as cityId')
                ->where('city.city_name','=',$request->keyword)->first();
            if(!empty($city)){
                $cityId = $city->cityId;
            }
        }

        //tob bar query
        $query = DB::table('bnTransactions as t');
        $query->select('bar.id as barId', 'bar.barName', 'bar.logo', 'address.cityId', DB::raw("SUM(t.amount) as totalSales"));
        $query->join('bnBars as bar', 'bar.id', '=', 't.barId');
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId');
        $query->where(['address.cityId' => $cityId, 't.status' => 1]);
        $query->where('t.paymentDate', '>=', $firstDay);
        $query->orderBy('t.amount', 'desc');
        $query->offset(0);
        $query->limit(3);
        $query->groupBy('t.barId');
        
        $result = $query->get();
        if (count($result)) {
            $toppers = array();
            foreach ($result as $key => $row) {
                $row->position = $key+1;
                $row->totalSales = number_format($row->totalSales, 2);
                $toppers[] = $row;
            }
            $data['topThreeBar'] = $toppers;
            $result = array('status' => 1, 'message' => 'data get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);

        /*$previous_month = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month"))); 
        $data['topThreeBar'] = TopBar::getTopBarList($previous_month, 3, 0,$request->keyword);
        if (count($data['topThreeBar'])) {
            $result = array('status' => 1, 'message' => 'data get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);*/
    }

    //filter top three spender list by city name 
    public function topSpenderListByCityName(Request $request)
    {
        $firstDay = date("Y-m-01");
        //get city ids by city name searching
        $cityId = 0;
        if(!empty($request->keyword))
        {
            $city = DB::table('bnCities as city')->select('city_id as cityId')
                ->where('city.city_name','=',$request->keyword)->first();
            if(!empty($city)){
                $cityId = $city->cityId;
            }
        }
 
        $query = DB::table('bnTransactions as t');
        $query->select('t.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', DB::raw("SUM(amount) as amountSpent"));
        $query->join('bnUsers as u', 'u.id', '=', 't.userId');
        $query->join('bnPatrons as p', 'p.userId', '=', 't.userId');
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'u.id');
        $query->where(['address.cityId' => $cityId, 't.status' => 1]);
        $query->where('t.paymentDate', '>=', $firstDay);
        $query->orderBy('t.amount', 'desc');
        $query->offset(0);
        $query->limit(3);
        $query->groupBy('t.userId');
        $result = $query->get();
        if (count($result)) {
            $toppers = array();
            foreach ($result as $key => $row) {
                $row->position = $key+1;
                $row->amountSpent = number_format($row->amountSpent, 2);
                $toppers[] = $row;
            }
            $data['topThreeSpender'] = $toppers;
            $result = array('status' => 1, 'message' => 'data get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);

        /*$previous_month = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));
        $data['topThreeSpender'] = TopSpender::getTopSpenderList($previous_month, 3, 0, $request->keyword);
        if (count($data['topThreeSpender'])) {
            $result = array('status' => 1, 'message' => 'data get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);*/
    }
}