<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;
use App\Transaction;

class AdminBarSettlementController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }
    //This function is used for get bar settlement list of last week
    public function getBarSettlementList(Request $request)
    {
        //Pagination setup
        $limit = 1500;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        //find earlier week  start date and end date 
        $previousWeek = strtotime("-1 week +1 day");
        $startWeek = strtotime("last sunday midnight",$previousWeek);
        $endWeek = strtotime("next saturday",$startWeek);
        $startWeek = date("Y-m-d",$startWeek);
        $endWeek = date("Y-m-d",$endWeek);
        //query for geting transaction list
        $query= DB::table('bnTransactions as t');
        $query->select('b.barName','t.barId','t.paymentMode','t.totalPrice');
        $query->where('t.createdAt' ,'>=', $startWeek);
        $query->where('t.createdAt' ,'<=', $endWeek);
        $query->leftJoin('bnBars as b', 'b.id', '=', 't.barId');
        if(!empty($request->barId)){
            $query->where(['t.barId' => $request->barId]);
        }
        if(!empty($request->keyword)){
            $query->where('b.barName','LIKE',"%{$request->keyword}%");
        }
        $query->limit($limit);
        $query->offset($startFrom);
        //$query->groupBY('t.barId');
        $list = $query->get();
        $appSetting = CommonFunction::getAppSettings();
        $settlement = array();
        $barIdArray = array();
        if (count($list)) {
            foreach($list  as $key=>$value){
                $possition = CommonFunction::getTopBarPossition($value->barId);
                if(!empty($value->barId)){
                    //get commission rate based on bar possition
                    if($appSetting && count($appSetting)){
                        if($possition == 1){
                            $commissionRate = $appSetting['topper1Comm'];
                        }elseif($possition == 2){
                            $commissionRate = $appSetting['topper2Comm'];
                        }elseif($possition == 3){
                            $commissionRate = $appSetting['topper3Comm'];
                        }else{
                            $commissionRate = $appSetting['commission'];
                        }
                        $taxCommissionRate =$appSetting['taxOnCommission'];
                    }else{
                        $commissionRate = 10;
                        $taxCommissionRate = 18;
                    }
                    $totalCashAmount = 0;
                    $totalOnlineAmount = 0;
                    $totalAmount = 0;
                    //calculate total cash,online and total sell amount
                    foreach($list as $inner_key => $item){
                        if($item->barId == $value->barId){
                            if(strtolower($item->paymentMode) == 'online'){
                                $totalOnlineAmount = $totalOnlineAmount + $item->totalPrice;
                            }elseif(strtolower($item->paymentMode) == 'cash'){
                                $totalCashAmount =  $totalCashAmount + $item->totalPrice;
                            }
                            $totalAmount = $totalAmount + $item->totalPrice;

                        }
                    }
                    //cash commission calculation
                    if($totalCashAmount > 0){
                        $cashCommission = ($totalCashAmount *$commissionRate/100);
                    }else{
                        $cashCommission = 0;
                    }
                     //online commission calculation
                    if($totalOnlineAmount > 0){
                        $onlineCommission = ($totalOnlineAmount *$commissionRate/100);
                    }else{
                        $onlineCommission = 0;
                    }
                    //total commission calculation
                    $totalCommission = $cashCommission + $onlineCommission;
                    //calculate gst tax amount
                    if($totalCommission > 0){
                        $gstTax = ($totalCommission * $taxCommissionRate/100);
                    }else{
                        $gstTax = 0;
                    }
                    //calculate total payable amount
                    if($totalOnlineAmount > 0){
                        $totalCommission = $totalCommission + $gstTax;
                        $totalPaybaleAmount = $totalAmount - $totalCommission;
                    }
                    //temp array create
                    $temp_array = array(
                        'barId' => $value->barId,
                        'barName' => $value->barName,
                        'position' => $possition,
                        'taxRate' => $taxCommissionRate,
                        'commissionRate' => $commissionRate,
                        'totalAmount' => $totalAmount,
                        'CashAmount' => $totalCashAmount,
                        'OnlineAmount' => $totalOnlineAmount,
                        'cashCommission' => round($cashCommission,2),
                        'onlineCommission' => round($onlineCommission,2),
                        'gstTax' => round($gstTax),
                        'totalCommission' => round($totalCommission),
                        'totalPaybaleAmount' => round($totalPaybaleAmount,2),
                        'startWeek' => $startWeek,
                        'endWeek'   => $endWeek
                    );
                    //check bar info already settelment or not
                    if(!in_array($value->barId, $barIdArray)) {
                        array_push($settlement, $temp_array);
                        array_push($barIdArray, $value->barId);
                    }
                }
            }
            //$result = array('status' => 1, 'message' => 'Data get successfully', 'data' => $list,'settelement'=> $settlement,'setting'=>$appSetting);
            $result = array('status' => 1, 'message' => 'Data get successfully','data'=> $settlement,'limit'=> $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No data  found');
        }
        return json_encode($result);
    }
    //This function is used for pat amount to bar by admin
    public function paySettlementAmount(Request $request){
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'amount' => 'required',
            'paidAmount' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'paymentMode' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $param = array(
            'barId' => $request->barId,
            'amount' => $request->amount,
            'paidAmount' => $request->paidAmount,
            'paymentStartDate' => date("Y-m-d H:i:s",strtotime($request->startDate)),
            'paymentEndDate' => date("Y-m-d H:i:s",strtotime($request->endDate)),
            'paymentMode' => $request->paymentMode,
            'paymentFor' => 1,
            'paymentStatus' => 1,
            'commissionPaid' => 'yes',
            'entryType' => 'paid',
            'createdAt' => $this->entryDate,
            'updatedAt' => $this->entryDate 
        );
        print_r($param);exit;
        if(DB::table('bnBarPayments')->insert($param)){
            $result = array('status' => 1, 'message' => 'Payment sucessfully pay to bar'); 
        }else{
            $result = array('status' => 0, 'message' => 'Internal server error'); 
        }
        return json_encode($result);
    }
}