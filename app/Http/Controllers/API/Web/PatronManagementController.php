<?php
namespace App\Http\Controllers\API\Web;

use App\EventBooking;
use App\Helper\WebCommonFunction;
use App\Http\Controllers\Controller;
use App\Order;
use App\Patron;
use App\TopSpender;
use App\UserNotification;
use App\User;
use App\Wallet;
use App\Visit;
use DB;
use Illuminate\Http\Request;
use Validator;
use CommonFunction;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class PatronManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }

    //This function is used for add patron by admin
    public function addPatron(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'userName' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            //'location' => 'required',
            'status' => 'required',
            //'itemImage' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        //Check UserName is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->userName, 'userName', 'U');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Username is already registered');
            return response()->json($result);
        }


        //Check Email is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->email, 'email', 'U');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }
        //Check Phone is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->phone, 'phone', 'U');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }
        $userParam = array(
            'userName' => $request->userName,
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => $request->status,
            'userType' => 'U',
            'platform' => 'bn',
            //'password' => bcrypt($request->password)
        );
        $user = User::create($userParam);
        if ($user['id']) {
            $patronParam = array(
                'userId' => $user['id'],
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
            );
            $patron = Patron::create($patronParam);
            $result['status'] = 1;
            $result['message'] = "Patron has been added successfully!";
        } else {
            $result['status'] = 0;
            $result['message'] = "Internal server error.Try again!";
        }
        return response()->json($result);
    }
    //This function is used for edit patron by admin
    public function editPatron(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'userName' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        //Check UserName is already registerd or not or not
        $query = WebCommonFunction::checkEditValueExist($request->userName, 'userName', 'U', $request->userId);
        if ($query) {
            $result = array('status' => 0, 'message' => 'Username is already registered', $request->userId);
            return response()->json($result);
        }

        //Check Email is already registerd or not or not
        $query = WebCommonFunction::checkEditValueExist($request->email, 'email', 'U', $request->userId);
        if ($query) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }
        //Check Phone is already registerd or not or not
        $query = WebCommonFunction::checkEditValueExist($request->phone, 'phone', 'U', $request->userId);
        if ($query) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }
        //prepare param
        $userParam = array(
            'updatedAt' => date("Y-m-d H:i:s"),
            'userName' => $request->userName,
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => $request->status,
            //'password' => bcrypt($request->password),
        );
        $patronParam = array(
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'updatedAt' => date("Y-m-d H:i:s"),
        );
        //update opertaions
        User::where('id', $request->userId)->update($userParam);
        Patron::where('userId', $request->userId)->update($patronParam);
        //prepare response
        $result['status'] = 1;
        $result['message'] = "Patron has been updated successfully!";
        return response()->json($result);
    }

    //This function is used for get all patron list
    public function getPatronDetails(Request $request)
    {

        /*$user = Auth::user();
        $response = array('status' => $this->successStatus, 'data' => $user);*/

        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Calling the Bar Now
        $userExist = User::select('bnUsers.id', 'bnUsers.userName', 'p.firstName', 'p.lastName', 'bnUsers.phone', 'bnUsers.email', 'bnUsers.status')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnUsers.id')
            ->where(['bnUsers.id' => $request->userId, 'bnUsers.userType' => 'U'])->first();
        if ($userExist) {

            //Converting NULL to "" String
            array_walk_recursive($userExist, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Profile get successfully', 'data' => $userExist);
        } else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }

        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //This function is used for get all patron list
    public function patronList(Request $request)
    {
        // $list = Patron::getAllList();

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        //print_r($request->all());exit;

        $sql = "SELECT u.id, p.firstName, p.lastName, u.userName, u.phone, u.email, u.status, p.profilePic, u.inactiveReason, u.createdAt FROM bnPatrons as p JOIN bnUsers as u on u.id = p.userId LEFT JOIN bnAddresses on bnAddresses.userId = p.userId WHERE userType = 'U'";

        /*===filter conditions===*/
        // Based on status
        if (isset($request->status)) { // && !empty($request->status)
            $sql .= " AND u.status = " . $request->status;
        }

        // Based onCity
        $city = commonFunction::getCityIdByName($request->city);
        if($city){
            $sql .= " AND bnAddresses.cityId = " . $city;
        }

        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {

            $keyword = strtolower($request->keyword);    
            $sql .= " AND (p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR u.email LIKE '%$keyword%' OR u.phone LIKE '%$keyword%')"; 
        
            // if (is_numeric($keyword)) 
            //     $sql .= " AND u.phone LIKE '%$keyword%'";
            // else 
            //     $sql .= " AND (p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR u.email LIKE '%$keyword%')";
        }
        $sql .= " order by u.id desc limit $startFrom, $limit";
        //echo $sql;exit;
        $user = DB::select(DB::raw($sql));

        if (count($user) > 0) {
            $result = array('status' => 1, 'message' => 'User get successfully', 'data' => $user, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //This function is used for get patron profile info.
    public function patronProfileInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        $list = Patron::getProfileInfo($request->userId);
        
        if ($list && count($list)) 
        {
            //wallet balnce
            $walletBalance = Wallet::select('amount')->where('userId', $request->userId)->first();
            if ($walletBalance && !empty($walletBalance)) {
                $data['walletBalance'] = $walletBalance->amount;
            } else {
                $data['walletBalance'] = 0;
            }
            
            $data['profileInfo'] = $list[0];

            // Visit History
            $data['visitHistory'] = Visit::select('bnVisits.id', 'bnVisits.visitDate', 'bnVisits.status', 'bar.barName')
                ->leftJoin('bnBars as bar', 'bar.id', '=', 'bnVisits.barId')
                ->where('bnVisits.userId', $request->userId)
                ->get()->toArray();
            
            // Order & Event History
            $data['orderHistory'] = Order::getOrderByUserId($request->userId);
            $data['eventHistory'] = EventBooking::getEventListByUserId($request->userId);

            //$data['preOrderHistory'] = Order::getOrderByUserId($request->userId, 'preorder');
            
            $result['status'] = 1;
            $result['data'] = $data;
            $result['message'] = "Patron profile get successfully";
            return response()->json($result);
        } 
        else {
            $result['status'] = 0;
            $result['message'] = "No record found";
            return response()->json($result);
        }
    }

    //This function is used for get to spender list till the earlier month
    public function getTopSpenderList(Request $request)
    {
        $previous_month = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));
        $limit = $request->limit;
        $startFrom = $request->startFrom;
        $list = TopSpender::getTopSpenderList($previous_month, $limit, $startFrom);
        if ($list && count($list)) {
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        } else {
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }

    // Function to change user status
    public function changeUserStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $userId = $request->userId;
        $status = $request->status;

        //create status msg according to status
        if ($status == 1) {
            $inactiveReason = NULL;
        }
        else if ($status == 2) {
            if(isset($request->reason))
                $inactiveReason = $request->reason;
            else
                $inactiveReason = NULL;
        }

        $param = array('status' => $status, 'inactiveReason' => $inactiveReason, 'updatedAt' => date("Y-m-d H:i:s"));
        $update = DB::table('bnUsers')->where('id', $userId)->update($param);

        if ($update) {
            //Create an entry in notification table and send PUSH Notification
            if($status == 2) {

                // Delete the logged In devices token
                DB::table('bnLoginTokens')->where(['userId' => $userId])->delete();
                $contactEmail = CommonFunction::GetSingleField('bnAppSettings', 'contactEmail', 'id', 1);

                $userEmail = "";
                $name = "";
                $deviceData = DB::table('bnUserDevices as d')->select('d.deviceToken', 'u.email', 'p.firstName', 'p.lastName')
                    ->leftJoin('bnUsers as u', 'u.id', '=', 'd.userId')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'd.userId')
                    ->where(["d.userId" => $userId])
                    ->get()->toArray();
                $tokens = array();
                if (!empty($deviceData)) {
                    foreach ($deviceData as $d) {
                        if (!empty($d->deviceToken) && $d->deviceToken != '') {
                            $tokens[] = $d->deviceToken;
                        }
                        $userEmail = $d->email;
                        $name = $d->firstName .' '.$d->lastName;
                    }
                }

                $message = 'Account blocked by Admin';
                $deviceToken = $tokens; //CommonFunction::getUserDeviceData($userId); 
                if ($deviceToken) { 
                    //Firebase Notification
                    $payLoadData = array('moduleType' => 'accountInactive', 'moduleId' => $userId, 'userId' => $userId, 'title' => 'Account Inactive', 'msg' => $message, 'contactEmail' => $contactEmail);
                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                }

                // Send Notification Email
                if($userEmail != "") {
                    $msg = '<h1 class="title">Hello ' . $name. '</h1>';

                    if($inactiveReason != NULL) {
                        $msg .= '<p>Your account at Bar Nights has been blocked because of the following reason.</p>
                            <p><b>Reason:</b> '.$inactiveReason.'</p>';
                    }
                    else {
                        $msg .= '<p>Your account at Bar Nights has been blocked due to some suspicious activity.</p>';
                    }
                    $msg .= '<p>Please contact at '.$contactEmail.' for more information.</p>';
                    
                    $data = array(
                        'msg' => $msg,
                        'subject' => "Account Blocked",
                    );

                    Mail::to($userEmail)->send(new SendMail($data));
                }
            }
            $result = array('status' => 1, 'message' => "Status has been updated successfully");
        }
        else
            $result = array('status' => 0, 'message' => "Something went wrong");
        
        return response()->json($result);
    }

}