<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Validator;

class AppDownloadController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //add or edit bnAppDownload Table
    public function addOrEditAppDownload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'deviceId' => 'required',
            'osType' => 'required|in:android,ios'
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        
        $check = DB::table('bnAppDownloads')->select('id')->where('deviceId', $request->deviceId)->get();
        if($check && count($check)){
            //$param['updatedAt'] = $this->entryDate;
            //$updateStatus = DB::table('bnAppDownloads')->where('id',$check[0]->id)->update($param);
            $updateStatus = 1;
            $message = 'Device id is already registered!';
        }else{
            $param = array(
                'deviceId' => $request->deviceId,
                'osType' => $request->osType,
                'updatedAt' => $this->entryDate,
                'createdAt' => $this->entryDate  
            );
            $updateStatus = DB::table('bnAppDownloads')->insert($param);
            $message = 'Device id is registered successfully!';
        }
        if($updateStatus){
            $result = array('status' => 1, 'message' => $message);
        }else{
            $result = array('status' => 0, 'message' => 'Internal server error!');
        }
        return response()->json($result);
    }
}