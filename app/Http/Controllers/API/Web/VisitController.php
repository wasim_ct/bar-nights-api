<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\GeoTagging;
use App\Http\Controllers\Controller;
use App\UserNotification;
use App\Visit;
use App\VisitCode;
use App\BookTable;
use App\EventBooking;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class VisitController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Funciton to Get list of visits and codes generated
    public function getVisits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;

        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $sql = "SELECT v.id, v.userId, v.createdAt, v.status as visitStatus, vc.code, vc.codeAvailed as codeStatus, v.cancelReason, v.cancelBy, p.firstName, p.lastName, u.userName, o.orderId, o.status as orderStatus FROM bnVisits v LEFT JOIN bnVisitCodes as vc on vc.visitId = v.id LEFT JOIN bnPatrons as p on p.userId = v.userId LEFT JOIN bnUsers as u on u.id = p.userId LEFT JOIN bnOrders as o on o.visitId = v.id WHERE v.barId = " . $request->barId;

        //Filter Conditions Start
        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = strtolower($request->keyword);

            //Status based search
            /*if ($keyword == 'pending') {
                $status = 1;
            } else if ($keyword == 'confirmed' || $keyword == 'confirm') {
                $status = 2;
            } else if ($keyword == 'cancelled' || $keyword == 'cancel') {
                $status = 3;
            } else if ($keyword == 'completed' || $keyword == 'complete') {
                $status = 4;
            }*/
            /*if (isset($status)) {
                $sql .= " AND v.status = $status";
            } else {*/
                $sql .= " AND (p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR vc.code LIKE '%$keyword%')";
            //}
        }

        if(isset($request->status) && !empty($request->status)){
            $sql .= " AND v.status = ".$request->status;
        }       

        $sql .= " group by v.id order by v.id desc limit $startFrom, $limit";
        $visits = DB::select(DB::raw($sql));

        if (!empty($visits)) {
            foreach ($visits as $visit) {
                //change the visit status to cancel if it is pending and code generated time is more than 15 mints
                if ($visit->visitStatus == 1) {
                    $currentTime = date('H:i:s');
                    //get code generated time from db
                    $time = $visit->createdAt;
                    $codeGeneratedTime = date('H:i:s', strtotime($time));

                    //get only minites by taking difference of code generated time and current time
                    //Then remove points by round function
                    //convert -ve value in +ve by using function abs
                    $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

                    if ($minutes > 15) {
                        //update visit status in table 
                        Visit::where(['id' => $visit->id])->update(['cancelReason' => 'Auto cancelled', 'status' => 3, 'updatedAt' => $this->entryDate]);
                        //get visit status to show
                        $visit->visitStatus = 3;
                    }
                }
                //Converting NULL to "" String
                array_walk_recursive($visit, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
                $data[] = $visit;
            }
            $result = array('status' => 1, 'message' => 'Visits get successfully', 'data' => $data, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to check that code we enter is exist or not
    public function checkCodeExist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        $currentTime = date('H:i:s');

        $code = 'BN' . date('d/m/') . $request->code;
        $visit = Visit::select('bnVisits.id', 'bnVisits.userId', 'bnVisits.createdAt', 'bnVisits.status as visitStatus', 'vc.code', 'vc.codeAvailed as codeStatus', 'p.firstName', 'p.lastName')
            ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'bnVisits.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['vc.code' => $code, 'bnVisits.barId' => $request->barId, 'vc.codeAvailed' => 0, 'bnVisits.status' => 1])->first();

        if ($visit) {
            //get code generated time from db
            $time = $visit->createdAt;
            $codeGeneratedTime = date('H:i:s', strtotime($time));

            //get only minites by taking difference of code generated time and current time
            //Then remove points by round function
            //convert -ve value in +ve by using function abs
            $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

            if ($minutes <= 15) {
                //Converting NULL to "" String
                array_walk_recursive($visit, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
                $result = array('status' => 1, 'message' => 'Code found successfully', 'data' => $visit);
            } else {
                $result = array('status' => 0, 'message' => 'Code Expired');
            }
        } 
        else {
            $code = 'BN' . strtoupper($request->code);
            $char = strtoupper(substr($request->code, 0, 1));
            if($char == 'T') {
                /* ### 
                * Search this code for booking table request ### 
                * ### */
                $booking = BookTable::select('bnBookTable.*', 'p.firstName', 'p.lastName')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnBookTable.userId')
                    ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
                    ->where(['bnBookTable.code' => $code, 'bnBookTable.barId' => $request->barId, 'bnBookTable.codeAvailed' => 0, 'bnBookTable.status' => 1, 'bnBookTable.paymentStatus' => 1, 'bnBookTable.bookingDate' => date('Y-m-d')])->first();
                
                if ($booking) {

                    // Expire the Request if 30 mins passed from booking time
                    $currentTime = date('Y-m-d H:i:s'); 
                    $time = $booking->bookingDate.' '.$booking->bookingTime;
                    $minutes = round((strtotime($currentTime) - strtotime($time)) / 60);
                    
                    $limit = config('app.table_time_limit');
                    if ($minutes > $limit) {
                        DB::table('bnBookTable')->where(['id' => $booking->id])->update(['status' => 3, 'updatedAt' => $this->entryDate]);
                        $result = array('status' => 0, 'message' => 'Code Expired');
                    }
                    else {
                        $result = array('status' => 1, 'message' => 'Code found successfully', 'data' => $booking);
                    }
                }
                else
                    $result = array('status' => 0, 'message' => 'Invalid request code');
            }
            else if($char == 'E') {
                /* ### Search this code for event booking request ### */
                $event = EventBooking::select('bnEventBookings.*', 'p.firstName', 'p.lastName')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnEventBookings.userId')
                    ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
                    ->where(['bnEventBookings.code' => $code, 'e.barId' => $request->barId, 'bnEventBookings.codeAvailed' => 0, 'e.startDate' => date("Y-m-d")])->first();
                if ($event) {
                    $result = array('status' => 1, 'message' => 'Code found successfully', 'data' => $event);
                }
                else
                    $result = array('status' => 0, 'message' => 'Invalid request code');
            }
            else
                $result = array('status' => 0, 'message' => 'Invalid request code');
        }

        return response()->json($result);
    }

    //Funciton to avail code, assign table/standing no to user
    public function availCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sittingType' => 'required',
            'tableStandingNo' => 'required',
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        
        // Check Table Number is already Assigned to Table Booking
        if($request->sittingType == 'table' && !isset($request->bookTable)) { 
            $tableBooked = DB::table('bnBookTable')->select('id')
                ->where(['paymentStatus' => 1, 'status' => 1, 'tableNumber' => $request->tableStandingNo, 'bookingDate' => date("Y-m-d")])->first();
            if(!empty($tableBooked)) {

                // Now check the visit status of the booking
                $visit = Visit::select('id', 'status')
                    ->where(['requestType' => 'table', 'requestId' => $tableBooked->id])
                    ->first();

                if(!empty($visit)) {
                    if($visit->status == 2) {
                        $result = array('status' => 0, 'message' => 'Table number '.$request->tableStandingNo.' is already occupied on a Table Booking request');    
                        return response()->json($result);
                    }
                }
                else { 
                    $result = array('status' => 0, 'message' => 'Table number '.$request->tableStandingNo.' is already occupied on a Table Booking request');
                    return response()->json($result);
                }
            }
        }

        //Check, table-standing number is already occupied or free
        $occupied = Visit::select('id')
            ->where(['sittingType' => $request->sittingType, 'status' => 2, 'tableStandingNo' => $request->tableStandingNo])->first();

        if (!empty($occupied) && !isset($request->bookTable)) {
            $result = array('status' => 0, 'message' => ucwords($request->sittingType) . ' number ' . $request->tableStandingNo . ' is already occupied');
        } 
        else {

            /* In case of table booking, 
             * copy the data from booking request to visit 
            */
            if(isset($request->bookTable)) {
                $bt = BookTable::select('*')->where(['id' => $request->visitId])->first();
                
                if(!empty($bt)) {

                    $btData = array('userId' => $bt->userId, 
                        'barId' => $bt->barId, 
                        'visitDate' => $bt->bookingDate, 
                        //'sittingType' => $request->sittingType, 
                        //'tableStandingNo' => $request->tableStandingNo , 
                        'requestId' => $bt->id, 
                        'requestType' => 'table', 
                        // 'status' => 2, 
                        'createdAt' => $this->entryDate, 
                        'updatedAt' => $this->entryDate
                    );

                    $visit = Visit::create($btData);
                    if($visit['id']) {
                        // Create entry in Visit code
                        $codeData = array('visitId' => $visit['id'], 
                            'code' => $bt->code, 
                            'codeAvailed' => 1, 
                            'createdAt' => $this->entryDate, 
                            'updatedAt' => $this->entryDate
                        );
                        $cd = VisitCode::create($codeData);

                        // Update the table booking code status
                        $udata = BookTable::where(['id' => $bt->id])
                            ->update(['codeAvailed' => 1, 'updatedAt' => $this->entryDate]);

                        // To continue the below code of visit
                        $request->visitId = $visit['id'];
                    }
                }
            }

            /* In case of event booking, 
             * copy the data from event booking request to visit 
            */
            else if(isset($request->eventBooking)) {
                $bt = EventBooking::select('bnEventBookings.*', 'e.startDate', 'e.barId')
                    ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
                    ->where(['bnEventBookings.id' => $request->visitId])->first();
                
                if(!empty($bt)) {

                    $btData = array('userId' => $bt->userId, 
                        'barId' => $bt->barId, 
                        'visitDate' => $bt->startDate, 
                        //'sittingType' => $request->sittingType, 
                        //'tableStandingNo' => $request->tableStandingNo , 
                        'requestId' => $bt->id, 
                        'requestType' => 'event', 
                        // 'status' => 2, 
                        'createdAt' => $this->entryDate, 
                        'updatedAt' => $this->entryDate
                    );

                    $visit = Visit::create($btData);
                    if($visit['id']) {
                        // Create entry in Visit code
                        $codeData = array('visitId' => $visit['id'], 
                            'code' => $bt->code, 
                            'codeAvailed' => 1, 
                            'createdAt' => $this->entryDate, 
                            'updatedAt' => $this->entryDate
                        );
                        $cd = VisitCode::create($codeData);

                        // Update the event booking code status
                        $udata = EventBooking::where(['id' => $bt->id])
                            ->update(['codeAvailed' => 1, 'updatedAt' => $this->entryDate]);

                        // To continue the below code of visit
                        $request->visitId = $visit['id'];
                    }
                }
            }

            //Get the Details of Visit first
            $visitData = CommonFunction::GetSingleRow('bnVisits', 'id', $request->visitId);
            if ($visitData) 
            {
                //Update the Table/Standing in the visits
                $tableAssign = Visit::where(['id' => $request->visitId])
                    ->update(['sittingType' => $request->sittingType, 'status' => 2, 'tableStandingNo' => $request->tableStandingNo, 'updatedAt' => $this->entryDate]);

                if ($tableAssign == 1) {
                    //Update the status of the code
                    $codeAvail = VisitCode::where(['visitId' => $request->visitId])
                        ->update(['codeAvailed' => 1]);

                    //Create entry in Geo Tagging table
                    $currentUser = GeoTagging::create(['visitId' => $request->visitId, 'barId' => $visitData->barId, 'userId' => $visitData->userId]);

                    //Create an entry in notification table and send PUSH Notification
                    $userId = $visitData->userId;
                    $barId = $visitData->barId;
                    $bar = Bar::select('barName')->where(['bnBars.id' => $barId])->first();
                    
                    if (!empty($bar)) {

                        if(isset($request->bookTable))
                            $message = 'Table booking';
                        else if(isset($request->eventBooking))
                            $message = 'Event booking';
                        else    
                            $message = 'Visit';

                        $message .= ' code has been availed at ' . ucwords(strtolower($bar->barName));

                        //Create an entry in notification table and send PUSH Notification
                        $visitId = $request->visitId;
                        $record = array('moduleType' => 'codeAvailed', 'moduleId' => $visitId, 'description' => $message, 'userId' => $userId);
                        $not = UserNotification::create($record);

                        if ($not['id']) {
                            $bar = Bar::select('bnBars.barName', 'bnBars.logo', 'a.address')
                                ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
                                ->where(['bnBars.id' => $barId])->first();
                            if (!empty($bar)) {
                                $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens
                                if ($deviceToken) {
                                    //Firebase Notification
                                    $payLoadData = array('moduleType' => 'codeAvailed', 'moduleId' => $visitId, 'userId' => $userId, 'barId' => $barId, 'title' => 'Code availed', 'msg' => $message, 'barName' => $bar->barName, 'address' => $bar->address, 'logo' => $bar->logo);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                }
                            }
                        }
                    }

                    $result = array('status' => 1, 'message' => 'Code availed successfully');
                } 
                else {
                    $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
                }

            } else {
                $result = array('status' => 0, 'message' => 'No visit exist');
            }
        }

        //$result = array('status' => 1, 'message' => 'Code availed successfully');
        return response()->json($result);
    }

    //Funciton to Get All list of visits and codes generated
    public function getAllVisits(Request $request)
    {
        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;

        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $city = commonFunction::getCityIdByName($request->keyword);
        
        $sql = "SELECT v.id, v.userId, v.barId, v.createdAt, v.status as visitStatus, vc.code, vc.codeAvailed as codeStatus, v.cancelReason, v.cancelBy, p.firstName, p.lastName, u.userName, o.orderId, o.status as orderStatus, bar.barName FROM bnVisits v LEFT JOIN bnVisitCodes as vc on vc.visitId = v.id LEFT JOIN bnPatrons as p on p.userId = v.userId LEFT JOIN bnUsers as u on u.id = p.userId LEFT JOIN bnOrders as o on o.visitId = v.id LEFT JOIN bnBars as bar on bar.id = v.barId LEFT JOIN  bnAddresses as address on address.userId = bar.userId WHERE v.id != 0";
        
        //Filter Conditions Start
        /*if (isset($request->barId) && !empty($request->barId) && $request->barId != 'null'){
            $sql .= " WHERE v.barId = $request->barId";
        }*/
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = strtolower($request->keyword);
            $sql .= " AND (p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR vc.code LIKE '%$keyword%' OR bar.barName LIKE '%$keyword%')";

            //Status based search
            /*if ($keyword == 'pending') {
                $status = 1;
            } else if ($keyword == 'confirmed' || $keyword == 'confirm') {
                $status = 2;
            } else if ($keyword == 'cancelled' || $keyword == 'cancel') {
                $status = 3;
            } else if ($keyword == 'completed' || $keyword == 'complete') {
                $status = 4;
            }

            if (isset($status)) {
                $sql .= " WHERE v.status = $status";
            }elseif(!empty($city)){
                $sql .= " WHERE address.cityId = $city";
            } else {
                $sql .= " WHERE (p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR vc.code LIKE '%$keyword%' OR bar.barName LIKE '%$keyword%')";
            }*/
        }

        if(isset($request->status) && !empty($request->status)){
            $sql .= " AND v.status = ".$request->status;
        }

        $sql .= " group by v.id order by v.id desc limit $startFrom, $limit";
        $visits = DB::select(DB::raw($sql));

        if (!empty($visits)) {
            foreach ($visits as $visit) {
                //change the visit status to cancel if it is pending and code generated time is more than 15 mints
                if ($visit->visitStatus == 1) {
                    $currentTime = date('H:i:s');
                    //get code generated time from db
                    $time = $visit->createdAt;
                    $codeGeneratedTime = date('H:i:s', strtotime($time));

                    //get only minites by taking difference of code generated time and current time
                    //Then remove points by round function
                    //convert -ve value in +ve by using function abs
                    $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

                    if ($minutes > 15) {
                        //update visit status in table
                        Visit::where(['id' => $visit->id])->update(['cancelReason' => 'Auto cancelled', 'status' => 3, 'updatedAt' => $this->entryDate]);
                        //get visit status to show
                        $visit->visitStatus = 3;
                    }
                }
                //Converting NULL to "" String
                array_walk_recursive($visit, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
                $data[] = $visit;
            }
            $result = array('status' => 1, 'message' => 'Visits get successfully', 'data' => $data, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    // update visit status
    public function changeVisitStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        
        extract($_POST);
        //check that visit id exist or not
        $visitExist = Visit::select('id')->where(['id' => $request->visitId])->first();

        if ($visitExist) {
            $param = array('status' => $request->status, 'cancelReason' => $request->reason, 'cancelBy' => 'bar', 'updatedAt' => $this->entryDate);
            $update = Visit::where(['id' => $request->visitId])->update($param);

            if ($update) {
                DB::table('bnGeoTaggings')->where(['visitId' => $request->visitId])->delete();
                CommonFunction::visitCancelNotification($request->visitId);
                
                $result = array('status' => 1, 'message' => "Visit has been updated successfully");
            } else {
                $result = array('status' => 0, 'message' => "Visit not updated");
            }
        } else {
            $result = array('status' => 0, 'message' => "Visit not exist");
        }
        return response()->json($result);
    }
}