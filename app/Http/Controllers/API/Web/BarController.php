<?php
namespace App\Http\Controllers\API\Web;

use App\Address;
use App\Bar;
use App\BarBanner;
use App\BarLicense;
use App\BarMediaFile;
use App\BarReview;
use App\BarTiming;
use App\BarUser;
use App\GeoTagging;
use App\Http\Controllers\Controller;
use App\Order;
use App\TopBar;
use App\Visit;
use App\VisitCode;
use App\HappyHoursTiming;
use CommonFunction;
use DB;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use Password;
use Validator;

class BarController extends Controller
{
    public $successStatus = 200;
    use SendsPasswordResetEmails;
    use ResetsPasswords;
    public $token;
    private $entryDate;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }
    /**
     * check which password broker is using
     *
     * @return \Illuminate\Http\Response
     */
    protected function broker()
    {
        return Password::broker('bars');
    }

    //Get Bar complete profile - on My Profile
    public function getBarProfileWeb(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Pagination Condition of startfrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $id = $request->id; //BarId
        $currentDay = strtolower(date('l'));
        $today = date("Y-m-d");

        if (isset($request->loadmore)) {
            $data = array();
            $loadmore = $request->loadmore;
            //Get the Media
            if ($loadmore == 'media') {
                $data = $this->getImagesVideos($id, $startFrom, $limit);
            }

            //Get the Review
            if ($loadmore == 'review') {
                $data = $this->getBarReviews($id, $startFrom, $limit);
            }

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Profile get successfully', 'data' => $data, 'limit' => $limit);
        } else {
            //Calling the Bar Now
            $userExist = Bar::select('bnBars.id as barId', 'bnBars.barName', 'bnBars.about', 'bnBars.logo', 'ad.address', 'ad.cityId', 'ad.stateId', 'ad.latitude', 'ad.longitude', 'city.city_name as cityName', 'st.state_name as stateName','u.status as barStatus','bnBars.userId')
                ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'bnBars.userId')
                ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
                ->leftJoin('bnStates as st', 'st.state_id', '=', 'ad.stateId')
                ->leftJoin('bnUsers as u', 'u.id', '=', 'bnBars.userId')
                ->where(['bnBars.id' => $id])->first();
            if ($userExist) {
                //Converting NULL to "" String
                array_walk_recursive($userExist, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
                $data['profile'] = $userExist;

                //bar opening and clossing time for the current day(openingStatus:-1:open,0:close)
                $openHour = array();
                $openingStatus = 0;

                $data['hours'] = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')->where(['barId' => $id, 'weekDay' => $currentDay])->first();

                //Get the Media
                $data['media'] = $this->getImagesVideos($id, $startFrom, $limit);

                //Get the Review
                $data['review'] = $this->getBarReviews($id, $startFrom, $limit);

                //Get Sum of Order & PreOrder for total sales
                /*$orderSum = DB::table('bnOrders')->where(['barId' => $id, 'orderDate' => $today, 'status' => 3])->sum('grandTotal');
                $todaySale = $orderSum;*/
                
                //Get Sum of total sales - today
                $todaySale = DB::table('bnTransactions')->where(['barId' => $id, 'paymentDate' => $today, 'status' => 1])->sum('amount');

                //Get the Total Footfall of the day (Visit with status: 2/4)
                /*$todayFootFall = GeoTagging::select('bnGeoTaggings.id')
                ->where(function ($query) use ($today) {
                if (!empty($today)) {
                $query->whereRaw('date(bnGeoTaggings.createdAt) = ?', $today);
                }

                })
                ->where(['bnGeoTaggings.barId' => $id])
                ->get();*/

                $todayFootFall = Visit::select('id')
                    ->where(['barId' => $id, 'visitDate' => $today])
                    ->where('bnVisits.status', '!=', 1)
                    ->where('bnVisits.status', '!=', 3)
                    ->get();

                //Get Active Patrons count
                $patrons = GeoTagging::select('bnGeoTaggings.id')
                    ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
                    ->where(function ($query) use ($today) {
                        if (!empty($today)) {
                            $query->whereRaw('date(bnGeoTaggings.createdAt) = ?', $today);
                        }

                    })
                    ->where(['bnGeoTaggings.barId' => $id, 'v.status' => 2]) //2 means confirmed
                    ->get();

                //Get Total Visits count for today
                $visits = Visit::select('id')
                    ->where(['barId' => $id, 'visitDate' => $today])
                    ->get();

                //Get Total Orders count for today
                $orders = Order::select('id')
                    ->where(['barId' => $id, 'orderDate' => $today])
                    ->get();

                $data['other'] = array('todaySale' => number_format($todaySale), 'todayFootFall' => number_format(count($todayFootFall)), 'patrons' => number_format(count($patrons)), 'visits' => number_format(count($visits)), 'orders' => number_format(count($orders)));

                //Get the banners - used in Bar App
                $data['banners'] = BarBanner::select('id', 'mediaName')
                    ->where(['barId' => $id])->orderBy('id', 'desc')->get()->toArray();

                //Get the Timing - used in Bar App
                $data['timing'] = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')->where('barId', $id)->get()->toArray(); //'openingStatus',

                // get happy hours for bars if exist for today
                $curentday = strtolower(date('l'));
                $happyHours = HappyHoursTiming::select('bnHappyHourTiming.id', 'bnHappyHourTiming.startTime', 'bnHappyHourTiming.endTime')
                    ->join('bnOfferDetails as od', 'od.id', '=', 'bnHappyHourTiming.odId')
                    ->where(['od.barId' => $id, 'od.status' => 1])
                    ->whereRaw("find_in_set('$curentday',bnHappyHourTiming.weekDay)")
                    ->first(); 
                if ($happyHours) {
                    $data['happyHourTime'][] = $happyHours;
                } else {
                    $data['happyHourTime'] = [];
                }

                // Calling the Bar Rank now
                $rank = 0;
                $firstDay = date("Y-m-01");
                $topBars = DB::table('bnTransactions as t')
                    ->select('bar.id as barId', 'bar.barName', 'bar.logo')
                    ->join('bnBars as bar', 'bar.id', '=', 't.barId')
                    ->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId')
                    ->where('t.paymentDate', '>=', $firstDay)
                    ->where(['t.status' => 1])
                    ->orderBy('t.amount', 'desc')
                    ->groupBy('t.barId')->get();
                
                $barToppers = array();
                if (!empty($topBars)) {
                    foreach ($topBars as $key => $row) {
                        if($row->barId == $id) {
                            $rank = $key+1;
                            break;
                        }
                    }
                }
                $data['rank'] = $rank;

                /*$lastMonth = strtolower(date('F', strtotime('-1 month', time()))) . '-' . date('Y');
                $rankData = TopBar::select('bnTopBars.position')
                    ->leftJoin('bnBars as b', 'b.id', '=', 'bnTopBars.barId')
                    ->where(['bnTopBars.month' => $lastMonth, 'bnTopBars.barId' => $id])
                    ->first();

                if (!empty($rankData)) {
                    $data['rank'] = $rankData->position;
                }
                else
                    $data['rank'] = 0;*/

                //Converting NULL to "" String
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $result = array('status' => 1, 'message' => 'Profile get successfully', 'data' => $data);
            } else {
                $result = array('status' => 0, 'message' => 'Invalid Bar');
            }
        }

        return response()->json($result);
    }

    //Function to Change Password
    public function barChangePassword(Request $request)
    {
        extract($_POST);
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'oldPass' => 'required',
            'newPass' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $userId = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $request->barId);
        if ($userId) {
            if (Auth::attempt(['id' => $userId, 'password' => request('oldPass'), 'userType' => 'B'])) {
                $data = array('password' => bcrypt($request->newPass), 'updatedAt' => $this->entryDate);
                $changePassword = BarUser::where('id', $userId)->update($data);

                if ($changePassword) {
                    //get bar name to create description
                    // $barName = CommonFunction::GetSingleField('bnBars', 'barName', 'id', $request->barId);
                    // $data = array('description' => $barName . ' has changed account password', 'barId' => $request->barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    //Entry in activity log table in db
                    //ActivityLog::create($data);

                    // Delete other Login Tokens
                    DB::table('bnLoginTokens')->where(['userId' => $userId])
                        ->where('token', '!=', $request->token)->delete();

                    $result = array('status' => 1, 'message' => 'Password has been changed successfully');
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
                }
            } else {
                $result = array('status' => 0, 'message' => 'Current password does not match');
            }

        } else {
            $result = array('status' => 0, 'message' => 'Bar profile not found');
        }

        return response()->json($result);
    }

    //Function to Forgot Password
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $email = request('email');

        /*===Old Email code===*/
        /*$emailExist = BarUser::select('id', 'email')->where(['email' => $email, 'userType' => 'B'])->first();

        if ($emailExist) {
        //print_r($request->only('email'));exit;
        $response = $this->broker()->sendResetLink(
        $request->only('email')
        );

        if ($response) {
        $result = array('status' => 1, 'message' => 'We have e-mailed your password reset link on email');
        } else {
        $result = array('status' => 0, 'message' => 'Something went wrong. Please try again');
        }
        } else {
        $result = array('status' => 0, 'message' => 'No account found for this email');
        }*/

        $user = DB::table('bnUsers')->where('email', $request->email)
            ->where('userType', '!=', 'U')->first();
        if (!empty($user) && ($user->userType == 'B' || $user->userType == 'A' || $user->userType == 'S')) {
            
            // Get the user's name
            if($user->userType == 'B') 
                $name = CommonFunction::GetSingleField('bnBars', 'barName', 'userId', $user->id);
            else if($user->userType == 'S')
                $name = CommonFunction::GetSingleField('bnStaffs', 'name', 'userId', $user->id);
            else $name = "Admin";

            // Generate Random password
            $length = 8;
            $password = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);

            $param = array('name' => $name, 'password' => $password, 'email' => $user->email);

            try {
                // Mail::to($request->email)->send(new ForgotMail($param));

                Mail::send('emails.forgotPassword', $param, function ($message) use ($param) {
                    $message->from('noreply@barnights.com', config('app.name'));
                    $message->subject('Forgot password');
                    //$message->to('wasim@coretechies.com');
                    $message->to($param['email']);
                });
                $update = DB::table('bnUsers')
                    ->where('email', $request->email)
                    ->update(['password' => bcrypt($password), 'updatedAt' => date('Y-m-d H:i:s')]);

                // Delete the Login Tokens
                DB::table('bnLoginTokens')->where(['userId' => $user->id])->delete();

                $result = array('status' => 1, 'message' => 'New password has been sent to your email address. Please check your inbox.');
                
                return response()->json($result);

            } catch (\Exception $e) {
                $error = $e->getMessage();
                $result = array('status' => 0, 'message' => $error);
                return response()->json($result);
            }
        } else {
            $result = array('status' => 0, 'message' => 'Email address does not exist');
            return response()->json($result);
        }

        //return response()->json($result);
    }

    // Get Bar complete profile - on Edit data loading
    public function getBarProfileData(Request $request)
    {
        /*$user = Auth::user();
        $response = array('status' => $this->successStatus, 'data' => $user);*/

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $id = $request->id;
        $currentDay = strtolower(date('l'));

        //Calling the Bar Now
        $userExist = Bar::select('bnBars.id as barId','bnBars.userId as barUserId','u.status as barStatus', 'bnBars.barName', 'u.email', 'u.phone', 'bnBars.about', 'bnBars.logo', 'ad.address', 'ad.cityId', 'ad.areaId', 'ad.stateId', 'ad.latitude', 'ad.longitude', 'city.city_name as cityName', 'st.state_name as stateName', 'bnBars.cgst', 'bnBars.sgst', 'bnBars.serviceCharge', 'bnBars.serviceChargeTax')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnBars.userId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'bnBars.userId')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnStates as st', 'st.state_id', '=', 'ad.stateId')
            //->where(['bnBars.id' => $id, 'u.status' => 1, 'u.userType' => 'B'])->first();
            ->where(['bnBars.id' => $id,'u.userType' => 'B'])->first();
        if ($userExist) {
            //bar opening and clossing time for all days(openingStatus:-1:open,0:close)
            $openHour = array();
            $openingStatus = 0;

            $openingHrs = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')->where('barId', $id)->get()->toArray(); //'openingStatus',

            /*if (count($openingHrs) > 0) {
            foreach ($openingHrs as $openHrs) {
            //checking the week day with current day to get open or close status of bar
            if ($openHrs->weekDay == $currentDay) {
            $openingStatus = $openHrs->openingStatus;
            }
            //remove the key from array which dont want
            //unset($openHrs['openingStatus']);

            //Converting NULL to "" String
            array_walk_recursive($openHrs, function (&$item, $key) {
            $item = null === $item ? '' : $item;
            });

            $openHour[] = $openHrs;
            }
            }
            $userExist['openingStatus'] = $openingStatus;*/

            //Converting NULL to "" String
            array_walk_recursive($userExist, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $data['profile'] = $userExist;
            $data['hours'] = $openingHrs;

            //get bar kyc docs
            $barlicense = array();
            $kycDocs = BarLicense::select('id', 'fileName', 'mediaType', 'licenseTypeId')->where('barId', $id)->orderBy('id', 'desc')->get();
            if(!empty($kycDocs)) {
                foreach ($kycDocs as $row) {
                    $docTypes = CommonFunction::licenseTypes();
                    $key = $row->licenseTypeId;
                    $docName = '';
                    if(array_key_exists($key, $docTypes))
                        $docName = $docTypes[$key];

                    $row->docName = $docName;
                    $barlicense[] = $row;
                }
            } 

            $license = array();
            /*if (count($barlicense) > 0) {
            foreach ($barlicense as $bl) {
            $licenseType = $bl->licenseTypeId;
            $licenseData = CommonFunction::licenseTypes();
            $array_new = [];
            if (array_key_exists($licenseType, $licenseData)) {
            $array_new = $licenseData[$licenseType];
            }
            $bl['licenseType'] = $array_new;
            $license[] = $bl;
            unset($bl['licenseTypeId']);
            }
            }*/
            $data['kycDocs'] = $barlicense;

            //Get the banners
            $data['banners'] = BarBanner::select('id', 'mediaName', 'displayOrder')
                ->where(['barId' => $id])->orderBy('id', 'desc')->get()->toArray();

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'Profile get successfully', 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }

        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Update user basic profile info
    public function editBarProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'barName' => 'required',
            //'logo' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg,SVG|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //print_r($request->all());exit;

        //Update in Table
        $update = array(
            'barName' => $request->barName,
            'about' => $request->about,
            'updatedAt' => date("Y-m-d H:i:s"),
        );

        //Check logo is exist or not in the request
        if ($request->hasFile("logo")) {
            //Call the current logo
            //$logo = CommonFunction::GetSingleField('bnBars', 'logo', 'id', $request->id);

            $file = $request->file("logo");
            //md5($file->getClientOriginalName())
            $ext = $file->getClientOriginalExtension(); //get extention of file
            //encrypt the name of file
            $logoName = time() * rand() . "." . $ext; //
            $upload = $file->move("uploads/bars/logo/", $logoName);
            if (!empty($upload)) {
                $update['logo'] = $logoName;
            }
        }
        $updateData = Bar::where('id', $request->id)->update($update);

        if ($updateData) {
            //get user id from bar table
            $barData = CommonFunction::GetSingleRow('bnBars', 'id', $request->id);

            //update bar email,phone in user table
            /*$updateUser = array(
            'email' => $email,
            'phone' => $phone,
            );
            $updateUser = User::where('id', $userId)->update($updateUser);*/

            //update bar adress data
            $updateAddress = array(
                'address' => $request->address,
                'cityId' => $request->cityId,
                'stateId' => $request->stateId,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
            );
            if(isset($request->areaId))
                $updateAddress['areaId'] = $request->areaId;
            $updateAddress = Address::where('userId', $barData->userId)->update($updateAddress);

            //Entry in activity log table in db
            // $data = array('description' => $request->barName . 'updated profile', 'barId' => $request->id, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            //ActivityLog::create($data);

            //update bar status i.e:open or close in bar Timing table
            /*$currentDay = strtolower(date('l'));
            $updateStatus = array(
            'openingStatus' => $openingStatus,
            );
            $updateStatus = BarTiming::where(['barId' => $id, 'weekDay' => $currentDay])->update($updateStatus);*/

            $result = array('status' => 1, 'message' => "Data updated successfully");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong, please try again later.");
        }
        return response()->json($result);
    }

    //Function to Update Taxes of Bar
    public function editBarTaxes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            //'cgst' => 'required',
            //'sgst' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $data = array(
            'cgst' => $request->cgst,
            'sgst' => $request->sgst,
            'serviceCharge' => $request->serviceCharge,
            'serviceChargeTax' => $request->serviceChargeTax,
            'updatedAt' => date("Y-m-d H:i:s"),
        );

        if (empty($request->cgst)) {
            $data['cgst'] = 0;
        }

        if (empty($request->sgst)) {
            $data['sgst'] = 0;
        }

        if (empty($request->serviceCharge)) {
            $data['serviceCharge'] = 0;
        }

        if (empty($request->serviceChargeTax)) {
            $data['serviceChargeTax'] = 0;
        }

        //unset($data['id']);
        $updateData = Bar::where('id', $request->id)->update($data);

        if ($updateData) {
            //Entry in activity log table in db
            // $barName = CommonFunction::GetSingleField('bnBars', 'barName', 'id', $request->id);
            // $bName = '';
            // if (!empty($barName)) {
            //     $bName = $barName;
            // }
            // $data = array('description' => $bName . 'updated tax rates', 'barId' => $request->id, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            //ActivityLog::create($data);

            $result = array('status' => 1, 'message' => "Tax updated successfully");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong, please try again later");
        }

        return response()->json($result);
    }

    //bar images and banners apis add,delete,list

    //bar image videos listing function
    public function getImagesVideos($barId = null, $startFrom, $limit)
    {
        if ($barId != null) {
            $barMedia = BarMediaFile::select('id', 'mediaName', 'mediaType')
                ->where(['barId' => $barId])
                ->offset($startFrom)->limit($limit)
                ->orderBy('id', 'desc')->get()->toArray();
            return $barMedia;
        }
    }

    //Add image and video function
    public function addBarMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            //'mediaName' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG,3gp ,3GP,avi,AVI,flv,FLV,h264,H264,m4v,M4V,MKV,MOV,MPG,MPEG,RM,SWF,VOB,WMV,mkv,mov,mp4,mpg,mpeg,rm ,swf,vob,wmv|max:500000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        //Media Uploading - From Bar Panel Mobile App
        if ($request->hasFile("mediaName") && !isset($request->base64Img)) {
            $file = $request->file("mediaName");
            $mediaSize = 0; //$file->getSize();
            $ext = $file->getClientOriginalExtension();
            $mediaType = $request->mediaType;

            //Figure out the Mime type of file to insert the media type
            /*$mimeType = $file->getMimeType();
            $mimeType = explode("/", $mimeType);
            $mediaType = $mimeType[0];
            if ($mediaType == 'image') {
                $mediaType = 1;
            } else {
                $mediaType = 2;
            }*/

            $folder = ($mediaType == 1) ? 'images' : 'videos';

            $mediaName = time() . rand() . "." . $ext;
            $upload = $file->move("uploads/bars/".$folder."/", $mediaName);
            if($upload)
                $fileName = $mediaName;
        }

        // Upload offer images, from base64 string. CROPPED - Web Panel
        if(!empty($request->mediaName) && isset($request->base64Img) && !$request->hasFile("mediaName")) {
            $mediaType = 1;
            $mediaSize = 0;
            $image = $request->mediaName;  
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time() * rand().'.'.'png';
            $upload =  \File::put("uploads/bars/images/" . $imageName, base64_decode($image));
            if ($upload) 
                $fileName = $imageName;   
        }

        //Check media is exist or not in the request
        if (isset($fileName)) {
            /*$file = $request->file("mediaName");
            $mediaSize = $file->getSize();

            if($mediaSize > 50000000) { // 50 MB - 50000000
                $result = array('status' => 0, 'message' => "Image/Video size should be maximum of 50 MB");
                return response()->json($result);
            }
            $ext = $file->getClientOriginalExtension(); //get extention of file

            //Figure out the Mime type of file to insert the media type
            $mimeType = $file->getMimeType();
            $mimeType = explode("/", $mimeType);
            $mediaType = $mimeType[0];
            if ($mediaType == 'image') {
                $mediaType = 1;
                //resize orignal image and create thumbnails
                $image_resize_thumb = Image::make($file->getRealPath());
                $image_resize_thumb->resize(100, 100);
            } else {
                $mediaType = 2;
            }

            //encrypt the name of file
            $mediaName = time() . rand() . "." . $ext;

            if ($mediaType == 1) {
                //path to save resize images
                $image_resize_thumb->save(public_path("uploads/bars/images/thumb/" . $mediaName));
                //path to save orignal images
                $upload = $file->move("uploads/bars/images/", $mediaName);
            } else {
                $upload = $file->move("uploads/bars/videos/", $mediaName);
            }*/

            $mediaName = $fileName;
            $data = array(
                'barId' => $request->barId,
                'mediaName' => $mediaName,
                'mediaType' => $mediaType,
                'mediaSize' => $mediaSize,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            );
            $media = BarMediaFile::create($data);

            //Return the data in resonse
            if ($media['id']) {
                $data = array('id' => $media['id'], 'mediaName' => $mediaName, 'mediaType' => $mediaType);
                $result = array('status' => 1, 'message' => 'Media file has been uploaded successfully', 'data' => $data);
            }
            else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please select a file to upload');
        }

        return response()->json($result);
    }

    //Add banners of bar function
    public function addBarBanners(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            //'mediaName' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        //print_r($request->all());exit;
        extract($_POST);

        //First check the count of Banners in a Bar
        $banners = BarBanner::select('bnBarBanners.id')->where(['bnBarBanners.barId' => $barId])->get();
        if (count($banners) < 5) {

            //Media Uploading - From Bar Panel Mobile App
            if ($request->hasFile("mediaName") && !isset($request->base64Img)) {
                /*$file = $request->file("mediaName");
                $mediaSize = $file->getSize(); //File size
                $ext = $file->getClientOriginalExtension(); //get extention of file
                
                //resize orignal image
                $image_resize = Image::make($file->getRealPath());
                $image_resize->resize(1200, 800);
                
                //Create thumbnails
                $image_resize_thumb = Image::make($file->getRealPath());
                $image_resize_thumb->resize(100, 100);

                //encrypt the name of file
                $mediaName = time() . rand() . "." . $ext;

                //$image_resize_thumb->save(public_path("uploads/bars/banners/thumb/" . $mediaName));
                $upload = $image_resize->save(public_path("uploads/bars/banners/" . $mediaName));

                if ($upload) {
                    $fileName = $mediaName;
                }*/

                $file = $request->file("mediaName");
                $ext = $file->getClientOriginalExtension();
                $mediaName = time() * rand() . "." . $ext;
                $upload = $file->move("uploads/bars/banners/", $mediaName);
                if ($upload) {
                    $fileName = $mediaName;
                }
            }

            // Upload offer images, from base64 string. CROPPED - Web Panel
            if(!empty($request->mediaName) && isset($request->base64Img) && !$request->hasFile("mediaName")) {
                $image = $request->mediaName;  
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = time() * rand().'.'.'png';
                $upload =  \File::put("uploads/bars/banners/" . $imageName, base64_decode($image));
                if ($upload) 
                    $fileName = $imageName;   
            }
            
            //Check media is exist or not in the request
            //if ($request->hasFile("mediaName")) {
            if (isset($fileName)) {
                $mediaName = $fileName;
                $data = array(
                    'barId' => $request->barId,
                    'mediaName' => $mediaName,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );
                $banner = BarBanner::create($data);

                //Return the data in resonse
                $data = array();
                if ($banner['id']) {
                    $data = array('id' => $banner['id'], 'mediaName' => $mediaName);
                }

                //Entry in activity log table in db
                /*$barName = CommonFunction::GetSingleField('bnBars', 'barName', 'id', $request->barId);
                $bName = '';
                if (!empty($barName)) {
                $bName = $barName;
                }*/

                // Activity Log Entry
                // $acData = array('description' => 'Uploaded new banner', 'barId' => $request->barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                //ActivityLog::create($acData);

                $result = array('status' => 1, 'message' => 'Banner uploaded successfully', 'data' => $data);

            } else {
                $result = array('status' => 0, 'message' => 'Please select a banner');
            }
        } else {
            $result = array('status' => 0, 'message' => 'No more banners allowed');
        }

        return response()->json($result);
    }

    //delete image or video
    public function deleteBarMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        $status = false;

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $oldPic = null;
        if ($request->type == 'banner') {
            //take image
            $pic = CommonFunction::GetSingleRow('bnBarBanners', 'id', $request->id);
            if (!empty($pic)) {
                $oldPic = $pic->mediaName;
            }

            //delete banner image of bar
            $bannerDelete = BarBanner::where(['id' => $request->id])->delete();
            if ($bannerDelete) {
                $status = true;
                if (isset($oldPic) && $oldPic != null) {
                    $imgToDel = public_path("uploads/bars/banners/" . $oldPic);
                    $thumbToDel = public_path("uploads/bars/banners/thumb/" . $oldPic);
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                    if (file_exists($thumbToDel)) {
                        unlink($thumbToDel);
                    }
                }
            }
        } else if ($request->type == 'media') {
            //take data
            $row = CommonFunction::GetSingleRow('bnBarMediaFiles', 'id', $request->id);
            if (!empty($row)) {
                $oldPic = $row->mediaName;
                $mediaType = $row->mediaType;
            }
            //delete bar images
            $mediaDelete = BarMediaFile::where(['id' => $request->id])->delete();
            if ($mediaDelete) {
                $status = true;
                if (isset($oldPic) && $oldPic != null) {
                    if ($mediaType == 1) {
                        $imgToDel = public_path("uploads/bars/images/" . $oldPic);
                        $thumbToDel = public_path("uploads/bars/images/thumb/" . $oldPic);
                    } else if ($mediaType == 2) {
                        $imgToDel = public_path("uploads/bars/videos/" . $oldPic);
                    }
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                    if (isset($thumbToDel) && file_exists($thumbToDel)) {
                        unlink($thumbToDel);
                    }
                }
            }
        } else if ($request->type == 'kyc') {
            //take file name
            $pic = CommonFunction::GetSingleRow('bnBarLicenses', 'id', $request->id);
            if (!empty($pic)) {
                $oldPic = $pic->fileName;
            }
            //delete bar license docs
            $mediaDelete = BarLicense::where(['id' => $request->id])->delete();
            if ($mediaDelete) {
                $status = true;
                if (isset($oldPic) && $oldPic != null) {
                    $imgToDel = public_path("uploads/bars/kycdoc/" . $oldPic);
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                }
            }
        }

        if ($status == true) {
            //Entry in activity log table in db
            // $barName = CommonFunction::GetSingleField('bnBars', 'barName', 'id', $pic->barId);
            // $bName = '';
            // if (!empty($barName)) {
            //     $bName = $barName;
            // }
            // $data = array('description' => $bName . 'upload new banner', 'barId' => $pic->barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            //ActivityLog::create($data);

            $result = array('status' => 1, 'message' => 'Media has been deleted successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Media not deleted');
        }

        return response()->json($result);
    }

    //bar kyc doc add,delete and license type list apis

    //Add kyc Doc function
    public function addLicenseDoc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            //'fileName' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG,doc,DOC,docx,DOCX,pdf,PDF,tex,TEX,txt,TXT|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //Check media is exist or not in the request
        if ($request->hasFile("fileName")) {
            $file = $request->file("fileName");
            $ext = $file->getClientOriginalExtension(); //get extention of file
            
            // Get Mime type of the file, to store mediaType in DB
            $mineType = $file->getMimeType();
            $mType = explode("/", $mineType);
            if($mType[0] == 'image')
                $mediaType = 1;
            else
                $mediaType = 2;

            $fileName = time() * rand() . "." . $ext;
            $upload = $file->move("uploads/bars/kycdoc/", $fileName);

            if (!empty($upload)) {
                $data = array(
                    'barId' => $request->barId,
                    'licenseTypeId' => $request->licenseTypeId,
                    'fileName' => $fileName,
                    'mediaType' => $mediaType,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );
                $license = BarLicense::create($data);

                //Return the data in resonse
                $data = array();
                if ($license['id']) {

                    $docTypes = CommonFunction::licenseTypes();
                    $key = $request->licenseTypeId;
                    $docName = '';
                    if(array_key_exists($key, $docTypes))
                        $docName = $docTypes[$key];

                    $data = array('id' => $license['id'], 'fileName' => $fileName, 'docName' => $docName, 'mediaType' => $mediaType);
                }

                $result = array('status' => 1, 'message' => 'Document has been uploaded successfully', 'data' => $data);
            } else {
                $result = array('status' => 0, 'message' => 'Problem in document uploading, please try again later');
            }

        } else {
            $result = array('status' => 0, 'message' => 'No file exist');
        }

        return response()->json($result);
    }

    //delete License Document
    public function deleteLicenseDoc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //get image from table
        $pic = CommonFunction::GetSingleField('bnBarLicenses', 'fileName', 'id', $request->id);
        if (!empty($pic)) {
            $oldPic = $pic->fileName;
        } else {
            $oldPic = null;
        }

        //delete the doc from bar license table
        $fileDelete = BarLicense::where(['id' => $request->id])->delete();
        if ($fileDelete) {
            //Delete the Privous Image
            if (isset($oldPic) && $oldPic != null) {
                $imgToDel = public_path("uploads/bars/kycdoc/" . $oldPic);
                if (file_exists($imgToDel)) {
                    unlink($imgToDel);
                }

            }
            $result = array('status' => 1, 'message' => 'Bar license deleted successfully');
        } else {
            $result = array('status' => 1, 'message' => 'Bar license not deleted');
        }
        return response()->json($result);
    }

    //license listing function
    public function getLicenseList(Request $request)
    {
        $licenseTypes = CommonFunction::licenseTypes();
        foreach ($licenseTypes as $key => $value) {
            $licenseData['id'] = $key;
            $licenseData['doc'] = $value;
            $licenseType[] = $licenseData;
        }
        // $record['licenseType'] = $licenseType;
        if ($licenseType) {
            $result = array('status' => 1, 'message' => 'License list get successfully', 'data' => $licenseType);
        } else {
            $result = array('status' => 0, 'message' => 'No data exist');
        }
        return response()->json($result);
    }

    //reviews and ratings function
    public function getBarReviews($barId = null, $startFrom, $limit)
    {
        if ($barId != null) {
            $barReviews = BarReview::select('bnBarReviews.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnBarReviews.rating', 'bnBarReviews.review')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnBarReviews.userId')
                ->orderBy('bnBarReviews.id', 'desc')
                ->offset($startFrom)->limit($limit)
                ->where(['bnBarReviews.barId' => $barId])->get()->toArray();
            return $barReviews;
        }
    }

    //add edit bar Timing function
    public function addEditBarTiming(Request $request)
    {
        //[{"weekDay":"thursday","openingTime":"8:00:00","closingTime":"10:00:00","id":""}, {"weekDay":"friday","openingTime":"16:00:00","closingTime":"10:00:00","id":""},{"weekDay":"monday","openingTime":"4:00:00","closingTime":"2:00:00","id":"26"},{"weekDay":"tuesday","openingTime":"1:00:00","closingTime":"2:00:00","id":"25"}]
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'barTiming' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }

        //print_r($request->all());exit;

        //Insert Timing
        if (!empty($request->barTiming)) {
            $barTiming = json_decode($request->barTiming, true);
            $i = 1;
            $day = 'day';

            foreach ($barTiming as $key => $val) {
                //print_r($val); echo $val[$day.$i.'Id']; exit;
                if (!empty($val)) {
                    extract($val);

                    if (empty($val[$day . $i . 'OpenTime']) || $val[$day . $i . 'OpenTime'] == 'null') {
                        $openingTime = null;
                    } else {
                        $openingTime = $val[$day . $i . 'OpenTime'];
                    }

                    if (empty($val[$day . $i . 'CloseTime']) || $val[$day . $i . 'CloseTime'] == 'null') {
                        $closingTime = null;
                    } else {
                        $closingTime = $val[$day . $i . 'CloseTime'];
                    }

                    $weekDay = $val[$day . $i];

                    // If end time is lower than start time, then return error message
                    /*if ($openingTime > $closingTime) {
                        $day = $val[$day . $i];
                        $result = array('status' => 0, 'message' => "Closing time should be lower than opening time on ".ucwords($day));
                        return response()->json($result);
                    }*/

                    // Time Validation
                    $result = CommonFunction::barTimingValidation($openingTime, $closingTime, $weekDay);
                    if(!empty($result) && $result['status'] == 0) {
                        $result = array('status' => 0, 'message' => $result['message']);
                        return response()->json($result);
                    }

                    //Update the data
                    if ($val[$day . $i . 'Id'] != '') {
                        $id = $val[$day . $i . 'Id'];
                        $data = array(
                            'weekDay' => $weekDay,
                            'openingTime' => $openingTime,
                            'closingTime' => $closingTime,
                            'updatedAt' => date("Y-m-d H:i:s"),
                        );

                        $btupdate = BarTiming::where(['id' => $id])->update($data);
                        $result = array('status' => 1, 'message' => 'Bar Timing has been updated successfully');
                    }
                    //Insert the data
                    else {
                        $data = array(
                            'weekDay' => $weekDay,
                            'openingTime' => $openingTime,
                            'closingTime' => $closingTime,
                            'barId' => $request->barId,
                            'createdAt' => $this->entryDate,
                            'updatedAt' => $this->entryDate,
                        );
                        $btinsert = BarTiming::create($data);

                        if ($btinsert['id']) {
                            $result = array('status' => 1, 'message' => 'Bar Timing has been added successfully');
                        } else {
                            $result = array('status' => 0, 'message' => 'Bar Timing not added');
                        }
                    }
                }
                $i++;
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please add the opening closing timing');
        }

        return response()->json($result);
    }

    //Get Bar Rank to display in Sidebar
    public function getBarRank(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $data = array();
        $id = $request->id; //BarId
        $firstDay = date("Y-m-01");
        $lastMonth = strtolower(date('F', strtotime('-1 month', time()))) . '-' . date('Y');

        // Calling the Bar Rank now
        /*$data = TopBar::select('bnTopBars.position', 'b.barName', 'b.logo')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnTopBars.barId')
            ->where(['bnTopBars.month' => $lastMonth, 'bnTopBars.barId' => $id])
            ->first();*/

        $topBars = DB::table('bnTransactions as t')
            ->select('bar.id as barId', 'bar.barName', 'bar.logo')
            ->join('bnBars as bar', 'bar.id', '=', 't.barId')
            ->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId')
            ->where('t.paymentDate', '>=', $firstDay)
            ->where(['t.status' => 1])
            ->orderBy('t.amount', 'desc')
            ->groupBy('t.barId')->get();
        
        $barToppers = array();
        if (!empty($topBars)) {
            foreach ($topBars as $key => $row) {
                if($row->barId == $id) {
                    $rank = $key+1;
                    $data = array('position' => $rank, 'barName' => $row->barName, 'logo' => $row->logo);
                }
            }
        }

        //get bar name if data of bar position is empty
        if (empty($data)) {
            $barInfo = Bar::select('barName', 'logo')
                ->where(['id' => $id])
                ->first();
            $data = array('position' => 0, 'barName' => $barInfo->barName, 'logo' => $barInfo->logo);
        }

        if ($data) {
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'Rank get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to Get States
    public function getAllStates(Request $request)
    {
        $states = CommonFunction::getAllStates();
        if ($states) {
            $result = array('status' => 1, 'message' => 'States get successfully', 'data' => $states);
        } else {
            $result = array('status' => 0, 'message' => 'No state found');
        }

        return response()->json($result);
    }

    //Funciton to Get States
    public function getAllCities(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'stateId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $cities = CommonFunction::getAllCities($request->stateId);
        if ($cities) {
            $result = array('status' => 1, 'message' => 'Cities get successfully', 'data' => $cities);
        } else {
            $result = array('status' => 0, 'message' => 'No city found');
        }

        return response()->json($result);
    }
 
    //Funciton to Get States
    public function getAllAreas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cityId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $areas = DB::table('bnCityAreas')->select('id','area')->where('cityId', $request->cityId)->orderBy('area', 'asc')->get()->toArray();
        if (count($areas)>0) {
            $result = array('status' => 1, 'message' => 'Areas get successfully', 'data' => $areas);
        } else {
            $result = array('status' => 0, 'message' => 'No area found');
        }

        return response()->json($result);
    }

    //Get top 10 bars
    public function getTopBars(Request $request)
    {

        $lastMonth = strtolower(date('F', strtotime('-1 month', time()))) . '-' . date('Y');

        //Calling the top 10 bars
        $data = TopBar::select('bnTopBars.position', 'b.barName')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnTopBars.barId')
            ->where(['bnTopBars.month' => $lastMonth])
            ->orderBy('bnTopBars.position', 'asc')
            ->limit(10)
            ->get();

        if (count($data) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'Top senders get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Get Bar complete profile - on My Profile
    public function getBarDashboard(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $id = $request->barId;
        $currentDay = strtolower(date('l'));
        $today = date("Y-m-d");

        //Calling the Bar Now
        $userExist = Bar::select('bnBars.id as barId', 'bnBars.barName', 'bnBars.about', 'bnBars.logo', 'ad.address', 'ad.cityId', 'ad.stateId', 'ad.latitude', 'ad.longitude', 'city.city_name as cityName', 'st.state_name as stateName')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'bnBars.userId')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnStates as st', 'st.state_id', '=', 'ad.stateId')
            ->where(['bnBars.id' => $id])->first();
        if ($userExist) {
            $data['profile'] = $userExist;
            //Get Active Patrons count
            $patrons = GeoTagging::select('bnGeoTaggings.id')
                ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
                ->where(function ($query) use ($today) {
                    if (!empty($today)) {
                        $query->whereRaw('date(bnGeoTaggings.createdAt) = ?', $today);
                    }

                })
                ->where(['bnGeoTaggings.barId' => $id, 'v.status' => 2]) //2 means confirmed
                ->get();

            //Get Sum  total sales
            $orderSum = DB::table('bnOrders')->where(['barId' => $id, 'orderDate' => $today, 'status' => 3])->sum('grandTotal');

            //get sum of special event sales
            $specialEvent = DB::table('bnEventBookings as eb')
                ->leftJoin('bnSpecialEvents as se', 'se.id', '=', 'eb.eventId')
                ->where(['se.barId' => $id, 'se.status' => 1])->whereRaw('date(eb.createdAt) = ?', $today)->sum('eb.amount');

            $totalSales = $orderSum;
            if (!empty($specialEvent)) {
                $totalSales = $orderSum + $specialEvent;
            }

            //Get Total code availed count for today
            $visits = VisitCode::select('bnVisitCodes.id')
                ->join('bnVisits as v', 'v.id', '=', 'bnVisitCodes.visitId')
                ->where(['v.barId' => $id, 'v.visitDate' => $today, 'bnVisitCodes.codeAvailed' => 1])
                ->get();

            //Get the Total Footfall of the day (Geotagging)
            $todayFootFall = Visit::select('id')
                ->where(['barId' => $id, 'visitDate' => $today])
                ->where('bnVisits.status', '!=', 1)
                ->where('bnVisits.status', '!=', 3)
                ->get();

            $data['other'] = array('patrons' => number_format(count($patrons)), 'todaySale' => number_format($totalSales), 'codeGenerated' => number_format(count($visits)), 'todayFootFall' => number_format(count($todayFootFall)));

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Profile get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'Invalid Bar');
        }

        return response()->json($result);
    }

    //edit bar Timing function for bar app
    public function editBarTiming(Request $request)
    {
        //[{"weekDay":"thursday","openingTime":"8:00:00","closingTime":"10:00:00","id":""}, {"weekDay":"friday","openingTime":"16:00:00","closingTime":"10:00:00","id":""},{"weekDay":"monday","openingTime":"4:00:00","closingTime":"2:00:00","id":"26"},{"weekDay":"tuesday","openingTime":"1:00:00","closingTime":"2:00:00","id":"25"}]
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'barTiming' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }

        //print_r($request->all());exit;

        //Insert Timing
        if (!empty($request->barTiming)) {
            $barTiming = json_decode($request->barTiming, true);

            foreach ($barTiming as $key => $val) {
                //print_r($val); echo $val[$day.$i.'Id']; exit;
                if (!empty($val)) {
                    extract($val);

                    if (empty($openingTime) || $openingTime == 'null') {
                        $openingTime = null;
                    } else {
                        $openingTime = $openingTime;
                    }

                    if (empty($openingTime) || $openingTime == 'null') {
                        $openTime = null;
                    } else {
                        $openTime = $openingTime;
                    }

                    if (empty($closingTime) || $closingTime == 'null') {
                        $closeTime = null;
                    } else {
                        $closeTime = $closingTime;
                    }

                    //Update the data
                    // $id = $val[$id];
                    $data = array(
                        'weekDay' => $weekDay,
                        'openingTime' => $openTime,
                        'closingTime' => $closeTime,
                        'updatedAt' => date("Y-m-d H:i:s"),
                    );

                    $btupdate = BarTiming::where(['id' => $id])->update($data);
                    if (!empty($btupdate)) {
                        $result = array('status' => 1, 'message' => 'Bar Timing has been updated successfully');
                    } else {
                        $result = array('status' => 0, 'message' => 'Bar Timing not added');
                    }

                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong');
                }
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please add the opening closing timing');
        }

        return response()->json($result);
    }

    // Funciton to mark banner as default
    public function updateBannerOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bannerId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $param = array('displayOrder' => 2);
        DB::table('bnBarBanners')->where('barId', $request->barId)->update($param);

        $param = array('displayOrder' => 1);
        DB::table('bnBarBanners')->where('id', $request->bannerId)->update($param);

        $result = array('status' => 1, 'message' => "Banner has been set as default");
        return response()->json($result);
    }
}