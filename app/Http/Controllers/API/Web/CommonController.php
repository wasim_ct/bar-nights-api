<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\DiscountOffer;
use App\DiscountOfferItem;
use App\HappyHoursTiming;
use App\Http\Controllers\Controller;
use App\TopBar;
use App\TopSpender;
use App\UserNotification;
use CommonFunction;
use DB;
use Exporter;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class CommonController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    public function excel()
    {
        $bar = DB::table('bnBars')
            ->select('id', 'userId', 'type', 'barName', 'about', 'logo', 'cgst', 'sgst', 'serviceCharge', 'serviceChargeTax')
            ->limit(10)->get()->toArray();

        if (!empty($bar)) {
            $barArray = [];
            // Define the Excel spreadsheet headers
            $barArray[] = ['id', 'barName', 'userId', 'type', 'about', 'logo', 'cgst', 'sgst', 'serviceCharge', 'serviceChargeTax'];

            $barName = 'Bar';
            foreach ($bar as $row) {
                $Id = $row->id;
                $barName = $row->barName;
                $userId = $row->userId;
                $type = $row->type;
                $about = $row->about;
                $logo = $row->logo;
                $cgst = $row->cgst;
                $sgst = $row->sgst;
                $serviceCharge = $row->serviceCharge;
                $serviceChargeTax = $row->serviceChargeTax;

                $barArray[] = array($row->id, ucwords(strtolower($row->barName)), $row->userId, $row->type, $row->about, $row->logo, $row->cgst, $row->sgst, $row->serviceCharge, $row->serviceChargeTax);
            }

            //print_r($studArray);exit;

            $data = collect($barArray);
            $fileName = 'Report.xlsx';
            $file_path = public_path("excel/" . $fileName); //asset('public/download/'.$fileName);

            $excel = Exporter::make('Excel');

            $excel->load($data);
            //return $excel->stream($fileName); //To Download directly
            $rs = $excel->save($file_path);

            $fileurl = asset("public/excel/" . $fileName);
            if ($fileurl) {
                $result = array('status' => 1, 'message' => 'File downloaded successfully', 'url' => $fileurl);
            } else {
                $result = array('status' => 0, 'message' => 'File not downloaded successfully');
            }
        } else {
            $result = array('status' => 0, 'message' => 'No bar exist');
        }
        return response()->json($result);
    }

    //Funciton to Get Templates for Offers
    public function getTemplates(Request $request)
    {
        //Get the Bar Info
        $barInfo = Bar::select('bnBars.type', 'bnBars.barName', 'bnBars.about', 'bnBars.logo')
            ->where(['bnBars.id' => $request->barId])->first();

        $data = DB::table('bnTemplates')
            ->where(['status' => 1])
            ->orderBy('id', 'desc')
            ->get()->toArray();

        if (!empty($data)) {
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'Templates get successfully', 'data' => $data, 'bar' => $barInfo);
        } else {
            $result = array('status' => 0, 'message' => 'No templates found', 'bar' => $barInfo);
        }

        return response()->json($result);
    }

    //Status Change Function for all Tables
    public function changeStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'primaryKey' => 'required',
            'id' => 'required',
            //'fieldName' => 'required',
            'value' => 'required',
            'table' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }

        $fieldName = 'status';
        $primaryKey = 'id';

        //In case of Order Payment status change request is received
        if ($request->table == 'bnOrdersPayment') {
            $fieldName = 'paymentStatus';
            $request->table = 'bnOrders';
        }
        //In case of Order Payment status change request is received
        else if ($request->table == 'bnBars') {
            $fieldName = 'openingStatus';
        }

        $update = DB::table($request->table)
            ->where([$primaryKey => $request->id])
            ->update([$fieldName => $request->value, 'updatedAt' => date("Y-m-d H:i:s")]);

        if ($update == 1) {
            //Update the visit status in case if Order is closing
            /*if ($request->value == 3 && $request->table == 'bnOrders') {
                $orderData = CommonFunction::GetSingleRow('bnOrders', 'id', $request->id);
                if ($orderData) {
                    $update = DB::table('bnVisits')
                        ->where(['id' => $orderData->visitId])
                        ->update([$fieldName => '4', 'updatedAt' => date("Y-m-d H:i:s")]);

                    $orderId = $request->id;
                    $userId = $orderData->userId;
                    $visitId = $orderData->visitId;
                    if($orderData->grandTotal > 0)
                        $status = 1;
                    else
                        $status = 0;

                    $bar = Bar::select('barName')->where(['bnBars.id' => $orderData->barId])->first();
                    if (!empty($bar)) {

                        if($orderData->orderType == 'order') {
                            $message = 'Your order has been completed and e-bill has generated for ' . $orderData->orderId . ' at ' . ucwords(strtolower($bar->barName));
                            $moduleType = 'orderCompleted';
                        }
                        else {
                            $message = 'Your order has been completed at ' . ucwords(strtolower($bar->barName));
                            $moduleType = 'preOrderCompleted';
                        }

                        $record = array('moduleType' => $moduleType, 'moduleId' => $orderId, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = UserNotification::create($record);

                        if ($not['id']) { 
                            $deviceToken = CommonFunction::getUserDeviceData($userId);

                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $orderId, 'userId' => $userId, 'title' => 'Order Completed', 'msg' => $message, 'status' => $status, 'visitId' => $visitId);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }
                    }
                }
            }*/
            if ($request->value == 1 || $request->value == 3 || $request->value == 4) {
                
                // Send Visit Cancel Notification
                /*if($request->table == 'bnVisits' && $request->value == 3) { 
                    // Delete Geo Tagged Entry
                    DB::table('bnGeoTaggings')->where(['visitId' => $request->id])->delete();
                    CommonFunction::visitCancelNotification($request->id);
                }*/

                // Send Notification Email To ADmin on Bar Activate Request
                if($request->table == 'bnUsers' && $request->value == 4) { 
                    CommonFunction::sendActivationEmailToAdmin($request->id);
                }

                // Send Notification Email To Bar on Activate Request Accepted
                if($request->table == 'bnUsers' && $request->value == 1) { 
                    CommonFunction::sendActivationEmailToBar($request->id);
                }

                $result = array('status' => 1, 'message' => 'Record has been activated successfully');
            } 
            else if ($request->value == 0 || $request->value == 2) {
                
                // In case of staff, delete the login tokens
                if($request->table == 'bnUsers') { 
                    DB::table('bnLoginTokens')->where(['userId' => $request->id])->delete();
                }

                $result = array('status' => 1, 'message' => 'Record has been deactivated successfully');
            } 
            else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
        }
        return response()->json($result);
    }

    //Delete data permanently Function for all Tables
    public function deleteData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'table' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }

        $status = false;
        $primaryKey = 'id';
        $offerDetail = '';

        //check that table is bnOfferDetails
        if ($request->table == 'bnOfferDetails') {
            //take data of offer detail table on bases of id
            $offerDetail = CommonFunction::GetSingleRow('bnOfferDetails', 'id', $request->id);
        }

        $delete = DB::table($request->table)->where([$primaryKey => $request->id])->delete();
        if ($delete == 1) {

            //check that offer detail is not empty
            if ($offerDetail != '') {
                if ($offerDetail->offerImage != null || $offerDetail->offerImage != '') {
                    $imgToDel = public_path("uploads/bars/offers/" . $offerDetail->offerImage);
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                }
                //check that offer type from detail is discount or happy hour
                if ($offerDetail->offerType == 'discount') {
                    //take data of dicountoffer
                    $discount = CommonFunction::GetSingleRow('bnDiscountOffers', 'odId', $request->id);
                    //delete discount offer if offer is discount
                    $disDelete = DiscountOffer::where(['odId' => $request->id])->delete();
                    if ($disDelete = 1) {
                        if ($discount != '') {
                            //delete items for that discount
                            $disDelete = DiscountOfferItem::where(['offerId' => $discount->id])->delete();
                        }
                        $status = true;
                    }
                }
                if ($offerDetail->offerType == 'happyhour') {
                    //take data of happy hour timing
                    $happyhour = CommonFunction::GetSingleRow('bnHappyHourTiming', 'odId', $request->id);
                    //delete happy hour timings or days or data
                    $hhDelete = HappyHoursTiming::where(['odId' => $request->id])->delete();
                    if ($hhDelete = 1) {
                        if ($happyhour != '') {
                            //delete happy hour items data
                            $disDelete = bnHappyHourItems::where(['hhTimingId' => $happyhour->id])->delete();
                        }
                        $status = true;
                    }
                }
            } else {
                $status = true;
            }
        }
        if ($status == true) {
            $result = array('status' => 1, 'message' => 'Record has been deleted successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Record not deleted');
        }
        return response()->json($result);
    }

    //Calculate the bar earning to insert top bar
    public function addTopBars(Request $request)
    {
        $status = false;
        $previousMonth = strtolower(date('Y-m', strtotime(date('Y-m') . " -1 month")));
        $stringPreviousMonth = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));

        //get app setting for set top three bar commission
        $appSetting = CommonFunction::getAppSettings();
        $topBarLimit = $appSetting['topBarLimit'];
        
        //get all city from bar
        $cityQuery = "SELECT a.cityId FROM bnBars as b LEFT JOIN bnAddresses as a  on a.userId = b.userId WHERE a.cityId IS NOT NULL GROUP BY a.cityId";
        $barCityList = DB::select(DB::raw($cityQuery));
        
        //get top theer bar over all
        $topTreeBarQuery = "SELECT sum(t.amount) as totalAmount, t.barId FROM bnTransactions as t  WHERE YEAR(t.createdAt)-MONTH(t.createdAt) = $previousMonth AND t.status = 1 GROUP BY t.barId ORDER BY totalAmount desc limit 3";
        $topThreeBar = DB::select(DB::raw($topTreeBarQuery));

        //Define rank on the basis of city
        if($barCityList && count($barCityList)){
            foreach($barCityList as $city){
                $amountSql = "SELECT sum(t.amount) as totalAmount, t.barId, a.cityId FROM bnTransactions as t JOIN bnBars as b  on b.id = t.barId LEFT JOIN bnAddresses as a  on a.userId = b.userId WHERE YEAR(t.createdAt)-MONTH(t.createdAt) = $previousMonth AND t.status = 1 AND a.cityId = $city->cityId  GROUP BY t.barId  ORDER BY totalAmount desc";
                $amount = DB::select(DB::raw($amountSql));
                //print_r($amount);
                if (!empty($amount)) {
                    $i = 1;
                    foreach ($amount as $am) {
                        $exist = TopBar::select('id')->where('barId', $am->barId)->where('month', $stringPreviousMonth)->where('cityId', $city->cityId)->first();
                        if ($exist) {
                            $status = true;
                            $val = 0;
                            $msg = "rank alerady exist";
                        } else {
                            //commission set here on the basis of bar rank over all
                            $commissionRate = 10;
                            foreach($topThreeBar as $key => $topBar){
                                if($appSetting && count($appSetting))
                                {
                                    if($key == 0 && $topBar->barId == $am->barId && ($topBar->totalAmount >= $topBarLimit)){
                                        $commissionRate = $appSetting['topper1Comm'];
                                    }elseif($key == 1 && $topBar->barId == $am->barId && ($topBar->totalAmount >= $topBarLimit)){
                                        $commissionRate = $appSetting['topper2Comm'];
                                    }elseif($key == 2 && $topBar->barId == $am->barId && ($topBar->totalAmount >= $topBarLimit)){
                                        $commissionRate = $appSetting['topper3Comm'];
                                    }
                                }
                            }
                            $dataInsert = array(
                                'month' => $stringPreviousMonth,
                                'position' => $i,
                                'barId' => $am->barId,
                                'cityId' => $am->cityId,
                                'commissionRate' => $commissionRate,
                                'createdAt' => $this->entryDate,
                                'updatedAt' => $this->entryDate,
                            );
                            $rankInsert = TopBar::create($dataInsert);
                            if ($rankInsert) {
                                $status = true;
                                $val = 1;
                                $msg = "Bar rank has been updated successfully";
                            }
                           // print_r($dataInsert);
                        }
                        $i++;
                    }
                }
            }
        }
        if ($status == true) {
            $result = array('status' => $val, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        return response()->json($result);
    }

    //Calculate the bar earning
    public function addTopSpenders(Request $request)
    {
        $status = false;
        $previousMonth = strtolower(date('Y-m', strtotime(date('Y-m') . " -1 month")));
        $stringPreviousMonth = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));
        //get all city from bar
        $cityQuery = "SELECT a.cityId FROM  bnPatrons as p  LEFT JOIN bnAddresses as a  on a.userId = p.userId WHERE a.cityId IS NOT NULL GROUP BY a.cityId";
        $cityList = DB::select(DB::raw($cityQuery));
        
        //get to top 10 spenders
        if($cityList && count($cityList)){
            foreach($cityList as $city){
                $amountSql = "SELECT sum(t.amount) as totalAmount,t.userId, a.cityId FROM bnTransactions as t LEFT JOIN bnAddresses as a  on a.userId = t.userId WHERE YEAR(t.createdAt)-MONTH(t.createdAt) = $previousMonth AND status = 1 AND a.cityId = $city->cityId GROUP BY t.userId  ORDER BY totalAmount desc limit 10";
                $amount = DB::select(DB::raw($amountSql));
                if (!empty($amount)) {
                    $i = 1;
                    $lastMonth = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));
                    foreach ($amount as $am) {
                        $exist = TopSpender::select('id')->where(['userId' => $am->userId, 'month' => $stringPreviousMonth, 'cityId' => $city->cityId])->first();
                        if ($exist) {
                            $status = true;
                            $val = 0;
                            $msg = "rank alerady exist";
                        } else {
                            $dataInsert = array(
                                'month' =>  $stringPreviousMonth,
                                'position' => $i,
                                'userId' => $am->userId,
                                'amountSpent' => $am->totalAmount,
                                'cityId' => $am->cityId,
                                'createdAt' => $this->entryDate,
                                'updatedAt' => $this->entryDate,
                            );
                            $topSpenderInsert = TopSpender::create($dataInsert);
                            if ($topSpenderInsert) {
                                $status = true;
                                $val = 1;
                                $msg = "Top spenders inserted successfully";
                            }
                        }
                        $i++;
                    }
                }
            }
        }
        if ($status == true) {
            $result = array('status' => $val, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        return response()->json($result);
    }
}