<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\EventBooking;
use App\Http\Controllers\Controller;
use App\Patron;
use App\SpecialEvent;
use App\UserNotification;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class SpecialEventController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //get list of all events of this bar and also on bases of filter
    public function getSpecialEventsFees(Request $request)
    {
        $eventSetupFee = CommonFunction::getSpecialEventFeeSetup();
        if ($eventSetupFee) {
            $result = array('status' => 1, 'message' => 'Special event fees get successfully', 'data' => $eventSetupFee);
        } else {
            $result = array('status' => 0, 'message' => 'No Special event fees found');
        }

        return response()->json($result);

    }

    //get list of all events of this bar and also on bases of filter
    public function getSpecialEvents(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        extract($_POST);

        // $orderBy = 'desc';
        // if (isset($request->orderBy)) {
        //     $orderBy = $request->orderBy;
        // }

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $currentDate = date('Y-m-d');

        $sql = "SELECT se.* FROM bnSpecialEvents as se  WHERE se.barId = " . $request->barId;

        //filter conditions
        // Based on keyword
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $sql .= " AND (se.title LIKE '%$keyword%' OR se.description LIKE '%$keyword%' OR se.entryFees LIKE '%$keyword%' OR se.coverCharges LIKE '%$keyword%' OR se.totalTickets LIKE '%$keyword%')";
        }

        //Status based search
        if (isset($request->status) && $request->status != 'null') {
            if($request->status == 1 || $request->status == 0)
                $sql .= " AND se.status = " . $request->status. " AND se.endDate >= '$currentDate'";
            else 
                $sql .= " AND se.endDate < '$currentDate'";
        }

        //Date filter
        $date = date('Y-m-d', strtotime($request->date));
        if (isset($request->date) && !empty($request->date)) {
            $sql .= " AND (se.startDate = '$date' OR se.endDate = '$date')";
        }
        $sql .= " order by se.id desc limit $startFrom, $limit";

        $specialEvents = DB::select(DB::raw($sql));
        
        if (count($specialEvents) > 0) {

            //Converting NULL to "" String
            array_walk_recursive($specialEvents, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => "Special events get successfully", 'data' => $specialEvents, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        return response()->json($result);
    }
    //Get All special event with filter
    public function getAllSpecialEvents(Request $request)
    {
        extract($_POST);

        //if limit is not set then default limit is 10
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $currentDate = date('Y-m-d');
        $query= DB::table('bnSpecialEvents as se');
        $query->select('se.*','bar.barName','address.cityId');
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'se.barId');
        $query->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId');
        if (isset($request->barId) && !empty($request->barId) && $request->barId != 'null'){
            $query->where('se.barId',$request->barId);
        }
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = $request->keyword;
            $city = commonFunction::getCityIdByName($request->keyword);
            if($city){
                $query->where('address.cityId','=',  $city);
            }
            else{
                $keyword = $request->keyword;
                $query->where(function($query2) use ($keyword){
                    $query2->where('se.title','LIKE',"%{$keyword}%");
                    $query2->orWhere('se.description', 'LIKE',"%{$keyword}%");
                    $query2->orWhere('se.entryFees', 'LIKE',"%{$keyword}%");
                    $query2->orWhere('se.coverCharges', 'LIKE',"%{$keyword}%");
                    $query2->orWhere('se.totalTickets', 'LIKE',"%{$keyword}%");
                });
            }
        } 
        /*if (isset($request->status) && ($request->status == 1 || $request->status == 0) && $request->status != 'null') {
            $query->where('se.status','=', $request->status);
        }*/

        //Status based search
        if (isset($request->status) && $request->status != 'null') {
            if($request->status == 1 || $request->status == 0) {
                $query->where('se.status','=', $request->status);
                $query->where('se.endDate','>=', $currentDate);
            }
            else {
                $query->where('se.endDate','<', $currentDate);
            }
        }


        $date = date('Y-m', strtotime($request->date));
        if (isset($request->date) && !empty($request->date)) {
            $query->where(function($query3) use ($date){
                //$query3->where('se.startDate', $date);
                //$query3->orWhere('se.endDate', $date);
                $query3->where(DB::raw("(DATE_FORMAT(se.startDate,'%Y-%m'))"),$date);
                $query3->where(DB::raw("(DATE_FORMAT(se.endDate,'%Y-%m'))"),$date);
            });
        }
        $query->orderBy('se.id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $query->groupBy('se.id');
        $specialEvents = $query->get();
        if (count($specialEvents) > 0) {

            //Converting NULL to "" String
            array_walk_recursive($specialEvents, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => "Special events get successfully", 'data' => $specialEvents, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        return response()->json($result);
    }

    //get details of particular special event on bases of particular id
    public function getSpecialEventsData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        extract($_POST);

        //get user basic profile data of bar with status 1 and user type b
        $specialEventsData = SpecialEvent::select('id', 'title', 'description', 'eventImage', 'publishedOn', 'venue', 'duration', 'coverCharges', 'discountRate', 'startDate', 'endDate', 'startTime', 'endTime', 'entryType', 'entryfees', 'status', 'totalTickets', 'createdAt')
            ->where(['id' => $request->id])
            ->first();

        if ($specialEventsData) {
            //Converting NULL to "" String
            array_walk_recursive($specialEventsData, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => "Special event get successfully", 'data' => $specialEventsData);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        return response()->json($result);
    }

    //add edit special events data
    public function addEditSpecialEvents(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'title' => 'required',
            'publishedOn' => 'required',
            'duration' => 'required',
            //'description' => 'required',
            'venue' => 'required',
            'startDate' => 'required',
            //'endDate' => 'required',
            //'startTime' => 'required',
            //'endTime' => 'required',
            'entryfees' => 'required',
            //'coverCharges' => 'required',
            'totalTickets' => 'required',
            //'discountRate' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        extract($_POST);
        $today = date("Y-m-d");
        /*
        if (isset($request->id) && !empty($request->id)) {
        //check special event already exist or not on edit
        $query = SpecialEvent::select('id');
        $query->where('barId', '=', $request->barId);
        $query->where('startTime', '=', $request->startTime);
        $query->where('endTime', '=', $request->endTime);
        $query->where('id', '!=', $request->id);
        $query->whereNotIn('startTime', ['00:00:00', '00:00']);
        $query->whereNotIn('endTime', ['00:00:00', '00:00']);
        $exist = $query->first();
        } else {
        //check special event already exist or not
        $query = SpecialEvent::select('id');
        $query->where('barId', '=', $request->barId);
        $query->where('startTime', '=', $request->startTime);
        $query->where('endTime', '=', $request->endTime);
        $query->whereNotIn('startTime', ['00:00:00', '00:00']);
        $query->whereNotIn('endTime', ['00:00:00', '00:00']);
        $exist = $query->first();
        } */

        $exist = 0;
        if ($exist) {
            $result = array('status' => 0, 'message' => 'Special event already exist');
        } else {

            // Time Validation
            if(!empty($request->startTime) && !empty($request->endTime)) {
                $result = CommonFunction::eventTimingValidation($request->startTime, $request->endTime, $request->barId, $request->startDate);
                if(!empty($result) && $result['status'] == 0) {
                    return response()->json($result);
                }
            }

            //Store NULL in start/end time if not set
            if (empty($request->startTime) || $request->startTime == '' || $request->startTime == 'null') {
                $request->startTime = null;
            }

            if (empty($request->endTime) || $request->endTime == '' || $request->endTime == 'null') {
                $request->endTime = null;
            }

            /*if(empty($request->coverCharges))
                $request->coverCharges = 0;

            if(empty($request->discountRate))
                $request->discountRate = 0;*/

            $eventData = array(
                'barId' => $request->barId,
                'title' => $request->title,
                'publishedOn' => $request->publishedOn,
                //'duration' => $request->duration,
                'status' => 1,
                //'price' => CommonFunction::GetSingleField('bnSpecialEventFeeSetup', 'price', 'duration', $request->duration),
                'description' => $request->description,
                'venue' => $request->venue,
                'startDate' => $request->startDate,
                'endDate' => $request->startDate, //$request->endDate,
                'startTime' => $request->startTime,
                'endTime' => $request->endTime,
                'entryType' => 2,
                'entryfees' => $request->entryfees,
                'coverCharges' => $request->coverCharges,
                'totalTickets' => $request->totalTickets,
                'discountRate' => $request->discountRate,
            );

            //Media Uploading - From Bar Panel Mobile App
            if ($request->hasFile("eventImage") && !isset($request->base64Img)) {
                $file = $request->file("eventImage");
                $ext = $file->getClientOriginalExtension(); //get extention of file
                $newName = time() * rand() . "." . $ext;
                $upload = $file->move("uploads/events/", $newName);
                if (!empty($upload)) {
                    $eventData['eventImage'] = $newName;
                }
            }

            // Upload offer images, from base64 string. CROPPED - Web Panel
            if(!empty($request->eventImage) && isset($request->base64Img) && !$request->hasFile("eventImage")) {
                $image = $request->eventImage;  
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = time() * rand().'.'.'png';
                $upload =  \File::put("uploads/events/" . $imageName, base64_decode($image));
                if ($upload) 
                    $eventData['eventImage'] = $imageName; 
            }
 
            if (isset($request->id) && !empty($request->id)) {
                $eventData['updatedAt'] = $this->entryDate;
                //update special events
                $specialEvents = SpecialEvent::where(['id' => $request->id])->update($eventData);
                $msg = 'Special event updated successfully';
                $data['id'] = $request->id;
            } else {
                $eventData['duration'] = $request->duration;
                $eventData['price'] = CommonFunction::GetSingleField('bnSpecialEventFeeSetup', 'price', 'duration', $request->duration);

                $eventData['updatedAt'] = $this->entryDate;
                $eventData['createdAt'] = $this->entryDate;
                $specialEvents = SpecialEvent::create($eventData);
                $data['id'] = $specialEvents['id'];
                $msg = 'Special event created successfully';

                CommonFunction::sendEventNotificationToPatrons($request->barId, $request->title, $specialEvents['id']);

                // Send Notification to Patrons for this event
                /*$bar = Bar::select('barName')->where(['bnBars.id' => $request->barId])->first();
                if (!empty($bar)) {
                    $specialEventId = $specialEvents['id'];
                    $message = 'New event '.$request->title.' is going to be held at ' . ucwords(strtolower($bar->barName));

                    //Now call patrons info
                    $userData = Patron::select('bnPatrons.userId')
                        ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                        ->where(['u.status' => 1])->get();

                    if (count($userData) > 0) {
                        foreach ($userData as $uid) {

                            $record = array('moduleType' => 'specialEventCreated', 'moduleId' => $specialEventId, 'description' => $message, 'userId' => $uid->userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                            $not = UserNotification::create($record);

                            if ($not['id']) {
                                $notCreated = true;
                                //$deviceToken[] = CommonFunction::getUserDeviceData($uid->userId);
                                //$userId[] = $uid->userId;
                            }

                        }

                        //If notification created, then send push notification also
                        if (isset($notCreated)) {
                            $deviceToken = CommonFunction::getAllUserDeviceData();
                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'specialEventCreated', 'moduleId' => $specialEventId, 'title' => 'New special event', 'msg' => $message);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                // print_r($rs);exit;
                            }
                        }
                    }
                }*/
            }

            if ($specialEvents) {
                $result = array('status' => 1, 'message' => $msg, 'data' => $data);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        }
        return response()->json($result);
    }
    //get details of user book see for this event function
    public function getEventDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'specialEventId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        extract($_POST);

        $currentDate = date('Y-m-d');

        //get user basic profile data of bar with status 1 and user type b
        $eventBooking = EventBooking::select('bnEventBookings.id', 'u.userName', 'p.firstName', 'p.lastName', 'bnEventBookings.eventId', 'bnEventBookings.userId', 'bnEventBookings.totalSeats', 'bnEventBookings.amount', 'bnEventBookings.createdAt')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnEventBookings.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnEventBookings.userId')
            ->where(['bnEventBookings.eventId' => $request->specialEventId])
        // ->limit($limit)->offset($startFrom)->orderBy('bnEventBookings.id','desc')
            ->get()->toArray();

        if (count($eventBooking) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($eventBooking, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => "Event details get successfully", 'data' => $eventBooking);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        return response()->json($result);

    }
    //get list of all event booking
    public function getTicketBookingList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'eventId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $keyword = $request->keyword; 
        $query = EventBooking::select('bnEventBookings.id', 'bnEventBookings.eventId', 'bnEventBookings.userId', 'u.userName', 'p.firstName', 'p.lastName', 'u.email', 'u.phone', 'bnEventBookings.totalSeats', 'bnEventBookings.paymentStatus as bookingPaymentStatus', 'bnEventBookings.amount', 'bnEventBookings.createdAt', 'e.barId')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnEventBookings.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnEventBookings.userId')
            ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
            ->where(['bnEventBookings.eventId' => $request->eventId]);
        if (!empty($keyword)) {
            $query->where('p.firstName', 'LIKE', "%{$keyword}%");
            $query->orWhere('p.lastName', 'LIKE', "%{$keyword}%");
            $query->orWhere('u.email', 'LIKE', "%{$keyword}%");
            $query->orWhere('u.phone', 'LIKE', "%{$keyword}%");
            $query->orWhere('bnEventBookings.totalSeats', 'LIKE', "%{$keyword}%");
        }
        $query->limit($limit);
        $query->offset($startFrom);
        $query->orderBy('bnEventBookings.id', 'desc');
        $eventBooking = $query->get();
        if ($eventBooking) {

            $eventData = array();
            foreach ($eventBooking as $row) { 

                // Get the payment info
                $trData = DB::table('bnTransactions as t')
                    ->select('t.amount', 't.paymentMode', 't.transactionId', 't.paymentDate')
                    ->where(['t.moduleType' => 'event', 't.moduleId' => $row->id, 't.userId' => $row->userId])
                    ->get();

                if(!empty($trData)) {
                    $payData = array();
                    foreach ($trData as $trow) {
                        if($trow->paymentMode == 'bnwallet')
                            $trow->paymentMode = "Bar Nights Wallet";

                        $payData[] = $trow;
                    }
                    $row->paymentData = $payData;
                }

                // Get the order-visit status of the event booking
                $orderData = DB::table('bnVisits as v')
                    ->select('v.id as visitId', 'v.status as visitStatus', 'o.id as orderPid', 'o.orderId', 'o.paymentStatus', 'o.grandTotal', 'o.status as orderStatus')
                    ->leftJoin('bnOrders as o', 'o.visitId', '=', 'v.id')
                    ->where(['v.requestType' => 'event', 'v.requestId' => $row->id])
                    ->first();
                if(!empty($orderData)) {
                    $row->visitId = $orderData->visitId;
                    $row->orderId = $orderData->orderId;
                    $row->orderPid = $orderData->orderPid;
                    $row->orderStatus = $orderData->orderStatus;
                    $row->visitStatus = $orderData->visitStatus;
                    $row->paymentStatus = $orderData->paymentStatus;
                }

                $eventData[] = $row;
            }


            $result = array('status' => 1, 'message' => 'Event booking list get successfully', 'data' => $eventData, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No Event booking list  found');
        }
        return response()->json($result);
    }

}