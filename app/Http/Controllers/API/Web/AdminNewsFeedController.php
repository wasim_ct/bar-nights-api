<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use App\NewsFeed;
use DB;
use Illuminate\Http\Request;
use Validator;

class AdminNewsFeedController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Add news feed function
    public function getReportedNewsFeed(Request $request)
    {
        extract($_POST);
        //Pagination setup
        $limit = 12;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        
        //get the new of user
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.description', 'bnNewsFeeds.userId', 'bnNewsFeeds.visibility', 'bnNewsFeeds.isDeleted', 'p.firstName', 'p.lastName', 'p.profilePic', 'umf.id as mediaId', 'umf.mediaName', 'umf.mediaType', 'bnNewsFeeds.createdAt')
            ->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->where(['bnNewsFeeds.isReported' => 1])
            //->where(['bnNewsFeeds.isDeleted' => 0])
            ->where(['bnNewsFeeds.isIgnore' => 0])
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->groupBy('bnNewsFeeds.id')
            ->offset($startFrom)->limit($limit)->get()->toArray();

        //Converting NULL to "" String
        if (!empty($newsfeed)) {
            array_walk_recursive($newsfeed, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }

        if (count($newsfeed) > 0) {
            $result = array('status' => 1, 'message' => 'News feed has been received successfully', 'data' => $newsfeed, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }

    //This function is used for delete barnotification
    public function deleteReportedNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        
        $feedDeleted = NewsFeed::where(['id' => $request->id])->update(['isDeleted' => 1, 'updatedAt' => $this->entryDate]);
        if (!empty($feedDeleted)) {
            $result = array('status' => 1, 'message' => 'News feed has been deleted successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        return response()->json($result);
    }

    //This function is used for delete barnotification
    public function ignoreReportedNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        
        $feedDeleted = NewsFeed::where(['id' => $request->id])->update(['isIgnore' => 1, 'updatedAt' => $this->entryDate]);
        if (!empty($feedDeleted)) {
            $result = array('status' => 1, 'message' => 'News feed has been ignored successfully');

            //Delete list of users who reported this
            DB::table('bnNewsFeedReportUsers')->where(['newsId' => $request->id])->delete();

        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        return response()->json($result);
    }

    // Function to get all getNewsFeedReporters
    public function getNewsFeedReporters(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newsId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $newsId = $request->newsId;

        // Get the newsfeed info
        $data['newsfeed'] = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.description', 'bnNewsFeeds.userId', 'bnNewsFeeds.isDeleted', 'u.userName', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnNewsFeeds.createdAt')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnNewsFeeds.userId')
            ->where(['bnNewsFeeds.id' => $newsId])->first();

        $data['newsMedia'] = DB::table('bnUserMediaFiles as m')->select('m.id','m.mediaType','m.mediaName')
            ->where(['m.moduleId' => $newsId])->get()->toArray();

        $data['reporters'] = DB::table('bnNewsFeedReportUsers as ru')->select('ru.*', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName')
            ->join('bnPatrons as p', 'p.userId', '=', 'ru.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'ru.userId')
            ->where(['ru.newsId' => $newsId])
            ->orderBy('id', 'desc')
            ->get()->toArray();

        //Converting NULL to "" String
        if (!empty($data)) {
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }

        if (count($data) > 0) {
            $result = array('status' => 1, 'message' => 'News feed reporters received successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }

    // delete News feed media files
    public function deleteNewsFeedMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'mediaName' => 'required'
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $delete = DB::table('bnUserMediaFiles')->where(['id' => $request->id])->delete();
        if ($delete) {    
            $fileToDel = public_path("uploads/patrons/newsfeed/" . $request->mediaName);
            if (file_exists($fileToDel)) {
                unlink($fileToDel);
            }
            $result = array('status' => 1, 'message' => 'Media has been deleted successfully');
        }
        else {
            $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
        }
        return response()->json($result);
    }
}