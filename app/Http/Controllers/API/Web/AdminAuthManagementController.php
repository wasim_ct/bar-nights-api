<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use DB;
use Auth;
use Mail;
use Hash;
class AdminAuthManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }
    //This function is used for login
    public function login(Request $request){
       $validator = Validator::make($request->all(), [ 
            'userName' => 'required',
            'password' =>'required'
        ]);
        if($validator->fails()){
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        if(Auth::attempt(['email' => request('userName'), 'password' => request('password')])){
            $login = true;
        }else if(Auth::attempt(['userName' => request('userName'), 'password' => request('password'), 'userType' => 'A'])){
            $login = true;
        }
        if(isset($login)) {
            $user = Auth::user();
            if($user->status == 2) {
                $result = array('status' => 0, 'message' => 'You account is blocked');
            }else{
                $userData = array(
                    'id' => $user->id,
                    'userName' => $user->userName,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'userType' => $user->userType
                );
                $result = array('status' => 1, 'message' => 'Login successful', 'data' => $userData);
            }
            return response()->json($result);
        }else{
            $result = array('status' => 0, 'message' => 'Invalid login credentials');
            return response()->json($result);
        }
    }

    //This function is used for forgot password
    Public function forgotPassword(Request $request){
       $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
        ]);
        if($validator->fails()){
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $admin = DB::table('bnUsers')->where('email',$request->email)->get();
        if($admin && count($admin)){
            $password = mt_rand(100000,999999);
            $param = array('name'=>$admin[0]->userName, 'password'=>$password, 'email'=>$admin[0]->email);
            //$param = array('name'=>$admin[0]->userName, 'password'=>$password, 'email'=>'ravindra@coretechies.com');
            try{
                Mail::send('emails.forgotPassword', $param, function ($message) use ($param) {
                    $message->from('noreply@barnightslive.com', 'Bar Nights');
                    $message->subject('Forgot password');
                    //$message->to('ravindra@coretechies.com');
                    $message->to($param['email']);
                });
                DB::table('bnUsers')->where('email',$request->email)->update(['password'=>bcrypt($password),'updatedAt'=> date('Y-m-d H:i:s')]);
                $result = array('status' => 1, 'message' => 'New password sent to your registered email address. Please check your inbox!');
                return response()->json($result);
            }catch(\Exception $e ){
                $error = $e->getMessage();
                $result = array('status' => 0, 'message' => $error);
                return response()->json($result);
            }
        }else{
            $result = array('status' => 0, 'message' => 'Email address  does not exist!');
            return response()->json($result);
        }
    }

    /*This function is used for change password*/
    public function changePassword(Request $request){ 
       $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'oldPass' => 'required',
            'newPass' => 'required|same:confPass',
            'confPass' => 'required',
        ]);
        if($validator->fails()){
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $userId = $request->userId;
        $oldPassword = $request->oldPass;
        $newPassword = $request->newPass; 
        $userType = $request->userType; 
        $token = $request->token; 

        if($oldPassword !== $newPassword) {
            $admin = DB::table('bnUsers')->select('*')->where('id', $userId)->get();
            
            if($admin && count($admin)){
                if(Auth::attempt(['id' => $userId, 'password' => $oldPassword, 'userType' => $userType])){
                    
                    DB::table('bnUsers')->where('id',$userId)
                    ->update(['password'=>bcrypt($newPassword),'updatedAt'=>date('Y-m-d H:i:s')]);

                    // Delete the other devices token
                    DB::table('bnLoginTokens')->where(['userId' => $userId])
                    ->where('token', '!=', $token)->delete();

                    $result = array('status' => 1, 'message' => 'Password has been updated successfully');
                    return response()->json($result);
                }else{
                    $result = array('status' => 0, 'message' => 'Current password does not match');
                    return response()->json($result);
                }
            }
            else{
                $result = array('status' => 0, 'message' => 'Invalid User');
                return response()->json($result);
            }
        }
        else{
            $result = array('status' => 0, 'message' => 'New password should be different than current password');
            return response()->json($result);
        }
    }
}