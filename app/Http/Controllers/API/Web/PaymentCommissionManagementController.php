<?php
namespace App\Http\Controllers\API\Web;

use App\BarPayment;
use App\Http\Controllers\Controller;
use App\Order;
use DB;
use Illuminate\Http\Request;
use Validator;

class PaymentCommissionManagementController extends Controller
{
    //This function is used for get admin commission list by order wise
    public function adminCommissionOrderWise(Request $request)
    {
        $list = Order::adminCommissionOrderWise();
        if ($list && count($list)) {
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        } else {
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for getbar income by month wise
    public function barIncomeMonthWise(Request $request)
    {
        $list = Order::barIncomeMonthWise();
        if ($list && count($list)) {
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        } else {
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for listing paid amount of bar list
    public function paidAmountToBarList(Request $request)
    {
        $list = BarPayment::paidAmountToBarList();
        if ($list && count($list)) {
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        } else {
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for get barIncome statement
    public function barIncomeStatement(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $barId = $request->barId;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $list = Order::barIncomeStatement($barId, $startDate, $endDate);
        if ($list && count($list)) {
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        } else {
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for getbar income by month wise
    public function barIncomeWeekWise(Request $request)
    {
        $list = "SELECT curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY as startDate, curdate() - INTERVAL DAYOFWEEK(curdate()) DAY as endDate  ,sum(totalPrice) as totalAmount from bnOrders  WHERE status = 3 AND orderDate >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND orderDate < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY GROUP BY barId ORDER BY orderDate ASC";
        $listAmount = DB::select(DB::raw($list));
        if (!empty($listAmount)) {
            $result = array('status' => 1, 'message' => 'amount get successfully', 'data' => $listAmount);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }

}