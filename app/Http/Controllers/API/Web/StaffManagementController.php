<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use App\Staffs;
use DB;
use Mail;
use Hash;
use CommonFunction;

class StaffManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //This function is used for add Staff by admin
    public function addStaff(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        $password = mt_rand(100000,999999);
        //Check Email is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->email, 'email', 'S');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }
        //Check Phone is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->phone, 'phone', 'S');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }
        //inser basic bar data
        $userParam = array(
            'userName' => $request->userName, 
            'email' => $request->email, 
            'phone' => $request->phone,
            'userType' => 'S',
            'platform' => 'bn',
            'status' => 1,
            'password' => bcrypt($password)
        );
        $user = User::create($userParam);
        if ($user['id']) {
            //Inser data in Staff table
            $staff = array(
                'userId' => $user['id'],
                'name' => $request->name,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            ); 
            Staffs::create($staff);
            $emailParam = array(
                'name'=> $request->name, 
                'password'=>$password, 
                'email'=>$request->email,
                'link' => $request->loginLink
            );
            try{
                Mail::send('emails.addStaff', $emailParam, function ($message) use ($emailParam) {
                    $message->from('noreply@barnightslive.com', config('app.name'));
                    $message->subject('Login Details');
                    //$message->to('ravindra@coretechies.com');
                    $message->to($emailParam['email']);
                });
                $result['status'] = 1;
                $result['message'] = "Staff has been added successfully!";
                $result['password'] = $password;
            }catch(\Exception $e ){
                $error = $e->getMessage();
                $result['status'] = 0;
                $result['message'] = $error;
            }
        } else {
            $result['status'] = 0;
            $result['message'] = "Internal server error. Try again!";
        }
        return response()->json($result);
    }
    //This function is used for add Staff by admin
    public function editStaff(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        $password = mt_rand(100000,999999);
        //Check Email is already registerd or not or not
        $emailExist = DB::table("bnUsers")->select('id')
        ->where('id','!=',$request->id)
        ->where('email',$request->email)
        ->where('userType','S')->get();
        if (count($emailExist)) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }
        //Check Phone is already registerd or not or not
        $phoneExist = DB::table("bnUsers")->select('id')
        ->where('id','!=',$request->id)
        ->where('phone',$request->phone)
        ->where('userType','S')->get();
        if (count($phoneExist)) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }
        //inser basic bar data
        $userParam = array(
            'email' => $request->email, 
            'phone' => $request->phone,
            'updatedAt' => $this->entryDate,
        );
        if (DB::table('bnUsers')->where('id', $request->id)->update($userParam)) {
            $staff = array(
                'name' => $request->name,
                'updatedAt' => $this->entryDate,
            );
            DB::table('bnStaffs')->where('userId', $request->id)->update($staff);
            $result['status'] = 1;
            $result['message'] = "Staff data updated successfully!";
        } else {
            $result['status'] = 0;
            $result['message'] = "Internal server error.Try again!";
        }
        return response()->json($result);
    }
    public static function getStaffList(Request $request)
    {
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $keyword = $request->keyword;
        $status = $request->status;
        $query= DB::table('bnUsers as u');
        $query->select('u.id','u.userName','u.email','u.phone','u.status','s.name');
        $query->where('u.userType','S');
        $query->leftJoin('bnStaffs as s', 's.userId', '=', 'u.id');
        if(!empty($keyword)){
            $query->where(function($query2) use ($keyword){
                $query2->orWhere('s.name', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.email', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.phone', 'LIKE',"%{$keyword}%");
            });
        }
        if($status == '1' || $status == '0'){
            //echo $status;exit;
            $query->where('u.status','=',$status);
        }
        $query->orderBy('u.id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $staffList = $query->get();
        if (count( $staffList) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($staffList, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $staffList, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }
    public static function getStaffDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        $query= DB::table('bnUsers as u');
        $query->select('u.id','u.userName','u.email','u.phone','u.status','s.name');
        $query->where('u.userType','S');
        $query->where('u.id',$request->id);
        $query->leftJoin('bnStaffs as s', 's.userId', '=', 'u.id');
        $staffList = $query->get();
        if (count( $staffList) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($staffList, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $staffList[0]);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }
    public function deleteStaff(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        if(DB::table('bnStaffs')->where('userId',$request->id)->delete()){
            DB::table('bnUsers')->where('id',$request->id)->delete();
            $result = array('status' => 1, 'message' => "Staff has been deleted successfully!");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong, please try again.");
        }
        return response()->json($result);   
    }
}