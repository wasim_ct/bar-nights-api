<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\BookTable;
use App\Http\Controllers\Controller;
use App\UserNotification;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class BookTableController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //get user current visit
    public function getAllTableBooking(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $sql = "SELECT bt.id, bt.tableNumber, bt.code, bt.userId, bt.barId, bt.tokenMoney, bt.message, bt.bookingDate, bt.bookingTime, bt.totalPerson, bt.status, bt.paymentStatus, bt.cancelReason, bt.cancelBy, u.userName, u.phone, p.firstName, p.lastName, p.profilePic, b.barName FROM bnBookTable as bt LEFT JOIN bnPatrons as p on p.userId = bt.userId LEFT JOIN bnUsers as u on u.id = p.userId LEFT JOIN bnBars as b on b.id = bt.barId WHERE bt.id != 0";

        /*====Filter Conditions Start====*/
        // Bar based search
        if (isset($request->barId) && !empty($request->barId)) {
            $sql .= " AND bt.barId = " . $request->barId;
        }

        // Status based search
        if ($request->status == '0' || $request->status == '1' || $request->status == '2') {
            $sql .= " AND bt.status = " . $request->status;
        }

        //Status based search
        if ($request->paymentStatus  == '0' || $request->paymentStatus == '1') {
            $sql .= " AND bt.paymentStatus = " . $request->paymentStatus;
        }

        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = $request->keyword;
            $sql .= " AND (bt.message LIKE '%$keyword%' OR bt.totalPerson LIKE '%$keyword%' OR u.userName LIKE '%$keyword%' OR p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR b.barName LIKE '%$keyword%')";
        }

        $sql .= " order by bt.id desc limit $startFrom, $limit";

        $tables = DB::select(DB::raw($sql));

        if (!empty($tables)) {
            $bookings = array();
            foreach ($tables as $row) { 

                // Expire the Request if 30 mins passed from booking date-time
                //$time = $row->bookingDate.' '.$row->bookingTime;
                $update = CommonFunction::updateTableBookingExpireStatus($row->id, $row->status, $row->bookingDate, $row->bookingTime, $row->paymentStatus);
                if($update) {
                    $row->status = 3;
                }
            
                // Get the order-visit status of the table booking
                $orderData = DB::table('bnVisits as v')
                    ->select('v.id as visitId', 'v.status as visitStatus', 'o.id as orderPid', 'o.orderId', 'o.paymentStatus', 'o.grandTotal', 'o.status as orderStatus')
                    ->leftJoin('bnOrders as o', 'o.visitId', '=', 'v.id')
                    ->where(['v.requestType' => 'table', 'v.requestId' => $row->id])
                    ->first();
                if(!empty($orderData)) {
                    $row->visitId = $orderData->visitId;
                    $row->orderId = $orderData->orderId;
                    $row->orderPid = $orderData->orderPid;
                    $row->orderStatus = $orderData->orderStatus;
                    $row->visitStatus = $orderData->visitStatus;
                    $row->orderPaymentStatus = $orderData->paymentStatus;
                }

                // Get the advance payment info
                $trData = DB::table('bnTransactions as t')
                    ->select('t.amount', 't.paymentMode', 't.transactionId', 't.paymentDate')
                    ->where(['t.moduleType' => 'table', 't.moduleId' => $row->id])
                    ->get();

                if(!empty($trData)) {
                    //$row->paymentData = $trData;
                    $payData = array();
                    foreach ($trData as $trow) {
                        if($trow->paymentMode == 'bnwallet')
                            $trow->paymentMode = "Bar Nights Wallet";

                        $payData[] = $trow;
                    }
                    $row->paymentData = $payData;
                }

                $bookings[] = $row;
            }

            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $bookings);
        } else {
            $result = array('status' => 0, 'message' => "No more record");
        }

        return response()->json($result);
    }

    //change booking table status
    public function changeBookTableStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
            //'tableNumber' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $id = $request->id;
        $status = $request->status;

        //get table book data from db
        $tableBookingData = BookTable::select('*')->where('id', $id)->first();
        if (!empty($tableBookingData)) 
        {
            $tableNo = $request->tableNo;
            //create status msg according to status
            if ($status == 1) {
                $smsg = 'accepted';
                $title = 'Booking accepted';
                //create table no msg
                $tableMsg = 'and your table no is ' . $tableNo;
                $tableNo = $tableNo;
                $cancelReason = NULL;
            }
            else if ($status == 2) {
                $smsg = 'rejected';
                $title = 'Booking rejected';
                $tableMsg = '';
                $tableNo = null;
                if(isset($request->reason))
                    $cancelReason = $request->reason;
                else
                    $cancelReason = NULL;
            }

            //update data in db
            $update = array('status' => $status, 'tableNumber' => $tableNo, 'cancelReason' => $cancelReason, 'updatedAt' => $this->entryDate);
            $changeTableBookStatus = BookTable::where('id', $id)->update($update);

            if ($changeTableBookStatus) 
            {
                //Create an entry in notification table and send PUSH Notification
                $userId = $tableBookingData->userId;
                $bar = Bar::select('bnBars.barName', 'bnBars.logo', 'a.address')
                    ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
                    ->where(['bnBars.id' => $tableBookingData->barId])->first();
                if (!empty($bar)) {
                    $message = 'Your request has ' . $smsg . ' from ' . ucwords(strtolower($bar->barName)) . ' ' . $tableMsg;
                    $record = array('moduleType' => 'bookTable', 'moduleId' => $id, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $not = UserNotification::create($record);
 
                    if ($not['id']) {
                        $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens
                        if ($deviceToken) {
                            //Firebase Notification
                            $payLoadData = array('moduleType' => 'changeBookTableStatus', 'moduleId' => $id, 'userId' => $userId, 'title' => $title, 'msg' => $message, 'barId' => $tableBookingData->barId, 'barName' => $bar->barName, 'address' => $bar->address, 'logo' => $bar->logo);
                            $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                        }
                    }
                }
                $result = array('status' => 1, 'message' => "Table booking status updated successfully");
            } else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }
        }
        return response()->json($result);
    }

    // Function to send reminder on accept bookings
    public function sendBookingReminder(Request $request)
    {
        $today = date("Y-m-d");
        $sql = "SELECT bt.id, bt.barId, bt.userId, bt.bookingDate, bt.bookingTime, b.barName, b.logo, a.address, u.email, p.firstName, p.lastName FROM bnBookTable as bt LEFT JOIN bnBars as b on b.id = bt.barId LEFT JOIN bnAddresses as a on a.userId = b.userId LEFT JOIN bnUsers as u on u.id = bt.userId LEFT JOIN bnPatrons as p on p.userId = bt.userId WHERE bt.status = 1 AND bt.paymentStatus = 0 AND bt.bookingDate = '$today' order by bt.id";
        $bookings = DB::select(DB::raw($sql));

        if (!empty($bookings)) {
            foreach ($bookings as $row) { 
                //Create an entry in notification table and send PUSH Notification
                $userId = $row->userId;
                $id = $row->id;
                $barId = $row->barId;
                $barName = ucwords(strtolower($row->barName));

                $message = 'You have a due payment for table booking at ' . $barName;
                $title = "Table booking due payment";

                // Send Email Notification
                //create html message
                $msg = '<h1 class="title">Hello ' . $row->firstName .' '.$row->lastName. '</h1>
                        <p>'.$message.'</p>
                        <p>Please visit Bar Nights app and make the advance payment for table booking confirmation.</p>';
                $data = array(
                    'msg' => $msg,
                    'subject' => "Table Booking Payment Due",
                );

                Mail::to($row->email)->send(new SendMail($data));

                // Send Push Notificaiton
                $record = array('moduleType' => 'bookTable', 'moduleId' => $id, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                $not = UserNotification::create($record);

                if ($not['id']) {
                    $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens
                    if ($deviceToken) {
                        //Firebase Notification
                        $payLoadData = array('moduleType' => 'tableBookingDue', 'moduleId' => $id, 'userId' => $userId, 'title' => $title, 'msg' => $message, 'barId' => $barId, 'barName' => $row->barName, 'address' => $row->address, 'logo' => $row->logo);
                        $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                    }
                }
            }
        }
    }
}