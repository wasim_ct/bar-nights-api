<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use DB;
class SpecialEventFeeSetupManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }
    //This function is used for add new fee setup
    public function addFee(Request $request){
        $validator = Validator::make($request->all(), [
            'duration' => 'required|integer|unique:bnSpecialEventFeeSetup',
            'price' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }

        $param = array(
            'duration' => $request->duration,
            'price' => $request->price,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s')
        );
        if(DB::table('bnSpecialEventFeeSetup')->insert($param)){
            $result['status'] = 1;
            $result['message'] = "Event price added successfully!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Internal server error!";
            return response()->json($result);
        }
        
    }
    //This function is used for edit fee setup
    public function editFee(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'duration' => 'required|integer|unique:bnSpecialEventFeeSetup,duration,'.$request->id,
            'price' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }

        $param = array(
            'duration' => $request->duration,
            'price' => $request->price,
            'updatedAt' => date('Y-m-d H:i:s')
        );
        if(DB::table('bnSpecialEventFeeSetup')->where('id',$request->id)->update($param)){
            $result['status'] = 1;
            $result['message'] = "Event price updated successfully!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Internal server error!";
            return response()->json($result);
        }
    }

    public function getFeeDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        $list = DB::table('bnSpecialEventFeeSetup')->select('id','duration','price')->where('id',$request->id)->get();
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list[0];
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    public function getAllList(Request $request)
    {
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $query = DB::table('bnSpecialEventFeeSetup')->select('id','duration','price');
        $query->orderBy('id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $list = $query->get();
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            //$result['data']['durationType'] = 'week';
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    public function deletefee(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        if(DB::table('bnSpecialEventFeeSetup')->where('id',$request->id)->delete()){
            $result = array('status' => 1, 'message' => "Event price has been deleted successfully!");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong, please try again.");
        }
        return response()->json($result);   
    }
}