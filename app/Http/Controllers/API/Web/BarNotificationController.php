<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Validator;
use CommonFunction;

class BarNotificationController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }
    //This function is used for get bar notification
    public function getBarNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        $token = $request->token;
            
        // To check Admin-Staff token is exist or not
        if(isset($request->userId)) {
            $userId = $request->userId;
            $exist = DB::table('bnLoginTokens')->select('id')->where(['token' =>$token, 'userId' => $userId])->first();
            if(empty($exist)) {
                $result = array('status' => 100, 'message' => "Logged out");
                return response()->json($result);
            }
        }
        else {
            // first will check bar token is exist or not
            $userId = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $request->barId);
            $exist = DB::table('bnLoginTokens')->select('id')->where(['token' =>$token, 'userId' => $userId])->first();
            if(empty($exist)) {
                $result = array('status' => 100, 'message' => "Logged out");
                return response()->json($result);
            }

            // Notification calling start
            if(isset($request->limit))
                $limit = $request->limit;
            else
                $limit = 10;

            $startFrom = 0;
            if (isset($request->page)) {
                $startFrom = $request->page * $limit;
            }

            $dateTime = '';
            if(isset($request->lastCallTime) && !empty($request->lastCallTime)) {
                $dateTime = $request->lastCallTime;
            }

            $list = DB::table('bnBarNotifications as b')->select('*')
                    ->where('b.barId', $request->barId)
                    ->where(function($query) use ($dateTime) {
                        if (!empty($dateTime)) 
                            $query->where('b.createdAt', '>', $dateTime);
                    })
                    ->orderBy('b.id', 'desc')
                    ->offset($startFrom)
                    ->limit($limit)
                    ->get();
            if ($list && count($list)) 
            {
                foreach ($list as $key => $value) 
                {
                    $moduleType = $value->moduleType;
                    if ($moduleType == 'order' || $moduleType == 'orderPayment') {
                        $order = DB::table('bnOrders')->select('orderId', 'orderType')->where('id', $value->moduleId)->get();
                        if ($order && count($order)) {
                            $orderType = strtolower($order[0]->orderType);
                            $list[$key]->orderId = $order[0]->orderId;
                            
                            if($moduleType == 'order')
                                $list[$key]->orderType = $orderType;
                            else
                                $list[$key]->orderType = $moduleType; //orderPayment
                        }
                    } 
                    else {
                        $list[$key]->orderId = $value->moduleId;
                        $list[$key]->orderType = $value->moduleType;
                    }
                }
                $result['status'] = 1;
                $result['data'] = $list;
                $result['limit'] = $limit;
                $result['message'] = "Data found!";
                $result['lastCallTime'] = date("Y-m-d H:i:s");
            } else {
                $result['status'] = 0;
                $result['message'] = "Data not found!";
            }
            return response()->json($result);
        }
    }

    //This function is used for delete barnotification
    public function deleteBarNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "id field is required!");
            return response()->json($result);
        }
        if (DB::table('bnBarNotifications')->where('id', $request->id)->delete()) {
            $result['status'] = 1;
            $result['message'] = "Notification deleted successfully!";
        } else {
            $result['status'] = 0;
            $result['message'] = "Something went wrong, please try again later";
        }
        return response()->json($result);
    }

    //This function is used for delete all notification of bar
    public function deleteBarAllNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        if (DB::table('bnBarNotifications')->where('barId', $request->barId)->delete()) {
            $result['status'] = 1;
            $result['message'] = "Notifications deleted successfully";
        } else {
            $result['status'] = 0;
            $result['message'] = "Something went wrong, please try again later";
        }
        return response()->json($result);
    }
}