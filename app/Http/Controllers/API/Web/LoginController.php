<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\UserNotification;
use App\Http\Controllers\Controller;
use CommonFunction;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Staffs;
use DB;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public $successStatus = 200;
    use SendsPasswordResetEmails;
    use ResetsPasswords;
    public $token;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //$this->middleware('guest');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }

    protected function broker()
    {
        return Password::broker('bars');
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function barLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userName' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        if (Auth::attempt(['email' => request('userName'), 'password' => request('password'), 'userName' => NULL])) {
            $login = true;
        } 
        else if (Auth::attempt(['phone' => request('userName'), 'password' => request('password'), 'userName' => NULL])) {
            $login = true;
        }
        /*else if (Auth::attempt(['userName' => request('userName'), 'password' => request('password'), 'userName' => NULL])) {
            $login = true;
        }*/

        if (isset($login)) {
            $user = Auth::user();
            if ($user->userType == 'B' || $user->userType == 'A' || $user->userType == 'S') {
                if ($user->status == 2) {
                    $result = array('status' => 0, 'message' => 'Your account is blocked');
                } 
                else {
                    // Creating the Token
                    $token = CommonFunction::createLoginToken($user->id); 
                    if($token) {
                        if ($user->userType == 'B') {
                            if (isset($deviceId) && isset($deviceToken) && isset($osVersion) && isset($modelName)) {
                                //Update the device Data of the User
                                $data = array('deviceId' => $deviceId, 'deviceToken' => $deviceToken, 'osVersion' => $osVersion, 'modelName' => $modelName, 'loginStatus' => 1);
                                CommonFunction::updateUserDevice($data, $user->id);
                            }
                            $userData = Bar::select('id', 'userId', 'type', 'barName', 'about', 'logo')->where('userId', $user->id)->first();

                            $userData['userName'] = $user->userName;
                            $userData['userType'] = $user->userType;
                            //Converting NULL to "" String
                            array_walk_recursive($userData, function (&$item, $key) {
                                $item = null === $item ? '' : $item;
                            });

                        } else {
                            $userData = array('userId' => $user->id, 'userName' => $user->userName, 'userType' => $user->userType);
                        }

                        $userData['token'] = $token;
                        $mediaPath = CommonFunction::getMediaPath();
                        $result = array('status' => 1, 'message' => 'Login successful', 'data' => $userData, 'mediaPath' => $mediaPath);
                    }
                    else {
                        $result = array('status' => 0, 'message' => 'Invalid token');
                    }
                }
            } else {
                $result = array('status' => 0, 'message' => 'No account found');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Invalid login credentials');
        }

        return response()->json($result);
    }

    // Logout function
    public function logout(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            //'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        if(isset($request->barId)) {
            $barId = $request->barId;
            $userId = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $barId);
        }
        else if(isset($request->userId)) {
            $userId = $request->userId;
        }

        if($userId) {
            DB::table('bnUserDevices')->where(['userId' => $userId])->delete();

            // Delete the token also
            if(isset($request->token) && !empty($request->token)) {
                DB::table('bnLoginTokens')->where(['token' =>$request->token, 'userId' => $userId])->delete();
            }

            $result = array('status' => 1, 'message' => 'Logout Successful');
        }
        else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }
        return response()->json($result);

        /*$request->bar()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);*/
    }
}