<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\User;
use App\Visit;
use DB;
use CommonFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Exporter;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public $successStatus = 200;
    private $entryDate;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Funciton to load data in graph
    public function loadGraph(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'time' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }
        $status = false;
        //BarID in the condition is pending

        $result = array(
            'status' => 0,
            'data' => '',
            'message' => 'Something went wrong',
        );

        $barId = $request->barId;
        $time = strtolower($request->time);
 
        if ($time == 'weekly') {
            $sql1 = "SELECT d.orderDate, sum(t.amount) AS total
            from (select DATE(curdate()) as orderDate union all
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join";

            $sql = $sql1 . ' bnTransactions t on t.paymentDate = d.orderDate and t.status = 1 AND t.barId = ' . $barId . ' GROUP BY d.orderDate ORDER BY d.orderDate asc';
            $sales = DB::select(DB::raw($sql));

            if (count($sales) > 0 || $sales != null) {
                $weeklySales = array();
                foreach ($sales as $os) {
                    $date = date('d-m-Y', strtotime($os->orderDate));
                    $weeklySales[] = array('key' => $date, 'value' => round($os->total, 2));
                }
                $data = $weeklySales;
                $status = true;
            }
        }
        //get total sales of orders according to month
        else if ($time == 'monthly') {
            $sql2 = 'SELECT
            SUM(IF(month = \'Jan\', round(total,2), 0)) AS \'Jan\',
            SUM(IF(month = \'Feb\', round(total,2), 0)) AS \'Feb\',
            SUM(IF(month = \'Mar\', round(total,2), 0)) AS \'Mar\',
            SUM(IF(month = \'Apr\', round(total,2), 0)) AS \'Apr\',
            SUM(IF(month = \'May\', round(total,2), 0)) AS \'May\',
            SUM(IF(month = \'Jun\', round(total,2), 0)) AS \'Jun\',
            SUM(IF(month = \'Jul\', round(total,2), 0)) AS \'Jul\',
            SUM(IF(month = \'Aug\', round(total,2), 0)) AS \'Aug\',
            SUM(IF(month = \'Sep\', round(total,2), 0)) AS \'Sep\',
            SUM(IF(month = \'Oct\', round(total,2), 0)) AS \'Oct\',
            SUM(IF(month = \'Nov\', round(total,2), 0)) AS \'Nov\',
            SUM(IF(month = \'Dec\', round(total,2), 0)) AS \'Dec\'
            FROM (
            SELECT DATE_FORMAT(t.paymentDate, "%b") AS month, sum(t.amount) as total
            FROM bnTransactions t';

            $sql = $sql2 . ' WHERE t.paymentDate <= NOW() AND t.barId = ' . $barId . ' AND t.status = 1 AND t.paymentDate >= Date_add(Now(),interval - 12 month) GROUP BY DATE_FORMAT(t.paymentDate, "%m-%Y")) as sub';

            $orderSale = DB::select(DB::raw($sql));
            $monthlySales = array();
            foreach ($orderSale[0] as $key => $value) {
                $monthlySales[] = array('key' => $key, 'value' => round($value, 2));
            }
               
            if (count($monthlySales) > 0) {
                $data = $monthlySales;
                $status = true;
            }
        }

        //get total no of sales according to year
        else if ($time == 'yearly') {
            $sql = "SELECT YEAR(t.paymentDate) AS year, SUM(t.amount) AS totalSale FROM bnTransactions t where t.status = 1 AND t.barId = $barId GROUP BY YEAR(t.paymentDate)";
            $orderSale = DB::select(DB::raw($sql));

            $year = [];
            $i = 0;
            $yearlySales = array();
            foreach ($orderSale as $res) {
                $yearlySales[] = array('key' => $res->year, 'value' => round($res->totalSale, 2));
            }

            if (count($yearlySales) > 0) {
                $data = $yearlySales;
                $status = true;
            }
        }

        if ($status == true) {
            $result = array('status' => 1, 'data' => $data, 'message' => 'Report has been get successfully');
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }

    //Funciton to change the status of Day on/off, also send daily report to admin and bar
    public function sendDailyReport(Request $request)
    {
        //echo config('app.front_url');exit;
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'dayOnOffStatus' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }
        //print_r($request->all());exit;

        // First create an entry in bnBarOnOffTime, if not exist
        $timing = DB::table('bnBarOnOffTime')->select('id', 'onDateTime', 'offDateTime')->where(['barId' => $request->barId])->first();
        if(empty($timing)) {
            $param = array('barId' => $request->barId, 'onDateTime' => NULL, 'offDateTime' => NULL, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            DB::table('bnBarOnOffTime')->insertGetId($param);
        }

        // Day On Working
        if ($request->dayOnOffStatus == 1) {

            $lastOffTime = CommonFunction::GetSingleField('bnBarOnOffTime', 'offDateTime', 'barId', $request->barId);
            if($lastOffTime) {
                $today = date("Y-m-d");
                $offDate = date("Y-m-d", strtotime($lastOffTime));
                $offTime = strtotime(date("H:i", strtotime($lastOffTime)));
                $currentTime = strtotime(date("H:i"));
                $maxTime = strtotime("5:00"); 

                if($offDate == $today && $offTime >= $maxTime) {
                    $notAllwed = true;
                }
                else if($currentTime < $maxTime) {
                    $notAllwed = true;
                }

                if(isset($notAllwed)) {
                    $result = array('status' => 0, 'message' => "You can't start the business today. Try it after 5 AM tomorrow");
                    return response()->json($result);
                }
            }

            $data = array('dayOnOffStatus' => 1, 'openingStatus' => 1, 'updatedAt' => date("Y-m-d H:i:s"));
            $update = Bar::where('id', $request->barId)->update($data);
            if ($update) {
                $result = array('status' => 1, 'message' => 'Business Day on!');

                //Update the time in record table
                $record = array('onDateTime' => $this->entryDate, 'updatedAt' => $this->entryDate);
                $rs = DB::table('bnBarOnOffTime')->where('barId', $request->barId)->update($record); 

                /*===Send Email to Admin===*/ 
                $barEmail = Bar::select('u.email', 'bnBars.barName')->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
                    ->where(['bnBars.id' => $request->barId, 'u.userType' => 'B'])->first();
                if (!empty($barEmail)) { 
                    $barName = $barEmail->barName;
                    $adminEmail = User::select('email')->where(['userType' => 'A'])->first();
                    if (!empty($adminEmail)) {
                        
                        //create message
                        $msg = '<h1 class="title">Hello Admin</h1>
                                <p><b>'.$barName.'</b> is opened for business.</p>';
                        $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                        $data = array(
                            'msg' => $msg,
                            'subject' => "Business Day Opened",
                        );
                        Mail::to($adminEmail)->send(new SendMail($data));
                    }
                }
            } 
            else {
                $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
            }
        } 
        else //Day Off
        {
            $activeVisit = Visit::select('id')
                ->where(['barId' => $request->barId, 'status' => 2])->first();
            
            if(empty($activeVisit)) 
            {
                $data = array('dayOnOffStatus' => 0, 'openingStatus' => 0, 'updatedAt' => date("Y-m-d H:i:s"));
                $update = Bar::where('id', $request->barId)->update($data);
                if ($update) {
                    //Update the time in record table
                    $record = array('offDateTime' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $rs = DB::table('bnBarOnOffTime')->where('barId', $request->barId)->update($record); 

                    //Get the day on/off time, to find the sell between that time
                    if(!empty($timing)) {
                        $onTime = $timing->onDateTime;
                        $offTime = $this->entryDate;  //$timing->offDateTime;
                        
                        //Now get today order and event booking total to send the mail
                        $today = date('Y-m-d');
                        $ototal = 0;
                        $setotal = 0;

                        // Getting total sales
                        $orderSql = "SELECT sum(amount) as total FROM bnTransactions WHERE barId = " . $request->barId . " and status = 1 AND createdAt >= '$onTime' AND createdAt <= '$offTime'";
                        $orderSale = DB::select(DB::raw($orderSql));
                        
                        if (!empty($orderSale)) {
                            $ototal = $orderSale[0]->total;

                            if(empty($ototal))
                                $ototal = 0;

                            /*===Send Email to Bar===*/
                            $barEmail = Bar::select('u.email', 'bnBars.barName')->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
                                ->where(['bnBars.id' => $request->barId, 'u.userType' => 'B'])->first();
                            if (!empty($barEmail)) {
                                $barName = $barEmail->barName;
                                //create message
                                $msg = '<h1 class="title">Hello ' . $barName . '</h1>
                                        <p>Sales summary for the day as follows:</p>
                                        <p><b>Total Sales:</b> Rs.' . $ototal . '</p>';
                                /*if ($setotal != 0) {
                                    $msg .= '<p>Total Events Ticket Booking: Rs.' . $setotal . '</p>';
                                }*/

                                $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                                $data = array(
                                    'msg' => $msg,
                                    'subject' => "Daily Sales Report",
                                );
 
                                Mail::to($barEmail)->send(new SendMail($data));
                            
                                /*===Send Email to Admin===*/
                                $adminEmail = User::select('email')->where(['userType' => 'A'])->first();
                                if (!empty($adminEmail)) {
                                    
                                    //create message
                                    $msg = '<h1 class="title">Hello Admin</h1>
                                            <p>Sales summary of <b>'.$barName.'</b> for the day as follows</p>
                                            <p><b>Total Sales:</b> Rs.' . $ototal. '</p>';
                                    /*if ($setotal != 0) {
                                        $msg .= '<p>Total Events Ticket Booking:-Rs.' . $setotal . '</p>';
                                    }*/
                                    $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                                    $data = array(
                                        'msg' => $msg,
                                        'subject' => "Daily Sales Report",
                                    );
                                    Mail::to($adminEmail)->send(new SendMail($data));
                                }
                            }
                        }
                    }
                    $result = array('status' => 1, 'message' => 'Day off! Daily sales report has been sent to your email.');
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
                }
            }
            else 
                $result = array('status' => 0, 'message' => 'Please close the active orders first');

        }
        return response()->json($result);
    }

    //Funciton to load data in graph
    public function loadGraphApp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'time' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }

        //BarID in the condition is pending
        $status = false;

        $result = array(
            'status' => 0,
            'data' => '',
            'message' => 'Something went wrong',
        );

        $barId = $request->barId;
        $time = strtolower($request->time);

        $name='Report.xlsx';
        $salesReport = public_path("excel/" . $name);
        if (file_exists($salesReport)) {
            unlink($salesReport);
        }

        //get total sales according to week
        /*if ($time == 'weekly') {
            $sql1 = "SELECT d.orderDate, sum(grandTotal) AS total
            from (select DATE(curdate()) as orderDate union all
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join";

            $sql = $sql1 . ' bnOrders o on o.orderDate = d.orderDate and o.status = 3 AND o.barId = ' . $barId . ' GROUP BY d.orderDate ORDER BY d.orderDate asc';
            $orderSale = DB::select(DB::raw($sql));
            $orderSales[] = array('key'=>'Date', 'value' => 'Amount');
            if (count($orderSale) > 0 || $orderSale != null) {
                foreach ($orderSale as $os) {
                    $date = date('d-m-Y', strtotime($os->orderDate));
                    $orderSales[] = array('key' => $date, 'value' => round($os->total, 2));
                }
                //$data = array('orderSale' => $orderSales);
                $data = $orderSales;
                $status = true;
            }
        }
        //get total sales of orders according to month
        else if ($time == 'monthly') {
            $sql2 = 'SELECT
            SUM(IF(month = \'Jan\', round(total,2), 0)) AS \'Jan\',
            SUM(IF(month = \'Feb\', round(total,2), 0)) AS \'Feb\',
            SUM(IF(month = \'Mar\', round(total,2), 0)) AS \'Mar\',
            SUM(IF(month = \'Apr\', round(total,2), 0)) AS \'Apr\',
            SUM(IF(month = \'May\', round(total,2), 0)) AS \'May\',
            SUM(IF(month = \'Jun\', round(total,2), 0)) AS \'Jun\',
            SUM(IF(month = \'Jul\', round(total,2), 0)) AS \'Jul\',
            SUM(IF(month = \'Aug\', round(total,2), 0)) AS \'Aug\',
            SUM(IF(month = \'Sep\', round(total,2), 0)) AS \'Sep\',
            SUM(IF(month = \'Oct\', round(total,2), 0)) AS \'Oct\',
            SUM(IF(month = \'Nov\', round(total,2), 0)) AS \'Nov\',
            SUM(IF(month = \'Dec\', round(total,2), 0)) AS \'Dec\'
            FROM (
            SELECT DATE_FORMAT(orderDate, "%b") AS month, sum(grandTotal) as total
            FROM bnOrders';

            $sql = $sql2 . ' WHERE orderDate <= NOW() AND barId = ' . $barId . ' AND status = 3 AND orderDate >= Date_add(Now(),interval - 12 month) GROUP BY DATE_FORMAT(orderDate, "%m-%Y")) as sub';

            $orderSale = DB::select(DB::raw($sql));
            $monthlySales = array();
            $monthlySales[] = array('key'=>'Month', 'value' => 'Amount');
            // if ($orderSale[0]->Jan == null || $orderSale[0]->Jan == 0) {
            foreach ($orderSale[0] as $key => $value) {
                //echo $key."\n";
                //$orderSale[0]->$key == 0;
                $monthlySales[] = array('key' => $key, 'value' => round($value, 2));
            }
            //}
            //print_r($monthlySales);exit;

            if (count($monthlySales) > 0) {
                //$data = array('orderSale' => $orderSale, 'time' => 'month', 'heading' => 'Todays Sale', 'side' => 'Orders', 'down' => 'Orders');
                $data = $monthlySales;
                $status = true;
            }
        }

        //get total no of sales according to year
        else if ($time == 'yearly') {

            $sql = "SELECT YEAR(orderDate) AS year, SUM(grandTotal) AS totalSale FROM bnOrders where status = 3 AND barId = $barId GROUP BY YEAR(orderDate)";
            $orderSale = DB::select(DB::raw($sql));

            $year = [];
            $i = 0;
            $yearlySales = array();
            $yearlySales[] = array('key'=>'Year', 'value' => 'Amount');
            foreach ($orderSale as $res) {
                $yearlySales[] = array('key' => $res->year, 'value' => round($res->totalSale, 2));
            }

            if (count($yearlySales) > 0) {
                $data = $yearlySales;
                $status = true;
            }
        }*/

        if ($time == 'weekly') {
            $sql1 = "SELECT d.orderDate, sum(t.amount) AS total
            from (select DATE(curdate()) as orderDate union all
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join";

            $sql = $sql1 . ' bnTransactions t on t.paymentDate = d.orderDate and t.status = 1 AND t.barId = ' . $barId . ' GROUP BY d.orderDate ORDER BY d.orderDate asc';
            $sales = DB::select(DB::raw($sql));

            $weeklySales[] = array('key'=>'Date', 'value' => 'Amount');
            if (count($sales) > 0 || $sales != null) {
                $weeklySales = array();
                foreach ($sales as $os) {
                    $date = date('d-m-Y', strtotime($os->orderDate));
                    $weeklySales[] = array('key' => $date, 'value' => round($os->total, 2));
                }
                $data = $weeklySales;
                $status = true;
            }
        }
        //get total sales of orders according to month
        else if ($time == 'monthly') {
            $sql2 = 'SELECT
            SUM(IF(month = \'Jan\', round(total,2), 0)) AS \'Jan\',
            SUM(IF(month = \'Feb\', round(total,2), 0)) AS \'Feb\',
            SUM(IF(month = \'Mar\', round(total,2), 0)) AS \'Mar\',
            SUM(IF(month = \'Apr\', round(total,2), 0)) AS \'Apr\',
            SUM(IF(month = \'May\', round(total,2), 0)) AS \'May\',
            SUM(IF(month = \'Jun\', round(total,2), 0)) AS \'Jun\',
            SUM(IF(month = \'Jul\', round(total,2), 0)) AS \'Jul\',
            SUM(IF(month = \'Aug\', round(total,2), 0)) AS \'Aug\',
            SUM(IF(month = \'Sep\', round(total,2), 0)) AS \'Sep\',
            SUM(IF(month = \'Oct\', round(total,2), 0)) AS \'Oct\',
            SUM(IF(month = \'Nov\', round(total,2), 0)) AS \'Nov\',
            SUM(IF(month = \'Dec\', round(total,2), 0)) AS \'Dec\'
            FROM (
            SELECT DATE_FORMAT(t.paymentDate, "%b") AS month, sum(t.amount) as total
            FROM bnTransactions t';

            $sql = $sql2 . ' WHERE t.paymentDate <= NOW() AND t.barId = ' . $barId . ' AND t.status = 1 AND t.paymentDate >= Date_add(Now(),interval - 12 month) GROUP BY DATE_FORMAT(t.paymentDate, "%m-%Y")) as sub';

            $orderSale = DB::select(DB::raw($sql));
            
            $monthlySales = array();
            $monthlySales[] = array('key'=>'Month', 'value' => 'Amount');
            foreach ($orderSale[0] as $key => $value) {
                $monthlySales[] = array('key' => $key, 'value' => round($value, 2));
            }
               
            if (count($monthlySales) > 0) {
                $data = $monthlySales;
                $status = true;
            }
        }

        //get total no of sales according to year
        else if ($time == 'yearly') {
            $sql = "SELECT YEAR(t.paymentDate) AS year, SUM(t.amount) AS totalSale FROM bnTransactions t where t.status = 1 AND t.barId = $barId GROUP BY YEAR(t.paymentDate)";
            $orderSale = DB::select(DB::raw($sql));

            $year = [];
            $i = 0;
            $yearlySales = array();
            $yearlySales[] = array('key'=>'Year', 'value' => 'Amount');
            foreach ($orderSale as $res) {
                $yearlySales[] = array('key' => $res->year, 'value' => round($res->totalSale, 2));
            }

            if (count($yearlySales) > 0) {
                $data = $yearlySales;
                $status = true;
            }
        }

        if ($status==true) {
            $reportExcel = collect($data);
            $fileName = 'Report.xlsx';
            $file_path = public_path("excel/" . $fileName); //asset('public/download/'.$fileName);

            $excel = Exporter::make('Excel');

            $excel->load($reportExcel);
            //return $excel->stream($fileName); //To Download directly
            $rs = $excel->save($file_path);

            $fileurl = asset("public/excel/" . $fileName);
                unset($data[0]);  
                // Re-index the array elements 
                $report = array_values($data);

            $result = array('status' => 1, 'data' => $report, 'message' => 'Report has been get successfully','url' => $fileurl);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
    }

    //Funciton to change the status of Day on/off, also send daily report to admin and bar
    public function closeBarBusinessDay(Request $request)
    {
        // Load all the bars with day ON Status
        $bars = DB::table('bnBars as b')->select('b.id as barId', 'b.barName', 'u.email', 't.onDateTime', 't.offDateTime')
            ->join('bnUsers as u', 'u.id', '=', 'b.userId')
            ->leftJoin('bnBarOnOffTime as t', 't.barId', '=', 'b.id')
            ->where(['dayOnOffStatus' => 1])->get();
        if(count($bars)) 
        {
            foreach ($bars as $row) {
                $barId = $row->barId;

                // First create an entry in bnBarOnOffTime, if not exist
                $timing = DB::table('bnBarOnOffTime')->select('id', 'onDateTime', 'offDateTime')->where(['barId' => $barId])->first();
                if(empty($timing)) {
                    $param = array('barId' => $barId, 'onDateTime' => NULL, 'offDateTime' => NULL, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    DB::table('bnBarOnOffTime')->insertGetId($param);
                }
            
                $param = array('dayOnOffStatus' => 0, 'openingStatus' => 0, 'updatedAt' => $this->entryDate);
                $update = Bar::where('id', $barId)->update($param);
                if ($update) {
                    //Update the time in record table
                    $record = array('offDateTime' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $rs = DB::table('bnBarOnOffTime')->where('barId', $barId)->update($record); 

                    //Get the day on/off time, to find the sell between that time
                    if(!empty($timing)) {
                        $onTime = $timing->onDateTime;
                        $offTime = $this->entryDate;
                        
                        //Now get today order and event booking total to send the mail
                        $today = date('Y-m-d');
                        $ototal = 0;
                        $setotal = 0;

                        // Getting total sales
                        $orderSql = "SELECT sum(amount) as total FROM bnTransactions WHERE barId = " . $barId . " and status = 1 AND createdAt >= '$onTime' AND createdAt <= '$offTime'";
                        $orderSale = DB::select(DB::raw($orderSql));
                        
                        if (!empty($orderSale)) {
                            $ototal = $orderSale[0]->total;

                            if(empty($ototal))
                                $ototal = 0;

                            /*===Send Email to Bar===*/
                            $barEmail = $row->email;
                            if (!empty($barEmail)) {
                                $barName = $row->barName;
                                //create message
                                $msg = '<h1 class="title">Hello ' . $barName . '</h1>
                                        <p>Sales summary for the day as follows:</p>
                                        <p><b>Total Sales:</b> Rs.' . $ototal . '</p>';
                                $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                                $data = array(
                                    'msg' => $msg,
                                    'subject' => "Daily Sales Report",
                                );

                                Mail::to($barEmail)->send(new SendMail($data));
                            
                                /*===Send Email to Admin===*/
                                $adminEmail = User::select('email')->where(['userType' => 'A'])->first();
                                if (!empty($adminEmail)) {
                                    //$adminEmail = 'wasim@coretechies.com';
                                    
                                    //create message
                                    $msg = '<h1 class="title">Hello Admin</h1>
                                            <p>Sales summary of <b>'.$barName.'</b> for the day as follows</p>
                                            <p><b>Total Sales:</b> Rs.' . $ototal. '</p>';
                                    $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                                    $data = array(
                                        'msg' => $msg,
                                        'subject' => "Daily Sales Report",
                                    );
                                    Mail::to($adminEmail)->send(new SendMail($data));
                                }
                            }
                        }
                    }
                    $result = array('status' => 1, 'message' => 'Day off! Daily sales report has been sent to your email.');
                } 
                else {
                    $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
                }
            }
        }
    }
}