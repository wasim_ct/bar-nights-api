<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use DB;
use Mail;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;

class TemplateManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    public function addTemplate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'templateName' => 'required',
            'mediaName' => 'required|mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }

        if ($request->hasFile("mediaName")) {
            $file = $request->file("mediaName");
            $mediaSize = $file->getSize(); //File size
            $ext = $file->getClientOriginalExtension(); //get extention of file
            $image_resize = Image::make($file->getRealPath());
            //$image_resize->resize(1200, 800);
            //encrypt the name of file
            $mediaName = time() . rand() . "." . $ext;

            $upload = $image_resize->save(public_path("uploads/templates/" . $mediaName));
            if (!empty($upload)) {
                $param = array(
                    'templateName' => NUll, //$request->templateName, 
                    'templateFrame' => $mediaName, 
                    'updatedAt' => $this->entryDate,
                );
                if(isset($request->id) && !empty($request->id))
                {
                    DB::table('bnTemplates')->where('id', $request->id)->update($param);
                    $result = array('status' => 1, 'message' =>'Template has been updated successfully!');
                }else{
                    $param['createdAt'] = $this->entryDate;
                    $insertId = DB::table('bnTemplates')->insertGetId($param);
                    $data = array('id' => $insertId, 'mediaName' => $mediaName);
                    $result = array('status' => 1, 'message' => 'Template has been added successfully!','data'=> $data);
                }
            }else{
                $result = array('status' => 0, 'message' => 'Problem in template uploading, please try again later');
            }
        }else {
            $result = array('status' => 0, 'message' => 'Please select a template');
        }
        return response()->json($result);
    }
    public static function getStaffList(Request $request)
    {
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $keyword = $request->keyword;
        $status = $request->status;
        $query= DB::table('bnUsers as u');
        $query->select('u.id','u.userName','u.email','u.phone','u.status','s.name');
        $query->where('u.userType','S');
        $query->leftJoin('bnStaffs as s', 's.userId', '=', 'u.id');
        if(!empty($keyword)){
            $query->where(function($query2) use ($keyword){
                $query2->orWhere('s.name', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.email', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.phone', 'LIKE',"%{$keyword}%");
            });
        }
        if($status == '1' || $status == '0'){
            //echo $status;exit;
            $query->where('u.status','=',$status);
        }
        $query->orderBy('u.id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $staffList = $query->get();
        if (count( $staffList) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($staffList, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $staffList, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }
    public function getTemplateList(Request $request)
    {
        $query= DB::table('bnTemplates');
        $query->select('id','templateName','templateFrame as mediaName');
        $list = $query->get();
        if (count( $list) > 0) {
            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $list);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }

    public function getTemplateDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }
        $query= DB::table('bnTemplates');
        $query->select('id','templateName','templateFrame as mediaName');
        $query->where('id', $request->id);
        $list = $query->get();
        if (count( $list) > 0) {
            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $list[0]);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }
}