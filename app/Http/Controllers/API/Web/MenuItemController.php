<?php
namespace App\Http\Controllers\API\Web;

use App\BarItem;
use App\BarItemPrice;
use App\Http\Controllers\Controller;
use App\ItemCategory;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class MenuItemController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }
    //Funciton to Get Categories
    public function getCategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //First get the Parent Categories
        $categories = ItemCategory::select('id', 'catName', 'status')->where(['parentId' => 0, 'barId' => 0])->get();
        if (count($categories) > 0) {
            $data = array();
            foreach ($categories as $cat) {
                $subCat = ItemCategory::select('id as subCatId', 'catName as subCatName', 'status as subCatStatus')
                    ->where(['parentId' => $cat->id, 'barId' => $request->barId])
                    ->orderBy('id', 'desc')
                    ->get()->toArray();
                if (!empty($subCat)) {
                    $cat['subCat'] = $subCat;
                } else {
                    $cat['subCat'] = array();
                }

                $data[] = $cat;
            }

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'Categories loaded successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No category found');
        }

        return response()->json($result);
    }

    //Funciton to Get Categories
    /*public function getAllCategories(Request $request)
    {
    $validator = Validator::make($request->all(), [
    'barId' => 'required',
    ]);
    if ($validator->fails()) {
    $result = array('status' => 0, 'message' => 'Validation error occurred');
    return response()->json($result);
    }

    $categories = ItemCategory::select('id', 'catName', 'status')->where(['barId' => 0, 'barId' => $request->barId])->get();
    //Converting NULL to "" String
    array_walk_recursive($categories, function (&$item, $key) {
    $item = null === $item ? '' : $item;
    });

    if (count($categories) > 0) {
    $result = array('status' => 1, 'message' => 'categories get successfully', 'data' => $categories);
    } else {
    $result = array('status' => 0, 'message' => 'No category found');
    }

    return response()->json($result);
    }*/

    //Funciton to add edit subcategory
    public function addEditSubCategory(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'parentId' => 'required',
            'subCatName' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //print_r($request->all());exit;

        $status = false;

        //check catName already exist or not when id is set and not empty
        if (isset($request->subCatId) && !empty($request->subCatId)) {
            $exist = ItemCategory::select('id')
                ->where(['catName' => $request->subCatName, 'barId' => $request->barId])
                ->where('id', '!=', $request->subCatId)->first();
        } else {
            //check catName already exist or not
            $exist = ItemCategory::select('id')
                ->where(['catName' => $request->subCatName, 'barId' => $request->barId])->first();
        }

        if ($exist) {
            $result = array('status' => 0, 'message' => 'Subcategory already exist');
        } else {
            $data = array(
                'barId' => $request->barId,
                'parentId' => $request->parentId,
                'catName' => $request->subCatName,
            );

            if (isset($request->subCatId) && !empty($request->subCatId)) {
                $data['updatedAt'] = date("Y-m-d H:i:s");
                $subCatData = ItemCategory::where(['id' => $request->subCatId])->update($data);
                $subCatId = $request->subCatId;
                $message = "Category has been updated successfully";
            } else {
                $data['status'] = 1;
                $data['createdAt'] = $this->entryDate;
                $data['updatedAt'] = $this->entryDate;
                $subCatData = ItemCategory::create($data);
                if (!empty($subCatData)) {
                    $subCatId = $subCatData['id'];
                }
                $message = "Category has been created successfully";
            }
            //'id as subCatId', 'catName as subCatName', 'status as subCatStatus'
            //$subCatData = array('subCatId' => $subCatId, 'parentId' => $request->catName, 'subCatName' => $request->subCatName, 'subCatStatus' => 1);

            if (!empty($subCatData)) {
                $result = array('status' => 1, 'message' => $message, 'data' => array());
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        }
        return response()->json($result);
    }

    //Funciton to get subCategories based on Parent ID
    public function getCategoryData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'catId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $subCats = ItemCategory::select('bnItemCategories.id as subCatId', 'bnItemCategories.catName as subCatName')
            ->where(['bnItemCategories.status' => 1, 'bnItemCategories.parentId' => $request->catId, 'bnItemCategories.barId' => $request->barId])->get()->toArray();

        //Converting NULL to "" String
        array_walk_recursive($subCats, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        if ($subCats) {
            $result = array('status' => 1, 'message' => 'Subcategories get successfully', 'data' => $subCats);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to add edit menu items
    public function addEditMenuItem(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'catId' => 'required',
            //'subCatId' => 'required',
            //'itemImage' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG|max:50000',
            'itemName' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        //echo "<pre>"; print_r($request->all());exit;

        $menuId = '';

        //check item name already exist or not when id is set, in case of edit
        if (isset($request->id) && !empty($request->id)) {
            $exist = BarItem::select('id')->where(['itemName' => $request->itemName, 'catId' => $request->catId]) //'subCatId' => $request->subCatId
                ->where('id', '!=', $request->id)
                ->where('barId', '=', $request->barId)
                ->first();
        } else {
            //check item name  already exist or not
            $exist = BarItem::select('id')
                ->where(['itemName' => $request->itemName, 'catId' => $request->catId, 'barId' => $request->barId]) // 'subCatId' => $request->subCatId,
                ->first();
        }

        if ($exist) {
            $result = array('status' => 0, 'message' => 'Item with the same name already exist');
        } else {

            // If categories are Alcoholic & non-Alcoholic
            if($request->catId == 1 || $request->catId == 2)
                $request->itemType = 0;

            //data to insert or update in Table
            $data = array(
                'barId' => $request->barId,
                'catId' => $request->catId,
                'subCatId' => $request->subCatId,
                'itemName' => $request->itemName,
                'itemType' => $request->itemType,
                'description' => $request->description,
                'status' => 1,
                'isCustomized' => $request->isCustomized,
            );

            //Check item image is exist or not in the request
            if ($request->hasFile("itemImage")) {
                $file = $request->file("itemImage");
                $mediaSize = $file->getSize(); //File size
                $ext = $file->getClientOriginalExtension(); //get extention of file
                //encrypt the name of file
                $itemImage = time() * rand() . "." . $ext;
                $upload = $file->move("uploads/bars/items/", $itemImage);
                if (!empty($upload)) {
                    $data['itemImage'] = $itemImage;
                }
                if (isset($request->id) && !empty($request->id)) {
                    $pic = CommonFunction::GetSingleField('bnBarItems', 'itemImage', 'id', $request->id);
                    if (!empty($pic)) {
                        $oldPic = $pic;
                    } else {
                        $oldPic = null;
                    }
                }
            }

            //Price customization
            if ($request->isCustomized == 0) {
                //if we update the isCustomised = 0 at the time of edit then if item unit prize exist then delete that
                if (isset($request->id) && !empty($request->id)) {
                    $unitPrice = BarItemPrice::where(['bnBarItemPricing.itemId' => $request->id])->delete();
                }
                $data['price'] = $request->price;
            }

            //update data if id is set and not empty in bar item table, in case of edit
            if (isset($request->id) && !empty($request->id)) {
                $data['updatedAt'] = date("Y-m-d H:i:s");
                $message = "Menu item has been updated successfully";
                $menuData = BarItem::where(['id' => $request->id])->update($data);
                $menuId = $request->id;
            } else {
                $data['createdAt'] = $this->entryDate;
                $data['updatedAt'] = $this->entryDate;
                //insert data in bar item table
                $menuData = BarItem::create($data);
                if ($menuData) {
                    $menuId = $menuData['id'];
                }
                $message = "Menu item has been created successfully";
            }

            if (!empty($menuData)) {
                //Delete the Privous Image
                if (isset($oldPic) && $oldPic != null) {
                    $imgToDel = public_path("uploads/bars/items/" . $oldPic);
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                }

                //check if isCustomized is 1 then create entry for the different unit types of items in bar item price table
                if ($request->isCustomized == 1) {
                    if (isset($request->itemUnitPrice) && !empty($request->itemUnitPrice)) {
                        $itemUnitPrices = json_decode($request->itemUnitPrice, true);
                        foreach ($itemUnitPrices as $key => $val) {
                            if (!empty($val)) {
                                extract($val);
                                //if item unit price id is empty the insert data else update data
                                if ($itemUnitPriceId == '') {
                                    $itemUnitPriceInsert = array('unitTypeId' => $unitTypeId, 'price' => $price, 'itemId' => $menuId);
                                    $insert = BarItemPrice::create($itemUnitPriceInsert);
                                } else if ($itemUnitPriceId != '') {
                                    $itemUnitPriceUpdate = array('unitTypeId' => $unitTypeId, 'price' => $price);
                                    $insert = BarItemPrice::where(['id' => $itemUnitPriceId])->update($itemUnitPriceUpdate);
                                }
                            }
                        }
                    }
                }
                $result = array('status' => 1, 'message' => $message, 'data' => $menuId);
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
            }
        }
        return response()->json($result);
    }

    //Funciton to Get menu detail in case of edit
    public function getMenuDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        /*$menuDetail = BarItem::select('bnBarItems.id', 'bnBarItems.catId', 'ic.catName', 'bnBarItems.subCatId', 'isc.catName as subCatName', 'bnBarItems.itemName', 'bnBarItems.itemImage', 'bnBarItems.description', 'bnBarItems.isCustomized', 'bnBarItems.price')
        ->leftJoin('bnItemCategories as ic', 'ic.id', '=', 'bnBarItems.catId')
        ->leftJoin('bnItemCategories as isc', 'isc.id', '=', 'bnBarItems.subCatId')
        ->where(['bnBarItems.id' => $request->id])->first();*/

        $menuDetail = BarItem::select('bnBarItems.*')
            ->where(['bnBarItems.id' => $request->id])->first();

        if (!empty($menuDetail)) {
            $unitPrice = array();
            if ($menuDetail->isCustomized == 1) {
                $unitPrice = BarItemPrice::select('bnBarItemPricing.id', 'bnBarItemPricing.unitTypeId', 'ut.unitType', 'bnBarItemPricing.price')
                    ->leftJoin('bnUnitTypes as ut', 'ut.id', '=', 'bnBarItemPricing.unitTypeId')
                    ->where(['bnBarItemPricing.itemId' => $menuDetail->id])
                    ->get()->toArray();
            }
            $menuDetail['unitPrice'] = $unitPrice;

            //Converting NULL to "" String
            array_walk_recursive($menuDetail, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Item data get successfully', 'data' => $menuDetail);
        } else {
            $result = array('status' => 0, 'message' => 'No menu item found');
        }

        return response()->json($result);
    }

    //Funciton to Get menu items and use filter also
    public function getMenu(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        // print_r($request->all());exit;

        $sql = "SELECT bi.*, ic.catName, isc.catName as subCatName FROM bnBarItems bi LEFT JOIN bnItemCategories as ic on ic.id = bi.catId LEFT JOIN bnItemCategories as isc on isc.id = bi.subCatId WHERE bi.barId = " . $request->barId;

        /*====Filter Conditions Start====*/
        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = strtolower($request->keyword);

            //Status based search
            /*if ($keyword == 'active') {
                $status = 1;
            } else if ($keyword == 'inactive') {
                $status = 0;
            }*/ 
            if (is_numeric($keyword)) {
                $sql .= " AND bi.price = $keyword";
            } 
            else {
                $sql .= " AND (bi.itemName LIKE '%$keyword%' OR ic.catName LIKE '%$keyword%' OR isc.catName LIKE '%$keyword%')";
            }
        }

        // Status based       
        if (isset($request->status) && $request->status != 'null') {
            $sql .= " AND bi.status = ".$request->status;
        }
        
        //If sub CAT ID is set
        if (isset($request->subCatId)) {
            $sql .= " AND bi.subCatId = " . $request->subCatId;
        }
        //If CAT ID is set
        if (isset($request->catId)) {
            $sql .= " AND bi.catId = " . $request->id;
        }

        $sql .= " order by bi.id desc limit $startFrom, $limit";

        $menu = DB::select(DB::raw($sql));
        if (!empty($menu)) {
            $result = array('status' => 1, 'message' => 'Menu Items get successfully', 'data' => $menu, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No items found');
        }

        return response()->json($result);
    }

    //get items function to display on add order,discount and happy hours of bar
    public function getItemsList_old(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',

        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $currentDate = date('Y-m-d');
        $barId = $request->barId;
        $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
        $happyHourData = CommonFunction::checkBarHappyHour($barId);

        //get all category of bar that have parant id 0 and bar id 0 that are active also
        $foodCat = array();
        $cat = array();
        $menu = array();

        $foodCategory = ItemCategory::select('id as catId', 'catName', 'catImage')->where(['barId' => 0, 'status' => 1, 'parentId' => 0])->get();

        if (count($foodCategory) > 0) {

            foreach ($foodCategory as $fc) {
                //get all category of bar that have parant id 0 and bar id 0 that are active also

                //$foodSubCategory = ItemCategory::select('id as subcatId', 'catName', 'catImage')->where(['barId' => $request->barId, 'status' => 1])->where('parentId', '=', $fc->catId)->get();

                //foreach ($foodSubCategory as $fsc) {

                    $catItems = array();
                    $menuItems = array();

                    $sql = "SELECT bi.*, ic.catName, isc.catName as subCatName FROM bnBarItems bi LEFT JOIN bnItemCategories as ic on ic.id = bi.catId LEFT JOIN bnItemCategories as isc on isc.id = bi.subCatId WHERE bi.barId = " . $request->barId . " AND bi.status = 1 AND bi.catId = " . $fc->catId;

                    /*====Filter Conditions Start====*/
                    //Keyword based search
                    if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                        $keyword = $request->keyword;
                        $sql .= " AND (bi.itemName LIKE '%$keyword%' OR ic.catName LIKE '%$keyword%' OR isc.catName LIKE '%$keyword%')";
                    }

                    $sql .= " group by bi.id order by bi.id desc";

                    $menu = DB::select(DB::raw($sql));

                    //If items are calling on Orders then pass the discounted/happy hour price
                    if (isset($request->module) && $request->module == 'order') {
                        if (count($menu) > 0) {
                            foreach ($menu as $items) {

                                //$items->discountPrice = 0;
                                //$items->happyHourOffer = '';
                                $items->discountType = '';
                                $items->buy = 0;
                                $items->free = 0;

                                //If discount is available for the day
                                if ($discountData) {
                                    $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $items->price);
                                    if (!empty($disData)) {
                                        $items->discountPrice = round($disData['discountPrice'], 2);
                                    }
                                }

                                //If Happy Hour is available for the day
                                if ($happyHourData) {
                                    $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $items->price);
                                    if (!empty($hhData)) {
                                        if (isset($hhData['happyHourPrice'])) {
                                            $items->discountType = 'discount';
                                            $items->happyHourPrice = round($hhData['happyHourPrice'], 2);
                                        } else {
                                            $items->discountType = 'offer';
                                            $items->buy = $hhData['buy'];
                                            $items->free = $hhData['free'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //$fsc['catItems'] = $menu;
                    //$cat[] = $fsc;

                    $fc['catItems'] = $menu;
                //}
                //$fc['subcat'] = $foodSubCategory;

                $foodCat[] = $fc;
            }
            $data = $foodCat;

            $mediaPath = CommonFunction::getMediaPath();

            if (count($data) > 0) {
                $result = array('status' => 1, 'message' => "Menu get successfully", 'data' => $data, 'mediaPath' => $mediaPath);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }
    
    public function getItemsList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',

        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $currentDate = date('Y-m-d');
        $barId = $request->barId;
        $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
        $happyHourData = CommonFunction::checkBarHappyHour($barId);

        //get all category of bar that have parant id 0 and bar id 0 that are active also
        $foodCat = array();
        $cat = array();
        $menu = array();

        $foodCategory = ItemCategory::select('id as catId', 'catName', 'catImage')->where(['barId' => 0, 'status' => 1, 'parentId' => 0])->get();

        if (count($foodCategory) > 0) {

            foreach ($foodCategory as $fc) {
                //get all category of bar that have parant id 0 and bar id 0 that are active also

                //$foodSubCategory = ItemCategory::select('id as subcatId', 'catName', 'catImage')->where(['barId' => $request->barId, 'status' => 1])->where('parentId', '=', $fc->catId)->get();

                //foreach ($foodSubCategory as $fsc) {

                    $catItems = array();
                    $menuItems = array();

                    $sql = "SELECT bi.*, ic.catName, isc.catName as subCatName, isc.catImage as subCatImage FROM bnBarItems bi LEFT JOIN bnItemCategories as ic on ic.id = bi.catId LEFT JOIN bnItemCategories as isc on isc.id = bi.subCatId WHERE bi.barId = " . $request->barId . " AND bi.status = 1 AND bi.catId = " . $fc->catId;

                    /*====Filter Conditions Start====*/
                    //Keyword based search
                    if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                        $keyword = $request->keyword;
                        $sql .= " AND (bi.itemName LIKE '%$keyword%' OR ic.catName LIKE '%$keyword%' OR isc.catName LIKE '%$keyword%')";
                    }

                    $sql .= " group by bi.id order by bi.id desc";

                    $menu = DB::select(DB::raw($sql));

                    //If items are calling on Orders then pass the discounted/happy hour price
                    if (isset($request->module) && $request->module == 'order') {
                        if (count($menu) > 0) {
                            foreach ($menu as $items) {

                                //$items->discountPrice = 0;
                                //$items->happyHourOffer = '';
                                $items->discountType = '';
                                $items->buy = 0;
                                $items->free = 0;

                                //If discount is available for the day
                                if ($discountData) {
                                    $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $items->price);
                                    if (!empty($disData)) {
                                        $items->discountPrice = round($disData['discountPrice'], 2);
                                    }
                                }

                                //If Happy Hour is available for the day
                                if ($happyHourData) {
                                    $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $items->price);
                                    if (!empty($hhData)) {
                                        if (isset($hhData['happyHourPrice'])) {
                                            $items->discountType = 'discount';
                                            $items->happyHourPrice = round($hhData['happyHourPrice'], 2);
                                        } else {
                                            $items->discountType = 'offer';
                                            $items->buy = $hhData['buy'];
                                            $items->free = $hhData['free'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //$fsc['catItems'] = $menu;
                    //$cat[] = $fsc;

                    $fc['catItems'] = $menu;
                //}
                //$fc['subcat'] = $foodSubCategory;

                $foodCat[] = $fc;
            }
            //$data = $foodCat;
            /* New test code here  start here  written by ravindra 19-02-2020*/
            foreach($foodCat as $mainCat){
                $mainCategory = array(
                    'catId' => $mainCat->catId,
                    'catName' => $mainCat->catName,
                    'catImage' => $mainCat->catImage,
                );
                $check_subCategory = array();
                if(!empty($mainCat['catItems']) && count($mainCat['catItems'])){
                    foreach($mainCat['catItems'] as $subCat){
                        if(!in_array($subCat->subCatName, $check_subCategory)){
                            $subCategory = array(
                                'subCatId' => $subCat->subCatId,
                                'subCatName' => $subCat->subCatName,
                                'subCatImage' => $subCat->subCatImage,
                            );
                            foreach($mainCat['catItems'] as $item){
                                if($subCat->subCatName == $item->subCatName){
                                    $subCategory['catItems'][] = $item;
                                }
                            }
                            $mainCategory['subCat'][] = $subCategory;
                            array_push($check_subCategory, $subCat->subCatName);
                        }
                    }
                }else{
                    $mainCategory['subCat'] = array();
                }
                $data[] = $mainCategory;
            }
            /*New test code end here */
            $mediaPath = CommonFunction::getMediaPath();

            if (count($data) > 0) {
                $result = array('status' => 1, 'message' => "Menu get successfully", 'data' => $data, 'mediaPath' => $mediaPath);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }
}