<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use DB;
use Mail;
use Hash;
use CommonFunction;

class TransactionManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    
    public function getAllTransaction(Request $request)
    {
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $keyword = $request->keyword;
        $city = commonFunction::getCityIdByName($request->keyword);
        
        $query= DB::table('bnTransactions as t');
        $query->select('t.id','t.amount as totalAmount','t.moduleType','paymentMode','t.transactionId','t.paymentDate', 't.description', 't.barId', 't.userId', 't.createdAt', 'b.barName','p.firstName','p.lastName','u.userName');
        $query->leftJoin('bnBars as b', 'b.id', '=', 't.barId');
        $query->leftJoin('bnPatrons as p', 'p.userId', '=', 't.userId');
        $query->leftJoin('bnUsers as u', 'u.id', '=', 't.userId');
        $query->leftJoin('bnAddresses as address', 'b.userId', '=', 'address.userId');
        if(!empty($request->barId) && $request->barId !='null'){
            $query->where('t.barId', $request->barId);
        }
        if(!empty($request->paymentMode) && $request->paymentMode !='null'){
            $query->where('t.paymentMode', $request->paymentMode);
        }
        if(!empty($request->date) && $request->date !='null'){
            $query->where('t.paymentDate', $request->date);
        } 
        
        if(!empty($keyword)){
            /*if(!empty($city)){
                $query->where(['address.cityId' => $city]);
            }else{*/
            $query->where(function($query2) use ($keyword){
                $query2->orWhere('p.firstName', 'LIKE',"%{$keyword}%");
                $query2->orWhere('p.lastname', 'LIKE',"%{$keyword}%");
                $query2->orWhere('u.userName', 'LIKE',"%{$keyword}%");
                $query2->orWhere('b.barName', 'LIKE',"%{$keyword}%");
                $query2->orWhere('t.transactionId', 'LIKE',"%{$keyword}%");
                $query2->orWhere('t.paymentMode', 'LIKE',"%{$keyword}%");
                $query2->orWhere('t.moduleType', 'LIKE',"%{$keyword}%");
            });
            //}
        }

        // Based on City
        if(!empty($request->city)){
            $cityId = commonFunction::getCityIdByName($request->city);
            if(!empty($cityId)){
                $query->where(['address.cityId' => $cityId]);
            }
        }

        $query->orderBy('t.id', 'desc');
        if (!empty($startFrom)) {
            $query->offset($startFrom);
        }
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $list = $query->get();
        if (count( $list) > 0) {
            array_walk_recursive($list, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => "Data get successfully", 'data' => $list);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        return response()->json($result);   
    }
}