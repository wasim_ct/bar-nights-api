<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use App\Patron;
use App\Visit;
use App\VisitCode;
use App\GeoTagging;
use DB;
class CmsManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }
    //This function is used for edit cms setting
    public function editCmsSetting(Request $request)
    {
        $param = array(
            'aboutUs' => $request->aboutUs,
            'termsAndConditions' => $request->termsCondtions,
            'privacyPolicy' => $request->privacyPolicy,
            'faq' => $request->faq,
            'contactEmail' => $request->contactEmail,
            'privacyPolicy' => $request->privacy,
            'commission' => $request->commission,
            'eventCommission' => $request->eventCommission,
            'taxOnCommission' => $request->taxOnCommission,
            'barOneCommission' => $request->barOneCommission,
            'barTwoCommission' => $request->barTwoCommission,
            'barThreeCommission' => $request->barThreeCommission,
            'topBarLimit' => $request->topBarLimit,
            'topSpenderMinLimit' => $request->topSpenderMinLimit,
            'cashBackOn' => $request->cashBackOn,
            'cashBackAmount' => $request->cashBackAmount,
            'abusiveWords' => $request->abusive,
            'bookingTokenMoney' => $request->bookingTokenMoney,
            'cgst' => $request->cgst,
            'sgst' => $request->sgst,
            'serviceCharge' => $request->serviceCharge,
            'serviceChargeTax' => $request->serviceChargeTax,
            'updatedAt' => date('Y-m-d H:i:s')
        );
        //print_r($param);
        //exit;
        //check data is already added
        $setting = DB::table('bnAppSettings')->select('id')->get();
        if($setting && count($setting)){
            DB::table('bnAppSettings')->where('id', $setting[0]->id)->update($param);
        }else{
            $param['createdAt'] = date('Y-m-d H:i:s');
            DB::table('bnAppSettings')->insert($param);
        }
        $result['status'] = 1;
        $result['message'] = "Data updated successfully!";
        return response()->json($result);
    }
    //This function is used for get cms setting data
    public function getCmsSetting(Request $request)
    {
        $setting = DB::table('bnAppSettings')->select('*')->first();
        if($setting){
            //Converting NULL to "" String
            array_walk_recursive($setting, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result['status'] = 1;
            $result['data'] = $setting;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for commission setting
    public function editCommissionSetting(Request $request)
    {
        $param = array(
            'commission' => $request->commission,
            'barOneCommission' => $request->barOneCommission,
            'barTwoCommission' => $request->bartwoCommission,
            'barThreeCommission' => $request->barThreeCommission,
            'updatedAt' => date('Y-m-d H:i:s')
        );
        //check data is already added
        $setting = DB::table('bnAppSettings')->select('id')->get();
        if($setting && count($setting)){
            DB::table('bnAppSettings')->where('id', $setting[0]->id)->update($param);
        }else{
            $codeparam['visitId'] = $request->visitId;
            $param['createdAt'] = date('Y-m-d H:i:s');
            DB::table('bnAppSettings')->insert($param);
        }
        $result['status'] = 1;
        $result['message'] = "Data updated successfully!";
        return response()->json($result);
    }
    //This function is used for get basic commission setting
    public function getCommissionSetting(Request $request)
    {
        $setting = DB::table('bnAppSettings')->select('commission','barOneCommission','bartwoCommission','barThreeCommission')->get();
        if($setting && count($setting)){
            $result['status'] = 1;
            $result['data'] = $setting;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }

    // This function is add/update city areas
    public function updateCityAreas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'stateId' => 'required',
            'cityId' => 'required',
            'areas' => 'required',
            // 'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        // Add new areas
        if(empty($request->id)){
            $cityAreas = explode(",", $request->areas);
            foreach ($cityAreas as $ar) {
                $param = array(
                    'area' => $ar,
                    'cityId' => $request->cityId,
                    'stateId' => $request->stateId,
                    'createdAt' => date('Y-m-d H:i:s'),
                    'updatedAt' => date('Y-m-d H:i:s')
                );
                DB::table('bnCityAreas')->insert($param);
            }
        }
        // Update City Area
        else {
            $param = array(
                'area' => $request->areas,
                'cityId' => $request->cityId,
                'stateId' => $request->stateId,
                'updatedAt' => date('Y-m-d H:i:s')
            );
            DB::table('bnCityAreas')->where('id', $request->id)->update($param);
        }

        $result['status'] = 1;
        $result['message'] = "City Areas updated successfully!";
        return response()->json($result);
    }

    // Funciton to get all areas 
    public function getCityAreas(Request $request)
    {
        $areas = DB::table('bnCityAreas as ca')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ca.cityId')
            ->leftJoin('bnStates as st', 'st.state_id', '=', 'city.state_id')
            ->select('ca.id', 'ca.cityId', 'ca.stateId', 'ca.area', 'city.city_name as cityName', 'st.state_name as stateName')
            ->orderBy('ca.id', 'desc')
            ->get()->toArray();
        if (!empty($areas)) {
            //Converting NULL to "" String
            array_walk_recursive($areas, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'City Areas get successfully', 'data' => $areas);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }
}