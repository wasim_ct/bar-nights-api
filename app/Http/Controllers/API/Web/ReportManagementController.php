<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use App\User;
use App\Patron;
use App\Order;
use App\BarPayment;
use DB;

class ReportManagementController extends Controller
{
   
    //This function is used for get active user list in bars
    public function activeUserList(Request $request)
    {
        $date = $request->date;
        $limit = $request->limit;
        $startFrom = $request->startFrom;
        if(empty($limit)){
            $limit = 20;
        }
         if(empty($startFrom)){
            $startFrom = 0;
        }
        $query = DB::table('bnGeoTaggings as geo')->select('patron.firstName','patron.lastName','user.email','user.phone','bar.barName','visit.visitDate','visit.visitTime','visit.status as visitStatus');
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'geo.barId');
        $query->leftJoin('bnUsers as user', 'user.id', '=', 'geo.userId');
        $query->leftJoin('bnPatrons as patron', 'patron.userId', '=', 'geo.userId');
        $query->leftJoin('bnVisits as visit', 'visit.id', '=', 'geo.visitId');
        //filter by date
        if(!empty($date)){
            $formateDate = date("Y-m-d",strtotime($date));
        }else{
            $formateDate = date("Y-m-d");
        }
        $query->where('visit.visitDate',$formateDate);
        //filter by keyword
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = $request->keyword;
            $query->where('patron.firstName','LIKE',"%{$keyword}%");
            $query->orWhere('patron.lastName', 'LIKE',"%{$keyword}%");
            $query->orWhere('user.email', 'LIKE',"%{$keyword}%");
            $query->orWhere('user.phone', 'LIKE',"%{$keyword}%");
        }
        //pagination setup
        $query->offset($startFrom);
        $query->limit($limit);
        $query->orderBy('patron.firstName');
        $list = $query->get();
        //check result
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
    //This function is used for get offer available history from bar visit
    public function offerAvailedHistory(Request $request)
    {
        $date = $request->date;
        $limit = $request->limit;
        $startFrom = $request->startFrom;
        if(empty($limit)){
            $limit = 20;
        }
         if(empty($startFrom)){
            $startFrom = 0;
        }
        $query = DB::table('bnVisits as visit')->select('patron.firstName','patron.lastName','user.email','user.phone','bar.barName','code.code','code.codeAvailed','visit.visitDate','visit.visitTime','visit.status as visitStatus','code.createdAt');
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'visit.barId');
        $query->leftJoin('bnUsers as user', 'user.id', '=', 'visit.userId');
        $query->leftJoin('bnPatrons as patron', 'patron.userId', '=', 'visit.userId');
        $query->leftJoin('bnVisitCodes as code', 'code.visitId', '=', 'visit.id');
        $query->where('code.codeAvailed',1);
        //Filter by date
        if(!empty($date)){
            $formateDate = date("Y-m-d",strtotime($date));
        }else{
            $formateDate = date("Y-m-d");
        }
        $query->where('visit.visitDate',$formateDate);
        //search by keyword
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
            $keyword = $request->keyword;
            $query->where('patron.firstName','LIKE',"%{$keyword}%");
            $query->orWhere('patron.lastName', 'LIKE',"%{$keyword}%");
            $query->orWhere('user.email', 'LIKE',"%{$keyword}%");
            $query->orWhere('user.phone', 'LIKE',"%{$keyword}%");
            $query->orWhere('code.code', 'LIKE',"%{$keyword}%");
        }
        //pagination setup
        $query->offset($startFrom);
        $query->limit($limit);
        $query->orderBy('bar.barName');
        $list = $query->get();
        //check result
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }
}