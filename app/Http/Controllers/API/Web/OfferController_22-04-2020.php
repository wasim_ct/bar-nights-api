<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\DiscountOffer;
use App\DiscountOfferItem;
use App\HappyHourItem;
use App\HappyHoursTiming;
use App\Http\Controllers\Controller;
use App\OfferDetail;
use App\Patron;
use App\UserNotification;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class OfferController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }
    //Funciton to add and Edit offers
    public function addEditOffer(Request $request)
    {
        // Time Validation
        /*$currentTime = date("H:i");
        if($request->startTime < $currentTime || $request->endTime < $currentTime) {
            $result = array('status' => 0, 'message' => "Start & End time should be greater than current time");
            return response()->json($result);
        }
        exit;*/

        /*$image = $request->offerImage;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        $upload =  \File::put("public/uploads/bars/offers/" . $imageName, base64_decode($image));*/

        extract($_POST);
        $today = date("Y-m-d");
        $currentTime = strtotime(date("H:i"));

        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'offerType' => 'required',
            'title' => 'required',
            //'offerImage' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG|max:100000',
            'appliedOn' => 'required',
            //'discountRate' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        $errorStatus = false ;
        //Discount Condition
        if ($request->offerType == 'discount') {

            // Time Validation
            $result = CommonFunction::discountTimingValidation($request->startTime, $request->endTime, $request->barId);
            if(!empty($result) && $result['status'] == 0) {
                return response()->json($result);
            }

            //to update offer .Here id is primay key of bnOfferDetail table
            if (isset($request->id) && $request->id != '') {
                
                // Validation with current time
                /*if($request->startTime < $currentTime || $request->endTime < $currentTime) {
                    $result = array('status' => 0, 'message' => "Start & End time should be greater than current time");
                    return response()->json($result);
                }*/

                $query = DiscountOffer::select('*');
                $query->where('barId', $request->barId);
                $query->where('odId', '!=', $request->id);
                $query->whereRaw('date(createdAt) = ?', $today);
                $query->where('status', 1);
                $exist = $query->get();
            } 
            else {
                //check discount already exist or not
                // $exist = DiscountOffer::select('id')
                //     ->where('startTime', '<=', $request->startTime)
                //     ->where('endTime', '>=', $request->endTime)
                //     ->where('barId', '=', $request->barId)
                //     ->whereRaw('date(createdAt) = ?', $today)
                //     ->first();
                //new condition set by ravindra
                $query = DiscountOffer::select('*');
                $query->where('barId', $request->barId);
                $query->whereRaw('date(createdAt) = ?', $today);
                $query->where('status', 1);
                $exist = $query->get();
            }
            //check for time overlap
            if(!empty($exist)){
                $startTime = strtotime($request->startTime);
                $endTime = strtotime($request->endTime);
                foreach($exist as $key => $value){

                    $dbStartTime = strtotime($value->startTime);
                    $dbEndTime = strtotime($value->endTime);

                    $raatKe1259 = strtotime("23:59");
                    $raatKe12bje = strtotime("00:00");
                    $maxTime = strtotime("5:00"); 

                    // AFter 12 AM
                    if($dbStartTime >= $raatKe12bje && $dbEndTime >= $raatKe12bje && $dbStartTime <= $maxTime && $dbEndTime <= $maxTime) {
                        echo "after 12 aM\n";
                        if($startTime >= $dbStartTime && $startTime <= $dbEndTime){
                            $errorStatus = 1; //true;
                        }elseif($endTime >= $dbStartTime && $endTime <= $dbEndTime){
                            $errorStatus = 2; //true;
                        }else if($startTime <= $dbStartTime && $endTime >= $dbEndTime){
                            $errorStatus = 3; //true;
                        }
                    } 
                    // Before 12 AM
                    else if($endTime <= $raatKe1259 && $dbStartTime <= $raatKe1259 && $dbEndTime <= $dbStartTime) {
                        echo "Before 12 aM \n";
                        if($startTime >= $dbStartTime && $startTime <= $dbEndTime){
                            $errorStatus = 1; //true;
                        }elseif($endTime >= $dbStartTime && $endTime <= $dbEndTime){
                            $errorStatus = 2; //true;
                        }else if($startTime <= $dbStartTime && $endTime >= $dbEndTime){
                            $errorStatus = 3; //true;                            
                        }
                    }
                    else if($dbStartTime <= $raatKe1259 && $dbEndTime >= $raatKe12bje) {
                        echo "In Between me 12-01 \n";
                        // Start time is before 12 AM
                        if($startTime >= $dbStartTime && $startTime <= $dbEndTime && $startTime <= $raatKe1259) {
                            $errorStatus = 1; //true;
                            echo "\n". $value->startTime;
                        }
                        else if($endTime >= $raatKe12bje && $endTime <= $dbEndTime && $endTime <= $maxTime) {
                            $errorStatus = 2; //true;
                        }
                    }
                }
            }
            if($errorStatus)
                echo $errorStatus;
            else echo "No error"; exit;
        }
        //HappyHour Condition
        else if ($request->offerType == 'happyhour') {
            if (isset($request->weekDay)) {

                $days = explode(',', $request->weekDay);
                $daysExist = array();
                foreach ($days as $day) {
                    //$day = $request->weekDay;
                    //to update happy hours .Here id is primay key of bnOfferDetail table
                    if (isset($request->id) && $request->id != '') {
                        //check happy hours already exist or not on edit
                        $exist = HappyHoursTiming::select('*')
                            ->where(['barId' => $request->barId])
                            //->where('startTime', '<=', $request->startTime)
                            //->where('endTime', '>=', $request->endTime)
                            ->where('odId', '!=', $request->id)
                            ->whereRaw("find_in_set('$day',weekDay)")
                            ->where('status', 1)
                            ->get();

                        if (!empty($exist)) {
                            $daysExist[] = $day;
                        }

                    } else {
                        //check happy hours already exist or not
                        $exist = HappyHoursTiming::select('*')
                            ->where(['barId' => $request->barId])
                            //->where('startTime', '<=', $request->startTime)
                            //->where('endTime', '>=', $request->endTime)
                            ->whereRaw("find_in_set('$day',weekDay)")
                            ->where('status', 1)
                            ->get();
                        if (!empty($exist)) {
                            $daysExist[] = $day;
                        }
                    }
                    if(!empty($exist)){
                        $startTime = strtotime($request->startTime);
                        $endTime = strtotime($request->endTime);
                        foreach($exist as $key=>$value){
                            $dbStartTime = strtotime($value->startTime);
                            $dbEndTime = strtotime($value->endTime);
                            if($startTime >= $dbStartTime && $startTime <= $dbEndTime){
                                $errorStatus = true;
                            }elseif($endTime >= $dbStartTime && $endTime <= $dbEndTime){
                                $errorStatus = true;
                            }else if($startTime <= $dbStartTime && $endTime >= $dbEndTime){
                                $errorStatus = true;
                            }
                        }
                    }
                }
            }
        }

        if ($errorStatus) {
            // For happy Hour message
            if (isset($daysExist)) {
                $result = array('status' => 0, 'message' => 'Offer already exist for this time period on: ' . implode(", ", $daysExist));
            } else { // For discount
                $result = array('status' => 0, 'message' => 'Offer already exist for this time period');
            }

        } else {
            $status = false;
            //data to insert or update in offer detail Table
            $odData = array(
                'offerType' => $request->offerType,
                'barId' => $request->barId,
                'title' => $request->title,
                'description' => $request->description,
                'templateId' => $request->templateId,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            ); 

            //Offer Image Uploading - From Bar Panel Mobile App
            if ($request->hasFile("offerImage") && !isset($request->base64Img)) {
                $file = $request->file("offerImage");
                $ext = $file->getClientOriginalExtension(); //get extention of file
                
                $offerImage = time() * rand() . "." . $ext;
                $upload = $file->move("uploads/bars/offers/", $offerImage);
                if (!empty($upload)) {
                    $odData['offerImage'] = $offerImage;
                }
            }

            // Upload offer images, from base64 string. CROPPED - Web Panel
            if(!empty($request->offerImage) && isset($request->base64Img) && !$request->hasFile("offerImage")) {
                $image = $request->offerImage;  
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = time() * rand().'.'.'png';
                $upload =  \File::put("uploads/bars/offers/" . $imageName, base64_decode($image));
                if ($upload) 
                    $odData['offerImage'] = $imageName;
            }

            //Delete the last image in case of new image upload or template selection
            $oldPic = null;
            if (isset($request->id) && $request->id != '' && $request->templateId != 0) {
                //get old pic
                $pic = CommonFunction::GetSingleField('bnOfferDetails', 'offerImage', 'id', $request->id);
                if (!empty($pic)) {
                    $oldPic = $pic;
                    $odData['offerImage'] = null;
                }
            }

            //update in offer detail
            if (isset($request->id) && $request->id != '') {
                $odData['updatedAt'] = date("Y-m-d H:i:s");
                $odCreate = OfferDetail::where('id', $request->id)->update($odData);
                $odCreateId = $request->id;
            }
            //insert in offer detail
            else {
                $odData['status'] = 1;
                $odCreate = OfferDetail::create($odData);
                $odCreateId = $odCreate['id'];
            }

            if (!empty($odCreate)) {
                //Delete the Privous Image
                if (isset($oldPic) && $oldPic != null) {
                    $imgToDel = public_path("uploads/bars/offers/" . $oldPic);
                    if (file_exists($imgToDel)) {
                        unlink($imgToDel);
                    }
                }

                //Discount Condition
                if ($request->offerType == 'discount') {
                    $disData = array(
                        'odId' => $odCreateId,
                        'barId' => $request->barId, //Used in time validation
                        'appliedOn' => $request->appliedOn,
                        'discountRate' => $request->discountRate,
                        'startDate' => $today,
                        'endDate' => $today,
                        'startTime' => $request->startTime,
                        'endTime' => $request->endTime,
                        'status' => 1,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    //update discount offers
                    if (isset($request->id) && $request->id != '') {

                        $disCreate = DiscountOffer::where(['odId' => $request->id])->update($disData);

                        //Get the Offer Id
                        $offerId = CommonFunction::GetSingleField('bnDiscountOffers', 'id', 'odId', $request->id);

                        //if Applied on All items then delete the items entry
                        if ($request->appliedOn == 1) {
                            DB::table('bnDiscountOfferItems')->where(['offerId' => $offerId])->delete();
                        }

                        $status = true;
                        $msg = 'Discount has been updated successfully';

                        //Return this data
                        $data = array
                            (
                            'offerId' => CommonFunction::encryptId($offerId),
                            'appliedOn' => $request->appliedOn,
                        );
                        if($request->appliedOn == 2){
                            $this->addEditOfferItems($request->itemIds, $offerId, $request->offerType);
                        }

                    }
                    //insert discount offers
                    else {
                        $disCreate = DiscountOffer::create($disData);
                        if ($disCreate['id']) {
                            $status = true;
                            $msg = 'Discount has been created successfully';
                            //Return this data
                            $data = array
                                (
                                'offerId' => CommonFunction::encryptId($disCreate['id']),
                                'appliedOn' => $request->appliedOn,
                            );
                            if($request->appliedOn == 2){
                                $this->addEditOfferItems($request->itemIds, $disCreate['id'], $request->offerType);
                            }
                        }
                    }
                }
                //HappyHour Condition
                else if ($request->offerType == 'happyhour') {
                   
                    //happy hour data to add or update
                    //passall days if appliedOnDay is 1 and specific days if appliedOnDay is 2(specific days)
                    if ($request->appliedOnDay == 1) {
                        $weekDay = 'sunday,monday,tuesday,wednesday,thursday,friday,saturday';
                    } else if ($request->appliedOnDay == 2) {
                        //$weekDay = implode(",", $request->weekDay);
                        $weekDay = $request->weekDay;
                    }

                    $hhData = array(
                        'barId' => $request->barId,
                        'barId' => $request->barId, //Used in time validation
                        'odId' => $odCreateId,
                        'weekDay' => $weekDay,
                        'appliedOnDay' => $request->appliedOnDay,
                        'appliedOn' => $request->appliedOn,
                        'startTime' => $request->startTime,
                        'endTime' => $request->endTime,
                        'discountType' => $request->discountType,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    //if discount type is offer pass buy and free
                    if ($request->discountType == 2) {
                        $hhData['buy'] = $request->buy;
                        $hhData['free'] = $request->free;
                        $hhData['discountRate'] = 0;
                    } else {
                        $hhData['buy'] = 0;
                        $hhData['free'] = 0;
                        $hhData['discountRate'] = $request->discountRate;
                    }

                    //edit happyhour offers
                    if (isset($request->id) && $request->id != '') {
                        $hhCreate = HappyHoursTiming::where(['odId' => $request->id])->update($hhData);

                        //Get the hhTimingId Id
                        $hhTimingId = CommonFunction::GetSingleField('bnHappyHourTiming', 'id', 'odId', $request->id);

                        //if Applied on All items then delete the specific items entry
                        if ($request->appliedOn == 1) {
                            DB::table('bnHappyHourItems')->where(['hhTimingId' => $hhTimingId])->delete();
                        }

                        $status = true;
                        $msg = 'Happy Hour has been updated successfully';
                        $data = array
                            (
                            'offerId' => CommonFunction::encryptId($hhTimingId),
                            'appliedOn' => $request->appliedOn,
                        );
                        if($request->appliedOn == 2){
                            $this->addEditOfferItems($request->itemIds, $hhTimingId, $request->offerType);
                        }

                    }
                    //insert happyhour offers
                    else {
                        $hhCreate = HappyHoursTiming::create($hhData);
                        if ($hhCreate['id']) {
                            $status = true;
                            $msg = 'Happy Hour has been created successfully';
                            $data = array
                                (
                                'offerId' => CommonFunction::encryptId($hhCreate['id']),
                                'appliedOn' => $request->appliedOn,
                            );
                            if($request->appliedOn == 2){
                                $this->addEditOfferItems($request->itemIds, $hhCreate['id'], $request->offerType);
                            }
                        }
                    }
                }
            }
            if ($status == true) {
                if (!isset($request->id) && $request->id == '') {
                    if ($request->offerType == 'discount') {
                        $moduleId = $disCreate['id'];
                        $moduleType = 'discount';
                    } 
                    else if ($request->offerType == 'happyhour') {
                        $moduleId = $hhCreate['id'];
                        $moduleType = 'happyhour';
                    }
                    CommonFunction::sendOfferNotificationToPatrons($request->barId, $moduleId, $moduleType);

                    // Send Notification to Patrons for this event
                    /*$bar = Bar::select('barName')->where(['bnBars.id' => $request->barId])->first();
                    if (!empty($bar)) {
                        if ($request->offerType == 'discount') {
                            $id = $disCreate['id'];
                            $message = 'New Discount offer started at ' . ucwords(strtolower($bar->barName));
                            $moduleType = 'discountCreated';
                            $title = 'New discount';
                        } else if ($request->offerType == 'happyhour') {
                            $id = $hhCreate['id'];
                            $message = 'New Happy Hour offer started at ' . ucwords(strtolower($bar->barName));
                            $moduleType = 'happyHourCreated';
                            $title = 'New happyHour';
                        }
                        //Now call patrons info
                        $userData = Patron::select('bnPatrons.userId')
                            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                            ->where(['u.status' => 1])->get();

                        if (count($userData) > 0) {
                            foreach ($userData as $uid) {
                                $record = array('moduleType' => $moduleType, 'moduleId' => $id, 'description' => $message, 'userId' => $uid->userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                    $not = UserNotification::create($record);
                                if ($not['id']) {
                                    $notCreated = true;
                                    //$deviceToken[] = CommonFunction::getUserDeviceData($uid->userId);
                                    //$userId[] = $uid->userId;
                                }

                            }

                            //If notification created, then send push notification also
                            if (isset($notCreated)) {
                                $deviceToken = CommonFunction::getAllUserDeviceData();
                                if ($deviceToken) {
                                    //Firebase Notification
                                $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $id, 'title' => $title, 'msg' => $message);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                    // print_r($rs);exit;
                                }
                            }
                        }
                    }*/
                }
                $result = array('status' => 1, 'message' => $msg, 'data' => $data);
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
            }
        }
        return response()->json($result);
    }

    //Funciton to add edit offers items of type discount or happy hours
    public function addEditItems(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'itemIds' => 'required',
            'offerId' => 'required',
            'offerType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }
        $status = false;
        //print_r($request->all());exit;

        //$offerItem = json_decode($request->offerItem, true);
        //$offerItem = implode(",", $itemIds);

        $itemIds = $request->itemIds;
        if (!empty($itemIds)) {
            $offerId = CommonFunction::decryptId($request->offerId);

            //First Delete the existing items
            if ($request->offerType == 'discount') {
                $rs = DB::table('bnDiscountOfferItems')->where(['offerId' => $offerId])->delete();
            } else
            if ($request->offerType == 'happyhour') {
                $rs = DB::table('bnHappyHourItems')->where(['hhTimingId' => $offerId])->delete();
            }

            foreach ($itemIds as $key => $val) {
                //discount condition
                if ($request->offerType == 'discount') {
                    $offerItemData = array(
                        'offerId' => $offerId,
                        'itemId' => $val,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );

                    $offerItemInsert = DiscountOfferItem::create($offerItemData);
                    if ($offerItemInsert) {
                        $status = true;
                        $msg = 'Offer has been updated successfully';
                    }
                }
                //happyhour condition
                else if ($request->offerType == 'happyhour') {
                    $hhItemData = array(
                        'hhTimingId' => $offerId,
                        'itemId' => $val,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    //insert happy hours items on which offer is applied
                    $hhItemInsert = HappyHourItem::create($hhItemData);
                    if ($hhItemInsert) {
                        $status = true;
                        $msg = 'Happy Hour has been updated successfully';
                    }
                }
            }
        }
        if ($status == true) {
            $result = array('status' => 1, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
        }

        return response()->json($result);
    }

    //Funciton to Get list of offers on bases of filter
    public function getOffersListing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'offerType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $currentDate = date('Y-m-d');

        //Discount Condition
        if ($request->offerType == 'discount') {

            $sql = "SELECT od.*, do.appliedOn, do.discountRate, do.startTime, do.endTime, do.endDate, t.templateFrame FROM bnOfferDetails as od LEFT JOIN bnDiscountOffers as do on do.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId WHERE od.offerType = 'discount' AND od.barId = " . $request->barId;

            /*====Filter Conditions Start====*/
            //Keyword based search
            if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                $keyword = $request->keyword;
                $sql .= " AND (od.title LIKE '%$keyword%' OR od.description LIKE '%$keyword%')";
            }

            //Status based search
            if (isset($request->status) && $request->status != 'null') {
                if($request->status == 1 || $request->status == 0)
                    $sql .= " AND od.status = " . $request->status. " AND do.endDate = '$currentDate'";
                else
                    $sql .= " AND do.endDate < '$currentDate'";
            }

            //Applied on Based Search
            if (isset($request->appliedOn) && !empty($request->appliedOn)) {
                $sql .= " AND do.appliedOn = " . $request->appliedOn;
            }

            $sql .= " group by od.id order by od.id desc limit $startFrom, $limit";

            $offers = DB::select(DB::raw($sql));
            if (count($offers) > 0) {
                $data = $offers;
                $msg = 'Discounts/Offers get successfully';
            }
        }
        //HappyHour Condition
        else if ($request->offerType == 'happyhour') {
            $sql = "SELECT od.*, hht.appliedOnDay, hht.weekDay, hht.discountType, hht.startTime, hht.free, hht.buy, hht.discountRate, hht.endTime, hht.appliedOn, t.templateFrame FROM bnOfferDetails as od LEFT JOIN bnHappyHourTiming as hht on hht.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId WHERE od.offerType = 'happyhour' AND od.barId = " . $request->barId;

            /*====Filter Conditions Start====*/
            //Keyword based search
            if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                $keyword = $request->keyword;
                $sql .= " AND (od.title LIKE '%$keyword%' OR od.description LIKE '%$keyword%' OR hht.weekDay LIKE '%$keyword%')";
            }

            //Status based search
            if (isset($request->status) && ($request->status == 1 || $request->status == 0)) {
                $sql .= " AND od.status = " . $request->status;
            }

            //Applied on Based Search for specipic items
            if (isset($request->appliedOn) && !empty($request->appliedOn)) {
                $sql .= " AND hht.appliedOn = " . $request->appliedOn;
            }

            //Applied on Based Search for days
            if (isset($request->appliedOnDay) && !empty($request->appliedOnDay)) {
                $appliedOnDay = $request->appliedOnDay;
                if ($appliedOnDay == 1) {
                    $sql .= " AND hht.appliedOnDay = " . $appliedOnDay;
                } else {
                    $sql .= " AND find_in_set('$appliedOnDay',hht.weekDay) AND hht.appliedOnDay = 2";
                }

            }

            $sql .= " group by od.id order by od.id desc limit $startFrom, $limit";

            $happyHours = DB::select(DB::raw($sql));

            if (count($happyHours) > 0) {
                $data = $happyHours;
                $msg = 'Happy Hours get successfully';
            }

        }
        if (!empty($data)) {
            $result = array('status' => 1, 'message' => $msg, 'data' => $data, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to Get All list of offers on bases of filter
    public function getAllOffersListing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offerType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occured");
            return response()->json($result);
        }

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $currentDate = date('Y-m-d');

        $cityId = commonFunction::getCityIdByName($request->city);

        // $city = commonFunction::getCityIdByName($request->keyword);
        //Discount Condition
        if ($request->offerType == 'discount') {

            //$sql = "SELECT od.*, do.appliedOn, do.discountRate, do.startTime, do.endTime, t.templateFrame FROM bnOfferDetails as od LEFT JOIN bnDiscountOffers as do on do.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId WHERE od.offerType = 'discount' AND od.barId = " . $request->barId;
            $sql = "SELECT od.*, do.appliedOn, do.discountRate, do.startTime, do.endTime, do.endDate, t.templateFrame, bar.barName FROM bnOfferDetails as od LEFT JOIN bnDiscountOffers as do on do.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId LEFT JOIN bnBars as bar on bar.id = od.barId LEFT JOIN  bnAddresses as address on address.userId = bar.userId WHERE od.offerType = 'discount' ";

            /*====Filter Conditions Start====*/
            if (isset($request->barId) && !empty($request->barId) && $request->barId != 'null'){
                $sql .= " AND od.barId = $request->barId";
            }

            // Based onCity
            if($cityId){
                $sql .= " AND address.cityId = " . $cityId;
            }

            //Keyword based search
            if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                $keyword = $request->keyword;
                $sql .= " AND (od.title LIKE '%$keyword%' OR od.description LIKE '%$keyword%' OR  bar.barName LIKE '%$keyword%')";
            }

            //Status based search
            /*if (isset($request->status) && ($request->status == 1 || $request->status == 0)) {
                $sql .= " AND od.status = " . $request->status;
            }*/

            if (isset($request->status) && $request->status != 'null') {
                if($request->status == 1 || $request->status == 0)
                    $sql .= " AND od.status = " . $request->status. " AND do.endDate = '$currentDate'";
                else
                    $sql .= " AND do.endDate < '$currentDate'";
            }

            //Applied on Based Search
            if (isset($request->appliedOn) && !empty($request->appliedOn)) {
                $sql .= " AND do.appliedOn = " . $request->appliedOn;
            }

            $sql .= " order by od.id desc limit $startFrom, $limit";

            $offers = DB::select(DB::raw($sql));
            if (count($offers) > 0) {
                $data = $offers;
                $msg = 'Discounts/Offers get successfully';
            }
        }
        //HappyHour Condition
        else if ($request->offerType == 'happyhour') {
            //$sql = "SELECT od.*, hht.appliedOnDay, hht.weekDay, hht.discountType, hht.startTime, hht.free, hht.buy, hht.discountRate, hht.endTime, hht.appliedOn, t.templateFrame FROM bnOfferDetails as od LEFT JOIN bnHappyHourTiming as hht on hht.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId WHERE od.offerType = 'happyhour' AND od.barId = " . $request->barId;
            $sql = "SELECT od.*, hht.appliedOnDay, hht.weekDay, hht.discountType, hht.startTime, hht.free, hht.buy, hht.discountRate, hht.endTime, hht.appliedOn, t.templateFrame, bar.barName FROM bnOfferDetails as od LEFT JOIN bnHappyHourTiming as hht on hht.odId = od.id LEFT JOIN bnTemplates as t on t.id = od.templateId LEFT JOIN bnBars as bar on bar.id = od.barId LEFT JOIN  bnAddresses as address on address.userId = bar.userId WHERE od.offerType = 'happyhour' ";
            /*====Filter Conditions Start====*/
            if (isset($request->barId) && !empty($request->barId) && $request->barId != 'null'){
                $sql .= " AND od.barId = $request->barId";
            }

            // Based onCity
            if($cityId){
                $sql .= " AND address.cityId = " . $cityId;
            }

            //Keyword based search
            if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {
                $keyword = $request->keyword;
                $sql .= " AND (od.title LIKE '%$keyword%' OR od.description LIKE '%$keyword%' OR hht.weekDay LIKE '%$keyword%' OR bar.barName LIKE '%$keyword%')";
                
            }

            //Status based search
            if (isset($request->status) && ($request->status == 1 || $request->status == 0)) {
                $sql .= " AND od.status = " . $request->status;
            }

            //Applied on Based Search for specipic items
            if (isset($request->appliedOn) && !empty($request->appliedOn)) {
                $sql .= " AND hht.appliedOn = " . $request->appliedOn;
            }

            //Applied on Based Search for days
            if (isset($request->appliedOnDay) && !empty($request->appliedOnDay)) {
                $appliedOnDay = $request->appliedOnDay;
                if ($appliedOnDay == 1) {
                    $sql .= " AND hht.appliedOnDay = " . $appliedOnDay;
                } else {
                    $sql .= " AND find_in_set('$appliedOnDay',hht.weekDay) AND hht.appliedOnDay = 2";
                }

            }

            $sql .= " order by od.id desc limit $startFrom, $limit";

            $happyHours = DB::select(DB::raw($sql));

            if (count($happyHours) > 0) {
                $data = $happyHours;
                $msg = 'Happy Hours get successfully';
            }

        }
        if (!empty($data)) {
            $result = array('status' => 1, 'message' => $msg, 'data' => $data, 'limit' => $limit);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }
    //Funciton to Get offers detail to load data on edit
    public function getOffersData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'id' => 'required',
            'offerType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation erro occured");
            return response()->json($result);
        }

        //Discount Condition
        if ($request->offerType == 'discount') {
            $offer = OfferDetail::select('bnOfferDetails.*', 'do.id as offerId', 'do.appliedOn', 'do.discountRate', 'do.startTime', 'do.endTime', 't.templateFrame')
                ->leftJoin('bnDiscountOffers as do', 'do.odId', '=', 'bnOfferDetails.id')
                ->leftJoin('bnTemplates as t', 't.id', '=', 'bnOfferDetails.templateId')
                ->where(['bnOfferDetails.offerType' => 'discount', 'bnOfferDetails.id' => $request->id, 'bnOfferDetails.barId' => $request->barId])
                ->first();

            if (!empty($offer)) {
                //Converting NULL to "" String
                array_walk_recursive($offer, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $data = $offer;
                $msg = 'Offer data get successfully';
            }
        }
        //HappyHour Condition
        else if ($request->offerType == 'happyhour') {
            $happyHours = OfferDetail::select('bnOfferDetails.*', 'hht.id as happyHId', 'hht.appliedOnDay', 'hht.weekDay', 'hht.startTime', 'hht.endTime', 'hht.status', 'hht.appliedOn', 'hht.discountType', 'hht.free', 'hht.buy', 'hht.discountRate', 't.templateFrame')
                ->leftJoin('bnHappyHourTiming as hht', 'hht.odId', '=', 'bnOfferDetails.id')
                ->leftJoin('bnTemplates as t', 't.id', '=', 'bnOfferDetails.templateId')
                ->where(['bnOfferDetails.offerType' => 'happyhour', 'bnOfferDetails.id' => $request->id, 'bnOfferDetails.barId' => $request->barId])
                ->first();
            if (!empty($happyHours)) {
                //Converting NULL to "" String
                array_walk_recursive($happyHours, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $data = $happyHours;
                $msg = 'Happy Hours data get successfully';
            }
        }

        if (isset($data)) {
            $result = array('status' => 1, 'message' => $msg, 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No recode found');
        }

        return response()->json($result);
    }

    //Funciton to Get offers Items data while edit
    public function getOffersItems(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offerId' => 'required',
            'offerType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }
        
        //$offerId = CommonFunction::decryptId($request->offerId);
        
        //Discount Condition
        if ($request->offerType == 'discount') {
            $offerId = $request->offerId;
            //get items on which discount offer is applied
            $offersItems = DiscountOfferItem::select('id', 'offerId', 'itemId')
                ->where(['offerId' => $offerId])
                ->get()->toArray();

            //get discount rate of the offer
            $discountRate = CommonFunction::GetSingleField('bnDiscountOffers', 'discountRate', 'id', $offerId);

            //Converting NULL to "" String
            array_walk_recursive($offersItems, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $data = $offersItems;
            $msg = 'Offer items successfully';
        }
        // Happy Hour Condition
        else if ($request->offerType == 'happyhour') {
            $offerId = $request->offerId;
            //get items on which happy hours offer is applied
            $happyHoursItems = HappyHourItem::select('id', 'hhTimingId', 'itemId')
                ->where(['hhTimingId' => $offerId])
                ->get()->toArray();

            //get discount rate of the offer
            $discountRate = CommonFunction::GetSingleField('bnHappyHourTiming', 'discountRate', 'id', $offerId);

            //Converting NULL to "" String
            array_walk_recursive($happyHoursItems, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $data = $happyHoursItems;
            $msg = 'Happy Hours items successfully';
        }

        if (!empty($data)) {
            $result = array('status' => 1, 'message' => $msg, 'data' => $data, 'discountRate' => $discountRate);
        } else {
            $result = array('status' => 0, 'message' => 'No data found', 'discountRate' => $discountRate);
        }

        return response()->json($result);
    }

    //This function is used for update or add item to offer
    public function addEditOfferItems($itemIds, $offerId,$offerType)
    {   
        $status = false;
        if (!empty($itemIds)) 
        {
            //First Delete the existing items
            if ($offerType == 'discount') {
                $rs = DB::table('bnDiscountOfferItems')->where(['offerId' => $offerId])->delete();
            } else
            if ($offerType == 'happyhour') {
                $rs = DB::table('bnHappyHourItems')->where(['hhTimingId' => $offerId])->delete();
            }
            $offerItem = explode(',', $itemIds);
            foreach ($offerItem  as $key => $val) {
                //discount condition
                if ($offerType == 'discount') {
                    $offerItemData = array(
                        'offerId' => $offerId,
                        'itemId' => $val,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );

                    $offerItemInsert = DiscountOfferItem::create($offerItemData);
                    if ($offerItemInsert) {
                        $status = true;
                        $msg = 'Offer has been updated successfully';
                    }
                }
                //happyhour condition
                else if ($offerType == 'happyhour') {
                    $hhItemData = array(
                        'hhTimingId' => $offerId,
                        'itemId' => $val,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    //insert happy hours items on which offer is applied
                    $hhItemInsert = HappyHourItem::create($hhItemData);
                    if ($hhItemInsert) {
                        $status = true;
                        $msg = 'Happy Hour has been updated successfully';
                    }
                }
            }
        }
        if ($status == true) {
            $result = array('status' => 1, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong, please try again');
        }

        return response()->json($result);
    }
}