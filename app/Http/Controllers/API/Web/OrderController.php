<?php
namespace App\Http\Controllers\API\Web;

use App\Bar;
use App\Friend;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderKot;
use App\OrderKotItem;
use App\UserNotification;
use App\Visit;
use App\EventBooking;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class OrderController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest:bar');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Funciton to get Order info with Visit
    public function getVisitOrderInfo(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $visitInfo = Visit::select('bnVisits.*', 'o.id as orderPKey', 'o.orderId')
            ->leftJoin('bnOrders as o', 'o.visitId', '=', 'bnVisits.id')
            ->where(['bnVisits.id' => $request->visitId, 'bnVisits.barId' => $request->barId])
            ->first();

        if (!empty($visitInfo)) {
            //Converting NULL to "" String
            array_walk_recursive($visitInfo, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Visit info get successfully', 'data' => $visitInfo);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to get order listing of both type
    public function getOrdersList(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            // 'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Pagination Condition of startFrom and no of records
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        $sql = "SELECT o.*, b.barName, p.firstName, p.lastName, u.userName FROM bnOrders as o JOIN bnPatrons as p on p.userId = o.userId JOIN bnUsers as u on u.id = o.userId JOIN bnBars as b on b.id = o.barId WHERE o.id != 0";

        /*===filter conditions===*/
        if (isset($request->barId) && !empty($request->barId)) {
            $sql .= " AND o.barId = " . $request->barId;
        }

        // Based on orderType
        if (isset($request->orderType) && !empty($request->orderType)) {
            $sql .= " AND o.orderType = '$request->orderType'";
        }
        //Keyword based search
        if (isset($request->keyword) && !empty($request->keyword) && $request->keyword != 'null') {

            $keyword = strtolower($request->keyword);
            /*if (is_numeric($keyword)) {
                $sql .= " AND o.grandTotal = $keyword";
            } 
            else {*/
                $sql .= " AND (o.orderId LIKE '%$keyword%' OR p.firstName LIKE '%$keyword%' OR p.lastName LIKE '%$keyword%' OR b.barName LIKE '%$keyword%')";
            //}

        }
        // Based on staus
        if (isset($request->status) && !empty($request->status)) {
            $sql .= " AND o.status = " . $request->status;
        }

        $sql .= " order by o.id desc limit $startFrom, $limit";
        $order = DB::select(DB::raw($sql));

        if (count($order) > 0) {
            foreach ($order as $orderKey => $value) {
                //echo "f:".$value->friendId." u:".$value->userId;
                if ($value->orderType == 'preorder') {
                    $friendList = '';
                    if (!empty($value->friendId)) {
                        $friend = Friend::getFriendNamebyFriendId($value->friendId, $value->userId);
                        if ($friend) {
                            $friendList .= $friend . ', ';
                        }
                    }
                    $order[$orderKey]->friendList = substr_replace($friendList, "", -2);
                } else {
                    $friendList = '';
                    $friend = Friend::getFriendNamebyOrderId($value->id);
                    //print_r($friend);
                    if ($friend) {
                        foreach ($friend as $key => $f) {
                            $friendList .= $f->firstName . " " . $f->lastName . ', ';
                        }
                    }
                    $order[$orderKey]->friendList = substr_replace($friendList, "", -2);
                    //print_r( $order[$orderKey]->friendList);
                }
            }
            $result = array('status' => 1, 'message' => 'Order get successfully', 'data' => $order);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }
 
    //Funciton to get order detail
    public function getOrderInvoice(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }

        $order = Order::select('bnOrders.id', 'bnOrders.userId', 'bnOrders.orderType', 'bnOrders.orderDate', 'bnOrders.totalPrice', 'bnOrders.cgst', 'bnOrders.cgstAmount', 'bnOrders.sgst', 'bnOrders.sgstAmount', 'bnOrders.serviceCharge', 'bnOrders.serviceChargeAmount', 'bnOrders.serviceChargeTax', 'bnOrders.serviceChargeTaxAmount', 'bnOrders.grandTotal', 'bnOrders.status', 'bnOrders.createdAt', 'bnOrders.paymentStatus', 'p.firstName', 'p.lastName', 'u.userName', 'v.requestId', 'v.requestType') //, 'bt.tokenMoney'
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnOrders.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnOrders.userId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            //->leftJoin('bnBookTable as bt', 'bt.id', '=', 'v.requestId')
            ->where(['bnOrders.orderId' => $request->orderId, 'bnOrders.barId' => $request->barId])
            ->first();

        if ($order) {
            if ($order->orderType == 'order') {
                //get list of kot items on order id bases
                $sql = "SELECT i.id, i.itemId, i.itemName, SUM(i.quantity) AS totalQty, SUM(i.amount) AS totalAmount, i.unitPrice FROM bnOrderKotItems i inner join bnOrderKots ok on ok.id = i.kotId WHERE i.orderId = $order->id AND (ok.status = 2 OR ok.status = 3) GROUP BY i.itemId, i.unitPrice";
                $orderDetail = DB::select(DB::raw($sql));
                if ($orderDetail) {
                    $order['orderItems'] = $orderDetail;
                    $data = $order;
                }
            } 
            else if ($order->orderType == 'preorder') 
            {
                //get list of order items
                $preOrderDetails = "SELECT id, itemId, itemName, suggestions, SUM(quantity) AS totalQty, SUM(amount) AS totalAmount, unitPrice FROM bnPreOrderDetails WHERE orderId = $order->id GROUP BY itemId,unitPrice";
                $preOrderDetail = DB::select(DB::raw($preOrderDetails));

                if ($preOrderDetail) {
                    $order['orderItems'] = $preOrderDetail;
                }
            }

            // Get the advance money, if available
            if($order->requestType == 'table') {
                $tokenMoney = CommonFunction::GetSingleField('bnBookTable', 'tokenMoney', 'id', $order->requestId);

                if($tokenMoney) {
                    /* Update the payment status to paid in order table, 
                     * if grand total is less than to token money
                     */
                    if(($order->grandTotal - $tokenMoney) <= 0){
                        Order::where(['id' => $order->id])->update(['paymentStatus' => 1, 'updatedAt' => $this->entryDate]);
                        $order->paymentStatus = 1;
                    }
                }
                $order['tokenMoney'] = $tokenMoney;
            }
            else if($order->requestType == 'event') {
                $event = EventBooking::select('bnEventBookings.totalSeats', 'e.coverCharges')
                    ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
                    ->where(['bnEventBookings.id' => $order->requestId])
                    ->where('e.coverCharges', '>', 0)
                    ->first();
                if(!empty($event)) {
                    $order['coverCharges'] = $coverCharges = $event->totalSeats * $event->coverCharges;

                    /* Update the payment status to paid in order table, 
                     * if grand total is less than to Cover Charges
                     */
                    if(($order->grandTotal - $coverCharges) <= 0){
                        Order::where(['id' => $order->id])->update(['paymentStatus' => 1, 'updatedAt' => $this->entryDate]);
                        $order->paymentStatus = 1;
                    }
                }
            }

            $data = $order;

            // Get the payment Status of the order
            $paymentData = DB::table('bnTransactions as t')
                ->select('t.*') 
                ->where(['t.moduleId' => $order->id, 't.moduleType' => 'order'])
                ->get();

            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            if(!empty($paymentData)) {
                array_walk_recursive($paymentData, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
            else 
                $paymentData = [];

            $result = array('status' => 1, 'message' => 'Order details get successfully', 'data' => $data, 'paymentData' => $paymentData);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    //Funciton to get order detail
    public function getOrderKots(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $status = false;

        $orderId = $request->orderId;
        $barId = $request->barId;
        //
        $orderKot = OrderKot::select('bnOrderKots.id', 'bnOrderKots.status', 'bnOrderKots.serveStatus', 'bnOrderKots.kotNumber', 'o.id as orderPKey', 'o.orderId', 'o.status as orderStatus', 'p.firstName as friendFirstName', 'p.lastName as friendLastName')
            ->join('bnOrders as o', 'o.id', '=', 'bnOrderKots.orderId')
            ->where(['o.orderId' => $orderId, 'o.barId' => $barId])
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnOrderKots.friendId')
            ->orderBy('bnOrderKots.id', 'desc')
            ->get();
        if (count($orderKot) > 0) {
            foreach ($orderKot as $ok) {
                //get list of kot items on kot id bases
                $orderKotItems = OrderKotItem::select('id', 'itemId', 'itemName', 'suggestions', 'quantity', 'unitTypeId', 'unitTypeName', 'amount')
                    ->where(['kotId' => $ok->id])
                    ->get()->toArray();
                $ok['items'] = $orderKotItems;

                //Converting NULL to "" String
                array_walk_recursive($ok, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $data[] = $ok;
            }
            $status = true;
            $msg = 'Kot data get successfully';
        }

        if ($status == true) {
            $result = array('status' => 1, 'message' => $msg, 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No data found');
        }

        return response()->json($result);
    }

    //Funciton to get KOT details based on the ID
    public function getKotDetail(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'kotNo' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $status = false;

        //Check Order KOT is exist or not
        $orderKot = OrderKot::select('bnOrderKots.id', 'bnOrderKots.kotNumber', 'bnOrderKots.orderId', 'v.sittingType', 'v.tableStandingNo', 'o.orderId as uniqueId')
            ->leftJoin('bnOrders as o', 'o.id', '=', 'bnOrderKots.orderId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'o.visitId')
            ->where(['bnOrderKots.kotNumber' => $request->kotNo, 'o.barId' => $request->barId])
            ->first();

        if ($orderKot) {
            //get list of kot items on kot id bases
            $orderKot['items'] = OrderKotItem::select('bnOrderKotItems.id', 'item.itemImage', 'bnOrderKotItems.itemId', 'bnOrderKotItems.itemName', 'bnOrderKotItems.quantity', 'bnOrderKotItems.amount')
                ->leftJoin('bnBarItems as item', 'item.id', '=', 'bnOrderKotItems.itemId')
                ->where(['kotId' => $orderKot->id])
                ->get()->toArray();
        }

        if ($orderKot) {
            //Converting NULL to "" String
            array_walk_recursive($orderKot, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Items get successfully', 'data' => $orderKot);
        } else {
            $result = array('status' => 0, 'message' => 'No data found');
        }

        return response()->json($result);
    }

    //Funciton to edit and delete kot item
    public function editKotItems(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            //'kotItems' => 'required',
            'orderId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'validation error occurred');
            return response()->json($result);
        }
        $status = false;
        $barId = $request->barId;
        $orderId = $request->orderId;
        //$kotItems = json_decode($request->kotItems, true);

        //if (!empty($kotItems)) {

        /*===Update/Delete KOT Items on Edit KOT items, not in use now===*/
        /*foreach ($kotItems as $key => $val)
        {
        extract($val);
        if ($qty == 0) {
        $orderKotItems = OrderKotItem::where(['id' => $kotItemId])->delete();
        }
        else if ($qty != 0) {
        $orderKotItems = OrderKotItem::where(['id' => $kotItemId])->update(['quantity' => $qty, 'amount' => $price, 'updatedAt' => $this->entryDate]);
        }
        $updateOrder = true;
        }*/

        /*===Delete the KOT with its items===*/
        if (isset($request->kotId)) {
            $kotId = $request->kotId;

            //Delete the KOT
            $delete = OrderKot::where(['id' => $kotId])->delete();
            if ($delete) {
                //Delete the KOT items
                OrderKotItem::where(['kotId' => $kotId])->delete();
                $updateOrder = true;
            }
        }

        /*===After KOT Items updates/Delete, lets updat the pricing of order===*/
        if (isset($updateOrder)) {
            $orderId = $request->orderId;
            $barId = $barId;
            //function to calculate amount on cancle kot
            $amountCal = CommonFunction::calculateRemainingAmount($orderId, $barId);
            $updateOrder = Order::where(['id' => $orderId])->update($amountCal);
            $status = true;
            $msg = 'Order has been updated successfully';
            $result = array('status' => 1, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        //}

        return response()->json($result);
    }

    //Function to insert offline orders of user from bar side
    public function createOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
            'visitId' => 'required',
            'cartItems' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $cartItems = json_decode($request->cartItems, true);
        $barId = $request->barId;
        $visitId = $request->visitId;

        if (!empty($cartItems)) {
            $order = array();
            $i = 1;
            $itemPrice1 = 0;
            $itemPrice2 = 0;

            //Get the active discount/offers, for item pricing
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            foreach ($cartItems as $key => $val) {
                if (!empty($val)) {
                    extract($val);

                    //Get the Item Price from DB
                    $itemPrice = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $itemId);
                    $price = $itemPrice;

                    //If dscount is available for the day
                    if ($discountData) {
                        $disData = CommonFunction::getDiscountedPrice($discountData, $itemId, $itemPrice);
                        if (!empty($disData)) {
                            $price = $disData['discountPrice'];
                        }
                    }

                    //If Happy Hour is available for the day
                    if ($happyHourData) {
                        $hhData = CommonFunction::getHappyHourPrice($happyHourData, $itemId, $itemPrice);
                        if (!empty($hhData)) {
                            if (isset($hhData['happyHourPrice'])) {
                                $price = $hhData['happyHourPrice'];
                            } else {
                                $hhOffer = $hhData;
                            }
                        }
                    }

                    //if Happy Hour Offer have buy-get free then calculate the price
                    if (isset($hhOffer)) {
                        $price = CommonFunction::getOfferPrice($hhOffer, $qty, $itemPrice);
                    } else {
                        $price = $price * $qty;
                    }

                    $itemPrice1 = $itemPrice1 + $price;
                }
            }

            //Call Tax info of the bar
            $taxes = CommonFunction::GetSingleRow('bnBars', 'id', $barId);
            $tableStandingNo = null;
            $userVisitId = 0;

            //Now insert the order details in the table
            $totalPrice = $itemPrice2 + $itemPrice1;

            //take total price calculation using Tax;
            $taxesCalculation = CommonFunction::calculateTax($totalPrice, $taxes);

            //take user visit data from visit table
            $visitData = CommonFunction::GetSingleRow('bnVisits', 'id', $visitId);

            //Check order is exist or not for this visit
            $orderData = Order::select('id', 'orderId')
                ->where(['visitId' => $visitId, 'barId' => $barId])
                ->first();
            //$orderId = CommonFunction::GetSingleField('bnOrders', 'id', 'visitId', $visitId);

            //If order data is set then update the record of the order
            $uniqueId = '';
            if ($orderData) {
                $update = array(
                    'totalPrice' => DB::raw('totalPrice + ' . $totalPrice),
                    'grandTotal' => DB::raw('grandTotal + ' . $taxesCalculation['grandTotal']),
                    'cgst' => $taxesCalculation['cgst'],
                    'cgstAmount' => DB::raw('cgstAmount + ' . $taxesCalculation['cgstAmount']),
                    'sgst' => $taxesCalculation['sgst'],
                    'sgstAmount' => DB::raw('sgstAmount + ' . $taxesCalculation['sgstAmount']),
                    'serviceCharge' => $taxesCalculation['serviceCharge'],
                    'serviceChargeAmount' => DB::raw('serviceChargeAmount + ' . $taxesCalculation['serviceChargeAmount']),
                    'serviceChargeTax' => $taxesCalculation['serviceChargeTax'],
                    'serviceChargeTaxAmount' => DB::raw('serviceChargeTaxAmount + ' . $taxesCalculation['serviceChargeTaxAmount']),
                );
                $updateOrder = Order::where(['id' => $orderData->id])->update($update);
                $lastOrderId = $orderData->id;
                $uniqueId = $orderData->orderId;
            }
            //Create new order if ID is not set
            else {
                $insert = array(
                    'userId' => $visitData->userId,
                    'barId' => $barId,
                    'tableNo' => $visitData->tableStandingNo,
                    'orderDate' => date('Y-m-d'),
                    'status' => 1,
                    'totalPrice' => $totalPrice,
                    'grandTotal' => $taxesCalculation['grandTotal'],
                    'cgst' => $taxesCalculation['cgst'],
                    'cgstAmount' => $taxesCalculation['cgstAmount'],
                    'sgst' => $taxesCalculation['sgst'],
                    'sgstAmount' => $taxesCalculation['sgstAmount'],
                    'serviceCharge' => $taxesCalculation['serviceCharge'],
                    'serviceChargeAmount' => $taxesCalculation['serviceChargeAmount'],
                    'serviceChargeTax' => $taxesCalculation['serviceChargeTax'],
                    'serviceChargeTaxAmount' => $taxesCalculation['serviceChargeTaxAmount'],
                    'visitId' => $visitId,
                    'orderType' => 'order',
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );
                $insertOrder = Order::create($insert);
                $lastOrderId = $insertOrder['id'];
            }

            //If order created/updated, then insert the KOT and Items
            if (isset($lastOrderId)) {

                if (!$orderData) {
                    //to get unique orderId multyply the last inserted id with random no and update in order table
                    $uniqueId = 'BN' . rand(100, 999) * $lastOrderId;
                    $update = array('orderId' => $uniqueId);
                    $updateOrder = Order::where('id', $lastOrderId)->update($update);
                }

                $itemPrice = 0;

                //generate kot no for order
                $kotNumber = CommonFunction::generateKot();
                $kotInsert = array(
                    'orderId' => $lastOrderId,
                    'status' => 2,
                    'kotNumber' => $kotNumber,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );

                //insert in kot table
                $insertOrderKot = OrderKot::create($kotInsert);

                if ($insertOrderKot['id']) {
                    foreach ($cartItems as $key => $val) {
                        if (!empty($val)) {
                            extract($val);

                            //Get the Item Price from DB
                            $itemPrice = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $itemId);
                            $price = $itemPrice;

                            //If dscount is available for the day
                            if ($discountData) {
                                $disData = CommonFunction::getDiscountedPrice($discountData, $itemId, $itemPrice);
                                if (!empty($disData)) {
                                    if($disData['discountPrice'] != 0)
                                        $price = $disData['discountPrice'];
                                }
                            }
                            //If Happy Hour is available for the day
                            if ($happyHourData) {
                                $hhData = CommonFunction::getHappyHourPrice($happyHourData, $itemId, $itemPrice);

                                if (!empty($hhData)) {
                                    if (isset($hhData['happyHourPrice'])) {
                                        $price = $hhData['happyHourPrice'];
                                    } else {
                                        $hhOffer = $hhData;
                                    }
                                }
                            }

                            //if Happy Hour Offer have buy-get free then calculate the price
                            if (isset($hhOffer)) {
                                $itemPrice = CommonFunction::getOfferPrice($hhOffer, $qty, $itemPrice);
                            } else {
                                $itemPrice = $price * $qty;
                            }

                            //insert kot item in kot item table
                            $insert = array(
                                'orderId' => $lastOrderId,
                                'kotId' => $insertOrderKot['id'],
                                'itemId' => $itemId,
                                'itemName' => $itemName,
                                'quantity' => $qty,
                                'amount' => $itemPrice,
                                'unitPrice' => $price,
                                'createdAt' => $this->entryDate,
                                'updatedAt' => $this->entryDate,
                            );
                            $insertKotItems = OrderKotItem::create($insert);
                            if ($insertKotItems['id']) {
                                $insertKotStatus = true;
                            }
                        }
                    }
                }
                $result = array('status' => 1, 'message' => "Order created successfully", 'orderId' => $uniqueId);
            } else {
                $result = array('status' => 0, 'message' => "Somethig went wrong");
            }
        } else {
            $result = array('status' => 0, 'message' => "Cart is empty");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Status Change Function for all Tables
    public function changeKotStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kotId' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }   
        //print_r($request->all()); exit;

        //'1:pending, 2:accepted , 3:ready, 4:cancelled':- kot status
        $update = DB::table('bnOrderKots')
            ->where(['id' => $request->kotId])
            ->update(['status' => $request->status, 'updatedAt' => date("Y-m-d H:i:s")]);

        if ($update == 1) 
        {
            $msg = 'Your order status is changed';
            $moduleType = 'changeOrderKotStatus';

            if ($request->status == 2) {
                $msg = 'Your order is accepted';
                $moduleType = 'orderAccept';
                $title = 'Order accepted';

                //calculate remaining order price
                $orderKot = OrderKot::select('bnOrderKots.orderId', 'o.barId')
                    ->join('bnOrders as o', 'o.id', '=', 'bnOrderKots.orderId')
                    ->where(['bnOrderKots.id' => $request->kotId])
                    ->first();
                    
                if (!empty($orderKot)) {
                    $orderId = $orderKot->orderId;
                    $barId = $orderKot->barId;

                    //function to calculate amount on cancle kot
                    $amountCal = CommonFunction::calculateRemainingAmount($orderId, $barId);
                    $updateOrder = Order::where(['id' => $orderId])->update($amountCal);
                }
            } 
            else if ($request->status == 4) {
                $msg = 'Your order is cancelled ';
                $moduleType = 'orderReject';
                $title = 'Order cancelled';

                // Delete the KOT items
                $updateOrder = OrderKotItem::where(['kotId' => $request->kotId])->delete();
                if ($updateOrder) {
                    //calculate remaining order price
                    $orderKot = OrderKot::select('bnOrderKots.orderId', 'o.barId')
                        ->join('bnOrders as o', 'o.id', '=', 'bnOrderKots.orderId')
                        ->where(['bnOrderKots.id' => $request->kotId])
                        ->first();
                    if (!empty($orderKot)) {
                        $orderId = $orderKot->orderId;
                        $barId = $orderKot->barId;
                        //function to calculate amount on cancle kot
                        $amountCal = CommonFunction::calculateRemainingAmount($orderId, $barId);
                        $updateOrder = Order::where(['id' => $orderId])->update($amountCal);
                    }
                }
            }

            //Send NOtificaiton fuctionality
            $orderKotData = CommonFunction::GetSingleRow('bnOrderKots', 'id', $request->kotId);
            if ($orderKotData) {
                $orderData = CommonFunction::GetSingleRow('bnOrders', 'id', $orderKotData->orderId);
                if ($orderData) {
                    $userId = $orderData->userId;
                    $orderKotId = $request->kotId;

                    $bar = Bar::select('bnBars.barName', 'bnBars.logo', 'a.address')
                        ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
                        ->where(['bnBars.id' => $orderData->barId])->first();
                    if (!empty($bar)) {
                        $message = $msg . ' at ' . ucwords(strtolower($bar->barName));

                        $record = array('moduleType' => $moduleType, 'moduleId' => $orderKotId, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = UserNotification::create($record);

                        if ($not['id']) {
                            $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens

                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $orderKotId, 'userId' => $userId, 'orderId' => $orderData->orderId, 'uniqueId' => $orderData->orderId, 'title' => $title, 'msg' => $message, 'barId' => $orderData->barId, 'barName' => $bar->barName, 'address' => $bar->address, 'logo' => $bar->logo);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }
                    }
                }
            }
            //Delete the KOT
            if ($request->status == 4) {
                $updateOrderKot = OrderKot::where(['id' => $request->kotId])->delete();
            }
            $result = array('status' => 1, 'message' => $msg);
        } 
        else {
            $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
        }
        return response()->json($result);
    }

    // Function to update Order Serve Status - After this no deletion allow of orders
    public function updateKotServeStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kotId' => 'required',
            'orderId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }   
        // print_r($request->all()); exit;

        $update = DB::table('bnOrderKots')
            ->where(['id' => $request->kotId, 'orderId' => $request->orderId])
            ->update(['serveStatus' => 1, 'updatedAt' => $this->entryDate]);

        if ($update) {
            $result = array('status' => 1, 'message' => "Order status has been updated successfully");
        } 
        else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        return response()->json($result);
    }

    //This function is used for get pending order kots list for Desktop notification(Bar Panel)
    public function getPendingOrderKots(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $barId = $request->barId;
        $today = date('Y-m-d');

        // Get Pending Orders List from Patron App
        $orderKot = OrderKot::select('b.barName', 'o.orderId', 'bnOrderKots.kotNumber')
            ->where(['bnOrderKots.status' => 1])
            ->join('bnOrders as o', 'o.id', '=', 'bnOrderKots.orderId')
            ->where([
                'o.barId' => $barId,
                'o.orderType' => 'order',
                'o.status' => 1,
                'o.orderDate' => $today,
            ])
            ->leftJoin('bnBars as b', 'b.id', '=', 'o.barId')
            ->orderBy('bnOrderKots.id', 'desc')
            ->get();

        // Get Closed Orders List
        $orders = Order::select('bnOrders.id', 'bnOrders.orderId', 'bnOrders.paymentStatus', 'b.barName', 'p.firstName', 'p.lastName', 'p.userId', 'v.sittingType', 'v.tableStandingNo')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnOrders.userId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            ->where([
                'bnOrders.status' => 3,
                'bnOrders.paymentStatus' => 0,
                'bnOrders.barId' => $barId,
                'bnOrders.orderType' => 'order',
                'bnOrders.orderDate' => $today,
            ])
            ->orderBy('bnOrders.id', 'desc')
            ->get();

        if (count($orderKot) > 0 || count($orders) > 0) {
            $result = array('status' => 1, 'message' => 'Data found', 'data' => $orderKot, 'orders' => $orders);
        } else {
            $result = array('status' => 0, 'message' => 'No data found');
        }

        return response()->json($result);
    }

    // Function to update order payment status, and create entry in transaction
    public function updatePaymentStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'paymentMode' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }   
        //print_r($request->all());exit;

        $order = Order::select('bnOrders.id', 'bnOrders.userId', 'bnOrders.barId', 'bnOrders.totalPrice', 'bnOrders.grandTotal', 'bnOrders.paymentStatus', 'v.requestId', 'v.requestType') 
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            ->where(['bnOrders.orderId' => $request->orderId])
            ->first();

        if ($order) {
            if($order->paymentStatus == 0) {
                $paymentWithTax = $order->grandTotal;
                $paymentWithoutTax = $order->totalPrice;

                // Get the advance money, if available
                if($order->requestType == 'table') {
                    $tokenMoney = CommonFunction::GetSingleField('bnBookTable', 'tokenMoney', 'id', $order->requestId);
                    $paymentWithTax = $order->grandTotal - $tokenMoney;
                    $paymentWithoutTax = $order->totalPrice - $tokenMoney;
                }
                else if($order->requestType == 'event') {
                    $event = EventBooking::select('bnEventBookings.totalSeats', 'e.coverCharges')
                        ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
                        ->where(['bnEventBookings.id' => $order->requestId])
                        ->where('e.coverCharges', '>', 0)
                        ->first();
                    if(!empty($event)) {
                        $coverCharges = $event->totalSeats * $event->coverCharges;
                        $paymentWithTax = $order->grandTotal - $coverCharges;
                        $paymentWithoutTax = $order->totalPrice - $coverCharges;
                    }
                }
            
                $update = DB::table('bnOrders')
                    ->where(['orderId' => $request->orderId])
                    ->update(['paymentStatus' => 1, 'updatedAt' => date("Y-m-d H:i:s")]);

                if ($update) {
                    //entry in transaction table
                    $param = array(
                        'userId' => $order->userId, 
                        'barId' => $order->barId, 
                        'description' => 'Paid for order '.$request->orderId, 
                        'moduleType' => 'order',
                        'moduleId' => $order->id, 
                        'totalPrice' => $paymentWithoutTax, 
                        'amount' => $paymentWithTax, 
                        'paymentMode' => $request->paymentMode, 
                        'remark' => $request->remark, 
                        'paymentDate' => date("Y-m-d"), 
                        'status' => 1, 
                        'createdAt' => $this->entryDate, 
                        'updatedAt' => $this->entryDate
                    );
                    DB::table('bnTransactions')->insertGetId($param);

                    // Create an entry in notification table and send PUSH Notification
                    $userId = $order->userId;
                    $bar = Bar::select('bnBars.barName', 'bnBars.logo', 'a.address')
                        ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
                        ->where(['bnBars.id' => $order->barId])->first();
                    if (!empty($bar)) {
                        $message = 'Your order has been completed at ' . ucwords(strtolower($bar->barName)); //orderCompleted
                        $record = array('moduleType' => 'orderPaymentUpdated', 'moduleId' => $order->id, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = UserNotification::create($record);
     
                        if ($not['id']) {
                            $deviceToken = CommonFunction::getUserDeviceData($userId);
                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'orderPaymentUpdated', 'moduleId' => $order->id, 'userId' => $userId, 'title' => 'Order Completed', 'msg' => $message, 'barId' => $order->barId, 'barName' => $bar->barName, 'address' => $bar->address, 'logo' => $bar->logo);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }
                    }

                    $result = array('status' => 1, 'message' =>  'Payment status has been updated successfully');
                }
                else {
                    $result = array('status' => 0, 'message' => 'Something went wrong, please try again later');
                }
            }
            else {
                $result = array('status' => 0, 'message' => 'Order payment is already done');
            }
        }
        else
            $result = array('status' => 0, 'message' => 'Invalid Order');
        
        return response()->json($result);
    }

    // Function to update order status
    public function changeOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }   
        //print_r($request->all());exit;

        $orderId = $request->orderId;
        $order = Order::select('bnOrders.id', 'bnOrders.userId', 'bnOrders.barId', 'bnOrders.grandTotal', 'bnOrders.orderType', 'bnOrders.paymentStatus', 'bnOrders.visitId', 'b.barName') 
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->where(['bnOrders.id' => $orderId])
            ->first();

        if ($order) {
            if($order->grandTotal == 0) {
                // Delete the order now
                DB::table('bnOrders')->where(['id' => $orderId])->delete();
                DB::table('bnOrderKots')->where(['orderId' => $orderId])->delete();
                DB::table('bnOrderKotItems')->where(['orderId' => $orderId])->delete();
                $result = array('status' => 2, 'message' => 'Order has been closed');
            } 
            else {
                // Update the order status
                DB::table('bnOrders')
                    ->where(['id' => $orderId])
                    ->update(['status' => 3, 'updatedAt' => $this->entryDate]);
                
                $userId = $order->userId;
                $visitId = $order->visitId;
                if($order->grandTotal > 0)
                    $status = 1;
                else
                    $status = 0;

                if($order->orderType == 'order') {
                    // Update the visit status
                    DB::table('bnVisits')
                        ->where(['id' => $visitId])
                        ->update(['status' => '4', 'updatedAt' => $this->entryDate]);

                    //Delete entry from geo tagging
                    DB::table('bnGeoTaggings')->where(['visitId' => $visitId])->delete();

                    $message = 'Your order has been completed and e-bill has generated for ' . $order->orderId . ' at ' . ucwords(strtolower($order->barName));
                    $moduleType = 'orderCompleted';
                }
                else {
                    $message = 'Your order has been completed at ' . ucwords(strtolower($order->barName));
                    $moduleType = 'preOrderCompleted';
                }

                $record = array('moduleType' => $moduleType, 'moduleId' => $orderId, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                $not = UserNotification::create($record);

                if ($not['id']) { 
                    $deviceToken = CommonFunction::getUserDeviceData($userId);

                    if ($deviceToken) {
                        //Firebase Notification
                        $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $orderId, 'userId' => $userId, 'title' => 'Order Completed', 'msg' => $message, 'status' => $status, 'visitId' => $visitId);
                        $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                    }
                }

                $result = array('status' => 1, 'message' => 'Order has been closed');
            }
        }
        else
            $result = array('status' => 0, 'message' => 'Invalid Order');
        
        return response()->json($result);
    }

}