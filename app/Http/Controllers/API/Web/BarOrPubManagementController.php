<?php
namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Helper\WebCommonFunction;
use CommonFunction; 
use App\User;
use App\Bar;
use App\TopBar;
use App\BarTiming;
use App\Address;
use App\BarRating;
use DB;
use Mail; 
use Hash;
use App\Mail\SendMail;
//use Illuminate\Support\Facades\Mail;

class BarOrPubManagementController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //This function is used for add patron by admin
    public function addBarOrPub(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barName' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            //'password' => 'required',
            //'address' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors()->first());
            return response()->json($result);
        }

        $password = mt_rand(100000,999999);
        //Check Email is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->email, 'email', 'B');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }

        //Check Phone is already registerd or not or not
        $query = WebCommonFunction::checkValueExist($request->phone, 'phone', 'B');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }

        $day = array('sunday','monday','tuesday','wednesday','thursday','friday','saturday');
        
        //inser basic bar data
        $userParam = array(
            'userName' => $request->userName, 
            'email' => $request->email, 
            'phone' => $request->phone,
            'userType' => 'B',
            'platform' => 'bn',
            'status' => 3,
            'password' => bcrypt($password)
        );
        $user = User::create($userParam);

        if ($user['id']) {

            $appSetting = CommonFunction::getAppSettings(); 
            //Insert bar data
            $barParam = array(
                'userId' => $user['id'], 
                'type' => 1, 
                'barName' => $request->barName,
                'cgst' => $appSetting['cgst'],
                'sgst' => $appSetting['sgst'],
                'serviceCharge' => $appSetting['serviceCharge'],
                'serviceChargeTax' => $appSetting['serviceChargeTax'],
            );
            $bar = Bar::create($barParam);
            //Inser bar timing data 
            if($bar['id']){
                foreach($day as $key=>$day){
                    $timing = array(
                        'weekDay' => $day,
                        'openingStatus' => 0,
                        'openingTime' => NULL,
                        'closingTime' => NULL,
                        'barId' => $bar['id'],
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    BarTiming::create($timing);
                }

                //Entry in Bar Rating table
                $ratingData = array(
                    'rating' => 0,
                    'totalUsers' => 0,
                    'barId' => $bar['id'],
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                ); 
                BarRating::create($ratingData);
            }
            //Inser data in Address table
            $address = array(
                'userId' => $user['id'],
                //'address' => $request->address,
                //'latitude' => $request->latitude,
                //'longitude' => $request->longitude,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            ); 
            Address::create($address);

            $emailParam = array(
                'name'=> $request->barName, 
                'password'=>$password, 
                'email'=>$request->email,
                'link' => $request->loginLink
            );
            try{ 
                Mail::send('emails.addBarOrPub', $emailParam, function ($message) use ($emailParam) {
                    $message->from('noreply@barnightslive.com', config('app.name'));
                    $message->subject('Login Details');
                    //$message->to('ravindra@coretechies.com');
                    $message->to($emailParam['email']);
                });
                $result['status'] = 1;
                $result['message'] = "Bar has been added successfully!";
                $result['password'] = $password;
            }catch(\Exception $e ){
                $error = $e->getMessage();
                $result['status'] = 0;
                $result['message'] = $error;
            }
        } else {
            $result['status'] = 0;
            $result['message'] = "Internal server error.Try again!";
        }
        return response()->json($result);
    }

    public function barList(Request $request)
    {
        //echo "<pre>"; print_r($request->all());exit;
        $limit = 15;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }
        $list = Bar::getAllList($startFrom,$limit,$request->keyword,$request->status,$request->city);
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['limit'] = $limit;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }
    }

     //This function is used for get to spender list till the earlier month
    public function getTopBarList(Request $request)
    {
        $previous_month = strtolower(date('F-Y', strtotime(date('Y-m')." -1 month")));
        $firstDay = date("Y-m-01");

        $limit = 10;
        $startFrom = 0;
        if (isset($request->page)) {
            $startFrom = $request->page * $limit;
        }

        // Get overall Top 10 Bars of Current Month
        $topBars = DB::table('bnTransactions as t')
            ->select('bar.id as barId', 'bar.barName', 'bar.logo', 'address.cityId', DB::raw("SUM(t.amount) as totalSales"))
            ->join('bnBars as bar', 'bar.id', '=', 't.barId')
            ->leftJoin('bnAddresses as address', 'address.userId', '=', 'bar.userId')
            ->where('t.paymentDate', '>=', $firstDay)
            ->where(['t.status' => 1])
            ->orderBy('t.amount', 'desc')
            ->offset($startFrom)->limit($limit)
            ->groupBy('t.barId')->get();
        
        $barToppers = array();
        if (!empty($topBars)) {
            foreach ($topBars as $key => $row) {
                $row->position = $key+1;
                $row->totalSales = number_format($row->totalSales, 2);
                $barToppers[] = $row;
            }
        }
        if(!empty($topBars)){
            $result = array('status' => 1, 'message' => 'Top bars get successfully', 'data' => $barToppers);
        }
        else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
        return response()->json($result);
        
        /*$list = TopBar::getTopBarList($previous_month,$limit,$startFrom);
        if($list && count($list)){
            $result['status'] = 1;
            $result['data'] = $list;
            $result['message'] = "Data found!";
            return response()->json($result);
        }else{
            $result['status'] = 0;
            $result['message'] = "Data not found!";
            return response()->json($result);
        }*/
    }

    // Function to Activate Bar Profile
    public function acceptBarRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $userId = $request->userId;
        $barId = $request->barId;
        $status = 4;

        /* === Bar Profile Validation Start === */
        $pendingItems = array();
        $bar = DB::table('bnBars as b')->select('b.*', 'a.address')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'b.userId')
            ->where(['b.id' => $barId])
            ->groupBy('b.id')->first();
        
        if(!empty($bar)) {
            if($bar->address == NULL)
                $pendingItems[] = "Profile basic information is pending.";

            // Check Tax Info
            if($bar->cgst == 0 || $bar->sgst == 0)
              $pendingItems[] = "GST rates are pending.";

            // Service Tax Info
            if($bar->serviceCharge > 0 && $bar->serviceChargeTax == 0)
              $pendingItems[] = "Tax on Service Charge is pending.";
        }

        // Opening-Closing Time Validation
        $timing = DB::table('bnBarTimings')->select('id')
            ->where(['barId' => $barId, 'openingTime' => NULL, 'closingTime' => NULL])
            ->get();
        if(count($timing) == 7)
            $pendingItems[] = "Opening Closing timing is pending.";

        // KYC Documents Validation
        $licTypes = CommonFunction::licenseTypes();
        $kycDocs = array();
        foreach ($licTypes as $key => $value) {
            $kyc = DB::table('bnBarLicenses')->select('id')
                ->where(['barId' => $barId, 'licenseTypeId' => $key])
                ->first();
            if(empty($kyc)) {
                $kycDocs[] = $value;
            }
        }
        if(!empty($kycDocs)) {
            $docs = implode(", ", $kycDocs);
            $pendingItems[] = "KYC Documents: ".$docs." need to be upload.";
        }

        // Banners Validation
        $banners = DB::table('bnBarBanners')->select('id')
            ->where(['barId' => $barId])->first();
        if(empty($banners))
            $pendingItems[] = "One banner image needs to be upload.";

        // Menu Item Validation
        $menu = DB::table('bnBarItems')->select('id')
            ->where(['barId' => $barId])
            ->offset(0)->limit(5)->get();
        if(count($menu) != 5)
            $pendingItems[] = "Add at least 5 menu items.";

        if(!empty($pendingItems)){
            $result = array('status' => 0, 'message' => "Profile is not completed", 'data' => $pendingItems);
            return response()->json($result);
        }
        /* === Bar Profile Validation End === */

        /* === Profile Activation Code Start === */
        $param = array('status' => $status, 'updatedAt' => date("Y-m-d H:i:s"));
        $update = DB::table('bnUsers')->where('id', $userId)->update($param);

        if ($update) {
            // Send Notification Email To Admin
            CommonFunction::sendActivationEmailToAdmin($userId);
            $result = array('status' => 1, 'message' => "Status has been updated successfully");
        }
        else
            $result = array('status' => 0, 'message' => "Something went wrong");
        
        return response()->json($result);
    }

    // Function to change user status
    public function rejectBarRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            //'status' => 'required',
            'reason' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $userId = $request->userId;
        $status = 3; //$request->status;
        $inactiveReason = $request->reason;

        $param = array('status' => $status, 'inactiveReason' => $inactiveReason, 'updatedAt' => date("Y-m-d H:i:s"));
        $update = DB::table('bnUsers')->where('id', $userId)->update($param);

        if ($update) {
            // Send Notificaiton email to Bar
            $bar = Bar::select('u.email', 'bnBars.barName')->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
                ->where(['bnBars.userId' => $userId, 'u.userType' => 'B'])->first();
                    
            if(!empty($bar)) {
                $barName = $bar->barName;
                $email = $bar->email;
                //create message
                $msg = '<h1 class="title">Hello '.$barName.'</h1>
                        <p>Your profile activation request has been rejected. Please find the reason below and correct your profile.</p>
                        <p><b>Reason:</b> '.$inactiveReason.'</p>';
                $msg .= '<p>Please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                $data = array(
                    'msg' => $msg,
                    'subject' => "Profile Activation Request Rejected",
                );
                Mail::to($email)->send(new SendMail($data));
            }

            $result = array('status' => 1, 'message' => "Status has been updated successfully");
        }
        else
            $result = array('status' => 0, 'message' => "Something went wrong");
        
        return response()->json($result);
    }
}