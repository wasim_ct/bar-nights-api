<?php
namespace App\Http\Controllers\API\Mobile;

use App\Friend;
use App\Http\Controllers\Controller;
use App\Story;
use App\Patron;
use App\UserMediaFile;
use App\UserNotification;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;
use DB;
use Carbon\Carbon;

class StoryController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    // Add Story Function
    public function addStory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        extract($_POST);

        //Check media is exist or not in the request
        if ($request->hasFile("mediaName")) {
            $filenames = $request->file('mediaName');
            //print_r($filenames);exit;
            foreach($filenames as $file){
                $ext = $file->getClientOriginalExtension(); //get extention of file
                $mediaName = time() * rand() . "." . $ext;
                //upload media to folder
                $upload = $file->move("uploads/patrons/story/", $mediaName);
                if (!empty($upload)) {
                    //Create an entry in Story table
                    $param = array('userId' => $userId, 'visibility' => $visibility, 'mediaName' => $mediaName, 'mediaType' => $mediaType, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    if (isset($jsonData)) {
                        $param['jsonData'] = $jsonData;
                    }

                    $storyId = DB::table('bnStories')->insertGetId($param);

                    if ($storyId) {
                        
                        // Send Notification to the friends of the user
                        $friendData = Friend::select('friendId')->where(['userId' => $userId])->get();
                        if (count($friendData) > 0) {
                            $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();
                            
                            $name = 'Someone has';
                            if (!empty($user)) {
                                $name = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has';
                            }
                            
                            foreach ($friendData as $fd) {
                                $message = $name . ' posted a new story';

                                // If friend is tagged, then need to change the message
                                if(isset($friendIds) && !empty($friendIds)){
                                    
                                    $ids = explode(",", $friendIds);

                                    if (in_array($fd->friendId, $ids)){
                                        $message = $name . ' tagged you in a story';
                                    }
                                }

                                $record = array('moduleType' => 'story', 'moduleId' => $storyId, 'description' => $message, 'userId' => $fd->friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                $not = UserNotification::create($record);

                                if ($not['id']) {
                                    $notCreated = true;
                                    $deviceToken[] = CommonFunction::getUserDeviceData($fd->friendId);
                                }
                            }
                            
                            //If notification created, then send push notification also
                            if (isset($notCreated)) 
                            {
                                $deviceToken = call_user_func_array('array_merge', $deviceToken);
                                if ($deviceToken) {
                                    //Firebase Notification
                                    $payLoadData = array('moduleType' => 'newsfeed', 'moduleId' => $storyId, 'title' => 'New story posted', 'msg' => $message);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                }
                            }
                        }
                        $result = array('status' => 1, 'message' => 'Story has been posted successfully');
                    } 
                    else {
                        $result = array('status' => 0, 'message' => 'Something went wrong');
                    }
                }
                else {
                    $result = array('status' => 0, 'message' => 'Media file not uploaded');
                }
            }
            $result = array('status' => 1, 'message' => 'Story has been posted successfully');
        }
        else {
            $result = array('status' => 0, 'message' => 'No media file selected');
        }

        return response()->json($result);
    }

    // Get Stories Function, returns all users list which have stories
    public function getStories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
        
        //Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 10;
        }
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //Get all the stories
        $stories = Story::select('bnStories.id','bnStories.userId', 'p.firstName', 'p.lastName', 'p.profilePic')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnStories.userId')
            ->where(['bnStories.isDeleted' => 0])
            ->where('bnStories.userId' , '!=', $userId)
            ->orderBy('bnStories.id', 'desc')
            ->groupBy('bnStories.userId')
            ->offset($startFrom)->limit($limit)->get()->toArray();
        
        //get count of stories
        $storyCount = Story::select('id')->where(['isDeleted' => 0])->groupBy('userId')->get();

        if (count($stories) > 0) {
            foreach($stories as $key => $value){
                $storySeen =DB::table('bnStorySeens')->select('id')->where(['storyId' => $value['id'], 'userId'=> $value['userId']])->get();
                if($storySeen && count($storySeen)){
                    $stories[$key]['storySeen'] = 1;
                    
                }else{
                    $stories[$key]['storySeen'] = 0;
                }
            }
            array_multisort(array_column($stories, "storySeen"), SORT_ASC, $stories );
            //Converting NULL to "" String
            array_walk_recursive($stories, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Stories has been recevied successfully', 'data' => $stories, 'totalStories' => count($storyCount));
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    // Get User Stories Function, of a single User
    public function getUserStories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
       
        //Get all the stories
        $stories = Story::select('bnStories.id', 'bnStories.mediaName', 'bnStories.mediaType', 'bnStories.jsonData')
            //->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnStories.userId')
            ->where(['bnStories.isDeleted' => 0, 'bnStories.userId' => $userId])
            ->orderBy('bnStories.id', 'asc')
            ->get()->toArray();
        
        if (count($stories) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($stories, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Stories has been recevied successfully', 'data' => $stories);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        return response()->json($result);
    }

    // Add Seen Story Function
    public function addSeenStory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'storyId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $storyUserId = CommonFunction::GetSingleField('bnStories', 'userId', 'id', $storyId);
        if($storyUserId) {
            if($storyUserId != $userId) {
            
                $alreadySeen = DB::table('bnStorySeens')->select('id')
                    ->where(['userId' => $userId, 'storyId' => $storyId])->first();

                if(empty($alreadySeen)) {
                    $param = array('userId' => $userId, 'storyId' => $storyId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $seenId = DB::table('bnStorySeens')->insertGetId($param);

                    if ($seenId) {
                        $result = array('status' => 1,'message'=>'Story has been viewed successfully');
                    }
                    else 
                        $result = array('status' => 0, 'message' => 'Something weng wrong');
                } 
                else 
                    $result = array('status' => 0, 'message' => 'Already seen');
            }
            else 
                $result = array('status' => 0, 'message' => 'Views can not be updated');
        }
        else 
            $result = array('status' => 0, 'message' => 'Invalid story');

        return response()->json($result);
    }

    // Delete Story Function
    public function deleteStory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'storyId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $record = array('isDeleted' => 1, 'updatedAt' => $this->entryDate);
        $update = Story::where(['id' => $storyId, 'userId' => $userId])->update($record);
        if($update)
            $result = array('status' => 1, 'message' => "Story has been deleted successfully");
        else
            $result = array('status' => 1, 'message' => "Something went wrong, please try again later");
        return response()->json($result);
    }

    // Delete Stories - after 24 hrs
    public function deleteDailyStories(Request $request)
    {
        //echo Carbon::now()->toDateTimeString(); 
        $sql = "DELETE FROM bnStories WHERE createdAt < (NOW() - INTERVAL 24 HOUR)";
        $delete = DB::select(DB::raw($sql));
        $result = array('status' => 1, 'message' => "Stories has been deleted");
        return response()->json($result);
    }
}