<?php
namespace App\Http\Controllers\API\Mobile;

use App\BarLicense;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->entryDate = date("Y-m-d H:i:s");
    }
    public function userLogin(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        extract($_POST);

        if (Auth::attempt(['email' => request('username'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        } else if (Auth::attempt(['userName' => request('username'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        } else if (Auth::attempt(['phone' => request('username'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        }

        if (isset($login)) {
            $user = Auth::user();
            $result = array('status' => 1, 'message' => 'Login successful', 'id' => $user->id);
        } else {
            $result = array('status' => 0, 'message' => 'Invalid login credentials');
        }

        return response()->json($result);
    }

    //Logout function
    public function UserLogout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    // Testing function to upload file
    public function uploadTestImage(Request $request) {

        $param = array(
            'description' => 'Cron Job Testing',
            'barId' => 1,
            'createdAt' => date("Y-m-d H:i:s"),
            'updatedAt' => date("Y-m-d H:i:s")
        );
        $id = DB::table('bnActivityLogs')->insertGetId($param);
        
        /*if ($request->hasFile("offerImage")) {
            $file = $request->file("offerImage");
            $ext = $file->getClientOriginalExtension(); //get extention of file
            
            $offerImage = time() * rand() . "." . $ext;
            $upload = $file->move("public/uploads/bars/offers/", $offerImage);
            if (!empty($upload)) {
                $result = array('status' => 1, 'message' => "Image Uploaded", "image" => $offerImage, 'extention' => $ext);
                return response()->json($result);
            }
            else
            {
                $result = array('status' => 0, 'message' => "Not uploaded");
                return response()->json($result);
            }
        }
        else{
            $result = array('status' => 0, 'message' => "No file selected");
            return response()->json($result);
        }*/
    }

}