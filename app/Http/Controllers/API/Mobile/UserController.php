<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Patron;
use App\SupportEnquire;
use App\User;
use App\UserDevice;
use App\UserNotificationSetting;
use App\VerifyUser;
use CommonFunction;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Password;
use Validator;
use DB;
use App\Address;

class UserController extends Controller
{
    public $successStatus = 200;
    use SendsPasswordResetEmails;
    use ResetsPasswords;
    public $token;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        $this->middleware('guest');
        $this->entryDate = date("Y-m-d H:i:s");
    }
 
    //Register Function - Step 1
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userName' => 'required',
            // 'phone' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            // 'smToken' => 'required',
            'platform' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //check abusive keyword start
        if (CommonFunction::isAbusiveContent($request->userName)) {
            $result = array('status' => 0, 'message' => 'Username should not contain abusive word');
            return response()->json($result);
        }elseif(CommonFunction::isAbusiveContent($request->email)){
            $result = array('status' => 0, 'message' => 'Email should not contain abusive word');
            return response()->json($result);
        }elseif(CommonFunction::isAbusiveContent($request->firstName)){
            $result = array('status' => 0, 'message' => 'First name should not contain abusive word');
            return response()->json($result);
        }elseif(CommonFunction::isAbusiveContent($request->lastName)){
            $result = array('status' => 0, 'message' => 'Last name should not contain abusive word');
            return response()->json($result);
        }
        //check abusive keyword end

        //Check UserName is already registered or not or not
        $query = CommonFunction::checkValueExist($request->userName, 'userName', 'U');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Username is already registered');
            return response()->json($result);
        }

        //Check Email is already registered or not or not
        $query = CommonFunction::checkValueExist($request->email, 'email', 'U'); 
        if ($query) {
            $result = array('status' => 0, 'message' => 'Email is already registered');
            return response()->json($result);
        }

        //Check Phone is already registered or not or not
        /*$query = CommonFunction::checkValueExist($request->phone, 'phone', 'U');
        if ($query) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        }*/

        $status = 0; $email_verified_at = NULL;
        if(strtolower($request->platform) != 'bn') {
            $status = 1;
            $email_verified_at = $this->entryDate;
        }

        //Insertion Code Start
        $input = $request->all();
        $input['userType'] = 'U';
        $input['password'] = bcrypt($request->password);
        $input['smToken'] = $request->smToken;
        $input['platform'] = $request->platform;
        $input['status'] = $status;
        $input['email_verified_at'] = $email_verified_at; 
        $input['createdAt'] = $this->entryDate;
        $input['updatedAt'] = $this->entryDate;
        $user = User::create($input);

        if ($user['id']) {

            // Update App Downloads
            if(isset($osType) && !empty($osType) && isset($deviceId) && !empty($deviceId)) {
                CommonFunction::updateAppDownloads($osType, $deviceId);
            }

            //get file image
            $imageName = NULL;
            /*$file = $request->file("profilePic");
            if ($request->hasFile("profilePic")) {
                //get extention of file
                $ext = $file->getClientOriginalExtension();
                //encrypt the name of file
                $img = time() * rand() . "." . $ext;
                //upload image to folder
                $upload = $file->move("uploads/patrons/profile/", $img); //Path & File name need to pass
                if (!empty($upload)) {
                    $imageName = $img;
                }
            }*/

            //Now Create an entry in bnUsers
            $record = array('userId' => $user['id'], 'firstName' => $request->firstName, 'lastName' => $request->lastName, 'profilePic' => $imageName, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            $patron = Patron::create($record);

            //insert city into bnAddresses Table
            /*if(isset($request->city)) {
                $cityId = CommonFunction::getCityIdByName($request->city);
                if(!$cityId)
                    $cityId = 0;
                
                $address = array(
                    'userId' => $user['id'],
                    'cityId' => $cityId,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                ); 
                Address::create($address);
            }*/

            //Email Verification
            if($request->platform == 'bn') {
                $verifyUser = VerifyUser::create([
                    'user_id' => $user->id,
                    'token' => str_random(40),
                ]); 
                
                Mail::to($user->email)->send(new VerifyMail($user));
                $message = "Your account has been successfully created. Please check your email for the verification"; 
            }
            else {
                $message = "Your account has been successfully created.";
            }

            $result['status'] = 1;
            $result['message'] = $message;
            $result['token'] = ""; //$user->createToken('MyApp')->accessToken;
            $result['userName'] = $user->userName;
            $result['userId'] = $user->id;
        } else {
            $result['status'] = 0;
            $result['message'] = "User not registered";
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Register Function - Step 2
    public function registerStep2(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city' => 'required',
            'phone' => 'required',
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        //Check Phone is already registered or not
        $exist = DB::table('bnUsers')->select('id')
            ->where(['phone' => $request->phone, 'userType' => 'U'])
            ->where('id', '!=', $userId)
            ->first();
        if (!empty($exist)) {
            $result = array('status' => 0, 'message' => 'Phone number is already registered');
            return response()->json($result);
        } 

        //check abusive keyword start
        if (CommonFunction::isAbusiveContent($request->city)) {
            $result = array('status' => 0, 'message' => 'City should not contain abusive word');
            return response()->json($result);
        } elseif(CommonFunction::isAbusiveContent($request->phone)){
            $result = array('status' => 0, 'message' => 'Phone number should not contain abusive word');
            return response()->json($result);
        }
        //check abusive keyword end

        // Check City Exist or not with the city name
        if(isset($request->city) && !empty($request->city)) {
            $cityId = CommonFunction::getCityIdByName($request->city);
            if(!$cityId) {
                $result = array('status' => 0, 'message' => 'Invalid city');
                return response()->json($result);
            }
        }

        // Update the phone number
        $input['phone'] = $request->phone;
        $input['updatedAt'] = $this->entryDate;
        $update = User::where('id', $userId)->update($input);

        if ($update) {
            //get file image
            if ($request->hasFile("profilePic")) {
                $file = $request->file("profilePic");
                $ext = $file->getClientOriginalExtension();
                $img = time() * rand() . "." . $ext;
                
                //upload image to folder
                $upload = $file->move("uploads/patrons/profile/", $img); 
                if (!empty($upload)) {
                    // Now update profile pic
                    $input = array(
                        'profilePic' => $img,
                        'updatedAt' => $this->entryDate
                    );
                    $updateImage = Patron::where('userId', $userId)->update($input);
                }
            }

            //insert city into bnAddresses Table
            $address = array(
                'userId' => $userId,
                'cityId' => $cityId,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            ); 
            Address::create($address);

            $result['status'] = 1;
            $result['message'] = "Profile has been updated successfully";
        } else {
            $result['status'] = 0;
            $result['message'] = "User not registered";
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to Resend Verification Email
    public function resendVerifyEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        if(CommonFunction::isAbusiveContent($request->email)){
            $result = array('status' => 0, 'message' => 'Email should not contain abusive word');
            return response()->json($result);
        }

        $user = User::select('*')->where(['email' => $email, 'userType' => 'U'])->first();
        if (!empty($user)) {
            // Email Verification
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40),
            ]); 
            
            Mail::to($user->email)->send(new VerifyMail($user));

            $result['status'] = 1;
            $result['message'] = "Please check your email for the verification";
            $result['userName'] = $user->userName;
            $result['userId'] = $user->id;
        } else {
            $result['status'] = 0;
            $result['message'] = "User not registered";
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        //print_r($request->all());exit;
        $validator = Validator::make($request->all(), [
            'userName' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        if (Auth::attempt(['email' => request('userName'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        } else if (Auth::attempt(['userName' => request('userName'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        } else if (Auth::attempt(['phone' => request('userName'), 'password' => request('password'), 'userType' => 'U'])) {
            $login = true;
        }
        
        if (isset($login)) {
            $user = Auth::user();
            if ($user->email_verified_at) {
                if ($user->status == 0) {
                    $result = array('status' => 0, 'message' => 'Your account is inactive', 'contactEmail' => config('app.contact_email'));
                }
                else if ($user->status == 2) {
                    $result = array('status' => 0, 'message' => 'Your account has been blocked', 'contactEmail' => config('app.contact_email'));
                }
                else {
                    //Update the device Data of the User
                    $data = array('deviceId' => $deviceId, 'deviceToken' => $deviceToken, 'osVersion' => $osVersion, 'modelName' => $modelName, 'loginStatus' => 1);

                    if(isset($osType))
                        $data['osType'] = $osType;

                    CommonFunction::updateUserDevice($data, $user->id);

                    $patronData = Patron::select('profilePic', 'firstName', 'lastName', 'bnPatrons.userId', 'c.city_name as cityName')
                        ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnPatrons.userId')
                        ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                        ->where('bnPatrons.userId', $user->id)->first();

                    $patronData['userName'] = $user->userName;
                    $patronData['email'] = $user->email;
                    $patronData['phone'] = $user->phone;

                    //Converting NULL to "" String
                    array_walk_recursive($patronData, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    }); 

                    // Generating the Token
                    $token = CommonFunction::createLoginToken($user->id); 
                    if($token == false)
                        $token = "";

                    $mediaPath = CommonFunction::getMediaPath();
                    $result = array('status' => 1, 'message' => 'Login successful', 'token' => $token, 'data' => $patronData, 'mediaPath' => $mediaPath);

                    //$user->createToken('MyApp')->accessToken
                }
            } else {
                $result = array('status' => 0, 'message' => 'Email is not verified');
            }

        } else {
            $result = array('status' => 0, 'message' => 'Invalid login credentials');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Logout function
    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
        
        //$logout = DB::table('bnUserDevices')->where(['userId' => $userId])->delete();

        $logout = DB::table('bnLoginTokens')->where(['token' =>$request->token, 'userId' => $userId])->delete();
        if($logout) {
            $result = array('status' => 1, 'message' => 'Logout Successful');    
        }
        else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }
        return response()->json($result);

        /*$request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);*/
    }

    /**
     * my profile api
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyProfile(Request $request)
    {
        /*$user = Auth::user();
        $response = array('status' => $this->successStatus, 'data' => $user);*/

        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //First check the user is exist or not
        $userExist = CommonFunction::GetSingleField('bnUsers', 'id', 'id', $userId);
        if ($userExist) {
            $patronData = Patron::select('bnPatrons.profilePic', 'bnPatrons.firstName', 'bnPatrons.lastName', 'u.email', 'u.phone', 'c.city_name as cityName')
                ->join('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnPatrons.userId')
                ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                ->where('bnPatrons.userId', $userId)->first();

            //Converting NULL to "" String
            array_walk_recursive($patronData, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'Profile has been received successfully', 'data' => $patronData, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Function to send user detail with Header
    public function getUserProfile(Request $request)
    {
        $user = Auth::user();
        $result = array('status' => $this->successStatus, 'data' => $user);

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    /**
     * Change Password
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        extract($_POST);
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'oldPass' => 'required',
            'newPass' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        if (Auth::attempt(['id' => request('userId'), 'password' => request('oldPass')])) {
            $data = array('password' => bcrypt($request->newPass), 'updatedAt' => $this->entryDate);
            $changePassword = User::where('id', $userId)->update($data);

            if ($changePassword) {
                
                // Delete other Login Tokens
                if(isset($token) && isset($tokenStatus) && $tokenStatus == 1) {
                    DB::table('bnLoginTokens')->where(['userId' => $userId])->where('token', '!=', $request->token)->delete();
                }

                $result = array('status' => 1, 'message' => 'Password has been changed successfully');
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }

        } else {
            $result = array('status' => 0, 'message' => 'Current password does not match');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    public function checkCurrentPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'oldPass' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        if (Auth::attempt(['id' => request('userId'), 'password' => request('oldPass')])) {
            $result = array('status' => 1, 'message' => 'Password is correct');
        } else {
            $result = array('status' => 0, 'message' => 'Current password does not match');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    /**
     * send email reset link
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $emailExist = User::select('id')->where(['email' => $email, 'userType' => 'U'])->first();
        if ($emailExist) {
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );

            /* === Default file location of Auth  === */
            // vendor/laravel/framework/src/Illuminate/Auth/Notifications/ResetPassword.php
            // vendor/laravel/framework/src/Illuminate/Notifications/resources/views/email.blade.php

            if ($response) {
                $result = array('status' => 1, 'message' => 'We have e-mailed your password reset link. Please check your inbox');
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong. Please try again');
            }
        } else {
            $result = array('status' => 0, 'message' => 'No account found for this email');
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    /**
     * check which password broker is using
     *
     * @return \Illuminate\Http\Response
     */
    protected function broker()
    {
        return Password::broker('users');
    }

    // Function to Social Media Signup
    public function socialMediaSignup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'platform' => 'required',
            'smToken' => 'required',
            //'email' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // Update App Downloads
        if(isset($osType) && !empty($osType) && isset($deviceId) && !empty($deviceId)) {
            CommonFunction::updateAppDownloads($osType, $deviceId);
        }

        // Check social media token exist or not
        $query = User::select('bnUsers.phone', 'bnUsers.email', 'p.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'a.cityId', 'c.city_name as cityName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnUsers.id')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'p.userId')
            ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
            ->where(['bnUsers.userType' => 'U', 'bnUsers.smToken' => $smToken, 'bnUsers.platform' => $platform])->first(); //'bnUsers.email' => $email, 
        if(!empty($query)) {
            $data = array(
                'userId' => $query->userId,
                'phone' => $query->phone,
                'email' => $query->email,
                'firstName' => $query->firstName,
                'lastName' => $query->lastName,
                'cityId' => $query->cityId,
                'cityName' => $query->cityName,
                'profilePic' => $query->profilePic,
            );

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'User already registered with this social media account', 'data' => $data);
        }
        //Check Email is already registered or not
        else if(!empty($request->email)) {
            $query = CommonFunction::checkValueExist($request->email, 'email', 'U');
            if ($query) {
                // link platform and token with users
                $userId = $query->id;
                $data = array('platform' => $platform, 'smToken' => $smToken, 'updatedAt' => $this->entryDate);
                $update = User::where(['id' => $userId])->update($data);

                //Call patron data to send in response
                $query = User::select('bnUsers.phone', 'bnUsers.email', 'p.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'a.cityId', 'c.city_name as cityName')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnUsers.id')
                    ->leftJoin('bnAddresses as a', 'a.userId', '=', 'p.userId')
                    ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                    ->where(['bnUsers.id' => $userId])->first();

                $patronData = array();
                if(!empty($query)) {
                    $patronData = array(
                        'userId' => $query->userId,
                        'phone' => $query->phone,
                        'email' => $query->email,
                        'firstName' => $query->firstName,
                        'lastName' => $query->lastName,
                        'cityId' => $query->cityId,
                        'cityName' => $query->cityName,
                        'profilePic' => $query->profilePic,
                    );

                    // Generating the Token
                    $token = CommonFunction::createLoginToken($userId); 
                    if($token == false)
                        $token = "";
                    $patronData['token'] = $osType;
                }

                $result = array('status' => 1, 'message' => 'User already registered with this email address', 'data' => $patronData);
            }
            else {
                $result = array('status' => 0, 'message' => 'New User registration');
            }
        }
        else {
            $result = array('status' => 0, 'message' => 'New User registration');
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to login via Social Media
    public function socialMediaLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'platform' => 'required',
            'email' => 'required',
            'smToken' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // Check social media token exist or not
        $query = User::select('id', 'userName', 'email', 'phone')
            ->where(['userType' => 'U', 'smToken' => $smToken, 'platform' => $platform])->first();
        //'email' => $email, 
        if(!empty($query)) {
            $patronData = Patron::select('profilePic', 'firstName', 'lastName', 'bnPatrons.userId', 'u.userName', 'u.phone', 'u.email', 'c.city_name as cityName')
                ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnPatrons.userId')
                ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                ->where('bnPatrons.userId', $query->id)->first();
            $patronData['platform'] = $deviceId;
            $patronData['deviceToken'] = $deviceToken;
            $patronData['osVersion'] = $osVersion;
            $patronData['smToken'] = $smToken;
            $patronData['modelName'] = $modelName;
            $patronData['osType'] = $osType;

            // Generating the Token
            $token = CommonFunction::createLoginToken($query->id); 
            if($token == false)
                $token = "";
            $patronData['token'] = $token;

            //Converting NULL to "" String
            array_walk_recursive($patronData, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'User already registered with this social media account', 'data' => $patronData);
        }
        else {
            //Check Email/Username is already registered or not
            $query = CommonFunction::checkValueExist($request->email, 'email', 'U');
            if ($query) 
            {
                // link platform and token with users
                $userId = $query->id;
                $data = array('platform' => $platform, 'updatedAt' => $this->entryDate);
                if(isset($smToken) && !empty($smToken))
                    $data['smToken'] = $smToken;
                $update = User::where(['id' => $userId])->update($data);

                //Call patron data to send in response
                $patronData = Patron::select('profilePic', 'firstName', 'lastName', 'bnPatrons.userId', 'u.userName', 'u.phone', 'c.city_name as cityName')
                    ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                    ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnPatrons.userId')
                    ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                    ->where('bnPatrons.userId', $userId)->first();
                $patronData['platform'] = $deviceId;
                $patronData['deviceToken'] = $deviceToken;
                $patronData['osVersion'] = $osVersion;
                $patronData['smToken'] = $smToken;
                $patronData['modelName'] = $modelName;
                $patronData['osType'] = $osType;

                // Generating the Token
                $token = CommonFunction::createLoginToken($userId); 
                if($token == false)
                    $token = "";
                $patronData['token'] = $osType;

                //Converting NULL to "" String
                array_walk_recursive($patronData, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $result = array('status' => 1, 'message' => 'Login successful', 'data' => $patronData);
            }
            else {
                $result = array('status' => 0, 'message' => 'User not registered');
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);

        //Check Email/Username is already registered or not
        // $query = CommonFunction::checkValueExist($request->email, 'email', 'U');

        /*if ($query) {
            $userId = $query->id;

            //update platform and token in users
            $data = array('platform' => $platform);
            if(isset($smToken) && !empty($smToken))
                $data['smToken'] = $smToken;
            $update = User::where(['id' => $userId])->update($data);

            //Now update the patron record
            $fullName = explode(" ", $name);
            $firstName = $fullName[0];

            $lastName = '';
            if (isset($fullName[1])) {
                $lastName = $fullName[1];
            }

            $data = array('firstName' => $firstName, 'lastName' => $lastName);
            $update = Patron::where(['userId' => $userId])->update($data);

            //Call patron data to send in response
            $patronData = Patron::select('profilePic', 'firstName', 'lastName', 'bnPatrons.userId', 'u.userName', 'u.phone', 'c.city_name as cityName')
                ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnPatrons.userId')
                ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
                ->where('bnPatrons.userId', $userId)->first();

            //Converting NULL to "" String
            array_walk_recursive($patronData, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Login successful', 'data' => $patronData, 'token' => "");
        }
        //Create New entry of user
        else {
            $data = array('email' => $email, 'userType' => 'U', 'email_verified_at' => date('Y-m-d H:i:s'), 'phone_verified_at' => date('Y-m-d H:i:s'), 'platform' => $platform);
            if(isset($smToken) && !empty($smToken))
                $data['smToken'] = $smToken;
            $user = User::create($data);

            if ($user['id']) {
                $userId = $user['id'];

                //Now update the patron record
                $fullName = explode(" ", $name);
                $firstName = $fullName[0];

                $lastName = '';
                if (isset($fullName[1])) {
                    $lastName = $fullName[1];
                }

                //Now Create an entry in bnUsers
                $record = array('userId' => $user['id'], 'firstName' => $firstName, 'lastName' => $lastName);
                $patron = Patron::create($record);

                //Call patron data to send in response
                $patronData = array(
                    'firstName' => $firstName, 
                    'lastName' => $lastName, 
                    'userId' => $userId,
                    'profilePic' => "", 
                    'userName' => "", 
                    'phone' => "",
                    'cityName' => "",
                );

                $result = array('status' => 1, 'message' => 'Login successful', 'token' => "", 'data' => $patronData);
                //$user->createToken('MyApp')->accessToken
            } else {
                $result = array('status' => 0, 'message' => 'User created but profile not updated');
                $result['app_version'] = CommonFunction::appVersion();
                return response()->json($result);
            }
        }

        if (isset($result) && isset($userId)) {
            //'latitude' => $latitude, 'longitude' => $longitude,
            //Update the device Data of the User
            $data = array('deviceId' => $deviceId, 'deviceToken' => $deviceToken, 'osVersion' => $osVersion, 'modelName' => $modelName, 'loginStatus' => 1);

            if(isset($osType))
                $data['osType'] = $osType;

            CommonFunction::updateUserDevice($data, $userId);

            $result['app_version'] = CommonFunction::appVersion();
            //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
            return response()->json($result);
        }*/
    }

    //insert contact Support
    public function contactSupport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //submit enquirey to contact
        $insert = array(
            'userId' => $userId,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
        );
        $enquireInsert = SupportEnquire::create($insert);
        if ($enquireInsert['id']) {
            $result = array('status' => 1, 'message' => "Enquiry has been submitted successfully");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //upload profile pic of user
    public function uploadProfilePic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'image' => 'mimes:jpg,JPG,JPEG,png,svg,jpeg,gif|max:5000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //get file image
        $file = $request->file("image");
        if ($request->hasFile("image")) {
            //get extention of file
            $ext = $file->getClientOriginalExtension();
            //encrypt the name of file
            $img = time() * rand() . "." . $ext;
            //get the alerady exist image
            $pic = CommonFunction::GetSingleField('bnPatrons', 'profilePic', 'userId', $userId);
            if (!empty($pic)) {
                $oldPic = $pic;
            }
            //upload image to folder
            $upload = $file->move("uploads/patrons/profile/", $img); //Path & File name need to pass
            if (!empty($upload)) {
                $imgName = $img;
                //Update in Table
                $update = array(
                    'profilePic' => $imgName,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );
                $updateImage = Patron::where('userId', $userId)->update($update);
                if ($updateImage) {
                    //Delete the Privous Image
                    if (isset($oldPic) && $oldPic != null) {
                        $imgToDel = public_path("uploads/patrons/profile/" . $oldPic);
                        if (file_exists($imgToDel)) {
                            unlink($imgToDel);
                        }
                    }
                    $result = array('status' => 1, 'message' => 'Picture has been updated successfully', 'profilePic' => $imgName);
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong');
                }
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please select a picture to upload');
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //upload user basic profile info
    public function updateMyProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        if(CommonFunction::isAbusiveContent($request->firstName)){
            $result = array('status' => 0, 'message' => 'First name should not contain abusive word');
            return response()->json($result);
        }
        elseif(CommonFunction::isAbusiveContent($request->lastName)){
            $result = array('status' => 0, 'message' => 'Last name should not contain abusive word');
            return response()->json($result);
        }

        // Check City Exist or not with the city name
        if(isset($request->city) && !empty($request->city)) {
            $cityId = CommonFunction::getCityIdByName($request->city);
            if(!$cityId) {
                $result = array('status' => 0, 'message' => 'Invalid city');
                return response()->json($result);
            }
        }

        /* === Update New Email/Phone === */
        $user = CommonFunction::GetSingleRow('bnUsers', 'id', $userId);
        if($user) {
            // If new email request, then send an verification email
            if($user->email != $request->email) {

                //Check Email is already registered or not or not
                $query = CommonFunction::checkValueExist($request->email, 'email', 'U');
                if ($query) {
                    $result = array('status' => 0, 'message' => 'Email is already registered');
                    return response()->json($result);
                }   

                // First delete previous request
                DB::table('bnEmailUpdates')->where(['userId' => $userId])->delete();

                //Create an entry in emailUpdates table
                $token = str_random(40);
                $param = array('userId' => $userId, 'token' => $token, 'email' => $request->email, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                $lastId = DB::table('bnEmailUpdates')->insertGetId($param);

                if($lastId) {
                    // Send email now
                    $emailParam = array(
                        'name'=> ucwords($request->firstName.' '.$request->lastName), 
                        'token'=>$token, 
                        'email'=>$request->email,
                    );
                    try{
                        Mail::send('emails.updateEmail', $emailParam, function ($message) use ($emailParam) {
                            $message->from('noreply@barnightslive.com', config('app.name'));
                            $message->subject('Email Verification');
                            $message->to($emailParam['email']);
                        });
                    }catch(\Exception $e ){
                        $error = $e->getMessage();
                    }
                }
            }

            // If new phone no is requested
            if($user->phone != $request->phone) {

                //Check Phone is already registered or not or not
                $query = CommonFunction::checkValueExist($request->phone, 'phone', 'U');
                if ($query) {
                    $result = array('status' => 0, 'message' => 'Phone number is already registered');
                    return response()->json($result);
                }
                
                $param = array('phone' => $request->phone, 'updatedAt' => $this->entryDate);
                $update = User::where('id', $userId)->update($param);
            }
        } 

        /* ===Update the Profile info=== */
        if ($request->hasFile("image")) {
            $file = $request->file("image");
            //get extention of file
            $ext = $file->getClientOriginalExtension();
            $img = time() * rand() . "." . $ext;
            
            //upload new image to folder
            $upload = $file->move("uploads/patrons/profile/", $img);
            if (!empty($upload)) {
                $imgName = $img; 
            } 
        } 

        //Update in Table
        $param = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'updatedAt' => $this->entryDate,
        );

        if(isset($imgName)) {
            $param['profilePic'] = $imgName;
            
            //Delete the Privous Image
            $oldPic = CommonFunction::GetSingleField('bnPatrons', 'profilePic', 'userId', $userId);
            if (!empty($oldPic)) {
                $imgToDel = public_path("uploads/patrons/profile/" . $oldPic);
                if (file_exists($imgToDel)) unlink($imgToDel);
            }
        }

        $rs = Patron::where('userId', $userId)->update($param);
        if ($rs) {
            
            // Update city into bnAddresses Table
            $param = array(
                'cityId' => $cityId,
                'updatedAt' => $this->entryDate,
            ); 
            Address::where(['userId' => $userId])->update($param);

            $result = array('status' => 1, 'message' => 'Profile has been updated successfully');
            
            if(isset($imgName))
                $result['profilePic'] = $imgName;
        }
        else
            $result = array('status' => 0, 'message' => 'Something went wrong');
        
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to update user's lat/long in background
    public function updateLatLong(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'deviceId' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'city' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // Get the City ID
        if(!empty($request->city)) {
            $city = CommonFunction::getCityIdByName($request->city);
            $cityId = ($city) ? $city : 0; 
        }
        else
            $cityId = 0;

        $param = array(
            'latitude' => $latitude, 
            'longitude' => $longitude, 
            'deviceId' => $deviceId, 
            'cityId' => $cityId, 
            'updatedAt' => $this->entryDate,
        );

        // First check the entry is exist or not with the user
        $exist = DB::table('bnUserCurrentLocation')
            ->where(['userId' => $userId, 'deviceId' => $deviceId])
            ->first();
        if (!empty($exist)) {
            $update = DB::table('bnUserCurrentLocation')->where(['userId' => $userId, 'deviceId' => $deviceId])->update($param);
            $result = array('status' => 1, 'message' => 'Location has been updated successfully');

            // Send Notification of Offers/Events to this patron
            CommonFunction::sendRegularNotification($userId, $latitude, $longitude, $cityId);
        }
        else {
            $param['userId'] = $userId;
            $param['createdAt'] = $this->entryDate;
            $update = DB::table('bnUserCurrentLocation')->insert($param);

            $result = array('status' => 1, 'message' => 'Location has been updated successfully');   
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to update Firebase Token in background
    public function updateFirebaseToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'deviceId' => 'required',
            'deviceToken' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //First check the user is exist or not
        $userExist = CommonFunction::GetSingleField('bnUsers', 'id', 'id', $userId);

        if ($userExist) {
            //Update the device Data of the User
            $data = array('deviceToken' => $deviceToken, 'updatedAt' => date('Y-m-d H:i:s'));
            $update = UserDevice::where(['deviceId' => $deviceId, 'userId' => $userId])->update($data);

            $result = array('status' => 1, 'message' => 'Record has been updated successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Invalid User');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Funtion to get all users, to tag in post and stores
    public function getAllUsers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 50;
        }
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        $sql = "select u.id, u.userName, p.firstName, p.lastName, p.profilePic from bnUsers u inner join bnPatrons p on p.userId = u.id where u.userType = 'U' AND u.status = 1";
        
        //If keyword is set
        if(isset($request->keyword)){
            $sql .= " AND (p.firstName LIKE '%".$request->keyword."%' OR p.lastName LIKE '%".$request->keyword."%')";
        }

        $sql.= " order by rand()";
        $users = DB::select(DB::raw($sql));
        if (!empty($users)) {

            //Converting NULL to "" String
            array_walk_recursive($users, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Users has been received successfully", 'data' => $users, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No user found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }
}