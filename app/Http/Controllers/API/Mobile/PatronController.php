<?php
namespace App\Http\Controllers\API\Mobile;

use App\BlockUser;
use App\Friend;
use App\Http\Controllers\Controller;
use App\NewsFeed;
use App\NewsFeedComment;
use App\NewsFeedReaction;
use App\Patron;
use App\User;
use App\UserNotification;
use App\Wallet;
use App\WalletTransaction;
use App\Transaction;
use App\UserNotificationSetting;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class PatronController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    // Function to get user public profile
    public function getPatronProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        
        /* === Get the Profile Info === */
        $profileInfo = Patron::select('profilePic', 'firstName', 'lastName', 'email', 'phone', 'userName')
            ->join('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
            ->where('bnPatrons.userId', $userId)->first();

        $friendStatus = 0; //Not a friend

        /* === Check Users friendhip condition === */
        $isFriend = Friend::select('bnFriendLists.id')
            ->where(['bnFriendLists.userId' => $myId, 'bnFriendLists.friendId' => $userId,  'bnFriendLists.status' => 1])->first();
        if(!empty($isFriend))
            $friendStatus = 1; // Friend hai
        else {
            // Check user has sent the request or not
            $isFriend = Friend::select('bnFriendLists.id')
                ->where(['bnFriendLists.userId' => $myId, 'bnFriendLists.friendId' => $userId,  'bnFriendLists.status' => 0])->first();
            if(!empty($isFriend))
                $friendStatus = 2; // Request Sent
            else {
                // Check user has received the request or not
                $isFriend = Friend::select('bnFriendLists.id')
                    ->where(['bnFriendLists.userId' => $userId, 'bnFriendLists.friendId' => $myId,  'bnFriendLists.status' => 0])->first();
                if(!empty($isFriend))
                    $friendStatus = 3; // Request Received
            }
        }

        $profileInfo['friendStatus'] = $friendStatus;
        $data['profileInfo'] = $profileInfo;

        /* === Get the News feed === */
        $newsFeedDetails = array();

        // Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 10;
        }
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.description', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt', 'p.firstName', 'p.lastName', 'p.profilePic', 'p.userId')
            ->leftJoin('bnNewsFeedFriends as nf', 'nf.newsId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->where(['bnNewsFeeds.userId' => $userId, 'bnNewsFeeds.isDeleted' => 0])
            ->orWhere(['nf.friendId' => $userId])
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->groupBy('bnNewsFeeds.id')
            ->offset($startFrom)->limit($limit)->get();

        //get count of  new feeds
        $nfcount = NewsFeed::select('bnNewsFeeds.id')->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')->where(['bnNewsFeeds.userId' => $userId])->get();
        $data['totalNewsFeeds'] = count($nfcount);

        if (count($newsfeed) > 0) {
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $myId, 'newsId' => $nf->id])->first();

                $nf['reacted'] = 0;

                if ($nfReact) {
                    $nf['reacted'] = $nfReact->reactionId;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();

                //Get the News Media files
                $nf['mediaFiles'] = DB::table('bnUserMediaFiles')->select('id', 'mediaName', 'mediaType')
                ->where(['moduleId' => $nf->id])->get()->toArray();

                $nf['totalComment'] = count($newsfeedComment);
                $nf['totalReaction'] = count($newsfeedReaction);
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }
        } 

        $data['newsFeedDetails'] = $newsFeedDetails;
        $result = array('status' => 1, 'message' => 'Patron profile get successfully', 'data' => $data);

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to update user INncognito mode
    public function udpdateIncognito(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'action' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        if($action == 1) {
            $param = array('userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
            $id = DB::table('bnIncognitoUsers')->insertGetId($param);
        }
        else {
            DB::table('bnIncognitoUsers')->where(['userId' => $userId])->delete();
        }

        $result = array('status' => 1, 'message' => 'Status updated successfully');
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to get wallet balance
    public function getWalletBalance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $transactionPayments = array();
        $walletPayments = array();

        $walletBalance = Wallet::select('id', 'amount')
            ->where('userId', $userId)
            ->first();
        if(empty($walletBalance))
            $walletBalance = (object) array();
        
        //get wallet transactions of user in it status 1:-success,2:-fail
        $walletTransaction = WalletTransaction::select('id', 'description', 'amount', 'type', 'createdAt')
            ->where(['userId' => $userId])
            ->orderBy('id', 'desc')
            ->get()->toArray();

        //get transactions of user 1:-success,2:-fail
        $transactions = Transaction::select('id', 'description', 'amount', 'paymentMode', 'status', 'createdAt')
            ->where(['userId' => $userId])
            ->where('paymentMode',  '!=', 'bnwallet')
            ->orderBy('id', 'desc')
            ->get()->toArray();
        
        $data['walletBalance'] = $walletBalance;
        $data['walletTransaction'] = $walletTransaction;
        $data['transactions'] = $transactions;

        //Converting NULL to "" String
        array_walk_recursive($data, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        $result = array('status' => 1, 'message' => "Wallet balance get successfully", 'data' => $data);

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }
 
    // Function to get city areas of user
    public function loadCityAreas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // Get User Incognito Status
        $userSet = DB::table('bnIncognitoUsers')->select('*')
            ->where(['userId' => $userId])->first();
        if(!empty($userSet)) {
            $date1 = date("Y-m-d H:i:s");
            $date2 = $userSet->createdAt;
            $timestamp1 = strtotime($date1);
            $timestamp2 = strtotime($date2);

            $hour = abs($timestamp2 - $timestamp1)/(60*60);
            if($hour > 24) {
                // Delete the entry
                DB::table('bnIncognitoUsers')->where(['userId' => $userId])->delete();
                $incognito = 0;
            }   
            else
                $incognito = 1;
        }
        else
            $incognito = 0;
        $data['incognito'] = $incognito;

        $cityId = CommonFunction::GetSingleField('bnAddresses', 'cityId', 'userId', $userId);
        if($cityId) {
            $areas = DB::table('bnCityAreas')->select('id', 'area')
                ->where(['cityId' => $cityId])->get()->toArray();

            if(!empty($areas)) {
                //Converting NULL to "" String
                array_walk_recursive($areas, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $data['areas'] = $areas;

                // Get User Selected Areas
                $userAreas = array();
                $uAreas = UserNotificationSetting::select('areaId')
                    ->where('userId', $userId)->get();
                if(!empty($uAreas)) {
                    foreach ($uAreas as $a) {
                        $userAreas[] = $a->areaId;
                    }
                }
                if(!empty($userAreas))
                    $selectedAreas = implode(",", $userAreas);
                else
                    $selectedAreas = "";

                $data['selectedAreas'] = $selectedAreas;
            }
            else{
                $data['areas'] = [];
                $data['selectedAreas'] = "";
            }
        }
        else{
            $data['areas'] = [];
            $data['selectedAreas'] = "";
        }
        
        $result = array('status' => 1, 'message' => "Areas get successfully", 'data' => $data);
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to update Notification Settings of user
    public function updateNotificationSetting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'areaIds' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // First delete the previous entries
        UserNotificationSetting::where('userId', $userId)->delete();

        $aIds = explode(",", $areaIds);
        foreach ($aIds as $id) {
            $param = array(
                'userId' => $userId,
                'areaId' => $id,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            );
            $insertNotification = UserNotificationSetting::create($param);
        }
        $result = array('status' => 1, 'message' => "Settings has been updated successfully");
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to update Notification Settings of user - Old
    public function updateNotificationSetting_Old(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $insert = array(
            'userId' => $userId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'status' => $status,
            'createdAt' => $this->entryDate,
            'updatedAt' => $this->entryDate,
        );
        if ($status == 0) {
            $exist = UserNotificationSetting::select('id')->where('userId', $userId)->first();
            if ($exist) {
                $deleteNotification = UserNotificationSetting::where('userId', $userId)->delete();
                if ($deleteNotification) {
                    $result = array('status' => 1, 'message' => "Notification has been deleted successfully");
                } else {
                    $result = array('status' => 0, 'message' => "Notification not deleted");
                }
            } else {
                $result = array('status' => 0, 'message' => "User not found");
            }
        } else if ($status == 1) {
            $insertNotification = UserNotificationSetting::create($insert);
            if ($insertNotification['id']) {
                $result = array('status' => 1, 'message' => "Notification settings has been added successfully");
            } else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }
}