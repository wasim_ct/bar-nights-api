<?php
namespace App\Http\Controllers\API\Mobile;

use App\Bar;
use App\BarBanner;
use App\BarNotification;
use App\BarRating;
use App\BarReview;
use App\BookTable;
use App\Http\Controllers\Controller;
use App\Patron;
use App\UserNotification;
use App\Visit;
use App\VisitInvitation;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class VisitController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Book Visit function to create a book in the bar
    public function bookVisit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* First Check User is block or not for 24 hrs for Vist Start */
        $isBlocked = true;
        $block = DB::table('bnVisitBlockedUsers as bu')->select('bu.createdAt', 'bu.updatedAt')
            ->where(['bu.userId' => $userId])
            ->orderBy('bu.id', 'desc')
            ->first();
        if($block) {
            $date1 = date("Y-m-d H:i:s");
            $date2 = $block->createdAt;  //"2020-01-12 15:25:00";
            $timestamp1 = strtotime($date1);
            $timestamp2 = strtotime($date2);
            $hour = abs($timestamp2 - $timestamp1)/(60*60);
            if($hour < 24) 
                $isBlocked = false;
        } 
        /* First Check User is block or not for 24 hrs for Vist End */

        //check id exist in visit table or not
        if($isBlocked) {
            $visit = Visit::select('id', 'status', 'createdAt')->where(['userId' => $userId])->orderBy('id', 'desc')->first();
            if ($visit) {
                //check that the status of visit is pending or not
                if ($visit->status == 1) 
                {
                    // Cancel the visit if 15 minutes passes
                    $currentTime = date('H:i:s');
                    $codeGeneratedTime = date('H:i:s', strtotime($visit->createdAt));
                    $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

                    if ($minutes > 15) {
                        Visit::where(['id' => $visit->id])->update(['status' => 3]);

                        // Create entry of blocked user
                        $record = array(
                            'visitId' => $visit->id,
                            'userId' => $userId,
                            'createdAt' => $this->entryDate,
                            'updatedAt' => $this->entryDate 
                        );
                        DB::table('bnVisitBlockedUsers')->insert($record);
                    }

                    $result = array('status' => 0, 'message' => "You have a pending booked visit. Please cancel your previous visit to book a new visit.");
                }
                else if ($visit->status == 2) {
                    $result = array('status' => 0, 'message' => "You have a running visit. So you can not place new visit.");
                } 
                else if ($visit->status == 4) // || $visit->status == 3
                {
                    //if status is completed check that is any review is given or not
                    $visitReview = BarReview::select('id')->where(['userId' => $userId, 'moduleId' => $visit->id, 'moduleType' => 'visit'])->first();

                    if ($visitReview) {
                        $result = commonFunction::visitInsert();
                    } else {
                        $result = array('status' => 0, 'message' => "Please post rating & review to your previous visit");
                    }
                } else {
                    $result = commonFunction::visitInsert();
                }

            } else {
                $result = commonFunction::visitInsert();
            }
        }
        else {
            $result = array('status' => 0, 'message' => "You can not place a new visit today, try again tomorrow."); 
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get listing of booking for user id
    public function getUserVisits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        extract($_POST);
        //checking visit exist or not
        $visitExist = Visit::select('id')->where(['userId' => $userId])->get();
        if (count($visitExist) > 0) {
            //get all data of user vist with code and bar info
            $Visits = Visit::select('bnVisits.id', 'bnVisits.sittingType', 'bnVisits.tableStandingNo', 'bnVisits.status', 'vc.code', 'b.id as barId', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnVisits.createdAt')
                ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'bnVisits.id')
                ->leftJoin('bnBars as b', 'b.id', '=', 'bnVisits.barId')
                ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
                ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
                ->offset($startFrom)->limit($limit)
                ->groupBy('bnVisits.id')
                ->where(['bnVisits.userId' => $userId])
                ->orderBy('bnVisits.id', 'desc')
                ->get()->toArray();

            if (count($Visits) > 0) {
                foreach ($Visits as $visit) {
                    $barId = $visit['barId'];
                    //get banner of bar
                    $mediaName = BarBanner::select('mediaName')
                        ->where(['barId' => $barId])->first();
                    $visit['mediaName'] = '';
                    if ($mediaName) {
                        $visit['mediaName'] = $mediaName->mediaName;
                    }
                    //get rating of bar
                    $rating = BarReview::select('rating')
                        ->where(['moduleId' => $visit['id'], 'moduleType' => 'visit'])->first();
                    $visit['rating'] = 0;
                    if ($rating) {
                        $visit['rating'] = $rating->rating;
                    }

                    $visits[] = $visit;
                }
            }

            $data = $visits;
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $mediaPath = CommonFunction::getMediaPath();
            if (count($Visits) > 0) {
                $result = array('status' => 1, 'message' => "Visits has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //update booking status for user id
    public function updateVisitStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //check that visit id exist or not
        $visitExist = Visit::select('*')->where(['id' => $id])->first();

        if ($visitExist) {
            $update = array('status' => $status, 'updatedAt' => $this->entryDate);
            
            if(isset($cancelReason) && !empty($cancelReason)) {
                $update['cancelReason'] = $cancelReason;
                $update['cancelBy'] = 'patron';
            }
            
            $statusUpdate = Visit::where(['id' => $id])->update($update);

            if ($statusUpdate) {
                $userId = $visitExist->userId;
                $visitId = $id;
                $barId = $visitExist->barId;

                //Block the User for 24 hrs, if visit automatically cancelled
                if(isset($block) && $status == 3) {
                    $record = array(
                        'visitId' => $id,
                        'userId' => $userId,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate 
                    );
                    DB::table('bnVisitBlockedUsers')->insert($record);
                }

                // Delete the orders also, in case of visit cancel
                if($status == 3) {
                    $orderId = CommonFunction::GetSingleField('bnOrders', 'id', 'visitId', $visitId);
                    if($orderId) {
                        DB::table('bnOrders')->where(['id' => $orderId])->delete();
                        DB::table('bnOrderKots')->where(['orderId' => $orderId])->delete();
                        DB::table('bnOrderKotItems')->where(['orderId' => $orderId])->delete();
                    }

                    //Delete entry from geo tagging
                    DB::table('bnGeoTaggings')->where(['visitId' => $visitId])->delete();
                }

                $bar = Bar::select('bnBars.barName', 'bnBars.logo', 'a.address')
                    ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
                    ->where(['bnBars.id' => $barId])->first();
                if (!empty($bar)) {

                    $message = 'Your visit has cancelled for '.ucwords(strtolower($bar->barName));

                    $record = array('moduleType' => 'visitCancelled', 'moduleId' => $visitId, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $not = UserNotification::create($record);

                    if ($not['id']) {
                        $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens

                        if ($deviceToken) {
                            //Firebase Notification
                            $payLoadData = array('moduleType' => 'visitCancelled', 'moduleId' => $visitId, 'userId' => $userId, 'title' => 'Visit is cancelled', 'msg' => $message, 'barId' => $barId, 'barName' => $bar->barName, 'address' => $bar->address, 'logo' => $bar->logo);
                            $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                        }
                    }
                }

                $result = array('status' => 1, 'message' => "Visit has been updated successfully");
            } else {
                $result = array('status' => 0, 'message' => "Visit not updated");
            }
        } else {
            $result = array('status' => 0, 'message' => "Visit not exist");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Function to check visit current status
    public function getVisitStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $visit = Visit::select('status')->where(['id' => $visitId])->first();

        if ($visit) {
            $result = array('status' => $visit->status, 'message' => "Visit status found");
        } 
        else {
            $result = array('status' => 0, 'message' => "Visit not exist");
        }
        return response()->json($result);
    }

    //sending invitation to users
    public function sendVisitInvitation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
            'toUserId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $userExist = CommonFunction::GetSingleField('bnUsers', 'id', 'id', $toUserId);
        if ($userExist) {
            //check that the toUserId and visit id exist or not
            $exist = VisitInvitation::select('id')->where(['toUserId' => $toUserId, 'visitId' => $visitId])->first();
            if ($exist) {
                $result = array('status' => 0, 'message' => "Invitation has been already sent to this user");
            } 
            else {
                //sending invitation
                $insert = array('toUserId' => $toUserId, 'visitId' => $visitId, 'status' => 1, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);

                $VisitInvitation = VisitInvitation::create($insert);
                if ($VisitInvitation['id']) {

                    //Create an entry in notification table and send PUSH Notification
                    $visit = Visit::select('bnVisits.userId', 'b.barName', 'p.firstName', 'p.lastName')
                        ->leftJoin('bnBars as b', 'b.id', '=', 'bnVisits.barId')
                        ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
                        ->where(['bnVisits.id' => $visitId])->first();

                    if (!empty($visit)) {

                        $message = ucwords(strtolower($visit->firstName . ' ' . $visit->lastName)) . ' has invited you to visit ' . ucwords(strtolower($visit->barName));

                        $record = array('moduleType' => 'visitInvitation', 'moduleId' => $visitId, 'description' => $message, 'userId' => $visit->userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        
                        $not = UserNotification::create($record);

                        if ($not['id']) {
                            $deviceToken = CommonFunction::getUserDeviceData($toUserId); //Get Device Tokens
                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'visitInvitation', 'moduleId' => $visitId, 'userId' => $toUserId, 'title' => 'New visit invitation', 'msg' => $message);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }
                    }
                    $result = array('status' => 1, 'message' => "Invitation has been sent successfully");
                } else {
                    $result = array('status' => 1, 'message' => "Invitation not sent");
                }

            }
        } else {
            $result = array('status' => 1, 'message' => "Invalid User");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //get listing of Visit Invitations send to me
    public function getVisitInvitations(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        extract($_POST);
        //checking visit invitaion exist or not
        $VisitInvitationExist = VisitInvitation::select('id')->where(['toUserId' => $userId])->get();
        if (count($VisitInvitationExist) > 0) {
            //get all visit invitation request
            $visits = VisitInvitation::select('bnVisitInvitations.id', 'v.id as visitId', 'v.visitDate', 'v.visitTime', 'v.sittingType', 'v.tableStandingNo', 'bnVisitInvitations.status as invitationStatus', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName')
                ->leftJoin('bnVisits as v', 'v.id', '=', 'bnVisitInvitations.visitId')
                ->leftJoin('bnBars as b', 'b.id', '=', 'v.barId')
                ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
                ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
                ->offset($startFrom)->limit($limit)
                ->orderBy('bnVisitInvitations.id', 'desc')
                ->where(['bnVisitInvitations.toUserId' => $userId])->get()->toArray();

            //Converting NULL to "" String
            array_walk_recursive($visits, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            if (count($visits) > 0) {
                $result = array('status' => 1, 'message' => "Invitation has been received successfully", 'data' => $visits);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //insert Rating for visit and pre order
    public function submitRating(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'moduleId' => 'required',
            'moduleType' => 'required',
            'rating' => 'required',
            //'review' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        if(CommonFunction::isAbusiveContent($request->review)){
            $result = array('status' => 0, 'message' => 'Review should not contain abusive word');
            return response()->json($result);
        }
        $reviewExist = BarReview::select('id')->where(['userId' => $userId, 'moduleId' => $moduleId, 'moduleType' => $moduleType])->first();
        
        if (!empty($reviewExist)) {
            $result = array('status' => 0, 'message' => "Rating and review for this visit is already submited");
        } else {
            //submit review of user for vist or order
            $insert = array(
                'userId' => $userId,
                'barId' => $barId,
                'moduleId' => $moduleId,
                'moduleType' => $moduleType,
                'rating' => $rating,
                'review' => $review,
            ); 
            $reviewInsert = BarReview::create($insert);
            if ($reviewInsert['id']) {
                //get total rating for bar
                $rating = CommonFunction::updateBarRating($barId);
                //update the ratings of bar and increase the users total no
                $update = array(
                    'rating' => $rating,
                    'updatedAt' => $this->entryDate,
                );
                $ratingExist = BarRating::select('id')->where(['barId' => $barId])->first();
                if (!empty($ratingExist)) {
                    $update['totalUsers'] = DB::raw('totalUsers + 1');
                    BarRating::where('barId', $barId)->update($update);
                } else {
                    $update['totalUsers'] = 1;
                    $update['createdAt'] = $this->entryDate;
                    $update['barId'] = $barId;
                    BarRating::create($update);
                }

                //Create an entry in notification table and send PUSH Notification
                $user = Patron::select('firstName', 'lastName')
                    ->where(['userId' => $userId])->first();
                if (!empty($user)) {
                    $message = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has submitted a review on the recent visit';

                    $record = array('moduleType' => 'review', 'moduleId' => $reviewInsert['id'], 'description' => $message, 'barId' => $barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $not = BarNotification::create($record);

                    if ($not['id']) {
                        $bar = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $barId);
                        $deviceToken = CommonFunction::getUserDeviceData($bar); //Get Device Tokens

                        if ($deviceToken) {
                            //Firebase Notification
                            $payLoadData = array('moduleType' => 'review', 'moduleId' => $reviewInsert['id'], 'barId' => $barId,'title' => 'New review received', 'msg' => $message);
                            $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'B');
                        }
                    }
                }

                $result = array('status' => 1, 'message' => "Rating has been submitted successfully");
            } else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //get user current visit
    public function getCurrentVisit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
 
        //get current visit details of user(status:-1:pending, 2:confirmed)
        /*$visits = Visit::select('bnVisits.id', 'bnVisits.createdAt', 'bnVisits.userId', 'bnVisits.createdAt', 'p.firstName', 'p.lastName', 'u.userName', 'bnVisits.status', 'br.rating', 'bnVisits.barId', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'state.state_name as stateName')
            ->join('bnBars as b', 'b.id', '=', 'bnVisits.barId')
            ->join('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->join('bnUsers as u', 'u.id', '=', 'bnVisits.userId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnStates as state', 'state.state_id', '=', 'ad.stateId')
            ->where(['bnVisits.userId' => $userId])
            // ->where('bnVisits.status', '!=', 4)
            ->where('bnVisits.status', '!=', 3)->orderBy('id', 'desc')->first();
        if ($visits) {
            //get all visit invitation request done on this invitation(status:-1:pending, 2:accept,3:reject)
            $visitsInvite = VisitInvitation::select('bnVisitInvitations.id', 'bnVisitInvitations.toUserId', 'bnVisitInvitations.status as invitationStatus', 'p.firstName', 'p.lastName', 'u.userName', 'p.profilePic')
                ->join('bnPatrons as p', 'p.userId', '=', 'bnVisitInvitations.toUserId')
                ->join('bnUsers as u', 'u.id', '=', 'bnVisitInvitations.toUserId')
                ->orderBy('bnVisitInvitations.id', 'desc')
                ->where(['bnVisitInvitations.visitId' => $visits->id])->get()->toArray();

            if ($visits->status == 1) {
                $currentTime = date('H:i:s');

                //get code generated time from db
                $time = $visits->createdAt;
                $codeGeneratedTime = date('H:i:s', strtotime($time));

                //get only minites by taking difference of code generated time and current time
                //Then remove points by round function
                //convert -ve value in +ve by using function abs
                $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

                if ($minutes > 15) {
                    //update visit status in table
                    Visit::where(['id' => $visits->id])->update(['status' => 3]);
                    //get visit status to show
                    $visits['status'] = 3;
                }
            }
            //Converting NULL to "" String
            if (!empty($visits)) {
                array_walk_recursive($visits, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            if ($visits['status'] == 3) {
                $visitData = " ";
            } else {
                $visitData = $visits;
            }

            $data = array('visit' => $visitData, 'inviteUsers' => $visitsInvite);

            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => "Visit has been received successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "data not exist");
        }*/

        // Get data of visit
        $data['visit'] = CommonFunction::getVisitData($userId);
        $result = array('status' => 1, 'message' => "Visit has been received successfully", 'data' => $data);

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //remove visit invitations
    public function removeVisitInvites(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inviteId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //delete invitations send to friends on that visit
        $visitInviteDelete = VisitInvitation::where(['id' => $inviteId])->delete();
        if ($visitInviteDelete) {
            $result = array('status' => 1, 'message' => "Visit invitation has been deleted successfully");
        } else {
            $result = array('status' => 0, 'message' => "No visit found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get user invites function
    public function getMyInvites(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $status = false;

        $visitInvitation = Visit::select('bnVisits.id', 'vi.id as inviteId', 'bnVisits.userId', 'bnVisits.createdAt', 'vi.status as invitationStatus', 'bnVisits.barId', 'b.barName', 'p.firstName', 'p.lastName', 'u.userName', 'p.profilePic')
            ->join('bnVisitInvitations as vi', 'vi.visitId', '=', 'bnVisits.id')
            ->join('bnBars as b', 'b.id', '=', 'bnVisits.barId')
            ->join('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->join('bnUsers as u', 'u.id', '=', 'bnVisits.userId')
            ->orderBy('vi.id', 'desc')
            ->where(['vi.toUserId' => $userId])
            ->where('bnVisits.status', '!=', 4)
            ->where('bnVisits.status', '!=', 3)
            ->groupBy('userId')
            ->get()->toArray();

        //Converting NULL to "" String
        array_walk_recursive($visitInvitation, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        if (count($visitInvitation) > 0) {
            $result = array('status' => 1, 'message' => "Invitation has been received successfully", 'data' => $visitInvitation);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Change invite status function
    public function changeInviteStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occured');
            return response()->json($result);
        }
        extract($_POST);

        //Get the required data
        $exist = DB::table('bnVisitInvitations as vi')->select('vi.toUserId', 'v.userId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'vi.visitId')
            ->where(['vi.id' => $id])
            ->first();
        //echo $exist->toUserId;exit;

        if ($exist) {
            $update = DB::table('bnVisitInvitations')
                ->where(['id' => $id])
                ->update(['status' => $status, 'updatedAt' => date("Y-m-d H:i:s")]);

            if ($update == 1) {
                if ($status == 2) {
                    $result = array('status' => 1, 'message' => 'Invitation has been accepted');
                    $msg = 'accepted';
                    $title = 'Visit invitation accepted';
                } else if ($status == 3) {
                    $result = array('status' => 1, 'message' => 'Invitation has been rejected');
                    $msg = 'rejected';
                    $title = 'Visit invitation rejected';
                } else {
                    $update = DB::table('bnVisitInvitations')->where(['id' => $id])->delete();
                    $result = array('status' => 0, 'message' => 'Invitation has been updated');
                }

                //Create an entry in notification table and send PUSH Notification to the Inviter
                if ($status == 2 || $status == 3) {
                    $userId = $exist->userId;
                    $toUserId = $exist->toUserId;

                    $user = Patron::select('firstName', 'lastName')
                        ->where(['userId' => $toUserId])->first();
                    if (!empty($user)) {
                        $message = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has ' . $msg . ' your invitation';

                        $record = array('moduleType' => 'changeInvitationStatus', 'moduleId' => $id, 'description' => $message, 'userId' => $userId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = UserNotification::create($record);

                        if ($not['id']) {
                            $deviceToken = CommonFunction::getUserDeviceData($userId); //Get Device Tokens
                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'changeInvitationStatus', 'moduleId' => $id, 'userId' => $userId, 'title' => $title, 'msg' => $message);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }
                    }
                }

            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'This request is no longer available');
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

}