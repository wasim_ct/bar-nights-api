<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\TopSpender;
use CommonFunction;
use Illuminate\Http\Request;

class ToppersController extends Controller
{
    //Funtion to top spenders of the last month
    public function getTopSpenders(Request $request)
    {
        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //Get last month
        $currentMonth = date('F');
        $lastMonth = strtolower(date('F-Y', strtotime(date('Y-m') . " -1 month")));

        $spenders = TopSpender::select('bnTopSpenders.*', 'p.firstName', 'p.lastName', 'p.profilePic')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnTopSpenders.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnTopSpenders.userId')
            ->where(['u.status' => 1, 'bnTopSpenders.month' => $lastMonth])
            ->orderBy('bnTopSpenders.position')
            ->offset($startFrom)->limit($limit)
            ->get()->toArray();

        if (!empty($spenders)) {

            //Converting NULL to "" String
            array_walk_recursive($spenders, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Top spenders has been received successfully", 'data' => $spenders, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No top spender found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

}