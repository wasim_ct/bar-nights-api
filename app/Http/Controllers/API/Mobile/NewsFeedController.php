<?php
namespace App\Http\Controllers\API\Mobile;

use App\Friend;
use App\Http\Controllers\Controller;
use App\NewsFeed;
use App\NewsFeedComment;
use App\NewsFeedReaction;
use App\Patron;
use App\UserMediaFile;
use App\UserNotification;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;
use DB;

class NewsFeedController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Add news feed function
    public function addNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            // 'media' => 'mimes:BMP,bmp,gif,GIF,IOC,ico,jpeg,jpg,JPG,JPEG,png,PNG,svg ,SVG,3gp ,3GP,avi,AVI,flv,FLV,h264,H264,m4v,M4V,MKV,MOV,MPG,MPEG,RM,SWF,VOB,WMV,mkv,mov,mp4,mpg,mpeg,rm ,swf,vob,wmv|max:50000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }
        
        //check abusive keyword 17-02-2020
        if (CommonFunction::isAbusiveContent($description)) {
            $result = array('status' => 0, 'message' => 'Description should not contain abusive word');
            return response()->json($result);
        }

        // If Location is tagged, from google map
        $locationId = 0;
        /*if(isset($latitude) && !empty($latitude) && isset($longitude) && !empty($longitude) && isset($address) && !empty($address)) {
            $location = DB::table('bnNewsFeedLocations')->select('id')
                ->where(['latitude' => $latitude, 'longitude' => $longitude, 'address' => $address])->first();

            if (!empty($location)) {
                $locationId = $location->id;
            }
            else{
                $param = array(
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'address' => $address,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate 
                );
                $locationId = DB::table('bnNewsFeedLocations')->insertGetId($param);
            }
        }*/

        if(isset($request->locationId) && !empty($request->locationId)) {
            $locationId = $request->locationId;
        }
 
        //Create an entry in News Feed table
        $data = array('userId' => $userId, 'visibility' => 1, 'locationId' => $locationId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
        if (isset($description)) {
            $data['description'] = $description;
        }

        // If area is tagged
        if (isset($locationTag) && !empty($locationTag)) {
            $data['locationTag'] = $locationTag = trim($locationTag);

            // Get location ID based on location
            $area = DB::table('bnCityAreas as ca')->select('ca.id', 'city.city_name as cityName')
                ->leftJoin('bnCities as city', 'city.city_id', '=', 'ca.cityId')
                ->where(['ca.area' => $locationTag])
                ->first();
            if(!empty($area)) {
                $data['locationTag'] = $locationTag.', '.$area->cityName;
                $data['locationId'] = $area->id;
            }
        }

        $feed = NewsFeed::create($data);

        if ($feed['id']) {
            //Check media is exist or not in the request
            if ($request->hasFile("media")) {
                $files = $request->file("media");
                foreach($files as $file){
                    $mediaSize = $file->getSize(); //File size
                    $ext = $file->getClientOriginalExtension(); //get extention of file
                    
                    // Get Mime type of the file, to store mediaType in DB
                    $mineType = $file->getMimeType();
                    $mType = explode("/", $mineType);
                    if($mType[0] == 'image')
                        $mediaType = 1;
                    else
                        $mediaType = 2;
                    
                    $mediaName = time() * rand() . "." . $ext;
                    $upload = $file->move("uploads/patrons/newsfeed/", $mediaName);
                    if ($upload) {
                        $mediaData = array(
                            'userId' => $userId,
                            'mediaName' => $mediaName,
                            'moduleId' => $feed['id'],
                            'mediaType' => $mediaType,
                            'mediaSize' => $mediaSize,
                        );
                        UserMediaFile::create($mediaData);
                    }
                }
            }

            // Store the hashtags, if available in the string
            if (isset($description) && !empty($description)) {
                $hashtags = CommonFunction::getHashTags($description);
                if(!empty($hashtags)) {
                    foreach ($hashtags as $h) {

                        $tag = DB::table('bnHashTags')->select('id')->where(['hashtag' => $h])->first();

                        if (!empty($hashtag)) {
                            $hashtagId = $hashtag->id;
                        }
                        else{
                            $param = array(
                                'hashtag' => strtolower($h),
                                'createdAt' => $this->entryDate,
                                'updatedAt' => $this->entryDate 
                            );
                            $hashtagId = DB::table('bnHashTags')->insertGetId($param);
                        }

                        if(isset($hashtagId)) {
                            $param = array(
                                'hashtagId' => $hashtagId,
                                'newsId' => $feed['id']
                            );
                            DB::table('bnHashTagPosts')->insertGetId($param);
                        }
                    }
                }
            }

            // Tag Friends, if availalbe
            if(isset($friendIds) && !empty($friendIds)){
                $friendIds = explode(",", $friendIds);
                foreach ($friendIds as $id) {
                    $param = array(
                        'newsId' => $feed['id'],
                        'friendId' => $id
                    );
                    DB::table('bnNewsFeedFriends')->insertGetId($param);
                }
            }

            // Send Notification to the friends of the user
            $friendData = Friend::select('friendId')->where(['userId' => $userId])->get();
            if (count($friendData) > 0) {
                $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();
                
                $name = 'Someone has';
                if (!empty($user)) {
                    $name = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has';
                }
                $newsFeed = $feed['id'];
                
                foreach ($friendData as $fd) {
                    $message = $name . ' posted a new post';

                    // If friend is tagged, then need to change the message
                    if(isset($friendIds) && !empty($friendIds)){
                        if (in_array($fd->friendId, $friendIds)){
                            $message = $name . ' tagged you in a post';
                        }
                    }

                    $record = array('moduleType' => 'newsfeed', 'moduleId' => $newsFeed, 'description' => $message, 'userId' => $fd->friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $not = UserNotification::create($record);

                    if ($not['id']) {
                        $notCreated = true;
                        $deviceToken[] = CommonFunction::getUserDeviceData($fd->friendId);
                        //$userId[] = $uid->userId;
                    }
                }
                
                //If notification created, then send push notification also
                if (isset($notCreated)) 
                {
                    $deviceToken = call_user_func_array('array_merge', $deviceToken);
                
                    //$deviceToken = CommonFunction::getAllUserDeviceData();
                    if ($deviceToken) {
                        //Firebase Notification
                        $payLoadData = array('moduleType' => 'newsfeed', 'moduleId' => $newsFeed, 'title' => 'New post posted', 'msg' => $message);
                        $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                        // print_r($rs);exit;
                    }
                }
            }
            $result = array('status' => 1, 'message' => 'News feed has been posted successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Post comments on news feed function
    public function postNewsComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'newsId' => 'required',
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => "Validation error occurred");
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }
        
        //check abusive keyword
        if (CommonFunction::isAbusiveContent($request->comment)) {
            $result = array('status' => 0, 'message' => 'Comment should not contain abusive word');
            return response()->json($result);
        }
        
        //First check the news is exist or not
        $postUserId = CommonFunction::GetSingleField('bnNewsFeeds', 'userId', 'id', $newsId);
        if ($postUserId) {
            //Create an entry in News Feed table
            $data = $request->all();
            $data['createdAt'] = $this->entryDate;
            $data['updatedAt'] = $this->entryDate;
            $com = NewsFeedComment::create($data);

            if ($com['id']) {

                if($postUserId != $userId) {
                    //Create an entry in notification table and send PUSH Notification
                    $user = Patron::select('firstName', 'lastName')
                        ->where(['userId' => $userId])->first();
                    $name = 'Some one has';
                    if (!empty($user)) {
                        $name = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has';
                    }

                    $message = $name . ' commented on your post';

                    $record = array('moduleType' => 'postComment', 'moduleId' => $newsId, 'description' => $message, 'userId' => $postUserId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $not = UserNotification::create($record);

                    if ($not['id']) {
                        $deviceToken = CommonFunction::getUserDeviceData($postUserId); //Get Device Tokens
                        if ($deviceToken) {
                            //Firebase Notification
                            $payLoadData = array('moduleType' => 'postComment', 'moduleId' => $newsId, 'userId' => $postUserId, 'title' => 'New comment posted', 'msg' => $message);
                            $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                        }
                    }
                }

                $result = array('status' => 1, 'message' => 'Comment has been posted successfully');
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }

        } else {
            $result = array('status' => 0, 'message' => 'No post found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Post comments on news feed function
    public function deleteComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //First check the news is exist or not
        $newsComment = new NewsFeedComment;
        $comment = $newsComment->find($id);
        if (!empty($comment)) {
            $delete = $comment->delete();

            if ($delete) {
                $result = array('status' => 1, 'message' => 'Comment has been deleted successfully');
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }

        } else {
            $result = array('status' => 0, 'message' => 'No comment found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //post News Reaction function
    public function postNewsReaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newsId' => 'required',
            'userId' => 'required',
            'reactionId' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }

        //First check the news is exist or not
        $postUserId = CommonFunction::GetSingleField('bnNewsFeeds', 'userId', 'id', $newsId);
        if ($postUserId) {
            //check status we are taking if 0 then delete the reaction else insert
            if ($status == 0) {
                $feedReactions = NewsFeedReaction::where(['userId' => $userId, 'newsId' => $newsId, 'reactionId' => $reactionId])->delete();
                if ($feedReactions) {
                    $result = array('status' => 1, 'message' => 'Reaction has been deleted successfully');
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong');
                }
            } 
            else {
                //check reaction for same user on same news id is exist or not
                $reactionsExist = NewsFeedReaction::select('*')->where(['userId' => $userId, 'newsId' => $newsId])->first();
                if ($reactionsExist) {

                    if ($reactionsExist->reactionId == $reactionId) {
                        //if exist check newsid,user id reaction id is same then delete
                        $feedReactionsExist = NewsFeedReaction::where(['userId' => $userId, 'newsId' => $newsId, 'reactionId' => $reactionId])->delete();
                        $status = true;
                        $msg = 'Reaction deleted successfully';

                    } else if ($reactionId == 0) {
                        //if exist check newsid,user id reaction id is same then delete
                        $feedReactionsExist = NewsFeedReaction::where(['userId' => $userId, 'newsId' => $newsId])->delete();
                        $status = true;
                        $msg = 'Reaction deleted successfully';

                    } else {
                        //if exist check newsid,user id is same and reaction id is different then  update
                        $feedReactionsExist = NewsFeedReaction::where(['userId' => $userId, 'newsId' => $newsId])->update(['reactionId' => $reactionId, 'updatedAt' => $this->entryDate]);
                        $status = true;
                        $msg = 'Reaction updated successfully';
                    }
                } else {
                    //Create an entry in News Feed reaction table
                    $data = array('userId' => $userId, 'newsId' => $newsId, 'reactionId' => $reactionId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $feedReactions = NewsFeedReaction::create($data);
                    if ($feedReactions['id']) 
                    {
                        if($postUserId != $userId) 
                        {
                            //Create an entry in notification table and send PUSH Notification
                            $user = Patron::select('firstName', 'lastName')
                                ->where(['userId' => $userId])->first();
                            $name = 'Some one has';
                            if (!empty($user)) {
                                $name = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has';
                            }

                            $message = $name . ' reacted on your post';

                            $record = array('moduleType' => 'postReaction', 'moduleId' => $newsId, 'description' => $message, 'userId' => $postUserId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                            $not = UserNotification::create($record);

                            if ($not['id']) {
                                $deviceToken = CommonFunction::getUserDeviceData($postUserId); //Get Device Tokens
                                if ($deviceToken) {
                                    //Firebase Notification
                                    $payLoadData = array('moduleType' => 'postReaction', 'moduleId' => $newsId, 'userId' => $postUserId, 'title' => 'New reaction posted', 'msg' => $message);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                }
                            }
                        }

                        $status = true;
                        $msg = 'News feed reaction inserted successfully';

                    }

                }

                //get total of reactions
                $newsfeedReaction = NewsFeedReaction::select('id')->where(['newsId' => $newsId])->get();
                if (count($newsfeedReaction) > 0) {
                    $totalReactions = count($newsfeedReaction);
                } else {
                    $totalReactions = 0;
                }
                if ($status == true) {
                    $result = array('status' => 1, 'message' => $msg, 'totalRows' => $totalReactions);
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong');
                }
            }
        } else {
            $result = array('status' => 0, 'message' => 'No data found');
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //delete News feed function
    public function deleteNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //First check the news is exist or not
        $newsExist = CommonFunction::GetSingleField('bnNewsFeeds', 'id', 'id', $id);
        if ($newsExist) {
            //delete the new from news feed table
            $newsDelete = NewsFeed::where(['id' => $id])->delete();
            if ($newsDelete) {
                //delete all the reactions for that news feed
                NewsFeedReaction::where(['newsId' => $id])->delete();
                //delete all the comment for that news feed
                NewsFeedComment::where(['newsId' => $id])->delete();
                //delete media of news feed
                UserMediaFile::where(['moduleId' => $id])->delete();

                //Delete tagged friends
                DB::table('bnNewsFeedFriends')->where(['newsId' => $id])->delete();

                $result = array('status' => 1, 'message' => 'News feed deleted successfully');
            } else {
                $result = array('status' => 1, 'message' => 'News feed not deleted');
            }
        } else {
            $result = array('status' => 1, 'message' => 'News feed not exist');
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //get News feed function
    public function getMyNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            // 'myId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }

        $newsFeedDetails = array();

        //Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get the news of user
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.description', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt', 'bnNewsFeeds.locationTag', 'p.firstName', 'p.lastName', 'p.profilePic', 'p.userId')
            ->leftJoin('bnNewsFeedFriends as nf', 'nf.newsId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->where(['bnNewsFeeds.userId' => $userId, 'bnNewsFeeds.isDeleted' => 0])
            // ->orWhere(['nf.friendId' => $userId])
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->groupBy('bnNewsFeeds.id')
            ->offset($startFrom)->limit($limit)->get();
        //get count of  new feeds
        $nfcount = NewsFeed::select('bnNewsFeeds.id')->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')->where(['bnNewsFeeds.userId' => $userId])->get();

        if (count($newsfeed) > 0) {
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $myId, 'newsId' => $nf->id])->first();

                $nf['reacted'] = 0;

                if ($nfReact) {
                    $nf['reacted'] = $nfReact->reactionId;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();

                //Get the News meda files
                $nf['mediaFiles'] = DB::table('bnUserMediaFiles')->select('id', 'mediaName', 'mediaType')
                ->where(['moduleId' => $nf->id])->get()->toArray();

                $nf['totalComment'] = count($newsfeedComment);
                $nf['totalReaction'] = count($newsfeedReaction);
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }

            $media = CommonFunction::getMediaPath();
            $countNews = count($nfcount);
            $result = array('status' => 1, 'message' => 'News feed get successfully', 'data' => $newsFeedDetails, 'mediaPath' => $media, 'totalNewsFeeds' => $countNews);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    // Function to get Tagged News feed
    public function getTaggedNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }
        
        $newsFeeds = NewsFeed::select('bnNewsFeeds.id', 'umf.mediaName', 'umf.mediaType')
            ->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnNewsFeedFriends as nf', 'nf.newsId', '=', 'bnNewsFeeds.id')
            ->where(['nf.friendId' => $userId, 'bnNewsFeeds.isDeleted' => 0])
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->groupBy('bnNewsFeeds.id')->get()->toArray();
        
        if (count($newsFeeds) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($newsFeeds, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $result = array('status' => 1, 'message' => 'News feed get successfully', 'data' => $newsFeeds);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Get single new feed detail
    public function getNewsFeedDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newsId' => 'required',
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $newsFeedDetails = array();

        //get the newsfeed
        $nf = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'bnNewsFeeds.description', 'bnNewsFeeds.locationTag',  'bnNewsFeeds.visibility', 'bnNewsFeeds.locationId', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['bnNewsFeeds.id' => $newsId])
            ->first();

        if ($nf) {

            // Check user reacted or not on this newsfeed
            $nf['reactionIdByUser'] = 0;
            $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $userId, 'newsId' => $nf->id])->first();

            if ($nfReact) {
                $nf['reactionIdByUser'] = $nfReact->reactionId;
            }
        
            //get reactions on news feed by news feed id and user Id
            $nf['reaction'] = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
            
            //get comments on news feed by news feed id and user Id
            $nf['comments'] = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();

            //Get the News meda files
            $nf['mediaFiles'] = DB::table('bnUserMediaFiles')->select('id', 'mediaName', 'mediaType')
            ->where(['moduleId' => $nf->id])->get()->toArray();

            //Get the tagged friends
            $nf['taggedFriends'] = DB::table('bnNewsFeedFriends as f')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'f.friendId')
                ->select('p.firstName', 'p.lastName', 'p.profilePic', 'p.userId')
                ->where(['f.newsId' => $nf->id])
                ->get()->toArray();

            // Get tagged location
            /*$location = DB::table('bnNewsFeedLocations as l')
                ->select('l.latitude', 'l.longitude', 'l.address')
                ->where(['l.id' => $nf->locationId])
                ->first();
            $nf['location'] = !empty($location) ? $location : (object) array();*/
            $nf['location'] = (object) array();
            
            $nf['totalComment'] = count($nf['comments']);
            $nf['totalReaction'] = count($nf['reaction']);
            
            //Converting NULL to "" String
            array_walk_recursive($nf, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'News feed details get successfully', 'data' => $nf);
        }
        else
            $result = array('status' => 0, 'message' => 'No record found');
        return response()->json($result);
    }

    //get comments and reactions of news feeds function
    public function getCommentsReactions(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newsId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $newsFeedDetails = array();

        //get reactions on news feed by news feed id and user Id
        $data['reaction'] = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')
            ->where(['bnNewsFeedReactions.newsId' => $newsId])
            ->orderBy('id', 'desc')->get()->toArray();

        //get comments on news feed by news feed id and user Id
        $data['comment'] = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')
            ->where(['bnNewsFeedComments.newsId' => $newsId])
            ->orderBy('id', 'desc')->get()->toArray();

        //Converting NULL to "" String
        array_walk_recursive($data, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        $media = CommonFunction::getMediaPath();

        if ($data != '') {
            $result = array('status' => 1, 'message' => 'News feed details get successfully', 'data' => $data, 'mediaPath' => $media);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //report news feed function
    public function reportNewsFeed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newsId' => 'required',
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        
        
        $alreadyRepored = DB::table('bnNewsFeedReportUsers')->select('id')
            ->where(['newsId' => $newsId, 'userId' => $userId])->first();
        if(!empty($alreadyRepored)) {
            $result = array('status' => 0, 'message' => 'You already reported this news feed.');
            return response()->json($result);
        }

        //First check the news is exist or not
        $newsFeedExist = NewsFeed::select('id')->where(['id' => $newsId])->first();

        if (!empty($newsFeedExist)) {
            $update = NewsFeed::where(['id' => $newsId])
                ->update(['isReported' => 1, 'isIgnore' => 0, 'updatedAt' => $this->entryDate]);
            
            if ($update) 
            {
                // Create Entry of the user who reported
                $param = array(
                    'userId' => $userId,
                    'newsId' => $newsId,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate 
                );
                
                if(isset($message) && !empty($message))
                    $param['message'] = $message;
                
                DB::table('bnNewsFeedReportUsers')->insertGetId($param); 

                $result = array('status' => 1, 'message' => 'News feed reported successfully');
            } 
            else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Invalid News Feed');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }
}