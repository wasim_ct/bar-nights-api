<?php
namespace App\Http\Controllers\API\Mobile;

use App\Bar;
use App\EventBooking;
use App\Http\Controllers\Controller;
use App\SpecialEvent;
use App\Wallet;
use App\Transaction;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class SpecialEventController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    // Function to get all events
    public function getEvents(Request $request)
    {
        extract($_POST);
        $data = [];

        //Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        $currentDate = date('Y-m-d');
        
        //Calling the Discount/Offers with 3 KMs
        $sql = "SELECT se.id, se.barId, se.title, se.eventImage, se.description, se.startDate, se.endDate, se.startTime, se.endTime, se.entryfees, se.coverCharges, se.totalTickets, se.totalStandingTickets,se.venue, se.discountRate,br.rating, b.barName, b.logo, a.address, a.latitude, a.longitude,c.city_name as cityName,
                111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnSpecialEvents se
                INNER JOIN bnBars b on b.id = se.barId
                INNER JOIN bnBarRatings br on br.barId = b.id
                INNER JOIN bnAddresses a on a.userId = b.userId
                INNER JOIN bnCities c on c.city_id = a.cityId
                INNER JOIN bnUsers u on u.id = b.userId
                WHERE se.status = 1 AND se.endDate >= '$currentDate'";

        //Filter Conditions Start
        //If keyword is set
        if (isset($keyword)) {
            $sql .= " AND se.title LIKE '%" . $keyword . "%' OR c.city_name LIKE '%" . $keyword . "%' OR se.description LIKE '%" . $keyword . "%'";
        }

        // If date is set
        if (isset($date) && $date != '') {
            $sql .= " AND se.startDate = '$date'";
        }

        // If week is set
        if (isset($filter) && $filter != '') {
            if ($filter == 'week') {
                $sql .= " AND se.startDate >= '$currentDate'";
            } 
            else if ($filter == 'month') {
                $sql .= " AND MONTH(se.startDate) = MONTH(CURRENT_DATE()) AND se.startDate >= '$currentDate'";
            }
        }

        $sql .= " AND u.userType = 'B' AND u.status = 1 HAVING distanceInKM <= 3000 ORDER BY se.startDate ASC limit $startFrom, $limit";

        // get special events
        $specialEvents = DB::select(DB::raw($sql));

        if (count($specialEvents) > 0) {

            $events = array();
            foreach ($specialEvents as $row) {
                //Converting NULL to "" String
                array_walk_recursive($row, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                // If values are empty, then pass 0
                if(empty($row->discountRate))
                    $row->discountRate = 0;

                if(empty($row->coverCharges))
                    $row->coverCharges = 0;

                if(empty($row->rating))
                    $row->rating = 0;

                $events[] = $row;
            }
            
            // Get the Wallet Balance of ther user
            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
            $walletAmt = ($walletAmt) ? $walletAmt : 0;

            $result = array('status' => 1, 'message' => "Special events has been received successfully", 'data' => $events, 'walletAmt' => $walletAmt);
        } 
        else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //insert in book event tickets
    public function bookEventTicket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'eventId' => 'required',
            'totalSeats' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // get entry fee, coverCharges of the event
        $eventDetail = SpecialEvent::select('barId', 'entryfees', 'totalTickets', 'coverCharges')->where('id', $eventId)->first();

        // count total seats that are alerady booked for that event
        $totalBooked = CommonFunction::countBookedSeats($eventId);

        // get seats available in that event
        $remainigSeats = ($eventDetail->totalTickets) - $totalBooked;

        if ($eventDetail->totalTickets == $totalBooked) {
            $result = array('status' => 0, 'message' => "No seats are available");
        } 
        else if ($remainigSeats >= $totalSeats) {

            $code = CommonFunction::generateEventCode();

            // calculate amount by multiply by seat no and entry fee
            $amount = ($eventDetail->entryfees + $eventDetail->coverCharges) * $totalSeats;
            $insert = array(
                'userId' => $userId,
                'eventId' => $eventId,
                'totalSeats' => $totalSeats,
                'amount' => $amount,
                'code' => $code,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate,
            ); 
            $eventBookingInsert = EventBooking::create($insert);
            if ($eventBookingInsert['id']) 
            {
                // Get the Wallet Balance of ther user
                $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
                $walletAmt = ($walletAmt) ? $walletAmt : 0;

                $data = array(
                    'bookingId' => $eventBookingInsert['id'],
                    'amount' => $amount,
                    'walletAmt' => $walletAmt,
                    'barId' => $eventDetail->barId,
                );
                $result = array('status' => 1, 'message' => "Ticket has been booked successfully", 'data' => $data);
            } 
            else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }
        } else if ($remainigSeats < $totalSeats) {
            $result = array('status' => 0, 'message' => "Only " . $remainigSeats . " seats are available");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to Update Event Payment and transaction
    public function updateEventPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'bookingId' => 'required',
            //'transactionId' => 'required',
            'paymentMode' => 'required', //|in:cash,online,card,wallet
            'amount' => 'required', //TokenMoney
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
        $response = (object) array();
        $status = false;

        /* === If all payment paid through BN Wallet === */
        if(isset($walletMoney) && $walletMoney > 0 && $walletMoney >= $amount) {
            $param = array(
                'barId' => $barId,
                'userId' => $userId,
                'moduleId' => $bookingId,
                'moduleType' => 'event',
                'paymentMode' => 'bnwallet',
                'amount'    => $amount,
                'description' => 'Paid for event booking',
                'totalPrice' => $amount,
                'paymentDate' => date('Y-m-d'),
                'status' => 1,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate
            );

            $transaction = Transaction::create($param);
            if($transaction){
                // Update the Event booking payment status
                $param = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
                EventBooking::where('id', $bookingId)->update($param);
                
                // Decrease the wallet balance
                Wallet::where('userId',$userId)->decrement('amount', $amount);

                /* === Transaction in wallet table === */
                $param = array('userId' => $userId, 'description' => "Paid for event booking", 'amount' => $amount, 'moduleId' => $bookingId, 'moduleType' => 'event', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                DB::table('bnWalletTransactions')->insertGetId($param); 

                $status = true;
            }
        }
        /* === If wallet money is less than to final amount === */
        else 
        {
            /* === If wallet money is used, then make a transaction in wallet table === */
            if(isset($walletMoney) && $walletMoney > 0) {
                // Decrease the wallet balance
                Wallet::where('userId',$userId)->decrement('amount', $walletMoney);

                $param = array('userId' => $userId, 'description' => "Paid for event booking", 'amount' => $walletMoney, 'moduleId' => $bookingId, 'moduleType' => 'event', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                DB::table('bnWalletTransactions')->insertGetId($param);
                
                // Wallet total price calculation;
                /*$ratio = ($walletMoney/$amount)*100;
                $ratioAmt = ($walletMoney/100)*$ratio;
                $walletTotalAmount = $walletMoney-$ratioAmt;*/

                $param = array(
                    'barId' => $barId,
                    'userId' => $userId,
                    'moduleId' => $bookingId,
                    'moduleType' => 'event',
                    'paymentMode' => 'bnwallet',
                    'amount'    => $walletMoney,
                    'description' => 'Paid for event booking',
                    'totalPrice' => $walletMoney,
                    'paymentDate' => date('Y-m-d'),
                    'status' => 1,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate
                );
                $transaction = Transaction::create($param);

                // Reset the Amount, to continue processing
                $amount = $amount - $walletMoney;
            }

            /* === Now transaction entry based on other payment modes === */
            $param = array(
                'barId' => $barId,
                'userId' => $userId,
                'moduleId' => $bookingId,
                'moduleType' => 'event',
                'transactionId' => $transactionId,
                'paymentMode' => $paymentMode,
                'amount'    => $amount,
                'description' => 'Paid for event booking',
                'totalPrice' => $amount,
                'paymentDate' => date('Y-m-d'),
                'status' => 1,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate
            );
            
            $transaction = Transaction::create($param);
            if($transaction){
                // Update the table booking payment status
                $param = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
                EventBooking::where('id', $bookingId)->update($param);
                
                /* === Add the amount in user wallet if available === */
                $appSettings = CommonFunction::getAppSettings();
                $cashBackOn = $appSettings['cashBackOn'];
                if($cashBackOn > 0 && $amount >= $cashBackOn) {

                    $thousandCount = (int)($amount/$cashBackOn);
                    $cashBackAmount = $appSettings['cashBackAmount'] * $thousandCount;

                    //Update the wallet amount or Insert
                    $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
                    if(!empty($walletAmt) || $walletAmt === 0.00) { //bcz 0 can be an amount
                        // Update the amount
                        $param = array('amount' => $walletAmt + $cashBackAmount,'updatedAt' => $this->entryDate);
                        $wStatus = DB::table('bnWallets')
                            ->where(['userId' => $userId])
                            ->update($param);
                    }
                    else {
                        $param = array('userId' => $userId, 'amount' => $cashBackAmount, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $wStatus = DB::table('bnWallets')->insertGetId($param);
                    }

                    // Create entry in wallet transaction
                    if(isset($wStatus)) {
                        $param = array('userId' => $userId, 'description' => "Cashback Received", 'amount' => $cashBackAmount, 'moduleId' => $bookingId, 'moduleType' => 'event', 'type' => 'received', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        DB::table('bnWalletTransactions')->insertGetId($param);

                        $response = array(
                            'cashBackReceived' => 1,
                            'cashBackAmount' => $cashBackAmount,
                        );
                    }
                } 

                $status = true;
            }
        }

        if($status){
            $result = array('status' => 1, 'message' => 'Payment Updated successfully', 'data' => $response);
        }
        else{
            $result = array('status' => 0, 'message' => 'Somethig went wrong, Please try again.');
        }
        return response()->json($result);
    }

    // Function to get My Booked Events
    public function getMyBookedEvents(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // get entry fee, coverCharges of the event
        $events = EventBooking::select('bnEventBookings.*', 'se.barId', 'se.title', 'se.eventImage', 'se.description', 'se.startDate', 'se.endDate', 'se.startTime', 'se.endTime', 'se.venue', 'br.rating', 'b.barName', 'b.logo', 'a.address', 'a.latitude', 'a.longitude', 'c.city_name as cityName')
            ->leftJoin('bnSpecialEvents as se', 'se.id', '=', 'bnEventBookings.eventId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'se.barId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'b.userId')
            ->leftJoin('bnCities as c', 'c.city_id', '=', 'a.cityId')
            ->where('bnEventBookings.userId', $userId)
            ->orderBy('bnEventBookings.id', 'desc')
            ->groupBy('bnEventBookings.id')
            ->get()->toArray();

        if(!empty($events)) {
            //Converting NULL to "" String
            array_walk_recursive($events, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $eventData = array();
            foreach ($events as $row) {
                $bookingId = $row['id'];
                $userId = $row['userId'];
                $paymentStatus = $row['paymentStatus'];
                $label = "";

                if($paymentStatus == 1)
                    $label = "Confirmed";
                
                // Get the order-visit status of the event booking
                $orderData = DB::table('bnVisits as v')
                    ->select('v.id as visitId', 'v.status as visitStatus', 'o.status as orderStatus')
                    ->leftJoin('bnOrders as o', 'o.visitId', '=', 'v.id')
                    ->where(['v.requestType' => 'event', 'v.requestId' => $bookingId])
                    ->first();
                if(!empty($orderData)) {
                    $vStatus = $orderData->visitStatus;

                    if($vStatus == 1)
                        $label = "Visit Pending";
                    else if($vStatus == 2)
                        $label = "Code Availed";
                    else if($vStatus == 3)
                        $label = "Visit Cancelled";
                    else if($vStatus == 4)
                        $label = "Completed";
                }
                else {
                    $today = date("Y-m-d");
                    if($today > $row['startDate'])
                        $label = "Expired";
                }
                $row['label'] = $label;

                // Get the payment info
                $trData = DB::table('bnTransactions as t')
                    ->select('t.amount', 't.paymentMode', 't.createdAt')
                    ->where(['t.moduleType' => 'event', 't.moduleId'=>$bookingId,'t.userId'=>$userId])
                    ->get();

                $payData = array();
                if(!empty($trData)) {
                    foreach ($trData as $trow) {
                        if($trow->paymentMode == 'bnwallet')
                            $trow->paymentMode = "Bar Nights Wallet";

                        $payData[] = $trow;
                    }
                }
                $row['paymentData'] = $payData;
                $eventData[] = $row;
            }
            
            $result = array('status' => 1, 'message' => "Events get successfully", 'data' => $eventData);
        }
        else{
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }
}