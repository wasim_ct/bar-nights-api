<?php
namespace App\Http\Controllers\API\Mobile;

use DB;
use App\Bar;
use App\BarBanner;
use App\BarItem;
use App\BarItemPrice;
use App\BarMediaFile;
use App\BarRating;
use App\BarReview;
use App\BarTiming;
use App\DiscountOffer;
use App\GeoTagging;
use App\HappyHoursTiming;
use App\Http\Controllers\Controller;
use App\ItemCategory;
use App\SpecialEvent;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;

class BarController extends Controller
{
    public $successStatus = 200;

    /**
     * bar profile api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    {
        $this->middleware('guest');
        $this->entryDate = date("Y-m-d H:i:s");
    }

    public function getBarProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $data = [];

        //get user basic profile data of bar with status 1 and user type b
        $data['barData'] = Bar::select('bnBars.id as barId', 'bnBars.type', 'bnBars.barName', 'bnBars.logo', 'bnBars.about', 'a.address', 'a.latitude', 'a.longitude', 'bnBars.cgst', 'bnBars.sgst', 'bnBars.serviceCharge', 'bnBars.serviceChargeTax', 'bnBars.openingStatus', 'u.phone')
            ->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
            ->where(['bnBars.id' => $barId, 'u.status' => 1, 'u.userType' => 'B'])->first();

        if ($data['barData']) {
            //Converting NULL to "" String
            if (!empty($data['barData'])) {
                array_walk_recursive($data['barData'], function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            //Getting current day opening/closing status
            $openingStatus = $data['barData']->openingStatus;
            
            // Get bar visibility
            $visibility = CommonFunction::getBarVisibility($data['barData']->barId);
            if($visibility) {
                $data['barData']->openingStatus = $openingStatus = $visibility['status'];
                $data['barData']->label = $visibility['message'];
            }

            //get banner of bar which(mediaType:-1:video,2:image)
            $data['banner'] = BarBanner::select('id', 'mediaName', 'mediaType')
                ->where('barId', $barId)
                ->orderBy('displayOrder', 'asc')
                ->get()->toArray();

            //bar opening and clossing time for all days(openingStatus:-1:open,0:close)
            $curentday = strtolower(date('l'));
            $openHour = array();
            $openingHrs = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')->where('barId', $barId)->where('openingTime', '!=', null)->where('closingTime', '!=', null)->get(); //'openingStatus',
            
            if (count($openingHrs) > 0) {
                foreach ($openingHrs as $openHrs) {
                    $openHour[] = $openHrs;
                }
            }

            $timing['openingStatus'] = $openingStatus;

            $timing['hours'] = $openHour;
            $data['timing'] = $timing;

            //get images and videos of bar in limit of 10(mediaType:-1:video,2:image)
            $data['imagesVideos'] = BarMediaFile::select('id', 'mediaName', 'mediaType')
                ->where('barId', $barId)->
                orderBy('id', 'desc')->limit(10)->get()->toArray();

            // Get GeoTagged Users
            $activePatrons = array();  $keysToUnset = array();
            $patrons = GeoTagging::select('bnGeoTaggings.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'p.userId', 'u.userName', 'u.email', 'u.phone')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnGeoTaggings.userId')
                ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
                ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
                ->orderBy('bnGeoTaggings.id', 'desc')
                ->where('bnGeoTaggings.barId', $barId)
                ->where('v.status', 2) //2 means confirmed
                ->limit(10)->get()->toArray();
            if(!empty($patrons)) {
                foreach ($patrons as $i => $row) {
                    //$row['incognito'] = CommonFunction::checkUserIncognitoMode($row['userId']);
                    $mode = CommonFunction::checkUserIncognitoMode($row['userId']);
                    if($mode == 0)
                        $activePatrons[] = $row;
                }
            } 
            $data['patrons'] = $activePatrons;

            //Get the active discount/offers
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            // get category and items
            $catItems = array();
            $foodCat = array();
            $foodCategory = ItemCategory::select('id', 'catName', 'catImage')->where(['barId' => $barId, 'status' => 1])->where('parentId', '!=', 0)->get();

            if (count($foodCategory) > 0) {
                $loop = 1;
                foreach ($foodCategory as $fc) {
                    if($fc->catImage == NULL)
                        $fc->catImage = "";

                    if ($loop == 1) { 
                        /*
                         * get all food items of bar with discounted/happy hour price and that are active
                         * also([itemType:-1:veg,2:non-veg])
                         */

                        $menu = BarItem::select('bnBarItems.id', 'bnBarItems.itemName', 'bnBarItems.itemImage', 'bnBarItems.description', 'bnBarItems.itemType', 'bnBarItems.price', 'bnBarItems.isCustomized', 'ip.price as unitPrice')
                            ->leftJoin('bnBarItemPricing as ip', 'ip.itemId', '=', 'bnBarItems.id')
                            ->where(['bnBarItems.subCatId' => $fc->id, 'bnBarItems.status' => 1])
                            ->orderBy('bnBarItems.id', 'desc')
                            ->groupBy('ip.itemId')->limit(5)->get();

                        foreach ($menu as $items) {
                            $items['discountPrice'] = 0;
                            $items['discount'] = 0;
                            $items['happyHourPrice'] = 0;
                            $items['happyHourOffer'] = 0;
                            $items['buy'] = 0;
                            $items['free'] = 0;

                            if(empty($items->description))
                                $items->description = ""; 

                            //Take item price in a variable for further calculation
                            if ($items->isCustomized == 0) {
                                $price = $items->price;
                            } else {
                                $price = $items->unitPrice;
                            }

                            //If discount is available for the day
                            if ($discountData) {
                                $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $price);

                                if (!empty($disData)) {
                                    $items['discount'] = $disData['discount'];
                                    $items['discountPrice'] = round($disData['discountPrice'], 2);
                                }
                            }

                            //If Happy Hour is available for the day
                            if ($happyHourData) {
                                $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $price);
                                if (!empty($hhData)) {
                                    if (isset($hhData['happyHourPrice'])) {
                                        $items['happyHourOffer'] = 'discount';
                                        $items['happyHourPrice'] = round($hhData['happyHourPrice'], 2);
                                    } else {
                                        $items['happyHourOffer'] = 'offer';
                                        $items['buy'] = $hhData['buy'];
                                        $items['free'] = $hhData['free'];
                                    }
                                }
                            }

                            $items->price = $price;
                            unset($items['unitPrice']);
                            $catItems[] = $items;
                            $fc['catItems'] = $catItems;
                        }
                    }
                    $foodCat[] = $fc;
                    $loop++;
                }
                //Converting NULL to "" String
                if (!empty($foodCat)) {
                    array_walk_recursive($foodCat, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }
                $data['menuItems'] = $foodCat;
            } 
            else {
                $data['menuItems'] = array();
            }

            //get average rating of bar given by users
            $rating = BarRating::select('id', 'rating', 'totalUsers')->where('barId', $barId)->first();
            if ($rating) {
                $data['rating'][] = $rating;
            } else {
                $data['rating'] = [];
            }

            //get all reviews of bars given by patorns with patorns name
            $data['review'] = BarReview::select('bnBarReviews.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnBarReviews.rating', 'bnBarReviews.review')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnBarReviews.userId')
                ->where('bnBarReviews.barId', $barId)
                ->limit(1000)
                // ->groupBy('bnBarReviews.userId')
                ->orderBy('bnBarReviews.id', 'desc')
                ->get()->toArray();

            //get happy hours for bars if exist for today
            /*$happyHours = HappyHoursTiming::select('bnHappyHourTiming.id', 'bnHappyHourTiming.startTime', 'bnHappyHourTiming.endTime', 'od.title', 'od.description')
                ->join('bnOfferDetails as od', 'od.id', '=', 'bnHappyHourTiming.odId')
                ->where(['od.barId' => $barId, 'od.status' => 1])
                ->whereRaw("find_in_set('$curentday',bnHappyHourTiming.weekDay)")
                ->first();

            if ($happyHours) {
                $data['happyHourTime'][] = $happyHours;
            } else {
                $data['happyHourTime'] = [];
            } */

            /*$currentTime = date("H:i:s");
            $discount = DB::table('bnDiscountOffers as d')
                ->select('d.id', 'd.startTime', 'd.endTime', 'od.title')
                ->join('bnOfferDetails as od', 'od.id', '=', 'd.odId')
                ->where(['od.barId' => $barId, 'od.status' => 1])
                ->where('d.endTime', '>=', $currentTime)
                //->where('d.startTime', '<=', $currentTime)
                ->orderBy('d.startTime')
                ->first();
            if ($discount) {
                $data['discountData'] = $discount;
            } else {
                $data['discountData'] = (object) array();
            }*/

            // Pass offer, if exist for today
            if(!empty($happyHourData)) {
                $offData = array(
                    'id' => $happyHourData->id,
                    'startTime' => $happyHourData->startTime,
                    'endTime' => $happyHourData->endTime,
                    'title' => $happyHourData->title,
                    'description' => $happyHourData->description,
                );
                $data['happyHourTime'][] = $offData;
            }
            else {
                $data['happyHourTime'] = [];
            }
            
            // Pass offer, if exist for today
            if(!empty($discountData)) {
                $offData = array(
                    'id' => $discountData->id,
                    'startTime' => $discountData->startTime,
                    'endTime' => $discountData->endTime,
                    'title' => $discountData->title,
                );
                $data['discountData'] = $offData;
            }
            else {
                $data['discountData'] = (object) array();
            }

            $mediaPath = CommonFunction::getMediaPath();

            //Converting NULL to "" String
            if (!empty($data)) {
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $result = array('status' => 1, 'message' => "Bar profile has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "Bar profile does not exist");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    public function getBarVisibilityStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $result = CommonFunction::getBarVisibility($barId);
        
        /*$barData = Bar::select('bnBars.openingStatus')
            ->where(['bnBars.id' => $barId])->first();

        if (!empty($barData)) {
            $msg = "";
            if($barData->openingStatus == 0) {
                $result = array('status' => 0, 'message' => "This bar is currently not serving");
            }
            else  {
                $result = array('status' => 1, 'message' => "Currently Active");
            }
        } else {
            $result = array('status' => 0, 'message' => "Invalid Bar");
        }*/

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get listing of cart for barprofile
    public function getBarProfileCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //get user basic profile data of bar with status 1 and user type b
        $data['barData'] = Bar::select('bnBars.id as barId', 'bnBars.type', 'bnBars.barName', 'bnBars.logo', 'bnBars.about', 'a.address', 'a.latitude', 'a.longitude', 'bnBars.cgst', 'bnBars.sgst', 'bnBars.serviceCharge', 'bnBars.serviceChargeTax', 'bnBars.openingStatus', 'u.phone')
            ->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bnBars.userId')
            ->where(['bnBars.id' => $barId, 'u.status' => 1, 'u.userType' => 'B'])->first();

        if ($data['barData']) {
            //Converting NULL to "" String
            if (!empty($data['barData'])) {
                array_walk_recursive($data['barData'], function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            //Getting current day opening/closing status
            $openingStatus = $data['barData']->openingStatus;

            // Get bar visibility
            $visibility = CommonFunction::getBarVisibility($data['barData']->barId);
            if($visibility) {
                $data['barData']->openingStatus = $openingStatus = $visibility['status'];
                $data['barData']->label = $visibility['message'];
            }

            //get banner of bar which(mediaType:-1:video,2:image)
            $data['banner'] = BarBanner::select('id', 'mediaName', 'mediaType')
                ->orderBy('displayOrder', 'asc')
                ->where('barId', $barId)->get()->toArray();

            //bar opening and clossing time for all days(openingStatus:-1:open,0:close)
            $curentday = strtolower(date('l'));
            $openHour = array();
            $openingHrs = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')->where('openingTime', '!=', null)->where('barId', $barId)->get(); //'openingStatus',
            if (count($openingHrs) > 0) {
                foreach ($openingHrs as $openHrs) {
                    $openHour[] = $openHrs;
                }
            }
            $timing['openingStatus'] = $openingStatus;

            $timing['hours'] = $openHour;
            $data['timing'] = $timing;

            //get images and videos of bar in limit of 10(mediaType:-1:video,2:image)
            $data['imagesVideos'] = BarMediaFile::select('id', 'mediaName', 'mediaType')
                ->where('barId', $barId)->
                orderBy('id', 'desc')->limit(10)->get()->toArray();

            // Get GeoTagged Users
            $activePatrons = array();
            $patrons = GeoTagging::select('bnGeoTaggings.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'p.userId', 'u.userName', 'u.email', 'u.phone')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnGeoTaggings.userId')
                ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
                ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
                ->orderBy('bnGeoTaggings.id', 'desc')
                ->where('bnGeoTaggings.barId', $barId)
                ->where('v.status', 2) //2 means confirmed
                ->limit(10)->get()->toArray();

            if(!empty($patrons)) {
                foreach ($patrons as $i => $row) {
                    $mode = CommonFunction::checkUserIncognitoMode($row['userId']);
                    if($mode == 0)
                        $activePatrons[] = $row;
                }
            } 
            $data['patrons'] = $activePatrons;

            //Get the active discount/offers
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            //get all category of bar to get items of that category and that are active also
            $catItems = array();
            $foodCat = array();
            $foodCategory = ItemCategory::select('id', 'catName', 'catImage')->where(['barId' => $barId, 'status' => 1])->where('parentId', '!=', 0)->get();

            if (count($foodCategory) > 0) {
                $loop = 1;
                foreach ($foodCategory as $fc) {
                    if ($loop == 1) {

                        /*
                         * get all food items of bar with discounted/happy hour price and that are active
                         * also([itemType:-1:veg,2:non-veg])
                         */

                        $menu = BarItem::select('bnBarItems.id', 'bnBarItems.itemName', 'bnBarItems.itemImage', 'bnBarItems.description', 'bnBarItems.itemType', 'bnBarItems.price', 'bnBarItems.isCustomized', 'ip.price as unitPrice')
                            ->leftJoin('bnBarItemPricing as ip', 'ip.itemId', '=', 'bnBarItems.id')
                            ->where(['bnBarItems.subCatId' => $fc->id, 'bnBarItems.status' => 1])
                            ->orderBy('bnBarItems.id', 'desc')
                            ->groupBy('ip.itemId')->limit(5)->get();

                        foreach ($menu as $items) {
                            $items['discountPrice'] = 0;
                            $items['discount'] = 0;
                            $items['happyHourPrice'] = 0;
                            $items['happyHourOffer'] = 0;
                            $items['buy'] = 0;
                            $items['free'] = 0;

                            //Take item price in a variable for further calculation
                            if ($items->isCustomized == 0) {
                                $price = $items->price;
                            } else {
                                $price = $items->unitPrice;
                            }

                            //If discount is available for the day
                            if ($discountData) {
                                $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $price);

                                if (!empty($disData)) {
                                    $items['discount'] = $disData['discount'];
                                    $items['discountPrice'] = round($disData['discountPrice'], 2);
                                }
                            }

                            //If Happy Hour is available for the day

                            if ($happyHourData) {
                                $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $price);
                                if (!empty($hhData)) {
                                    if (isset($hhData['happyHourPrice'])) {
                                        $items['happyHourOffer'] = 'discount';
                                        $items['happyHourPrice'] = round($hhData['happyHourPrice'], 2);
                                    } else {
                                        $items['happyHourOffer'] = 'offer';
                                        $items['buy'] = $hhData['buy'];
                                        $items['free'] = $hhData['free'];
                                    }

                                }

                            }
                            $items->price = $price;
                            unset($items['unitPrice']);
                            $catItems[] = $items;
                            $fc['catItems'] = $catItems;
                        }
                    }
                    $foodCat[] = $fc;
                    $loop++;
                }
                //Converting NULL to "" String
                if (!empty($foodCat)) {
                    array_walk_recursive($foodCat, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }
                // $category['category'] = $foodcat;
                $data['menuItems'] = $foodCat;
            } else {
                $data['menuItems'] = array();
            }

            //get average rating of bar given by users
            $rating = BarRating::select('id', 'rating', 'totalUsers')->where('barId', $barId)->first();
            if ($rating) {
                $data['rating'][] = $rating;
            } else {
                $data['rating'] = [];
            }

            //get all reviews of bars given by patorns with patorns name
            $data['review'] = BarReview::select('bnBarReviews.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnBarReviews.rating', 'bnBarReviews.review')
                ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnBarReviews.userId')
                ->where('bnBarReviews.barId', $barId)
                ->limit(1000)
            // ->groupBy('bnBarReviews.userId')
                ->orderBy('bnBarReviews.id', 'desc')
                ->get()->toArray();

            //get happy hours for bars if exist for today
            /*$happyHours = HappyHoursTiming::select('bnHappyHourTiming.id', 'bnHappyHourTiming.startTime', 'bnHappyHourTiming.endTime', 'od.title', 'od.description')
                ->join('bnOfferDetails as od', 'od.id', '=', 'bnHappyHourTiming.odId')
                ->where(['od.barId' => $barId, 'od.status' => 1])
                ->whereRaw("find_in_set('$curentday',bnHappyHourTiming.weekDay)")
                ->first();

            if ($happyHours) {
                $data['happyHourTime'][] = $happyHours;
            } else {
                $data['happyHourTime'] = [];
            } */

            /*$currentTime = date("H:i:s");
            $discount = DB::table('bnDiscountOffers as d')
                ->select('d.id', 'd.startTime', 'd.endTime', 'od.title')
                ->join('bnOfferDetails as od', 'od.id', '=', 'd.odId')
                ->where(['od.barId' => $barId, 'od.status' => 1])
                ->where('d.endTime', '>=', $currentTime)
                //->where('d.startTime', '<=', $currentTime)
                ->orderBy('d.startTime')
                ->first();
            if ($discount) {
                $data['discountData'] = $discount;
            } else {
                $data['discountData'] = (object) array();
            }*/

            // Pass offer, if exist for today
            if(!empty($happyHourData)) {
                $offData = array(
                    'id' => $happyHourData->id,
                    'startTime' => $happyHourData->startTime,
                    'endTime' => $happyHourData->endTime,
                    'title' => $happyHourData->title,
                    'description' => $happyHourData->description,
                );
                $data['happyHourTime'][] = $offData;
            }
            else {
                $data['happyHourTime'] = [];
            }
            
            // Pass offer, if exist for today
            if(!empty($discountData)) {
                $offData = array(
                    'id' => $discountData->id,
                    'startTime' => $discountData->startTime,
                    'endTime' => $discountData->endTime,
                    'title' => $discountData->title,
                );
                $data['discountData'] = $offData;
            }
            else {
                $data['discountData'] = (object) array();
            }
            
            //Converting NULL to "" String
            if (!empty($data)) {
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $mediaPath = CommonFunction::getMediaPath();

            //calculate price and qty 
            $cartAmount = CommonFunction::calculateCartAmount();
            $data['cartData'] = null;
            if (!empty($cartAmount)) {
                $data['cartData'] = array('cartItems' => $cartAmount['item'], 'barId' => $cartAmount['cartBarId'], 'itemsInCart' => $cartAmount['itemsInCart'], 'orderType' => $cartAmount['orderType'], 'price' => round($cartAmount['price'], 2), 'cgstAmount' => $cartAmount['taxes']['cgstAmount'], 'sgstAmount' => $cartAmount['taxes']['sgstAmount'], 'serviceChargeAmount' => $cartAmount['taxes']['serviceChargeAmount'], 'serviceChargeTaxAmount' => $cartAmount['taxes']['serviceChargeTaxAmount'], 'grandTotal' => round($cartAmount['taxes']['grandTotal'], 2));
            }
            $result = array('status' => 1, 'data' => $data, 'mediaPath' => $mediaPath);

        } else {
            $result = array('status' => 0, 'message' => "Bar not found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get special events of bar
    public function getBarSpeicalEvents(Request $request)
    {
        extract($_POST);
        $data = [];

        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        // $limit = 10;
        // $startFrom = 0;
        // $pageNo = 0;
        // if (isset($page)) {
        //     $startFrom = $page * $limit;
        //     $pageNo = $page;
        // }

        //get all special events available for the bar and that are active also([entryType:-1:free,2:paid],[passApplicable:-1:yes,0:no])
        $data['specialEvents'] = SpecialEvent::select('id', 'title', 'eventImage', 'startDate', 'endDate', 'entryType', 'entryfees', 'bookingStartDate', 'bookingendDate', 'passApplicable', 'totalTickets', 'totalStandingTickets', 'venue')
            ->offset($startFrom)->limit($limit)->where(['barId' => $barId, 'status' => 1])->get()->toArray();

        //Converting NULL to "" String
        if (!empty($data)) {
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }

        if (count($data['specialEvents']) > 0) {
            $result = array('status' => 1, 'message' => "Special Events has been received successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get all offers and discounts of bar
    public function getBarOffers(Request $request)
    {
        extract($_POST);
        $data = [];

        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get all discounts and offers available for the bar and that are active also(appliedOn:-1:all,2:specific items)
        $data['discountOffers'] = DiscountOffer::select('id', 'title', 'discountRate', 'offerImage', 'appliedOn', 'startDate', 'endDate')
            ->offset($startFrom)->limit($limit)->where(['barId' => $barId, 'status' => 1])->where('endDate', '>=', $currentDate)->get()->toArray();

        //Converting NULL to "" String
        if (!empty($data)) {
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }

        if (count($data['discountOffers']) > 0) {
            $result = array('status' => 1, 'message' => "Offers has been received successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get users of bar
    public function getBarPatrons(Request $request)
    {
        extract($_POST);
        $data = [];

        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get list of all patorns of bar which are presently active in bar using geo tagging table
        $patrons = GeoTagging::select('bnGeoTaggings.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'p.userId', 'u.userName', 'u.email', 'u.phone')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnGeoTaggings.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnGeoTaggings.userId')->orderBy('bnGeoTaggings.id', 'desc')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnGeoTaggings.visitId')
            ->where(['bnGeoTaggings.barId' => $barId, 'u.status' => 1, 'u.userType' => 'U', 'v.status' => 2])
            ->offset($startFrom)->limit($limit)->get()->toArray();

        $activePatrons = array();
        if(!empty($patrons)) {
            foreach ($patrons as $i => $row) {
                $mode = CommonFunction::checkUserIncognitoMode($row['userId']);
                if($mode == 0)
                    $activePatrons[] = $row;
            }
        } 
        $data['patrons'] = $activePatrons;

        //Converting NULL to "" String
        if (!empty($data)) {
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }
        $mediaPath = CommonFunction::getMediaPath();

        if (count($data['patrons']) > 0) {
            $result = array('status' => 1, 'message' => "Patrons has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get images and videos of bar
    public function getBarMultimedia(Request $request)
    {
        extract($_POST);
        $data = [];

        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $currentDate = date('Y-m-d H:i:s');
        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get images and videos of bar in limit of 10(mediaType:-1:video,2:image)
        $data['imagesVideos'] = BarMediaFile::select('id', 'mediaName', 'mediaType')
            ->orderBy('id', 'desc')->where('barId', $barId)
            ->offset($startFrom)->limit($limit)->get()->toArray();

        //Converting NULL to "" String
        if (!empty($data)) {
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
        }

        $mediaPath = CommonFunction::getMediaPath();

        // //Convert NULL value with empty string
        // array_walk_recursive($data, function (&$item) {$item = strval($item)});

        if (count($data['imagesVideos']) > 0) {
            $result = array('status' => 1, 'message' => "Multimedia has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get reviews and rating of bar
    public function getBarReviews(Request $request)
    {
        extract($_POST);
        $data = [];

        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get average rating of bar given by users
        $rating = BarRating::select('id', 'rating')->where('barId', $barId)->first();

        //get all reviews of bars given by patorns with patorns name
        $review = BarReview::select('bnBarReviews.id', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnBarReviews.rating', 'bnBarReviews.review')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnBarReviews.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnBarReviews.userId')->orderBy('bnBarReviews.id', 'desc')
            ->where(['bnBarReviews.barId' => $barId, 'u.status' => 1, 'u.userType' => 'U'])
            ->offset($startFrom)->limit($limit)->get()->toArray();

        if (count($review) > 0 || $rating) {
            $data['ratings'] = $rating;
            $data['reviews'] = $review;

            //Converting NULL to "" String
            if (!empty($data)) {
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
            $mediaPath = CommonFunction::getMediaPath();

            $result = array('status' => 1, 'message' => "Reviews has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get menu of bar - old
    public function getBarMenu_old(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $data = [];
        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        } 

        //get all category of bar to get items of that category and that are active also
        $foodCat = array();
        $foodCategory = ItemCategory::select('id', 'catName', 'catImage')->where(['barId' => $barId, 'status' => 1])->where('parentId', '!=', 0)->get();

        if (count($foodCategory) > 0) {

            //Get the active discount/offers, for item pricing
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            foreach ($foodCategory as $fc) {

                $catItems = array();
                $menuItems = array();

                //get all food items of bar with discount and that are active also([itemType:-1:veg,2:non-veg])
                $menu = BarItem::select('bnBarItems.id', 'bnBarItems.itemName', 'bnBarItems.itemImage', 'bnBarItems.description', 'bnBarItems.itemType', 'bnBarItems.price', 'bnBarItems.isCustomized')
                    ->leftJoin('bnBarItemPricing as ip', 'ip.itemId', '=', 'bnBarItems.id')
                    ->where(['bnBarItems.status' => 1])
                    ->where('bnBarItems.subCatId', '=', $fc->id)
                    ->where('bnBarItems.barId', '=', $barId)
                    ->groupBy('bnBarItems.id')
                    ->orderBy('bnBarItems.id', 'desc')->get();

                if (count($menu) > 0) {
                    foreach ($menu as $items) {

                        $items['discountPrice'] = 0;
                        $items['discount'] = 0;
                        $items['happyHourPrice'] = 0;
                        $items['type'] = 0;
                        $items['finalPrice'] = $items->price;
                        $items['label'] = '';
                        $uItem['happyHourOffer'] = 0;
                        $uItem['discountType'] = 0;
                        $uItem['buy'] = 0;
                        $uItem['free'] = 0;

                        //If discount is available for the day
                        if ($discountData) {
                            $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $items->price);
                            if (!empty($disData)) {
                                if($disData['discountPrice'] != 0) {
                                    $items['discount'] = $disData['discount'];
                                    $items['discountPrice'] = round($disData['discountPrice'], 2);
                                    $items['finalPrice'] = $items['discountPrice'];
                                    $items['label'] = $disData['discount']. ' % off';
                                    $items['type'] = 1;
                                }
                            }
                        }

                        //If Happy Hour is available for the day
                        if ($happyHourData) {
                            $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $items->price);

                            if (!empty($hhData)) {
                                if (isset($hhData['happyHourPrice'])) {
                                    $items['happyHourOffer'] = 'discount';
                                    $items['happyHourPrice'] = round($hhData['happyHourPrice'], 2);
                                    $items['finalPrice'] = $items['happyHourPrice'];
                                    $items['label'] = $happyHourData->discountRate. ' % off';
                                    $items['type'] = 2;
                                } else {
                                    $items['happyHourOffer'] = 'offer';
                                    $items['buy'] = $hhData['buy'];
                                    $items['free'] = $hhData['free'];
                                    $items['finalPrice'] = $items->price;
                                    $items['label'] = "buy ".$hhData['buy']." get ". $hhData['free']. " free";
                                    $items['type'] = 3;
                                }
                            }

                        }

                        $items['unitPricing'] = array();
                    
                    }
                }
                $fc['catItems'] = $menu;
                if (count($menu) > 0) {
                    $foodCat[] = $fc;
                }

            }
            $data['menuItems'] = $foodCat;

            //calculate price and qty of user's cart
            $cartAmount = CommonFunction::calculateCartAmount();
            if (!empty($cartAmount)) {
                $data['cartData'] = array('status' => 1, 'itemsInCart' => $cartAmount['itemsInCart'], 'price' => round($cartAmount['price'], 2), 'cartItems' => $cartAmount['item']);
            } else {
                $data['cartData'] = array('status' => 0);
            }

            // print_r($itemPricing);exit;
            //Converting NULL to "" String
            if (!empty($data)) {
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $mediaPath = CommonFunction::getMediaPath();

            if (count($data) > 0) {
                $result = array('status' => 1, 'message' => "Menu has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get menu of bar
    public function getBarMenu(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $data = [];
        $currentDate = date('Y-m-d H:i:s');

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        } 

        // get all category of bar to get items of that category and that are active also
        $foodCat = array();
        $foodCategory = ItemCategory::select('id', 'catName', 'catImage')->where(['barId' => 0, 'status' => 1, 'parentId' => 0])->get();
        
        if (count($foodCategory) > 0) {
            //Get the active discount/offers, for item pricing
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            foreach ($foodCategory as $fc) {
                $catItems = array();
                $menuItems = array();
                //get all food items of bar with discount and that are active also([itemType:-1:veg,2:non-veg])
                $menu = BarItem::select('bnBarItems.id', 'bnBarItems.itemName', 'bnBarItems.itemImage', 'bnBarItems.description', 'bnBarItems.itemType', 'bnBarItems.price', 'bnBarItems.isCustomized','subCat.catName as subCatName')
                    ->leftJoin('bnBarItemPricing as ip', 'ip.itemId', '=', 'bnBarItems.id')
                    ->leftJoin('bnItemCategories as subCat', 'subCat.id', '=', 'bnBarItems.subCatId')
                    ->where(['bnBarItems.status' => 1])
                    ->where('bnBarItems.catId', '=', $fc->id)
                    ->where('bnBarItems.barId', '=', $barId)
                    ->groupBy('bnBarItems.id')
                    ->orderBy('bnBarItems.id', 'desc')->get();

                if (count($menu) > 0) {
                    foreach ($menu as $items) {

                        $items['discountPrice'] = 0;
                        $items['discount'] = 0;
                        $items['happyHourPrice'] = 0;
                        $items['type'] = 0;
                        $items['finalPrice'] = $items->price;
                        $items['label'] = '';
                        $uItem['happyHourOffer'] = 0;
                        $uItem['discountType'] = 0;
                        $uItem['buy'] = 0;
                        $uItem['free'] = 0;

                        //If discount is available for the day
                        if ($discountData) {
                            $disData = CommonFunction::getDiscountedPrice($discountData, $items->id, $items->price);
                            if (!empty($disData)) {
                                if($disData['discountPrice'] != 0) {
                                    $items['discount'] = $disData['discount'];
                                    $items['discountPrice'] = round($disData['discountPrice'], 2);
                                    $items['finalPrice'] = $items['discountPrice'];
                                    $items['label'] = $disData['discount']. ' % off';
                                    $items['type'] = 1;
                                }
                            }
                        }

                        //If Happy Hour is available for the day
                        if ($happyHourData) {
                            $hhData = CommonFunction::getHappyHourPrice($happyHourData, $items->id, $items->price);

                            if (!empty($hhData)) {
                                if (isset($hhData['happyHourPrice'])) {
                                    $items['happyHourOffer'] = 'discount';
                                    $items['happyHourPrice'] = round($hhData['happyHourPrice'], 2);
                                    $items['finalPrice'] = $items['happyHourPrice'];
                                    $items['label'] = $happyHourData->discountRate. ' % off';
                                    $items['type'] = 2;
                                } else {
                                    $items['happyHourOffer'] = 'offer';
                                    $items['buy'] = $hhData['buy'];
                                    $items['free'] = $hhData['free'];
                                    $items['finalPrice'] = $items->price;
                                    $items['label'] = "buy ".$hhData['buy']." get ". $hhData['free']. " free";
                                    $items['type'] = 3;
                                }
                            }

                        }

                        $items['unitPricing'] = array();
                    
                    }
                }
                $fc['catItems'] = $menu;
                if (count($menu) > 0) {
                    $foodCat[] = $fc;
                }

            }
            // $data['menuItems'] = $foodCat;

            /* Code to group items with sub-category start here */
            $menuItems = array();
            foreach($foodCat as $mainCat) {
                $mainCategory = array(
                    'catId' => $mainCat->id,
                    'catName' => $mainCat->catName,
                    'catImage' => $mainCat->catImage,
                );
                $check_subCategory = array();

                if(!empty($mainCat['catItems']) && count($mainCat['catItems'])){
                    foreach($mainCat['catItems'] as $subCat){
                        if(!in_array($subCat->subCatName, $check_subCategory)){
                            $subCategory = array(
                                //'subCatId' => $subCat->id,
                                'subCatName' => $subCat->subCatName,
                                //'subCatImage' => $subCat->subCatImage,
                            );
                            foreach($mainCat['catItems'] as $item){
                                if($subCat->subCatName == $item->subCatName){
                                    $subCategory['catItems'][] = $item;
                                }
                            }
                            $mainCategory['subCat'][] = $subCategory;
                            array_push($check_subCategory, $subCat->subCatName);
                        }
                    }
                }else{
                    $mainCategory['subCat'] = array();
                }
                $menuItems[] = $mainCategory;
            }
            $data['menuItems'] =  $menuItems; 
            /* Code to group items with sub-category end here */

            //calculate price and qty of user's cart
            $cartAmount = CommonFunction::calculateCartAmount();
            if (!empty($cartAmount)) {
                $data['cartData'] = array('status' => 1, 'itemsInCart' => $cartAmount['itemsInCart'], 'price' => round($cartAmount['price'], 2), 'cartItems' => $cartAmount['item']);
            } else {
                $data['cartData'] = array('status' => 0);
            }

            // print_r($itemPricing);exit;
            //Converting NULL to "" String
            if (!empty($data)) {
                array_walk_recursive($data, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $mediaPath = CommonFunction::getMediaPath();

            if (count($data) > 0) {
                $result = array('status' => 1, 'message' => "Menu has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }

    //get all categories of bar
    public function getBarCategories(Request $request)
    {
        extract($_POST);
        $data = [];
        $validator = Validator::make($request->all(), [
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //get user basic profile data of bar with status 1 and user type b
        $data['restData'] = Bar::select('bnBars.id', 'bnBars.type', 'bnBars.barName', 'bnBars.about', 'u.userName')->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')->where(['bnBars.id' => $barId, 'status' => 1, 'userType' => 'B'])->first();
        if ($data['restData']) {
            //get all category of bar and that are active also
            $data['foodCategory'] = ItemCategory::select('id', 'catName', 'catImage')->where(['barId' => $barId, 'status' => 1])->get();
            $result = array('status' => 1, 'message' => "Categories has been received successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

        return response()->json($result);
    }
}