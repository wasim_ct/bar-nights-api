<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\Wallet;
use App\WalletTransaction;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;

class PaymentController extends Controller
{
    public $successStatus = 200;

    /**
     * bar profile api
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}