<?php
namespace App\Http\Controllers\API\Mobile;

use App\BarNotification;
use App\BookTable;
use App\Http\Controllers\Controller;
use App\Patron;
use App\BarTiming;
use App\Wallet;
use App\Transaction;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;
use DB;

class BookTableController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    // Book Visit function to create a booking in the bar
    public function bookTable(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'bookingDate' => 'required',
            'bookingTime' => 'required',
            'totalPerson' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        if ($totalPerson > 30) {
            $result = array('status' => 0, 'message' => "Total person can not exceed 30");
        } 
        else {
            $canBook = true;
            /* === Bar time validation with booking time === */
            $result = CommonFunction::getBarTimingSatus($barId, $bookingTime, $bookingDate, 'table');
            if(!empty($result) && $result['status'] == 0) {
                $canBook = false;
                $msg = $result['message'];
            }

            /* === 1 hr future validation === */
            if($bookingDate == date("Y-m-d")) {
                $bTime = strtotime($bookingTime);
                $currentTime = strtotime(date('H:i:s'));
                $minutes = round(($bTime - $currentTime) / 60);
                if($minutes < 60) {
                    $canBook = false;
                    $msg = "Booking time must be at least 1 hour greater than current time";
                }
            }
 
            /*$day = strtolower(date('l', strtotime($bookingDate)));
            $openingHrs = BarTiming::select('bnBarTimings.id', 'bnBarTimings.weekDay', 'bnBarTimings.openingTime', 'bnBarTimings.closingTime')
                ->leftJoin('bnBars as b', 'b.id', '=', 'bnBarTimings.barId')
                ->where(['barId' => $barId, 'weekDay' => $day])->first();

            if ($openingHrs) {
                // take time 60 mint less then bar closing time
                $selectedTime = $openingHrs->closingTime;
                $endTime = strtotime("-60 minutes", strtotime($selectedTime));
                $closingTime = date('H:i:s', $endTime);

                // checking visit time of user with the bar opening and closing time
                if ($bookingTime >= $openingHrs->openingTime && $bookingTime < $closingTime) {
                    $canBook = true;
                }
                else {
                    $canBook = false;
                    $msg = "Bar does not take bookings on the requested time";
                }
            } 
            else {
                $canBook = false;
                $msg = "Bar is closed on selected date";
            }*/

            if($canBook) {
                $tableExist = BookTable::select('id')
                    ->where(['userId' => $userId, 'barId' => $barId, 'bookingDate' => $bookingDate])
                    ->where('status', '!=', 2)->where('status', '!=', 3)->first();
                if ($tableExist) {
                    $result = array('status' => 0, 'message' => "You can't submit a new request for the same day");
                } 
                else 
                {
                    $comments = null;
                    if (isset($comment) && $comment != '') {
                        $comments = $comment;
                    }

                    $code = CommonFunction::generateBookingCode();
                    $tokenMoney = CommonFunction::getAppSettings();

                    $insert = array('userId' => $userId, 'barId' => $barId, 'code' => $code, 'message' => $comments, 'bookingDate' => $bookingDate, 'bookingTime' => $bookingTime, 'totalPerson' => $totalPerson, 'tokenMoney' => $tokenMoney['bookingTokenMoney'], 'status' => 0, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);

                    $bookTable = BookTable::create($insert);
                    if ($bookTable['id']) 
                    {
                        //Create an entry in notification table and send PUSH Notification
                        $userId = $userId;
                        $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();
                        if (!empty($user)) {
                            
                            $message = 'New table booking request from ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));
                            
                            $record = array('moduleType' => 'bookTable', 'moduleId' => $bookTable['id'], 'description' => $message, 'barId' => $barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                            $not = BarNotification::create($record);

                            if ($not['id']) {
                                $bar = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $barId);
                                $deviceToken = CommonFunction::getUserDeviceData($bar); //Get Device Tokens
                                if ($deviceToken) {
                                    //Firebase Notification
                                    $payLoadData = array('moduleType' => 'bookTable', 'moduleId' => $bookTable['id'], 'barId' => $barId,'title' => 'New table booking', 'msg' => $message);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'B');
                                }
                            }
                        }
                        $result = array('status' => 1, 'message' => "The Request has been submitted successfully");
                    } else {
                        $result = array('status' => 0, 'message' => "Something went wrong");
                    }
                }
            }
            else {
                $result = array('status' => 0, 'message' => $msg);
            }
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to get user Booking table requests
    public function getTableBooking(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);  

        $tables = BookTable::select('bnBookTable.id', 'bnBookTable.code', 'bnBookTable.tableNumber', 'bnBookTable.tokenMoney', 'bnBookTable.message', 'bnBookTable.bookingDate', 'bnBookTable.bookingTime', 'bnBookTable.totalPerson', 'bnBookTable.status', 'bnBookTable.cancelReason',  'bnBookTable.paymentStatus', 'bnBookTable.barId', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'state.state_name as stateName')
            ->join('bnBars as b', 'b.id', '=', 'bnBookTable.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnStates as state', 'state.state_id', '=', 'ad.stateId')
            ->where(['bnBookTable.userId' => $userId])
            ->orderBy('id', 'desc')->get()->toArray();
        if (count($tables) > 0) {
            //Converting NULL to "" String
            array_walk_recursive($tables, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $bookings = array();
            foreach ($tables as $row) {

                // Expire the Request if 30 mins passed from booking time
                // $time = $row['bookingDate'].' '.$row['bookingTime'];
                $update = CommonFunction::updateTableBookingExpireStatus($row['id'], $row['status'], $row['bookingDate'], $row['bookingTime'], $row['paymentStatus']);
                if($update) {
                    $row['status'] = 3;
                }

                $info = $this->getTableInfo($row);
                $row['label'] = $info['label'];
                $row['paymentData'] = $info['paymentData'];
                $bookings[] = $row;
            }

            // Get the Wallet Balance of ther user
            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
            $walletAmt = ($walletAmt) ? $walletAmt : 0;

            $result = array('status' => 1, 'message' => "Bookings has been received successfully", 'data' => $bookings, 'walletAmt' => $walletAmt);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    // Function to get Booking details
    public function getBookingDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bookingId' => 'required',
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);  

        $tableRow = BookTable::select('bnBookTable.id', 'bnBookTable.code', 'bnBookTable.tableNumber', 'bnBookTable.tokenMoney', 'bnBookTable.message', 'bnBookTable.bookingDate', 'bnBookTable.bookingTime', 'bnBookTable.totalPerson', 'bnBookTable.status', 'bnBookTable.cancelReason', 'bnBookTable.paymentStatus', 'bnBookTable.barId', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'state.state_name as stateName')
            ->join('bnBars as b', 'b.id', '=', 'bnBookTable.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnStates as state', 'state.state_id', '=', 'ad.stateId')
            ->where(['bnBookTable.id' => $bookingId])
            ->first();

        if (!empty($tableRow)) {
            //Converting NULL to "" String
            array_walk_recursive($tableRow, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $info = $this->getTableInfo($tableRow);
            $tableRow['label'] = $info['label'];
            $tableRow['paymentData'] = $info['paymentData'];

            $bookingData = $tableRow;

            // Get the Wallet Balance of ther user
            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
            $walletAmt = ($walletAmt) ? $walletAmt : 0;

            $result = array('status' => 1, 'message' => "Booking details has been received successfully", 'data' => $bookingData, 'walletAmt' => $walletAmt);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    // Function Update Table Booking Payment and Transaction
    public function updateTablePayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'tableBookId' => 'required',
            //'transactionId' => 'required',
            'paymentMode' => 'required', //|in:cash,online,card,wallet
            'amount' => 'required', //TokenMoney
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
        $response = (object) array();
        $status = false;

        /* === If all payment paid through BN Wallet === */
        if(isset($walletMoney) && $walletMoney > 0 && $walletMoney >= $amount) {
            $param = array(
                'barId' => $barId,
                'userId' => $userId,
                'moduleId' => $tableBookId,
                'moduleType' => 'table',
                'paymentMode' => 'bnwallet',
                'description' => 'Paid for table booking',
                'amount'    => $amount,
                'totalPrice' => $amount,
                'paymentDate' => date('Y-m-d'),
                'status' => 1,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate
            );

            $transaction = Transaction::create($param);
            if($transaction){
                // Update the table booking payment status
                $param = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
                BookTable::where('id', $tableBookId)->update($param);
                
                // Decrease the wallet balance
                Wallet::where('userId',$userId)->decrement('amount', $amount);

                /* === Transaction in wallet table === */
                $param = array('userId' => $userId, 'description' => "Paid for table booking", 'amount' => $amount, 'moduleId' => $tableBookId, 'moduleType' => 'table', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                DB::table('bnWalletTransactions')->insertGetId($param); 

                $status = true;
            }
        }
        /* === If wallet money is less than to final amount === */
        else 
        {
            /* === If wallet money is used, then make a transaction in wallet table === */
            if(isset($walletMoney) && $walletMoney > 0) {
                // Decrease the wallet balance
                Wallet::where('userId',$userId)->decrement('amount', $walletMoney);

                $param = array('userId' => $userId, 'description' => "Paid for table booking", 'amount' => $walletMoney, 'moduleId' => $tableBookId, 'moduleType' => 'table', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                DB::table('bnWalletTransactions')->insertGetId($param);
                
                // Wallet total price calculation;
                /*$ratio = ($walletMoney/$amount)*100;
                $ratioAmt = ($walletMoney/100)*$ratio;
                $walletTotalAmount = $walletMoney-$ratioAmt;*/

                $param = array(
                    'barId' => $barId,
                    'userId' => $userId,
                    'moduleId' => $tableBookId,
                    'moduleType' => 'table',
                    'paymentMode' => 'bnwallet',
                    'amount'    => $walletMoney,
                    'description' => 'Paid for table booking',
                    'totalPrice' => $walletMoney,
                    'paymentDate' => date('Y-m-d'),
                    'status' => 1,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate
                );
                $transaction = Transaction::create($param);

                // Reset the Amount, to continue processing
                $amount = $amount - $walletMoney;
            }

            /* === Now transaction entry based on other payment modes === */
            $param = array(
                'barId' => $barId,
                'userId' => $userId,
                'moduleId' => $tableBookId,
                'moduleType' => 'table',
                'transactionId' => $transactionId,
                'paymentMode' => $paymentMode,
                'amount'    => $amount,
                'description' => 'Paid for table booking',
                'totalPrice' => $amount,
                'paymentDate' => date('Y-m-d'),
                'status' => 1,
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate
            );
            
            $transaction = Transaction::create($param);
            if($transaction){
                // Update the table booking payment status
                $param = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
                BookTable::where('id', $tableBookId)->update($param);
                
                /* === Add the amount in user wallet if available === */
                $appSettings = CommonFunction::getAppSettings();
                $cashBackOn = $appSettings['cashBackOn'];
                if($cashBackOn > 0 && $amount >= $cashBackOn) {

                    $thousandCount = (int)($amount/$cashBackOn);
                    $cashBackAmount = $appSettings['cashBackAmount'] * $thousandCount;

                    //Update the wallet amount or Insert
                    $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
                    if(!empty($walletAmt) || $walletAmt === 0.00) { //bcz 0 can be an amount
                        // Update the amount
                        $param = array('amount' => $walletAmt + $cashBackAmount,'updatedAt' => $this->entryDate);
                        $wStatus = DB::table('bnWallets')
                            ->where(['userId' => $userId])
                            ->update($param);
                    }
                    else {
                        $param = array('userId' => $userId, 'amount' => $cashBackAmount, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $wStatus = DB::table('bnWallets')->insertGetId($param);
                    }

                    // Create entry in wallet transaction
                    if(isset($wStatus)) {
                        $param = array('userId' => $userId, 'description' => "Cashback Received", 'amount' => $cashBackAmount, 'moduleId' => $tableBookId, 'moduleType' => 'table', 'type' => 'received', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        DB::table('bnWalletTransactions')->insertGetId($param);

                        $response = array(
                            'cashBackReceived' => 1,
                            'cashBackAmount' => $cashBackAmount,
                        );
                    }
                } 

                $status = true;
            }
        }

        if($status){
            $result = array('status' => 1, 'message' => 'Payment Updated successfully', 'data' => $response);
        }
        else{
            $result = array('status' => 0, 'message' => 'Somethig went wrong, Please try again.');
        }
        return response()->json($result);
    }

    // Book table payment function
    public function updateTablePayment_Old(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'tableBookId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $bookTableData = BookTable::select('*')->where(['id' => $tableBookId])->first();
        if (!empty($bookTableData)) {

            $param = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
            $update = BookTable::where('id', $tableBookId)->update($param);
            if ($update) {
                $result = array('status' => 0, 'message' => "Payment has been updated successfully");
            } else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }

            /*$currentTime = date('H:i:s');
            $time = date('H:i:s', strtotime(date('H:i:s', strtotime($bookTableData->updatedAt))) + 10800);

            if ($currentTime < $time && $currentTime < $bookTableData->bookingTime) {
                $result = array('status' => 1, 'message' => "Payment has been successfully completed");
            } 
            else {
                $update = array('status' => 2, 'updatedAt' => $this->entryDate);
                $bookTableUpdate = BookTable::where('id', $tableBookId)->update($update);
                if ($bookTableUpdate) {
                    $result = array('status' => 0, 'message' => "Your table booking is cancelled");
                } else {
                    $result = array('status' => 0, 'message' => "Something went wrong");
                }
            }*/
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to retun Table status and payment data
    public function getTableInfo($row) {
        $status = $row['status'];
        $paymentStatus = $row['paymentStatus'];
        $bookingId = $row['id'];
        $label = "";

        if($status == 0)
            $label = "Pending";
        else if($status == 1 && $paymentStatus == 0)
            $label = "Accepted";
        else if($status == 1 && $paymentStatus == 1)
            $label = "Confirmed";
        else if($status == 2)
            $label = "Rejected";
        else if($status == 3)
            $label = "Expired";

        // Get the order-visit status of the table booking
        $orderData = DB::table('bnVisits as v')
            ->select('v.id as visitId', 'v.status as visitStatus', 'o.status as orderStatus')
            ->leftJoin('bnOrders as o', 'o.visitId', '=', 'v.id')
            ->where(['v.requestType' => 'table', 'v.requestId' => $bookingId])
            ->first();
        if(!empty($orderData)) {
            $vStatus = $orderData->visitStatus;

            if($vStatus == 1)
                $label = "Visit Pending";
            else if($vStatus == 2)
                $label = "Code Availed";
            else if($vStatus == 3)
                $label = "Visit Cancelled";
            else if($vStatus == 4)
                $label = "Completed";
        }
        else {
            $today = date("Y-m-d");
            if($today > $row['bookingDate'])
                $label = "Expired";
        }
        $result['label'] = $label;

        // Get the advance payment info
        $trData = DB::table('bnTransactions as t')
            ->select('t.amount', 't.paymentMode', 't.createdAt')
            ->where(['t.moduleType' => 'table', 't.moduleId' => $bookingId])
            ->get();

        $payData = array();
        if(!empty($trData)) {
            foreach ($trData as $trow) {
                if($trow->paymentMode == 'bnwallet')
                    $trow->paymentMode = "Bar Nights Wallet";

                $payData[] = $trow;
            }
        }
        $result['paymentData'] = $payData;

        return $result;
    }
}