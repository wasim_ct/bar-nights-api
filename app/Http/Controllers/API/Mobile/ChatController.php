<?php
namespace App\Http\Controllers\API\Mobile;

use App\Friend;
use App\Http\Controllers\Controller;
use App\UserNotification;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class ChatController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Function to upload chat media, return Chat Media and Path
    public function uploadChatMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mediaType' => 'required',
            //'mediaName' => 'mimes:jpg,JPG,JPEG,png,svg,jpeg,gif|max:500000',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //File check validation
        if ($request->hasFile("mediaName")) {
            $file = $request->file("mediaName");

            //get extention of file
            $ext = $file->getClientOriginalExtension();
            //encrypt the name of file
            $img = time() * rand() . "." . $ext;

            //upload media to folder
            $upload = $file->move("uploads/chat/", $img); //Path & File name need to pass
            if (!empty($upload)) {
                //Create entry in Table
                $data = array(
                    'mediaName' => $img,
                    'mediaType' => $mediaType,
                    'createdAt' => $this->entryDate,
                    'updatedAt' => $this->entryDate,
                );
                $res = DB::table('bnChatMedias')->insert($data);
                if ($res) {
                    //Return media data with path
                    $mediaPath = asset('uploads/chat/' . $img);
                    $data = array('mediaName' => $img, 'mediaType' => $mediaType, 'mediaPath' => $mediaPath);
                    $result = array('status' => 1, 'message' => 'Media has been uploaded successfully', 'data' => $data);
                } else {
                    $result = array('status' => 0, 'message' => 'Something went wrong');
                }

            } else {
                $result = array('status' => 0, 'message' => 'Media has not been uploaded');
            }

        } else {
            $result = array('status' => 0, 'message' => 'Please select a media file to upload');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to upload chat media, return Chat Media and Path
    public function chatNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'senderId' => 'required',
            'receiverId' => 'required',
            'message' => 'required',
            'senderName' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $mt = explode(' ', microtime());
        $time = ((int) $mt[1]) * 1000 + ((int) round($mt[0] * 1000));
        if (isset($messageTimeinMillisec) && $messageTimeinMillisec != '') {
            $time = $messageTimeinMillisec;
        }

        $userId = $senderId;
        $message = $message;

        $record = array('moduleType' => 'chat', 'moduleId' => 0, 'description' => $message, 'userId' => $receiverId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
        $not = UserNotification::create($record);

        $notData = array('messageTimeinMillisec' => $time,
            'senderId' => $senderId,
            'receiverId' => $receiverId,
            'messageId' => $messageId,
            'userProfile' => $userProfile,
            'senderName' => $senderName,
            'message' => $message);

        if ($not['id']) {
            $deviceToken = CommonFunction::getUserDeviceData($receiverId); //Get Device Tokens

            if ($deviceToken) {
                //Firebase Notification
                $payLoadData = array('moduleType' => 'chat', 'moduleId' => 0, 'userId' => $receiverId, 'data' => $notData);
                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
            }

            $result = array('status' => 1, 'message' => 'Notification has been sent sucessfully', 'data' => $notData);
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to upload chat media, return Chat Media and Path
    public function getFriendStatus(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'senderId' => 'required',
            'receiverId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $friendRequest = Friend::select('status')->where(['userId' => $senderId, 'friendId' => $receiverId])->first();
        if (!empty($friendRequest)) {
            $result = array('status' => 1, 'message' => 'Status has been received successfully', 'data' => $friendRequest);
        } else {
            $result = array('status' => 0, 'message' => 'No request exist');
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to upload chat media, return Chat Media and Path
    public function getNotifications(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        //Pagination Condition of startfrom and no of records
        if (!isset($limit)) {
            $limit = 10;
        }
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        $notifications = UserNotification::select('id', 'moduleType', 'description', 'createdAt')
            ->where(['userId' => $userId])
            ->where('moduleType', '!=', 'chat')
            //->offset($startFrom)
            //->limit($limit)
            ->orderBy('id', 'desc')
            ->get();
        if (count($notifications) > 0) {
            $result = array('status' => 1, 'message' => 'Notification has been received successfully', 'data' => $notifications);
        } else {
            $result = array('status' => 0, 'message' => 'No request exist');
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to upload chat media, return Chat Media and Path
    public function changeNotificationStatus(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        if ($status == 0) {
            $notificationDelete = UserNotification::where(['id' => $id])->delete();
            if (!empty($notificationDelete)) {
                $result = array('status' => 1, 'message' => 'Notification has been deleted successfully');
            } else {
                $result = array('status' => 0, 'message' => 'Something went wrong');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Something went wrong');
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

}