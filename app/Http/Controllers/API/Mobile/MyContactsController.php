<?php
namespace App\Http\Controllers\API\Mobile;

use App\BlockUser;
use App\Friend;
use App\Http\Controllers\Controller;
use App\Patron;
use App\User;
use App\UserNotification;
use App\Visit;
use App\VisitInvitation;
use CommonFunction;
use Illuminate\Http\Request;
use Validator;

class MyContactsController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //Funtion to get all patrons
    public function getAllPatrons(Request $request)
    {
        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        $users = User::select('bnUsers.id', 'email', 'phone', 'userName', 'p.firstName', 'p.lastName', 'p.profilePic')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnUsers.id')
            ->where(['bnUsers.userType' => 'U', 'bnUsers.status' => 1])
            ->offset($startFrom)->limit($limit)
            ->get()->toArray();

        if (!empty($users)) {

            //Converting NULL to "" String
            array_walk_recursive($users, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Users has been received successfully", 'data' => $users, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No user found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }

    //Funtion to check contacts in the DB
    public function getMyContacts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'numbers' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //First check the user is exist or not
        $userExist = CommonFunction::GetSingleField('bnUsers', 'id', 'id', $userId);
        if ($userExist) {
            $data = array();

            //get current visit details of user(status:-1:pending, 2:confirmed)
            $visits = Visit::select('id')->where(['userId' => $userId])->where('status', '!=', 4)->where('status', '!=', 3)->orderBy('id', 'desc')->first();

            //Numbers JSON
            //$numbers = '[{"name":"abc","phone":"946274002"},{"name":"xyz","phone":"8005523673"}]';

            $numArray = json_decode($numbers, true);
            foreach ($numArray as $key => $val) {
                extract($val);
                $contact = User::select('bnUsers.id', 'bnUsers.phone', 'p.firstName', 'p.lastName', 'p.profilePic', 'bnUsers.userName')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnUsers.id')
                    ->where(['bnUsers.userType' => 'U', 'bnUsers.status' => 1, 'bnUsers.phone' => trim($phone)])
                    ->where('bnUsers.id', '!=', $userId)
                    ->first();

                if (!empty($contact)) {
                    // Get invitation status
                    $inviteStatus = 0;
                    if (!empty($visits) && $visits != null) {
                        // get visit invitation status for this user
                        $visitInvite = VisitInvitation::select('status as invitationStatus')
                            ->where(['visitId' => $visits->id, 'toUserId' => $contact->id])->first();
                        if ($visitInvite) {
                            $inviteStatus = $visitInvite->invitationStatus;
                        }
                    }

                    //Converting NULL to "" String 
                    array_walk_recursive($contact, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                    $contact['name'] = $contact->firstName . ' ' . $contact->lastName;
                    $contact['status'] = 1;
                    $contact['inviteStatus'] = $inviteStatus;

                    // Get barId data, if user is active in a bar
                    $mode = CommonFunction::checkUserIncognitoMode($contact->id);
                    if($mode == 0) { 
                        $active = CommonFunction::isUserGeoTagged($contact->id);
                        if(!empty($active)) {
                            $contact['barId'] = $active->barId;
                            $contact['barName'] = $active->barName;
                        }
                        else {
                            $contact['barId'] = 0;
                            $contact['barName'] = "";
                        }
                    }
                    else {
                        $contact['barId'] = 0;
                        $contact['barName'] = "";
                    }
                    //$contact['barId'] = $barId ? $barId : 0;

                    $data[] = $contact;
                } else {
                    $record = array('id' => '', 'name' => $name, 'phone' => $phone, 'status' => 0, 'inviteStatus' => 0, 'barId' => 0, 'firstName' => '', 'lastName' => '', 'profilePic' => '', 'userName' => '');
                    $data[] = $record;
                }
            }

            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Contacts has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "Invalid User");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }

    //Funtion to insert friends in the DB
    public function addFriend(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'friendId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $userFriendBlock = BlockUser::select('id')->where(['userId' => $userId, 'friendId' => $friendId])->first();
        if ($userFriendBlock) {
            $result = array('status' => 0, 'message' => "This user is blocked by you. Please unblock to send request");
        } else {
            $friendUserBlock = BlockUser::select('id')->where(['userId' => $friendId, 'friendId' => $userId])->first();
            if ($friendUserBlock) {
                $result = array('status' => 0, 'message' => "You can not send the request");
            } else {
                $exist = Friend::select('id')->where(['userId' => $userId, 'friendId' => $friendId])->first();
                if (!empty($exist)) {
                    $result = array('status' => 0, 'message' => "This user is alerady exist in your friend list");
                } else {
                    $data = array('userId' => $userId, 'friendId' => $friendId, 'status' => 0, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                    $fr = Friend::create($data);
                    // $fr = Friend::create(['userId' => $friendId, 'friendId' => $userId, $data]);

                    if ($fr['id']) {
                        //Create an entry in notification table and send PUSH Notification
                        $user = Patron::select('firstName', 'lastName')
                            ->where(['userId' => $userId])->first();
                        $name = '';
                        if (!empty($user)) {
                            $name = ucwords(strtolower($user->firstName . ' ' . $user->lastName));
                        }
                        $message = 'You have recieved a friend request from ' . $name;

                        $record = array('moduleType' => 'friendRequest', 'moduleId' => $fr['id'], 'description' => $message, 'userId' => $friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = UserNotification::create($record);

                        if ($not['id']) {
                            $deviceToken = CommonFunction::getUserDeviceData($friendId); //Get Device Tokens
                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'friendRequest', 'moduleId' => $fr['id'], 'userId' => $userId, 'title' => 'New friend request', 'msg' => $message);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                            }
                        }

                        $result = array('status' => 1, 'message' => "Friend request has been sent successfully");
                    } else {
                        $result = array('status' => 0, 'message' => "Something went wrong");
                    }
                }
            }
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }

    // Funtion to get friend requests (Received/Send)
    public function getRequestList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        //get friends list who send request to me
        $data['recieveRequest'] = Friend::select('bnFriendLists.UserId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnFriendLists.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnFriendLists.userId')
            ->where(['bnFriendLists.friendId' => $userId, 'bnFriendLists.status' => 0])->get();

        //get friends list to whome i send request
        $data['sendRequest'] = Friend::select('bnFriendLists.friendId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnFriendLists.friendId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnFriendLists.friendId')
            ->where(['bnFriendLists.userId' => $userId, 'bnFriendLists.status' => 0])->get();

        if (count($data) > 0) {
            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Friends has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Funtion to get list of friends who are alerady my friends
    public function getMyFriends(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //get current visit details of user(status:-1:pending, 2:confirmed)
        $visits = Visit::select('id')->where(['userId' => $userId])->where('status', '!=', 4)->where('status', '!=', 3)->orderBy('id', 'desc')->first();

        $friends = Friend::select('bnFriendLists.friendId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnFriendLists.friendId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnFriendLists.friendId')
            ->where(['bnFriendLists.userId' => $userId, 'bnFriendLists.status' => 1])->get();
        if (count($friends) > 0) {
            foreach ($friends as $f) 
            {
                // Get barId data, if user is active in a bar
                $mode = CommonFunction::checkUserIncognitoMode($f->friendId);
                if($mode == 0) {
                    $active = CommonFunction::isUserGeoTagged($f->friendId);
                    if(!empty($active)) {
                        $f['barId'] = $active->barId;
                        $f['barName'] = $active->barName;
                    }
                    else {
                        $f['barId'] = 0;
                        $f['barName'] = "";
                    }
                }
                else {
                    $f['barId'] = 0;
                    $f['barName'] = "";
                }

                // Get invitation status
                $f['inviteStatus'] = 0;
                if ($visits) {
                    //get all visit invitation request done on this invitation(status:-1:pending, 2:accept,3:reject)
                    $visitInvite = VisitInvitation::select('status as invitationStatus')
                        ->where(['visitId' => $visits->id, 'toUserId' => $f->friendId])->first();
                    if ($visitInvite) {
                        $f['inviteStatus'] = $visitInvite->invitationStatus;
                    }
                }
            }
            $data = $friends;
            $mediaPath = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => "Friends has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);

        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Funtion to delete friends in the DB
    public function deleteFriend(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'friendId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $deleteFriend = Friend::where(['userId' => $userId, 'friendId' => $friendId])->delete();
        $deleteFriend = Friend::where(['userId' => $friendId, 'friendId' => $userId])->delete();

        $result = array('status' => 1, 'message' => "Friend has been removed successfully");
        
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }

    //Funtion to change status in the DB
    public function updateFriendRequestStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'friendId' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        if ($status == 1) {
            $UpdateStatus = Friend::where(['userId' => $friendId, 'friendId' => $userId])->update(['status' => 1]);
            if (!empty($UpdateStatus)) {
                Friend::create(['userId' => $userId, 'friendId' => $friendId, 'status' => 1]);
            }
            $msg = 'accepted';
            $title = 'Friend request accepted';
        } else if ($status == 2) {
            $UpdateStatus = Friend::where(['userId' => $friendId, 'friendId' => $userId])->delete();
            $msg = 'declined';
            $title = 'Friend request rejected';
        }

        if (!empty($UpdateStatus)) {
            //Create an entry in notification table and send PUSH Notification
            $user = Patron::select('firstName', 'lastName')
                ->where(['userId' => $userId])->first();
            if (!empty($user)) 
            {
                $message = ucwords(strtolower($user->firstName . ' ' . $user->lastName)) . ' has ' . $msg . ' your friend request';
                $friendNotId = Friend::select('id')->where(['userId' => $friendId, 'friendId' => $userId])->first();
                $notificationId = 0;
                if (!empty($friendNotId)) {
                    $notificationId = $friendNotId->id;
                }
                $record = array('moduleType' => 'friendRequestStatus', 'moduleId' => $notificationId, 'description' => $message, 'userId' => $friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                $not = UserNotification::create($record);

                if ($not['id']) {
                    $deviceToken = CommonFunction::getUserDeviceData($friendId); //Get Device Tokens
                    if ($deviceToken) {
                        //Firebase Notification
                        $payLoadData = array('moduleType' => 'friendRequestStatus', 'moduleId' => $notificationId, 'userId' => $userId, 'title' => $title, 'msg' => $message);
                        $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                    }
                }
            }

            $result = array('status' => 1, 'message' => "Friend request has been updated successfully");
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);

    }

    //Funtion to change status in the DB
    public function addToBlockList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'friendId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        $status = false;

        extract($_POST);
        if ($action == 'add') {
            $exist = BlockUser::select('id')->where(['userId' => $userId, 'friendId' => $friendId])->first();
            if ($exist) {
                $status = true;
                $msg = 'User alerady exist in block list';
            } else {
                $Block = BlockUser::create(['userId' => $userId, 'friendId' => $friendId]);
                if (!empty($Block)) {
                    //delete data from friends list table
                    $UpdateStatus = Friend::where(['userId' => $userId, 'friendId' => $friendId])->delete();
                    $UpdateStatus = Friend::where(['userId' => $friendId, 'friendId' => $userId])->delete();
                    $status = true;
                    $msg = 'User has been blocked successfully';
                }
            }
        }
        if ($action == 'remove') {
            $UpdateBlockUser = BlockUser::where(['userId' => $userId, 'friendId' => $friendId])->delete();
            if (!empty($UpdateBlockUser)) {
                $status = true;
                $msg = 'User has been removed from block list';
            }
        }

        if ($status == true) {
            $result = array('status' => 1, 'message' => $msg);
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }
        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

}