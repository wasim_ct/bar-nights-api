<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\NewsFeed;
use App\NewsFeedComment;
use App\NewsFeedReaction;
use App\DiscountOffer;
use App\HappyHoursTiming;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{
    //Home Screen function to load the data on map
    public function getHomeData(Request $request)
    {
        extract($_POST);
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //SQL Query to load bar near by 3 KM
        $sql = "SELECT b.id as barId, b.barName, a.address, a.latitude, a.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnBars b inner join bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1
                HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT 0,50"; //3 KM later on
        $mapData = DB::select(DB::raw($sql));

        //get data of visit
        $visit = CommonFunction::getVisitData($userId);

        $data = array('mapData' => $mapData, 'visitData' => $visit);
        if (!empty($data)) {
            $mediaPath = CommonFunction::getMediaPath();

            $result = array('status' => 1, 'message' => "Data has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    //get Bar Listing Function with different filters
    public function getBarsListing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination condition setup
        //if limit is not set then default limit is 10
        if (!isset($limit) && empty($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom) && empty($startFrom)) {
            $startFrom = 0;
        }
        $totaloffers = '';
        $currentTime = date('H:i:s');
        $today = date('Y-m-d');
        $Date = date('Y-m-d');

        //Calling the Discount/Offers with 3 KMs
        /*$sql = "SELECT d.id,d.startDate,d.endDate, d.barId, od.title, d.appliedOn, d.discountRate, b.barName, a.address, a.latitude, a.longitude,od.offerImage, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnDiscountOffers d INNER JOIN bnBars b on b.id = d.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnOfferDetails od on od.id = d.odId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1
                HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT  $startFrom,$limit";*/

        //get the Discount/Offers with Happy Hours within 3 KMs count
        $sql = "SELECT od.id, od.barId, od.title, od.offerType, od.createdAt, b.barName, a.address, a.latitude, a.longitude, od.offerImage, od.templateId, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnOfferDetails od INNER JOIN bnBars b on b.id = od.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1 AND od.status = 1
                HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT $startFrom,$limit";

        //get the Discount/Offers with Happy Hours within 3 KMs count
        $offerCountSql = "SELECT od.id, od.offerType, od.createdAt, a.latitude, a.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude)) * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude)) + SIN(RADIANS($latitude)) * SIN(RADIANS(a.latitude)))) AS distanceInKM
                 FROM bnOfferDetails od INNER JOIN bnBars b on b.id = od.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1 AND od.status = 1
                HAVING distanceInKM <= 3000";

        //Calling the Discount/Offers with 3 KMs
        /* $sql = "SELECT d.id,d.startDate,d.endDate, d.barId, od.title, d.appliedOn, d.discountRate, b.barName, a.address, a.latitude, a.longitude,od.offerImage, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnDiscountOffers d INNER JOIN bnBars b on b.id = d.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnOfferDetails od on od.id = d.odId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1
                HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT  $startFrom,$limit";
        // AND d.startDate <= '$today' AND d.endDate >= '$today'

        //get the Discount/Offers with 3 KMs count
        $offerCountSql = "SELECT d.id, a.latitude, a.longitude,111.045 * DEGREES(ACOS(COS(RADIANS($latitude)) * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude)) + SIN(RADIANS($latitude)) * SIN(RADIANS(a.latitude)))) AS distanceInKM
                 FROM bnDiscountOffers d INNER JOIN bnBars b on b.id = d.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1
                HAVING distanceInKM <= 3000"; */

        // $countOffer = DB::select(DB::raw($offerCountSql));

        if (isset($loadMore) && !empty($loadMore)) {
            if ($loadMore == 'offer') {
                //$data['offerData'] = 
                $offerData = DB::select(DB::raw($sql));
                $countOffer = DB::select(DB::raw($offerCountSql));
                if ($countOffer) {
                    $totaloffers = count($countOffer);
                }
            }
        } else {
            //echo $sql;exit;
            // $data['offerData'] = 
            $offerData = DB::select(DB::raw($sql));
            $countOffer = DB::select(DB::raw($offerCountSql));
            if ($countOffer) {
                $totaloffers = count($countOffer);
            }
        }

        // If offer found, then need to check start-end date of the offers
        $offers = array();
        if(isset($offerData) && count($offerData) > 0){
            foreach ($offerData as $off) {

                $off->templateName = CommonFunction::GetSingleField('bnTemplates', 'templateFrame', 'id', $off->templateId);
                
                // Discount only of today
                if($off->offerType == 'discount') 
                {
                    //$offerDate = date("Y-m-d", strtotime($off->createdAt));
                    $today = date("Y-m-d");
                    $disData = DiscountOffer::select('id', 'appliedOn', 'discountRate')
                        ->where(['odId' => $off->id, 'startDate' => $today, 'endDate' => $today])
                        ->first();
                    if(!empty($disData)){
                        $off->appliedOn = $disData->appliedOn;
                        $off->discountRate = $disData->discountRate;
                        $off->label = $disData->discountRate. "% Off";
                        $off->startDate = $today;
                        $off->endDate = $today;

                        $offers[] = $off;
                    }
                }
                // Happy hour which relates to current day
                else if($off->offerType == 'happyhour') {
                    $today = strtolower(date('l'));
                    
                    $hhData = HappyHoursTiming::select('id', 'appliedOn', 'discountType', 'discountRate', 'buy', 'free')->where(['odId' => $off->id])
                        ->whereRaw("find_in_set('$today', weekDay)")
                        ->first();

                    if(!empty($hhData)){
                        $off->appliedOn = $hhData->appliedOn;
                        $off->discountRate = $hhData->discountRate;
                        if($hhData->discountType == 1)
                            $off->label = $hhData->discountRate. "% Off";
                        else
                            $off->label = "Buy ". $hhData->buy. " Get ".$hhData->free." free";

                        $off->startDate = $today;
                        $off->endDate = $today;

                        $offers[] = $off;
                    }
                }
            }
        }
        $data['offerData'] = $offers;
        
        //Calling the Bar Now
        $barData = array();
        $sql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName, bb.mediaName, rr.rating FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnBarRatings rr on rr.barId = b.id LEFT JOIN bnCities city on city.city_id = ad.cityId LEFT JOIN bnBarBanners bb on bb.barId = b.id WHERE u.status = 1";

        //Filter Conditions Start
        //If keyword is set
        if (isset($keyword)) {
            //Get the city ID based on keyword
            $cityId = CommonFunction::GetSingleField('bnCities', 'city_id', 'city_name', $keyword);
            if (!empty($cityId)) {
                $sql .= " AND city.city_id = " . $cityId;
            } else {
                $sql .= " AND b.barName LIKE '%" . $keyword . "%'";
            }

        }

        //On load more, will receive the previous Bar IDs, so each time new data will be passed
        if (isset($lastBarIds) && !empty($lastBarIds) && $lastBarIds != 0) {
            $sql .= " AND b.id NOT IN ($lastBarIds)";
        }

        $sql .= " group by b.id";
        $sql .= " order by rand()";

        /* if (isset($page)) {
        $startFrom = ($page) ? $page * $limit : 0;
        $pageNo = $page;
        }*/

        $sql .= " limit $limit";

        if (isset($loadMore) && !empty($loadMore)) {
            if ($loadMore == 'bar') {
                $barData = DB::select(DB::raw($sql));
                $data['barData'] = $barData;
            }
        } else {
            $barData = DB::select(DB::raw($sql));
            $data['barData'] = $barData;
        }
        //Converting NULL to "" String
        array_walk_recursive($data, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        //get data of visit
        $visit = CommonFunction::getVisitData($userId);
        $data['visitData'] = $visit;

        if (!empty($data)) {
            //get count of bars that are active
            $countSql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnCities city on city.city_id = ad.cityId WHERE u.status = 1";
            $totalBars = '';
            if (isset($loadMore) && !empty($loadMore)) {
                if ($loadMore == 'bar') {
                    $count = DB::select(DB::raw($countSql));
                    if ($count) {
                        $totalBars = count($count);
                    }
                }
            } else {
                $count = DB::select(DB::raw($countSql));
                if ($count) {
                    $totalBars = count($count);
                }

            }

            $mediaPath = CommonFunction::getMediaPath();

            $result = array('status' => 1, 'message' => 'Bar listing has been received successfully', 'data' => $data, 'totalBars' => $totalBars, 'totaloffers' => $totaloffers, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    //Pass All the Boards / Classes etc.
    public function getCommonData()
    {
        //Get Common Data
        $settings = CommonFunction::getAppSettings();
        $result = array('status' => 1, 'message' => 'Common Data has been successfully', 'settings' => $settings);

        $mediaPath = CommonFunction::getMediaPath();
        $result['app_version'] = CommonFunction::appVersion();
        $result['mediaPath'] = $mediaPath;
        return response()->json($result);
    }

    //Home Screen function to load newsfeeds of users
    public function loadHomeData(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $idLast = 0;
        if (isset($lastId)) {
            $idLast = $lastId;
        }

        //get the new of user
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'bnNewsFeeds.description', 'bnNewsFeeds.visibility', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['isDeleted' => 0])
            ->where(function ($query) use ($idLast) {
                if ($idLast) {
                    $query->where('bnNewsFeeds.id', '<', $idLast);
                }
            })
            ->limit(10)
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->get();

        if (count($newsfeed) > 0) {
            $newsFeedDetails = array();
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $userId, 'newsId' => $nf->id])->first();

                $nf['reactionIdByUser'] = 0;

                if ($nfReact) {
                    $nf['reactionIdByUser'] = $nfReact->reactionId;
                }
                $nf['mediaType'] = $nf->mediaType;
                if ($nf->mediaType == '') {
                    $nf['mediaType'] = 0;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();

                //Get the News meda files
                $nf['mediaFiles'] = DB::table('bnUserMediaFiles')->select('id', 'mediaName', 'mediaType')
                ->where(['moduleId' => $nf->id])->get()->toArray();
                
                $nf['totalComment'] = count($newsfeedComment);
                $nf['totalReaction'] = count($newsfeedReaction);
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }
            $data['newsFeedDetails'] = $newsFeedDetails;
        }

        //get data of visit
        $visit = CommonFunction::getVisitData($userId);
        $data['visitData'] = $visit;

        if (isset($data)) {
            //$data = array('newsFeedDetails' => $newsFeedDetails, 'visitData' => $visit);
            $media = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'News feed has been received successfully', 'data' => $data, 'mediaPath' => $media);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    //Home Screen function to load previous newsfeeds of users
    public function loadHomePrevData(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'firstId' => 'required',
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $idFirst = 0;
        if (isset($firstId)) {
            $idFirst = $firstId;
        }

        $newsFeedDetails = array();

        //get the new of user
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'bnNewsFeeds.description', 'bnNewsFeeds.visibility', 'umf.id as mediaId', 'umf.mediaName', 'umf.mediaType', 'umf.mediaSize', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt')
            ->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['isDeleted' => 0])
            ->where(function ($query) use ($idFirst) {
                if ($idFirst) {
                    $query->where('bnNewsFeeds.id', '>', $idFirst);
                }
            })
            ->limit(10)
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->get();

        if (count($newsfeed) > 0) {
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $userId, 'newsId' => $nf->id])->first();

                $nf['reactionIdByUser'] = 0;

                if ($nfReact) {
                    $nf['reactionIdByUser'] = $nfReact->reactionId;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }

            $data = array('newsFeedDetails' => $newsFeedDetails);

            $media = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'News feed has been received successfully', 'data' => $data, 'mediaPath' => $media);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }
}