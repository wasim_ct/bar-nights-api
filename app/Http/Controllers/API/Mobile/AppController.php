<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\NewsFeed;
use App\NewsFeedComment;
use App\NewsFeedReaction;
use App\DiscountOffer;
use App\HappyHoursTiming;
use App\Friend;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;

class AppController extends Controller
{
    //Home Screen function to load the data on map
    public function getHomeData(Request $request)
    {
        extract($_POST);
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        //SQL Query to load bar near by 3 KM
        $sql = "SELECT b.id as barId, b.barName, a.address, a.latitude, a.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnBars b inner join bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1
                HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT 0,50"; //3 KM later on
        $mapData = DB::select(DB::raw($sql)); 

        // Get data of visit
        $visit = CommonFunction::getVisitData($userId);

        $data = array('mapData' => $mapData, 'visitData' => $visit);
        if (!empty($data)) {
            $mediaPath = CommonFunction::getMediaPath();

            $result = array('status' => 1, 'message' => "Data has been received successfully", 'data' => $data, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    //get Bar Listing Function with different filters
    public function getBarsListing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }

        //Pagination condition setup
        //if limit is not set then default limit is 10
        if (!isset($limit) && empty($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom) && empty($startFrom)) {
            $startFrom = 0;
        }
        $totaloffers = '';
        $currentTime = date('H:i:s');
        $today = date('Y-m-d');
        $Date = date('Y-m-d');

        /* ### Calling the Discount/Offers with 3 KMs ### */
        
        //get the Discount/Offers with Happy Hours within 3 KMs count
        $sql = "SELECT od.id, od.barId, od.title, od.description, od.offerType, od.createdAt, b.barName, ad.address, ad.latitude, ad.longitude, od.offerImage, od.templateId, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(ad.latitude))
                 * COS(RADIANS(ad.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(ad.latitude))))
                 AS distanceInKM
                FROM bnOfferDetails od INNER JOIN bnBars b on b.id = od.barId INNER JOIN bnAddresses ad on ad.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1 AND od.status = 1";
        
        // Skip the previously sent offes
        if (isset($lastOfferIds) && !empty($lastOfferIds) && $lastOfferIds != 0) {
            $sql .= " AND od.id NOT IN ($lastOfferIds)";
        }

        $sql .= " group by od.id HAVING distanceInKM <= 3000
                ORDER BY distanceInKM ASC
                LIMIT $startFrom, $limit";

        //get the Discount/Offers with Happy Hours within 3 KMs count
        $offerCountSql = "SELECT od.id, od.offerType, od.createdAt, a.latitude, a.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude)) * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude)) + SIN(RADIANS($latitude)) * SIN(RADIANS(a.latitude)))) AS distanceInKM
                 FROM bnOfferDetails od INNER JOIN bnBars b on b.id = od.barId INNER JOIN bnAddresses a on a.userId = b.userId
                inner join bnUsers u on u.id = b.userId
                WHERE u.status = 1 AND od.status = 1
                HAVING distanceInKM <= 3000";

        if (isset($loadMore) && !empty($loadMore)) {
            if ($loadMore == 'offer') {
                //$data['offerData'] = 
                $offerData = DB::select(DB::raw($sql));
                $countOffer = DB::select(DB::raw($offerCountSql));
                if ($countOffer) {
                    $totaloffers = count($countOffer);
                }
            }
        } else {
            $offerData = DB::select(DB::raw($sql));
            $countOffer = DB::select(DB::raw($offerCountSql));
            if ($countOffer) {
                $totaloffers = count($countOffer);
            }
        }

        // If offer found, then need to check start-end date of the offers
        $offers = array();
        if(isset($offerData) && count($offerData) > 0){
            foreach ($offerData as $off) {

                $off->templateName = CommonFunction::GetSingleField('bnTemplates', 'templateFrame', 'id', $off->templateId);
                
                // Discount only of today
                if($off->offerType == 'discount') 
                {
                    //$offerDate = date("Y-m-d", strtotime($off->createdAt));
                    $today = date("Y-m-d");
                    $disData = DiscountOffer::select('id', 'appliedOn', 'discountRate')
                        ->where(['odId' => $off->id, 'startDate' => $today, 'endDate' => $today])
                        ->first();
                    if(!empty($disData)){
                        $off->appliedOn = $disData->appliedOn;
                        $off->discountRate = $disData->discountRate;
                        $off->label = $disData->discountRate. "% Off";
                        $off->startDate = $today;
                        $off->endDate = $today;

                        $offers[] = $off;
                    }
                }
                // Happy hour which relates to current day
                else if($off->offerType == 'happyhour') {
                    $today = strtolower(date('l'));
                    
                    $hhData = HappyHoursTiming::select('id', 'appliedOn', 'discountType', 'discountRate', 'buy', 'free')->where(['odId' => $off->id])
                        ->whereRaw("find_in_set('$today', weekDay)")
                        ->first();

                    if(!empty($hhData)){
                        $off->appliedOn = $hhData->appliedOn;
                        $off->discountRate = $hhData->discountRate;
                        if($hhData->discountType == 1)
                            $off->label = $hhData->discountRate. "% Off";
                        else
                            $off->label = "Buy ". $hhData->buy. " Get ".$hhData->free." free";

                        $off->startDate = $today;
                        $off->endDate = $today;

                        $offers[] = $off;
                    }
                }
            }
        }
        $data['offerData'] = $offers;
        
        /* ==== Calling the Bar Now ==== */
        $barData = array(); // bb.mediaName,
        $sql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName, rr.rating, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(ad.latitude))
                 * COS(RADIANS(ad.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(ad.latitude))))
                 AS distanceInKM FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnBarRatings rr on rr.barId = b.id LEFT JOIN bnCities city on city.city_id = ad.cityId WHERE u.status = 1"; 
                //3 KM later on 
        //LEFT JOIN bnBarBanners bb on bb.barId = b.id 

        //Filter Conditions Start
        //If keyword is set
        if (isset($keyword)) {
            //Get the city ID based on keyword
            $cityId = CommonFunction::GetSingleField('bnCities', 'city_id', 'city_name', $keyword);
            if (!empty($cityId)) {
                $sql .= " AND city.city_id = " . $cityId;
            } else {
                $sql .= " AND b.barName LIKE '%" . $keyword . "%'";
            }
        }

        //On load more, will receive the previous Bar IDs, so each time new data will be passed
        if (isset($lastBarIds) && !empty($lastBarIds) && $lastBarIds != 0) {
            $sql .= " AND b.id NOT IN ($lastBarIds)";
        }

        $sql .= " group by b.id HAVING distanceInKM <= 3000";
        $sql .= " ORDER BY distanceInKM ASC limit $startFrom, $limit";
       
        if (isset($loadMore) && !empty($loadMore)) {
            if ($loadMore == 'bar') {
                $barData = DB::select(DB::raw($sql));
                //$data['barData'] = $barData;
            }
        } else {
            $barData = DB::select(DB::raw($sql));
            //$data['barData'] = $barData;
        }

        if(isset($barData) && !empty($barData)) {
            $bnBars = array();
            foreach ($barData as $row) {
                //Converting NULL to "" String
                array_walk_recursive($row, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
                $visibility = CommonFunction::getBarVisibility($row->barId);
                if($visibility) {
                    $row->openingStatus = $visibility['status'];
                    $row->label = $visibility['message'];
                } 

                //get banner of bar which(mediaType:-1:video,2:image)
                $banner = DB::table('bnBarBanners')->select('mediaName')
                    ->where('barId', $row->barId)
                    ->orderBy('displayOrder', 'asc')
                    ->first();
                if(!empty($banner))
                    $row->mediaName = $banner->mediaName;
                else
                    $row->mediaName = "";
                
                if(empty($row->rating))
                    $row->rating = 0;

                $bnBars[] = $row;
            }
            $data['barData'] = $bnBars;
        }


        //Converting NULL to "" String
        array_walk_recursive($data, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        /* === Get Visit Info === */ 
        $visit = CommonFunction::getVisitData($userId);
        $data['visitData'] = $visit;

        /* === Get User Status === */ 
        $data['userStatus'] = CommonFunction::GetSingleField('bnUsers', 'status', 'id', $userId);
        $data['contactEmail'] = config('app.contact_email');

        if (!empty($data)) {
            
            //get count of bars that are active
            /*$countSql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnCities city on city.city_id = ad.cityId WHERE u.status = 1"; */

            $countSql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(ad.latitude))
                 * COS(RADIANS(ad.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(ad.latitude))))
                 AS distanceInKM
                FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnCities city on city.city_id = ad.cityId WHERE u.status = 1 HAVING distanceInKM <= 3000"; 
            
            $totalBars = '';
            if (isset($loadMore) && !empty($loadMore)) {
                if ($loadMore == 'bar') {
                    $count = DB::select(DB::raw($countSql));
                    if ($count) {
                        $totalBars = count($count);
                    }
                }
            } else {
                $count = DB::select(DB::raw($countSql));
                if ($count) {
                    $totalBars = count($count);
                }
            }

            $mediaPath = CommonFunction::getMediaPath();

            $result = array('status' => 1, 'message' => 'Bar listing has been received successfully', 'data' => $data, 'totalBars' => $totalBars, 'totaloffers' => $totaloffers, 'mediaPath' => $mediaPath);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    //Pass All the Boards / Classes etc.
    public function getCommonData()
    {
        //Get Common Data
        $settings = CommonFunction::getAppSettings();
        $result = array('status' => 1, 'message' => 'Common Data has been successfully', 'settings' => $settings);

        $mediaPath = CommonFunction::getMediaPath();
        $result['app_version'] = CommonFunction::appVersion();
        $result['mediaPath'] = $mediaPath;
        return response()->json($result);
    }

    // Home Screen function to load active patrons - geotagged
    public function loadActiveUsers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        /* === Get the Active Friends/Contacts in the Bar === */
        $activeUsers = Friend::select('bnFriendLists.friendId', 'p.firstName', 'p.lastName', 'p.profilePic', 'b.barName', 'gt.barId', 'bn.mediaName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnFriendLists.friendId')
            ->join('bnGeoTaggings as gt', 'gt.userId', '=', 'bnFriendLists.friendId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'gt.barId')
            ->leftJoin('bnBarBanners as bn', 'bn.barId', '=', 'gt.barId')
            ->where(['bnFriendLists.userId' => $userId, 'bnFriendLists.status' => 1])
            ->groupBy('bnFriendLists.friendId')
            ->get()->toArray();

        if (!empty($activeUsers)) {
            $patrons = array();
            if(!empty($activeUsers)) {
                foreach ($activeUsers as $i => $row) {
                    $mode = CommonFunction::checkUserIncognitoMode($row['friendId']);
                    if($mode == 0) {
                        if($row['barId'] == null) {
                            $row['barId'] = 0;
                            $row['barName'] = "";
                            $row['mediaName'] = "";
                        }

                        $patrons[] = $row;
                    }
                }
            }

            //Converting NULL to "" String
            array_walk_recursive($activeUsers, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Active users has been received successfully', 'data' => $patrons);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Home Screen function to load newsfeeds of users
    public function loadHomeData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }

        /* === Get the Active Friends/Contacts in the Bar === */
        /*$activeFriends = Friend::select('bnFriendLists.friendId', 'p.firstName', 'p.lastName', 'p.profilePic', 'b.barName', 'gt.barId', 'bn.mediaName')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnFriendLists.friendId')
            ->leftJoin('bnGeoTaggings as gt', 'gt.userId', '=', 'bnFriendLists.friendId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'gt.barId')
            ->leftJoin('bnBarBanners as bn', 'bn.barId', '=', 'gt.barId')
            ->where(['bnFriendLists.userId' => $userId, 'bnFriendLists.status' => 1])
            ->groupBy('bnFriendLists.friendId')
            ->get()->toArray();
        $data['activeUsers'] = $activeFriends;*/

        /* === get the new of user === */
        $idLast = 0;
        if (isset($lastId)) {
            $idLast = $lastId;
        }
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'bnNewsFeeds.description', 'bnNewsFeeds.locationTag', 'bnNewsFeeds.locationId', 'bnNewsFeeds.visibility', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['isDeleted' => 0])
            ->where(function ($query) use ($idLast) {
                if ($idLast) {
                    $query->where('bnNewsFeeds.id', '<', $idLast);
                }
            })
            ->limit(10)
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->get();

        if (!empty($newsfeed)) {
            $newsFeedDetails = array();
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $userId, 'newsId' => $nf->id])->first();

                $nf['reactionIdByUser'] = 0;

                if ($nfReact) {
                    $nf['reactionIdByUser'] = $nfReact->reactionId;
                }
                $nf['mediaType'] = $nf->mediaType;
                if ($nf->mediaType == '') {
                    $nf['mediaType'] = 0;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.createdAt', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();

                //Get the News meda files
                $nf['mediaFiles'] = DB::table('bnUserMediaFiles')->select('id', 'mediaName', 'mediaType')
                ->where(['moduleId' => $nf->id])->get()->toArray();
                if(!empty($nf['mediaFiles'])) {
                    foreach ($nf['mediaFiles'] as $row) {
                        $nf['mediaType'] = $row->mediaType;
                    }
                }

                //Get the tagged friends
                $taggedFriends = DB::table('bnNewsFeedFriends as f')
                    ->leftJoin('bnPatrons as p', 'p.userId', '=', 'f.friendId')
                    ->select('p.firstName', 'p.lastName', 'p.profilePic', 'p.userId')
                    ->where(['f.newsId' => $nf->id])
                    ->get()->toArray();

                /*$location = DB::table('bnNewsFeedLocations as l') 
                    ->select('l.latitude', 'l.longitude', 'l.address')
                    ->where(['l.id' => $nf->locationId])
                    ->first();
                $location = !empty($location) ? $location : (object) array();*/

                // Get tagged location
                $sql = "SELECT a.id, a.area, a.cityId, city.city_name as cityName FROM bnCityAreas a INNER JOIN bnCities city on city.city_id = a.cityId where a.id = ".$nf->locationId;
                $tag = DB::select(DB::raw($sql));
                if(!empty($tag)) {
                    foreach ($tag as $row) {
                        $location = array(
                            'areaId' => $row->id,
                            'area' => $row->area,
                            'cityId' => $row->cityId,
                            'cityName' => $row->cityName,
                        );
                    }
                }
                else 
                    $location = (object) array();

                $nf['totalComment'] = count($newsfeedComment);
                $nf['totalReaction'] = count($newsfeedReaction);
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;
                $nf['taggedFriends'] = $taggedFriends;
                $nf['location'] = $location;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }
            $data['newsFeedDetails'] = $newsFeedDetails;
        }

        //get data of visit
        //$visit = CommonFunction::getVisitData($userId);
        //$data['visitData'] = $visit;

        /* === Get User Status === */ 
        $data['userStatus'] = CommonFunction::GetSingleField('bnUsers', 'status', 'id', $userId);
        $data['contactEmail'] = config('app.contact_email');

        if (isset($data)) {
            //Converting NULL to "" String
            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            //$data = array('newsFeedDetails' => $newsFeedDetails, 'visitData' => $visit);
            $media = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'News feed has been received successfully', 'data' => $data, 'mediaPath' => $media);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Home Screen function to load previous newsfeeds of users
    public function loadHomePrevData(Request $request)
    {
        extract($_POST);

        $validator = Validator::make($request->all(), [
            'firstId' => 'required',
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        $idFirst = 0;
        if (isset($firstId)) {
            $idFirst = $firstId;
        }

        $newsFeedDetails = array();

        //get the news of user
        $newsfeed = NewsFeed::select('bnNewsFeeds.id', 'bnNewsFeeds.userId', 'p.firstName', 'p.lastName', 'p.profilePic', 'u.userName', 'bnNewsFeeds.description', 'bnNewsFeeds.locationTag', 'bnNewsFeeds.visibility', 'umf.id as mediaId', 'umf.mediaName', 'umf.mediaType', 'umf.mediaSize', 'bnNewsFeeds.createdAt', 'bnNewsFeeds.updatedAt')
            ->leftJoin('bnUserMediaFiles as umf', 'umf.moduleId', '=', 'bnNewsFeeds.id')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeeds.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'p.userId')
            ->where(['isDeleted' => 0])
            ->where(function ($query) use ($idFirst) {
                if ($idFirst) {
                    $query->where('bnNewsFeeds.id', '>', $idFirst);
                }
            })
            ->limit(10)
            ->orderBy('bnNewsFeeds.id', 'desc')
            ->get();

        if (count($newsfeed) > 0) {
            foreach ($newsfeed as $nf) {

                $nfReact = NewsFeedReaction::select('id', 'reactionId')->where(['userId' => $userId, 'newsId' => $nf->id])->first();

                $nf['reactionIdByUser'] = 0;

                if ($nfReact) {
                    $nf['reactionIdByUser'] = $nfReact->reactionId;
                }

                //get reactions on news feed by news feed id and user Id
                $newsfeedReaction = NewsFeedReaction::select('bnNewsFeedReactions.id', 'bnNewsFeedReactions.reactionId', 'bnNewsFeedReactions.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedReactions.userId')->where(['bnNewsFeedReactions.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                //get comments on news feed by news feed id and user Id
                $newsfeedComment = NewsFeedComment::select('bnNewsFeedComments.id', 'bnNewsFeedComments.comment', 'bnNewsFeedComments.userId', 'p.firstName', 'p.lastName', 'p.profilePic')->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnNewsFeedComments.userId')->where(['bnNewsFeedComments.newsId' => $nf->id])->orderBy('id', 'desc')->get()->toArray();
                $nf['reaction'] = $newsfeedReaction;
                $nf['comments'] = $newsfeedComment;

                //Converting NULL to "" String
                array_walk_recursive($nf, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $newsFeedDetails[] = $nf;
            }

            $data = array('newsFeedDetails' => $newsFeedDetails);

            $media = CommonFunction::getMediaPath();
            $result = array('status' => 1, 'message' => 'News feed has been received successfully', 'data' => $data, 'mediaPath' => $media);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        //return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
        return response()->json($result);
    }

    // Function to Search Records, Bar/NewsFeeds etc.
    public function searchRecords(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required',
            //'latitude' => 'required',
            //'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination condition setup
        if (!isset($limit) && empty($limit)) {
            $limit = 10;
        }
        if (!isset($startFrom) && empty($startFrom)) {
            $startFrom = 0;
        }

        /* === If Keyword start with # tag, then search the News Feed  === */
        $char = substr($keyword, 0, 1);
        if($char === "#") {
            $keyword = str_replace(" ", "", ltrim($keyword, '#'));
            $hashtag = DB::table('bnHashTags as h')
                ->leftJoin('bnHashTagPosts as hp', 'hp.hashtagId', '=', 'h.id')
                ->leftJoin('bnNewsFeeds as nf', 'nf.id', '=', 'hp.newsId')
                ->leftJoin('bnUserMediaFiles as m', 'm.moduleId', '=', 'hp.newsId')
                ->select('hp.newsId', 'nf.userId', 'm.mediaName', 'm.mediaType')->where(['h.hashtag' => $keyword])
                ->offset($startFrom)->limit($limit)
                ->orderBy('nf.id', 'desc')
                ->groupBy('nf.id')
                ->get()->toArray();
            if($hashtag) {
                //Converting NULL to "" String
                array_walk_recursive($hashtag, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $result = array('status' => 1, 'message' => 'Records has been received successfully', 'data' => $hashtag, 'searchType' => 'hashtag');
            }
            else {
                $result = array('status' => 0, 'message' => 'No record found', 'searchType' => 'hashtag');
            }
        }
        /* === Bars Searching === */ 
        else 
        {
            //$sql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName, bb.mediaName, rr.rating FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnBarRatings rr on rr.barId = b.id LEFT JOIN bnCities city on city.city_id = ad.cityId LEFT JOIN bnBarBanners bb on bb.barId = b.id WHERE u.status = 1"; 

            $sql = "SELECT b.id as barId, b.barName, b.logo, ad.address, ad.latitude, ad.longitude, city.city_name as cityName, bb.mediaName, rr.rating, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(ad.latitude))
                 * COS(RADIANS(ad.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(ad.latitude))))
                 AS distanceInKM FROM bnBars b INNER JOIN bnUsers u on u.id = b.userId LEFT JOIN bnAddresses ad on ad.userId = b.userId LEFT JOIN bnBarRatings rr on rr.barId = b.id LEFT JOIN bnCities city on city.city_id = ad.cityId LEFT JOIN bnCityAreas ar on ar.id = ad.areaId LEFT JOIN bnBarBanners bb on bb.barId = b.id WHERE u.status = 1"; 
                //3 KM later on
            
            if(isset($searchOn) && $searchOn == 'area') {
                $sql .= " AND (ad.address LIKE '%" . $keyword . "%' OR city.city_name LIKE '%" . $keyword . "%' OR ar.area LIKE '%" . $keyword . "%')";
            }
            else {
                $sql .= " AND (b.barName LIKE '%" . $keyword . "%')";
            }

            //Get the city ID based on keyword
            /*$cityId = CommonFunction::GetSingleField('bnCities', 'city_id', 'city_name', $keyword);
            if (!empty($cityId)) {
                $sql .= " AND city.city_id = " . $cityId;
            } else {
                $sql .= " AND (b.barName LIKE '%" . $keyword . "%' OR b.about LIKE '%" . $keyword . "%' OR ad.address LIKE '%" . $keyword . "%' OR city.city_name LIKE '%" . $keyword . "%')";
            }*/

            //On load more, will receive the previous Bar IDs, so each time new data will be passed
            if (isset($lastBarIds) && !empty($lastBarIds) && $lastBarIds != 0) {
                $sql .= " AND b.id NOT IN ($lastBarIds)";
            }
            $sql .= " group by b.id ORDER BY distanceInKM ASC limit $startFrom, $limit";
    
            $barData = DB::select(DB::raw($sql));

            $bars = array();
            if(!empty($barData)) {
                foreach ($barData as $row) {
                    //Converting NULL to "" String
                    array_walk_recursive($row, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });   

                    $bars[] = $row;    
                }
            }

            if (!empty($bars)) {
                $result = array('status' => 1, 'message' => 'Records has been received successfully', 'data' => $bars, 'searchType' => 'bar');
            } else {
                $result = array('status' => 0, 'message' => 'No record found', 'searchType' => 'bar');
            }        
        } 
    
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to Search News Feeds basec on Area.
    public function searchAreaNewsFeeds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'areaId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $newsFeeds = DB::table('bnNewsFeeds as n')
            //->join('bnCityAreas as ca', 'ca.id', '=', 'n.locationId')
            ->leftJoin('bnUserMediaFiles as m', 'm.moduleId', '=', 'n.id')
            ->select('n.id', 'n.userId', 'm.mediaName', 'm.mediaType')
            ->where(['n.locationId' => $areaId])
            //->offset($startFrom)->limit($limit)
            ->orderBy('n.id', 'desc')
            ->groupBy('n.id')
            ->get()->toArray();

        // If no record found on Area, then search on City also
        if(empty($newsFeeds)) {
            // Get city ID from area name
            $cityId = CommonFunction::GetSingleField('bnCityAreas', 'cityId', 'id', $areaId);

            $newsFeeds = DB::table('bnNewsFeeds as n')
                ->leftJoin('bnCityAreas as ca', 'ca.id', '=', 'n.locationId')
                ->leftJoin('bnUserMediaFiles as m', 'm.moduleId', '=', 'n.id')
                ->select('n.id', 'n.userId', 'm.mediaName', 'm.mediaType')
                ->where(['ca.cityId' => $cityId])
                ->groupBy('n.id')
                ->get()->toArray();
        }

        if($newsFeeds) {
            //Converting NULL to "" String
            array_walk_recursive($newsFeeds, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Records has been received successfully', 'data' => $newsFeeds);
        }
        else {
            $result = array('status' => 0, 'message' => 'No record found');
        }
    
        
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to Get Local Areas, by searching
    public function getLocalAreas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'keyword' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        if(isset($keyword) && !empty($keyword)) {
           $cityId = CommonFunction::GetSingleField('bnCities', 'city_id', 'city_name', $keyword);
        }

        $sql = "SELECT a.id, a.area, a.cityId, city.city_name as cityName FROM bnCityAreas a INNER JOIN bnCities city on city.city_id = a.cityId where a.id != 0";
        
        if(isset($cityId) && !empty($cityId)) {
            $sql .= " AND a.cityId = $cityId";
        }
        else if(isset($keyword) && !empty($keyword)) {
            $sql .= " AND a.area LIKE '" . $keyword . "%'";
        }
        $sql .= " ORDER BY a.area ASC";

        $areas = DB::select(DB::raw($sql));
        //Converting NULL to "" String
        array_walk_recursive($areas, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        if (!empty($areas)) {
            $result = array('status' => 1, 'message' => 'Areas has been received successfully', 'data' => $areas);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }        
    
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }
}