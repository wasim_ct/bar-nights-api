<?php
namespace App\Http\Controllers\API\Mobile;

use App\BarItemPrice;
use App\BarNotification;
use App\BarTiming;
use App\Cart;
use App\Http\Controllers\Controller;
use App\Visit;
use App\Order;
use App\OrderKot;
use App\OrderKotItem;
use App\Patron;
use App\PreOrderDetail;
use App\UserNotification;
use App\Wallet;
use CommonFunction;
use DB;
use Illuminate\Http\Request;
use Validator;
use App\Transaction;
use App\EventBooking;

class OrderController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //add items to cart
    public function addToCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'cartItems' => 'required',
            //'orderType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* 
        * First check, visit is running of the with this bar, if yes, then treat the order as
        * Visiting Order
        */
        /*$visitExsit = Visit::select('id')
            ->where(['userId' => $userId, 'barId' => $barId, 'status' => 2])
            ->first();
        if(!empty($visitExsit) > 0)
            $orderType = 'order';*/

        $status = false;
        $id = '';
        $cartCalculation = '';
        //if user cart exist then delete the complete cart
        $cartDelete = Cart::where(['userId' => $userId])->delete(); //'orderType' => $orderType
 
        //insert the data in cart after delete
        $cart = CommonFunction::addItemsInCart();
 
        //get cart total qty
        $quantity = Cart::select('id', 'quantity', 'itemId')->where(['userId' => $userId])->sum('quantity'); //'orderType' => $orderType

        //calculate price and qty
        $cartAmount = CommonFunction::calculateCartAmount();
        if (!empty($cartAmount)) {
            $cartCalculation = array('status' => 1, 'itemsInCart' => (int) $quantity, 'price' => round($cartAmount['price'], 2));
        } else {
            $cartCalculation = array('status' => 0);
        }

        if ($cart) {
            $status = true;
            $val = 1;
            $msg = "Item has been added in the cart";
            $data = $cart;
        }

        if ($status == true) {
            $result = array('status' => $val, 'message' => $msg, 'data' => $data, 'cartData' => $cartCalculation);
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //delete  cart item
    public function removeCartItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //check that the cart exist or not
        $cartExist = Cart::select('id')->where(['id' => $id])->first();
        if ($cartExist) {
            //delete cart Item
            $cartDelete = Cart::where(['id' => $id])->delete();
            if ($cartDelete) {
                $result = array('status' => 1, 'message' => "Cart item has been deleted successfully");
            } else {
                $result = array('status' => 0, 'message' => "Something went wrong");
            }
        } else {
            $result = array('status' => 1, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //delete cart for user of particular order type
    public function clearCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            // 'orderType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $delete = Cart::where(['userId' => $userId])->delete();

        if (isset($barId) && !empty($barId) && isset($userId) && !empty($userId) && isset($item) && !empty($item) && isset($quantity) && !empty($quantity)) {
            $cart = CommonFunction::addItemsInCart();
        }
        $result = array('status' => 1, 'message' => "Cart has been cleared successfully");

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //update item quantity
    public function updateCartItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            // 'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        // update cart Item quantity
        if(isset($quantity)) {
            if ($quantity > 0) {
                $update = array('quantity' => $quantity, 'updatedAt' => $this->entryDate);
                $updateCart = Cart::where(['id' => $id])->update($update);
            } else { //Delete the item from the cart
                $updateCart = Cart::where(['id' => $id])->delete();
            }
        } 
        
        // Update suggesstion if passed
        if(isset($suggestions)){
            $update = array('suggestions' => trim($suggestions), 'updatedAt' => $this->entryDate);
            $updateCart = Cart::where(['id' => $id])->update($update);
        }

        if (isset($updateCart)) {
            $result = array('status' => 1, 'message' => "Cart has been updated successfully");
            if(isset($suggestions)) $result['suggestions'] = trim($suggestions);
        } else {
            $result = array('status' => 0, 'message' => "Something went wrong");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get listing of cart
    public function getCartItems(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            //'orderType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check the Block Status of the User === */
        $result = CommonFunction::isUserBlocked($userId);
        if(!empty($result)) {
            return response()->json($result);
        }

        $order = array();
        //get list of carts items of user
        $userCartList = Cart::select('bnCarts.id', 'bnCarts.barId', 'bnCarts.quantity', 'bnCarts.suggestions', 'bnCarts.itemId', 'i.itemName')
            ->leftJoin('bnBarItems as i', 'i.id', '=', 'bnCarts.itemId')
            ->where(['bnCarts.userId' => $userId, 'i.status' => 1]) //'bnCarts.orderType' => $orderType
            ->get()->toArray();

        if (count($userCartList) > 0) {
            // call the Discount/Happy Hours of the requested Bar ID
            // $barId = CommonFunction::GetSingleField('bnCarts', 'barId', 'userId', $userId);
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            $order = array();
            foreach ($userCartList as $ucl) {
                $unitPrice = 0;
                $itemPricing = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $ucl['itemId']);

                if ($itemPricing)
                {
                    $ucl['label'] = '';
 
                    $price = $unitPrice = $itemPricing;
                    // If dscount is available for the day
                    if ($discountData) {
                        $disData = CommonFunction::getDiscountedPrice($discountData, $ucl['itemId'], $itemPricing);
                        if (!empty($disData)) {
                            if($disData['discountPrice'] != 0){
                                $price = $disData['discountPrice'];
                                $ucl['label'] = $discountData->discountRate. ' % off';
                            }
                        }
                    }

                    //If Happy Hour is available for the day
                    $offerExist = false;
                    if ($happyHourData) {
                        $hhData = CommonFunction::getHappyHourPrice($happyHourData, $ucl['itemId'], $itemPricing);
                        
                        if (!empty($hhData)) {
                            if (isset($hhData['happyHourPrice'])) {
                                $price = $hhData['happyHourPrice'];
                                $ucl['label'] = $happyHourData->discountRate. ' % off';
                            } else {
                                $hhOffer = $hhData;
                                $ucl['label'] = "buy ".$hhData['buy']." get ". $hhData['free']. " free";
                                $offerExist = true;
                            }
                        }
                    }
                    
                    //if Happy Hour Offer have buy-get free then calculate the price    
                    if (isset($hhOffer) && $offerExist) {
                        $price = CommonFunction::getOfferPrice($hhOffer, $ucl['quantity'], $itemPricing);
                        $unitPrice = $itemPricing;
                    } else {
                        $unitPrice = $price;
                        $price = $price * $ucl['quantity'];
                    }
                    
                    $itemPricing = round($price, 2);
                }

                $ucl['unitPrice'] =  round($unitPrice, 2);
                $ucl['price'] = $itemPricing;
                $order[] = $ucl;
            }
            //Converting NULL to "" String
            array_walk_recursive($order, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            //calculate price and qty
            $cartAmount = CommonFunction::calculateCartAmount(); 
            if (!empty($cartAmount)) {
                $cartData = array('status' => 1, 'itemsInCart' => $cartAmount['itemsInCart'], 'barId' => $cartAmount['cartBarId'], 'barName' => $cartAmount['barName'], 'logo' => $cartAmount['logo'], 'price' => round($cartAmount['price'], 2), 'totalAmount' => $cartAmount['taxes']['totalprice'], 'cgst' => $cartAmount['taxes']['cgst'], 'sgst' => $cartAmount['taxes']['sgst'], 'serviceCharge' => $cartAmount['taxes']['serviceCharge'], 'serviceChargeTax' => $cartAmount['taxes']['serviceChargeTax'], 'cgstAmount' => $cartAmount['taxes']['cgstAmount'], 'sgstAmount' => $cartAmount['taxes']['sgstAmount'], 'serviceChargeAmount' => $cartAmount['taxes']['serviceChargeAmount'], 'serviceChargeTaxAmount' => $cartAmount['taxes']['serviceChargeTaxAmount'], 'grandTotal' => round($cartAmount['taxes']['grandTotal'], 2));
            } else {
                $cartData = array('status' => 0);
            }

            // Get the Wallet Balance of ther user
            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
            $walletAmt = ($walletAmt) ? $walletAmt : 0;

            $result = array('status' => 1, 'message' => "Cart data get successfully", 'data' => $order, 'cartData' => $cartData, 'walletAmt' => $walletAmt);
            //Get the Bar Profile with Tax info
            // $barData = Bar::select('barName', 'logo', 'cgst', 'sgst', 'serviceCharge', 'serviceChargeTax')
            //     ->where(['id' => $barId])->first();
            // if (!empty($barData)) {
            //     $result['barData'] = $barData;
            // }

        } else {
            $result = array('status' => 0, 'message' => "No record found", 'data' => $order);
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Funciton to get all pre order of the user - History
    public function getPreOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get list of orders of user(status 1:-completed,2:running)
        $PreOrderList = Order::select('bnOrders.id', 'br.Rating', 'bnOrders.barId', 'bnOrders.status', 'bnOrders.totalPrice', 'bnOrders.grandTotal', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnOrders.cgst', 'bnOrders.cgstAmount', 'bnOrders.sgst', 'bnOrders.sgstAmount', 'bnOrders.serviceCharge', 'bnOrders.serviceChargeAmount', 'bnOrders.serviceChargeTax', 'bnOrders.serviceChargeTaxAmount', 'bnOrders.createdAt as orderDate', 'bnOrders.friendId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->orderBy('bnOrders.id', 'desc')
            ->where(['bnOrders.userId' => $userId, 'bnOrders.orderType' => 'preorder'])
            ->offset($startFrom)->limit($limit)
            ->get();
        if (count($PreOrderList) > 0) {
            foreach ($PreOrderList as $preordlist) {
                $userData = "";
                if ($preordlist->friendId != 0) {
                    $userData = Patron::select('firstName', 'lastName', 'ProfilePic')
                        ->where(['userId' => $preordlist->friendId])
                        ->first();
                }
                $preordlist['giftedTo'] = $userData;
                $data[] = $preordlist;
                $result = array('status' => 1, 'message' => "Pre orders get successfully", 'data' => $data);
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get details of order
    public function preOrderDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //get list of orders of user
        $preOrderDetail = PreOrderDetail::select('id', 'orderId', 'itemId', 'itemName', 'quantity', 'unitTypeId', 'unitTypeName', 'amount', 'createdAt', 'updatedAt')
            ->where(['orderId' => $id])
            ->get()->toArray();

        //Converting NULL to "" String
        array_walk_recursive($preOrderDetail, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });

        if (count($preOrderDetail) > 0) {
            $result = array('status' => 1, 'message' => "Order detail get successfully", 'data' => $preOrderDetail);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //insert pre-orders of user
    public function insertPreOrder(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $orderDate = date('Y-m-d');
        $currentTime = date('H:i:s');
        $day = strtolower(date('l', strtotime($orderDate)));

        /* === Bar time validation with order time === */
        $result = CommonFunction::getBarTimingSatus($barId, $currentTime, $orderDate, 'order');
        if(!empty($result) && $result['status'] == 0) {
            return response()->json($result);
        }

        //get list of carts of user on bases of order type
        $userCartList = Cart::select('bnCarts.id', 'i.barId', 'bnCarts.orderType', 'b.cgst', 'bnCarts.quantity', 'bnCarts.suggestions', 'bnCarts.itemId', 'i.itemName', 'fc.id as catId', 'fc.catName', 'bnCarts.unitTypeId', 'ut.unitType')
            ->leftJoin('bnBarItems as i', 'i.id', '=', 'bnCarts.itemId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'i.barId')
            ->leftJoin('bnItemCategories as fc', 'fc.id', '=', 'i.catId')
            ->leftJoin('bnUnitTypes as ut', 'ut.id', '=', 'bnCarts.unitTypeId')
            ->where(['bnCarts.userId' => $userId])
            ->get();
            //, 'bnCarts.orderType' => 'preorder'

        if (!empty($userCartList)) {
            $order = array();
            $i = 1;
            $itemPrice1 = 0;
            $itemPrice2 = 0;

            //Get the active discount/offers, for item pricing
            $currentDate = date('Y-m-d');
            $discountData = CommonFunction::checkBarOffers($barId, $currentDate);
            $happyHourData = CommonFunction::checkBarHappyHour($barId);

            foreach ($userCartList as $ucl) {                                

                //check that unit id is 0 or not in cart
                if ($ucl->unitTypeId == 0) {
                    //get item price if unitid is 0
                    $itemPricing = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $ucl->itemId);
                    if ($itemPricing) 
                    {
                        $price = $itemPricing;
                        
                        //If dscount is available for the day
                        if ($discountData) {
                            $disData = CommonFunction::getDiscountedPrice($discountData, $ucl->itemId, $itemPricing);
                            if (!empty($disData)) {
                                if($disData['discountPrice'] != 0)
                                    $price = round($disData['discountPrice'], 2);
                            }
                        }

                        //If Happy Hour is available for the day
                        $offerExist = false;
                        if ($happyHourData) {
                            $hhData = CommonFunction::getHappyHourPrice($happyHourData, $ucl->itemId, $itemPricing);
                            if (!empty($hhData)) {
                                if (isset($hhData['happyHourPrice'])) {
                                    $price = round($hhData['happyHourPrice'], 2);
                                } else {
                                    $hhOffer = $hhData;
                                    $offerExist = true;
                                }
                            }
                        }

                        //if Happy Hour Offer have buy-get free then calculate the price
                        if (isset($hhOffer) && $offerExist) {
                            $price = CommonFunction::getOfferPrice($hhOffer, $ucl->quantity, $itemPricing);

                        } else {
                            $price = $price * $ucl->quantity;
                        }

                        $itemPrice1 = $itemPrice1 + $price;
                    }
                } 
                
                $order[] = $ucl;
            }
            $giftFriend = 0;
            if (isset($friendId) && !empty($friendId)) {
                $giftFriend = $friendId;
            }
            //Get the Tax information of the Bar
            $taxes = CommonFunction::GetSingleRow('bnBars', 'id', $barId);

            //Sum of Total Price
            $totalPrice = $itemPrice2 + $itemPrice1;

            //take total price calculation;
            $taxesCalculation = CommonFunction::calculateTax($totalPrice, $taxes);

            //Now insert the order details in the table
            $insert = array(
                'userId' => $userId,
                'barId' => $barId,
                'orderDate' => date('Y-m-d'),
                'status' => 1,
                'totalPrice' => $totalPrice,
                'friendId' => $giftFriend,
                'grandTotal' => $taxesCalculation['grandTotal'],
                'cgst' => $taxesCalculation['cgst'],
                'cgstAmount' => $taxesCalculation['cgstAmount'],
                'sgst' => $taxesCalculation['sgst'],
                'sgstAmount' => $taxesCalculation['sgstAmount'],
                'serviceCharge' => $taxesCalculation['serviceCharge'],
                'serviceChargeAmount' => $taxesCalculation['serviceChargeAmount'],
                'serviceChargeTax' => $taxesCalculation['serviceChargeTax'],
                'serviceChargeTaxAmount' => $taxesCalculation['serviceChargeTaxAmount'],
                'orderType' => 'preorder',
                'paymentStatus' => 0, //it will be 0 after payment-gateway
                'createdAt' => $this->entryDate,
                'updatedAt' => $this->entryDate, 
            );
            $insertOrder = Order::create($insert);

            if ($insertOrder['id']) {

                //to get unique orderId multyply the last inserted id with random no and update in order table
                $lastOrderId = $insertOrder['id'];
                $uniqueId = 'BN' . rand(100, 999) * $lastOrderId;
                $update = array('orderId' => $uniqueId);
                $updateOrder = Order::where('id', $lastOrderId)->update($update);

                //Now insert the items in User Order History table
                $itemPrice = 0;
                foreach ($userCartList as $ucl) {
                    //check that unit id is 0 or not in cart
                    if ($ucl->unitTypeId == 0) 
                    {
                        //get item price if unitid is 0
                        $itemPricing = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $ucl->itemId);
                        
                        if ($itemPricing) 
                        {
                            $price = $itemPricing;

                            //If dscount is available for the day
                            if ($discountData) {
                                $disData = CommonFunction::getDiscountedPrice($discountData, $ucl->itemId, $itemPricing);
                                if (!empty($disData)) {
                                    if($disData['discountPrice'] != 0)
                                        $price = $disData['discountPrice'];
                                }
                            }

                            //If Happy Hour is available for the day
                            $offerExist = false;
                            if ($happyHourData) {
                                $hhData = CommonFunction::getHappyHourPrice($happyHourData, $ucl->itemId, $itemPricing);
                                if (!empty($hhData)) {
                                    if (isset($hhData['happyHourPrice'])) {
                                        $price = $hhData['happyHourPrice'];
                                    } else {
                                        $hhOffer = $hhData;
                                        $offerExist = true;
                                    }

                                }
                            }

                            //if Happy Hour Offer have buy-get free then calculate the price
                            if (isset($hhOffer) && $offerExist) {
                                $itemPrice = CommonFunction::getOfferPrice($hhOffer, $ucl->quantity, $itemPricing);
                            } else {
                                $itemPrice = $price * $ucl->quantity;
                            }
                        }
                    } 

                    $insert = array(
                        'orderId' => $insertOrder['id'],
                        'itemId' => $ucl->itemId,
                        'itemName' => $ucl->itemName,
                        'quantity' => $ucl->quantity,
                        'suggestions' => $ucl->suggestions,
                        'unitTypeId' => $ucl->unitTypeId,
                        'unitTypeName' => $ucl->unitType,
                        'amount' => $itemPrice,
                        'unitPrice' => $price,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate,
                    );
                    $insertOrderDetail = PreOrderDetail::create($insert);
                    if ($insertOrderDetail['id']) {
                        $orderStatus = true;
                    }
                }

                //delete the cart items for this user as the data inserted in preorder table
                if (isset($orderStatus)) {
                    Cart::where(['userId' => $userId])->delete(); //'orderType' => 'preorder'

                    $userId = $userId;
                    $orderId = $insertOrder['id'];

                    $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();

                    if (!empty($user)) {
                        //notification for friend to whome we gifted order
                        if (isset($friendId) && $friendId != '') {
                            $message = 'You have a new gift from ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));

                            $record = array('moduleType' => 'orderGifted', 'moduleId' => $orderId, 'description' => $message, 'userId' => $friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                            $not = UserNotification::create($record);

                            if ($not['id']) {
                                $deviceToken = CommonFunction::getUserDeviceData($friendId); //Get Device Tokens

                                if ($deviceToken) {
                                    //Firebase Notification
                                    $payLoadData = array('moduleType' => 'orderGifted', 'moduleId' => $orderId, 'userId' => $friendId, 'title' => 'New gift recieved', 'msg' => $message);
                                    $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                }
                            }
                        }

                        //notification for bar
                        $message = 'New order recieved from ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));

                        $record = array('moduleType' => 'order', 'moduleId' => $orderId, 'description' => $message, 'barId' => $barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = BarNotification::create($record);

                        if ($not['id']) {
                            $bar = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $barId);
                            $deviceToken = CommonFunction::getUserDeviceData($bar); //Get Device Tokens

                            if ($deviceToken) {
                                //Firebase Notification
                                $payLoadData = array('moduleType' => 'orderPlaced', 'moduleId' => $orderId, 'barId' => $barId,'title' => 'New order placed', 'msg' => $message);
                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'B');
                            }
                        }
                    }
                }
                $result = array('status' => 1, 'message' => "Pre Order has been placed successfully", 'uniqueId' => $uniqueId, 'orderId' => $lastOrderId, 'amountWithoutTax' => $totalPrice, 'amount' => $taxesCalculation['grandTotal']);
            } else {
                $result = array('status' => 0, 'message' => "Somethig went wrong");
            }
        } 
        else {
            $result = array('status' => 0, 'message' => "Cart is empty");
        }
  
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Function to insert orders of user
    public function insertOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            //'visitId' => 'required',
        ]);

        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        /* === Check Is it gift order === */
        if(isset($friendId) && !empty($friendId) && $friendId != 0){
            $result = array('status' => 2, 'message' => "Confirm your order");
            $result['app_version'] = CommonFunction::appVersion();
            return response()->json($result);
        }

        /* 
        * If visit is running of the with this bar, if yes, then treat the order as
        * Visiting Order
        */
        $visitData = Visit::select('id', 'barId')
            ->where(['userId' => $userId, 'barId' => $barId, 'status' => 2])
            ->first();
        // $visitData = CommonFunction::GetSingleRow('bnVisits', 'id', $visitId);

        if(!empty($visitData)) {
            if ($visitData->status != 1 && $visitData->status != 3 && $visitData->status != 4) {

                $visitId = $visitData->id;

                //get list of carts of user on bases of order type
                $userCartList = Cart::select('bnCarts.id', 'i.barId', 'b.cgst', 'bnCarts.quantity', 'bnCarts.suggestions', 'bnCarts.itemId', 'i.itemName', 'fc.id as catId', 'fc.catName', 'bnCarts.unitTypeId', 'ut.unitType')
                    ->leftJoin('bnBarItems as i', 'i.id', '=', 'bnCarts.itemId')
                    ->leftJoin('bnBars as b', 'b.id', '=', 'i.barId')
                    ->leftJoin('bnItemCategories as fc', 'fc.id', '=', 'i.catId')
                    ->leftJoin('bnUnitTypes as ut', 'ut.id', '=', 'bnCarts.unitTypeId')
                    ->where(['bnCarts.userId' => $userId])
                    ->get();
                    //, 'bnCarts.orderType' => 'order'

                if (count($userCartList) > 0) {
                    $order = array();
                    $i = 1;
                    $itemPrice1 = 0;
                    $itemPrice2 = 0;

                    //Get the active discount/offers, for item pricing
                    $currentDate = date('Y-m-d');
                    $discountData = CommonFunction::checkBarOffers($visitData->barId, $currentDate);
                    $happyHourData = CommonFunction::checkBarHappyHour($visitData->barId);

                    foreach ($userCartList as $ucl) {

                        //check that unit id is 0 or not in cart
                        if ($ucl->unitTypeId == 0) {

                            $unitPrice = 0;
                            //get item price if unitid is 0
                            $itemPricing = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $ucl->itemId);

                            if ($itemPricing) {
                                $price = $unitPrice = $itemPricing;
                                $itemPrice = $itemPricing;

                                //If dscount is available for the day
                                if ($discountData) {
                                    $disData = CommonFunction::getDiscountedPrice($discountData, $ucl->itemId, $itemPrice);
                                    if (!empty($disData)) {
                                        if($disData['discountPrice'] != 0)
                                            $price = $disData['discountPrice'];
                                    }
                                } 

                                //If Happy Hour is available for the day
                                $offerExist = false;
                                if ($happyHourData) {
                                    $hhData = CommonFunction::getHappyHourPrice($happyHourData, $ucl->itemId, $itemPrice);
                                    if (!empty($hhData)) {
                                        if (isset($hhData['happyHourPrice'])) {
                                            $price = $hhData['happyHourPrice'];
                                        } else {
                                            $hhOffer = $hhData;
                                            $offerExist = true;
                                        }
                                    }
                                }

                                //if Happy Hour Offer have buy-get free then calculate the price
                                if (isset($hhOffer) && $offerExist) {
                                    $price = CommonFunction::getOfferPrice($hhOffer, $ucl->quantity, $itemPrice);
                                    $unitPrice = $itemPricing;
                                } else {
                                    $unitPrice = $price;
                                    $price = $price * $ucl->quantity;
                                }

                                $itemPrice1 = $itemPrice1 + $price;
                            }
                        } 
                        $order[] = $ucl;
                        $barId = $ucl->barId;

                        $taxes = CommonFunction::GetSingleRow('bnBars', 'id', $barId);
                    }

                    //Now insert the order details in the table
                    $totalPrice = $itemPrice2 + $itemPrice1;

                    //take total price calculation;
                    $taxesCalculation = CommonFunction::calculateTax($totalPrice, $taxes);
                    //print_r($taxesCalculation);exit;

                    //take table no from visitId
                    $tableStandingNo = $visitData->tableStandingNo;

                    //If order ID is set then update the record of the order
                    $orderId = Order::select('id')->where(['userId' => $userId, 'visitId' => $visitId])
                        ->first();
                    if (!empty($orderId)) {
                        $giftFriend = 0;
                        $giftFriendId = CommonFunction::GetSingleField('bnOrders', 'friendId', 'id', $orderId->id);
                        if ($giftFriendId == 0 && isset($friendId) && $friendId != '') {
                            $giftFriend = $friendId;
                        } else if ($giftFriendId != 0 && isset($friendId) && $friendId != '') {
                            $giftFriend = $giftFriendId . ',' . $friendId;
                        } else if ($giftFriendId != 0 && isset($friendId) && $friendId == '') {
                            $giftFriend = $giftFriendId;
                        }
                        $update = array(
                            'friendId' => $giftFriend,
                        );
                        $updateOrder = Order::where(['id' => $orderId->id])->update($update);
                        $lastOrderId = $orderId->id;
                    }
                    //Create new order if ID is not set
                    else {
                        $giftFriend = 0;
                        //check that is friend id is set or not
                        if (isset($friendId) && $friendId != '') {
                            $giftFriend = $friendId;
                        }

                        $insert = array(
                            'userId' => $userId,
                            'barId' => $barId,
                            'tableNo' => $tableStandingNo,
                            'orderDate' => date('Y-m-d'),
                            'friendId' => $giftFriend,
                            'status' => 1,
                            'totalPrice' => 0,
                            'grandTotal' => 0,
                            'cgst' => 0,
                            'cgstAmount' => 0,
                            'sgst' => 0,
                            'sgstAmount' => 0,
                            'serviceCharge' => 0,
                            'serviceChargeAmount' => 0,
                            'serviceChargeTax' => 0,
                            'serviceChargeTaxAmount' => 0,
                            'visitId' => $visitId,
                            'orderType' => 'order',
                            'createdAt' => $this->entryDate,
                            'updatedAt' => $this->entryDate,
                        );
                        $insertOrder = Order::create($insert);
                        $lastOrderId = $insertOrder['id'];
                    }

                    //If order created/updated, then insert the KOT and Items
                    if (isset($lastOrderId)) {
                        //to get unique orderId multyply the last inserted id with random no and update in order table
                        $uniqueId = 'BN' . rand(100, 999) * $lastOrderId;
                        $update = array('orderId' => $uniqueId);
                        $updateOrder = Order::where('id', $lastOrderId)->update($update);

                        $itemPrice = 0;
                        $giftFriend = 0;
                        //check that is friend id is set or not
                        if (isset($friendId) && $friendId != '') {
                            $giftFriend = $friendId;
                        }

                        //generate kot no for order
                        $kotNumber = CommonFunction::generateKot();
                        $kotInsert = array(
                            'orderId' => $lastOrderId,
                            'status' => 1,
                            'kotNumber' => $kotNumber,
                            'friendId' => $giftFriend,
                            'createdAt' => $this->entryDate,
                            'updatedAt' => $this->entryDate,
                        );

                        //insert in kot table
                        $insertOrderKot = OrderKot::create($kotInsert);

                        if ($insertOrderKot['id']) {
                            foreach ($userCartList as $ucl) {
                                //check that unit id is 0 or not in cart
                                if ($ucl->unitTypeId == 0) {
                                    $unitPrice = 0;

                                    //get item price if unitid is 0
                                    $itemPricing = CommonFunction::GetSingleField('bnBarItems', 'price', 'id', $ucl['itemId']);
                                    
                                    if ($itemPricing) {
                                        $price = $unitPrice = $itemPricing;
                                        $itemPrice = $unitPrice = $itemPricing;

                                        //If dscount is available for the day
                                        if ($discountData) {
                                            $disData = CommonFunction::getDiscountedPrice($discountData, $ucl->itemId, $itemPrice);
                                            if (!empty($disData)) {
                                                if($disData['discountPrice'] != 0)
                                                    $price = $disData['discountPrice'];
                                            }
                                        }

                                        //If Happy Hour is available for the day
                                        $offerExist = false;
                                        if ($happyHourData) {
                                            $hhData = CommonFunction::getHappyHourPrice($happyHourData, $ucl->itemId, $itemPrice);

                                            if (!empty($hhData)) {
                                                if (isset($hhData['happyHourPrice'])) {
                                                    $price = $hhData['happyHourPrice'];
                                                } else {
                                                    $hhOffer = $hhData;
                                                    $offerExist = true;
                                                }
                                            }
                                        }

                                        //if Happy Hour Offer have buy-get free then calculate the price
                                        if (isset($hhOffer) && $offerExist) {
                                            $price = CommonFunction::getOfferPrice($hhOffer, $ucl->quantity, $itemPrice);
                                            $unitPrice = $itemPricing;
                                        } else {
                                            $unitPrice = $price;
                                            $price = $price * $ucl->quantity;
                                        }
                                    }
                                } 
                                
                                //insert kot item in kot item table
                                $insert = array(
                                    'orderId' => $lastOrderId,
                                    'kotId' => $insertOrderKot['id'],
                                    'itemId' => $ucl->itemId,
                                    'itemName' => $ucl->itemName,
                                    'quantity' => $ucl->quantity,
                                    'suggestions' => $ucl->suggestions,
                                    'unitTypeId' => $ucl->unitTypeId,
                                    'unitTypeName' => $ucl->unitType,
                                    'amount' => $price,
                                    'unitPrice' => $unitPrice,
                                    'createdAt' => $this->entryDate,
                                    'updatedAt' => $this->entryDate,
                                );
                                $insertKotItems = OrderKotItem::create($insert);
                                if ($insertKotItems['id']) {
                                    $insertKotStatus = true;
                                }
                            }

                            //delete the cart items for this user as the data inserted in order table
                            if (isset($insertKotStatus)) {
                                Cart::where(['userId' => $userId])->delete(); //, 'orderType' => 'order'

                                $userId = $userId;
                                $orderId = $lastOrderId;

                                $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();

                                if (!empty($user)) {
                                    //notification for friend to whome we gifted order
                                    if (isset($friendId) && $friendId != '') {
                                        $message = 'You have a new gift from ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));

                                        $record = array('moduleType' => 'orderGifted', 'moduleId' => $orderId, 'description' => $message, 'userId' => $friendId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                        $not = UserNotification::create($record);

                                        if ($not['id']) {
                                            $deviceToken = CommonFunction::getUserDeviceData($friendId); //Get Device Tokens

                                            if ($deviceToken) {
                                                //Firebase Notification
                                                $payLoadData = array('moduleType' => 'orderGifted', 'moduleId' => $orderId, 'userId' => $friendId, 'title' => 'New gift recieved', 'msg' => $message);
                                                $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                                            }
                                        }
                                    }
                                    
                                    //notification for bar
                                    $message = 'New order recieved from ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));

                                    $record = array('moduleType' => 'order', 'moduleId' => $orderId, 'description' => $message, 'barId' => $barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                    $not = BarNotification::create($record);

                                    if ($not['id']) {
                                        $bar = CommonFunction::GetSingleField('bnBars', 'userId', 'id', $barId);
                                        $deviceToken = CommonFunction::getUserDeviceData($bar); //Get Device Tokens

                                        if ($deviceToken) {
                                            //Firebase Notification
                                            $payLoadData = array('moduleType' => 'orderPlaced', 'moduleId' => $orderId, 'barId' => $barId,'title' => 'New order placed', 'msg' => $message);
                                            $rs = CommonFunction::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'B');
                                        }
                                    }
                                }
                            }
                        }
                        $result = array('status' => 1, 'message' => "Order has been placed successfully", 'uniqueId' => $uniqueId);
                    } else {
                        $result = array('status' => 0, 'message' => "Somethig went wrong");
                    }
                } else {
                    $result = array('status' => 0, 'message' => "Cart is empty");
                }

            } 
            else {
                if ($visitData->status == 3) {
                    $result = array('status' => 0, 'message' => "Your visit is cancelled");
                } else if ($visitData->status == 4) {
                    $result = array('status' => 0, 'message' => "Your visit is completed");
                } else {
                    $result = array('status' => 0, 'message' => "Your visit is not confirmed");
                }
            }
        }
        else {
            //$result = $this->insertPreOrder();
            $result = array('status' => 2, 'message' => "Confirm your order");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Funciton to get all order of the user - Not in Use
    public function getOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get list of orders of user(status 1:-completed,2:running)
        $orderList = Order::select('bnOrders.id', 'br.Rating', 'bnOrders.barId', 'bnOrders.status', 'bnOrders.totalPrice', 'bnOrders.tableNo', 'bnOrders.grandTotal', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnOrders.cgst', 'bnOrders.cgstAmount', 'bnOrders.sgst', 'bnOrders.sgstAmount', 'bnOrders.serviceCharge', 'bnOrders.serviceChargeAmount', 'bnOrders.serviceChargeTax', 'bnOrders.serviceChargeTaxAmount', 'bnOrders.createdAt as orderDate', 'bnOrders.friendId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->orderBy('bnOrders.id', 'desc')
            ->where(['bnOrders.userId' => $userId, 'bnOrders.orderType' => 'order'])
            ->offset($startFrom)->limit($limit)
            ->get();
        if (count($orderList) > 0) {
            $result = array('status' => 1, 'message' => "Orders get successfully", 'data' => $orderList);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    } 

    // Functin to get current orders on visit - suborders
    public function getCurrentOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);
        $orderId = '';

        if (isset($id)) {
            $orderId = $id;
        }
        if (isset($visitId) && $visitId != '') {
            $Id = Order::select('bnOrders.id')
                ->where(['bnOrders.visitId' => $visitId, 'bnOrders.orderType' => 'order'])
                ->first();
            if (!empty($Id)) {
                $orderId = $Id->id;
            } else {
                $result = array('status' => 0, 'message' => "Visit not exist");
            }
        }

        $kotItemDetail = array();
        //get kot for orderId
        $kotDetail = OrderKot::select('id', 'kotNumber', 'friendId', 'status', 'createdAt', 'updatedAt')
            ->where('orderId', $orderId)->get();
        if (count($kotDetail) > 0) {
            foreach ($kotDetail as $kd) {

                $userName = "";
                if ($kd->friendId != 0) {
                    $userData = Patron::select('firstName', 'lastName', 'ProfilePic')
                        ->where(['userId' => $kd->friendId])
                        ->first();
                    $userName = $userData->firstName;
                }
                $kd['giftedTo'] = $userName;

                //status 1:pending, 2:ready, 3:collected, 4:cancelled

                //get list of kot items on kot id bases
                $kotDetails = OrderKotItem::select('id', 'kotId', 'itemId', 'itemName', 'quantity', 'unitTypeId', 'unitTypeName', 'amount')
                    ->where(['kotId' => $kd->id])
                    ->get()->toArray();
                $kd['items'] = $kotDetails;
                //Converting NULL to "" String
                array_walk_recursive($kd, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $kotItemDetail[] = $kd;

            }

            if ($kotDetail) {
                // get order Data of user
                $grandTotal = CommonFunction::GetSingleField('bnOrders', 'grandTotal', 'visitId', $visitId);
                if(empty($grandTotal))
                    $grandTotal = 0;

                //$kotItemDetail['grandTotal'] = $grandTotal;

                $result = array('status' => 1, 'message' => "Order detail get successfully", 'data' => $kotItemDetail, 'grandTotal' => $grandTotal);
            } else {
                $result = array('status' => 0, 'message' => "No record found");
            }
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //get listing of recievedgifts
    public function getRecievedGifts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            // 'orderType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get list of orders of user(status 1:-completed,2:running)
        $gifts = Order::select('bnOrders.id', 'bnOrders.orderId', 'br.Rating', 'bnOrders.barId', 'bnOrders.status', 'bnOrders.totalPrice', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnOrders.orderType', 'bnOrders.createdAt as orderDate', 'bnOrders.userId', 'bnOrders.friendId')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->orderBy('bnOrders.id', 'desc')
            ->orWhereRaw("find_in_set('$userId',bnOrders.friendId)")
            ->orWhere('bnOrders.userId', '=', $userId)
            ->offset($startFrom)->limit($limit)
            ->get();

        if (count($gifts) > 0) {
            foreach ($gifts as $gift) {
                if($gift->friendId != NULL && $gift->friendId != 0) {
                    $gift['order'] = array();
                    
                    // Get sender/receiver user info
                    if($userId == $gift->userId) {
                        $patronId = $gift->friendId;
                        $key = 'giftedto';
                        $gift['giftType'] = 'sent';
                    }
                    else {
                        $patronId = $gift->userId;
                        $key = 'giftedby';
                        $gift['giftType'] = 'receive';
                    }
                    
                    $userData = Patron::select('bnPatrons.userId', 'bnPatrons.firstName', 'bnPatrons.lastName', 'bnPatrons.ProfilePic', 'u.userName')
                            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnPatrons.userId')
                            ->where(['bnPatrons.userId' => $patronId])
                            ->first();
                    $gift['userDetail'] = $userData;

                    if ($gift->orderType == 'order') 
                    {
                        $orderGift = OrderKot::select('id', 'kotNumber', 'status', 'createdAt', 'updatedAt')
                            ->where(['friendId' => $userId, 'orderId' => $gift->id])->get();
                        
                        if (count($orderGift) > 0) 
                        {
                            //$og['giftedby'] = "";
                            $og['items'] = array();
                            $kot = array();
                            foreach ($orderGift as $og) {
                                // IN case of KOT Gift, pass the KOT Number in order ID
                                $gift->orderId = $og->kotNumber;

                                //get list of kot items on kot id bases
                                $kotDetails = OrderKotItem::select('id', 'kotId', 'itemId', 'itemName', 'quantity', 'amount') //'unitTypeId', 'unitTypeName',
                                    ->where(['kotId' => $og->id])
                                    ->get()->toArray();
                                
                                $gift['order'] = $kotDetails;
                            }
                        }
                    } 
                    else if ($gift->orderType == 'preorder') {
                        //get list of orders of user
                        $preOrderDetail = PreOrderDetail::select('id', 'orderId', 'itemId', 'itemName', 'quantity', 'amount', 'createdAt', 'updatedAt') //'unitTypeId', 'unitTypeName',
                            ->where(['orderId' => $gift->id])
                            ->get()->toArray();

                        $gift['order'] = $preOrderDetail;
                    }
                    $data[] = $gift;
                }
            }
        }

        if (!empty($data)) {
            $result = array('status' => 1, 'message' => "Gift items get successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Function to get order receipt with items and pricing
    public function getOrderReceipt(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'barId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => $validator->errors());
            return response()->json($result);
        }
        $order = Order::select('bnOrders.id', 'bnOrders.barId', 'br.Rating', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnOrders.userId', 'bnOrders.orderType', 'bnOrders.visitId', 'bnOrders.orderDate', 'bnOrders.totalPrice', 'bnOrders.cgst', 'bnOrders.cgstAmount', 'bnOrders.sgst', 'bnOrders.sgstAmount', 'bnOrders.serviceCharge', 'bnOrders.serviceChargeAmount', 'bnOrders.serviceChargeTax', 'bnOrders.serviceChargeTaxAmount', 'bnOrders.grandTotal', 'bnOrders.createdAt', 'bnOrders.paymentStatus', 'p.firstName', 'p.lastName', 'u.userName', 'v.requestId', 'v.requestType') //'bt.tokenMoney'
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnOrders.userId')
            ->leftJoin('bnUsers as u', 'u.id', '=', 'bnOrders.userId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            //->leftJoin('bnBookTable as bt', 'bt.id', '=', 'v.requestId')
            ->where(['bnOrders.orderId' => $request->orderId, 'bnOrders.barId' => $request->barId])
            ->first();

        if ($order) 
        {
            // Get the Wallet Balance of ther user
            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $order->userId);
            $order['walletAmt'] = ($walletAmt) ? $walletAmt : 0;

            if ($order->orderType == 'order') {
                //get list of kot items on order id bases, by coverting string to integer
                $sql = "SELECT i.id, i.itemId, i.kotId, i.itemName, CAST(SUM(i.quantity) as UNSIGNED) as quantity, SUM(i.amount) AS totalAmount, i.unitPrice FROM bnOrderKotItems i inner join bnOrderKots ok on ok.id = i.kotId WHERE i.orderId = $order->id AND (ok.status = 2 OR ok.status = 3) GROUP BY i.itemId, i.unitPrice";

                $orderDetail = DB::select(DB::raw($sql));
                if ($orderDetail) {
                    $order['orderItems'] = $orderDetail;
                    $data = $order;
                }
            } 
            else if ($order->orderType == 'preorder') {
                //get list of order items
                $preOrderDetails = "SELECT id, itemId, itemName, CAST(SUM(quantity) as UNSIGNED) AS totalQty, SUM(amount) AS totalAmount FROM bnPreOrderDetails WHERE orderId = $order->id GROUP BY itemId";
                $preOrderDetail = DB::select(DB::raw($preOrderDetails));

                if ($preOrderDetail) {
                    $order['orderItems'] = $preOrderDetail;
                }
            }

            // Get the advance money, if available
            $advancePayment = (object) array();
            if($order->requestType == 'table') {
                $tokenMoney = CommonFunction::GetSingleField('bnBookTable', 'tokenMoney', 'id', $order->requestId);

                if($tokenMoney) {
                    /* Update the payment status to paid in order table, 
                     * if grand total is less than to token money
                     */
                    $payableAmount = $dueAmt = $order->grandTotal - $tokenMoney;
                    if($dueAmt <= 0){
                        Order::where(['id' => $order->id])->update(['paymentStatus' => 1, 'updatedAt' => $this->entryDate]);
                        $order->paymentStatus = 1;
                        $payableAmount = 0;
                    }

                    $advancePayment = array(
                        'paidFor' => 'table',
                        'labelToShow' => 'Token Money',
                        'amount' => $tokenMoney,
                        'payableAmount' => $payableAmount,
                    );
                }
            }
            else if($order->requestType == 'event') {
                $event = EventBooking::select('bnEventBookings.totalSeats', 'e.coverCharges')
                    ->leftJoin('bnSpecialEvents as e', 'e.id', '=', 'bnEventBookings.eventId')
                    ->where(['bnEventBookings.id' => $order->requestId])
                    ->where('e.coverCharges', '>', 0)
                    ->first();
                if(!empty($event)) {
                    $coverCharges = $event->totalSeats * $event->coverCharges;

                    /* Update the payment status to paid in order table, 
                     * if grand total is less than to Cover Charges
                     */
                    
                    $payableAmount = $dueAmt = $order->grandTotal - $coverCharges;
                    if($dueAmt <= 0){
                        Order::where(['id' => $order->id])->update(['paymentStatus' => 1, 'updatedAt' => $this->entryDate]);
                        $order->paymentStatus = 1;
                        $payableAmount = 0;
                    }
                    $advancePayment = array(
                        'paidFor' => 'event',
                        'labelToShow' => 'Cover Charges',
                        'amount' => $coverCharges,
                        'payableAmount' => $payableAmount,
                    );
                }
            }

            $order['advancePayment'] = $advancePayment;
            $data = $order;

            array_walk_recursive($data, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });

            $result = array('status' => 1, 'message' => 'Order details get successfully', 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => 'No record found');
        }

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Funciton to get all order/preorders of the user - History
    public function getMyOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'orderType' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        //Pagination Condition of startfrom and no of records
        //if limit is not set then default limit is 10
        if (!isset($limit)) {
            $limit = 10;
        }
        //if startFrom is not set then default startFrom is 0
        if (!isset($startFrom)) {
            $startFrom = 0;
        }

        //get list of orders of user(status 1:-completed,2:running)
        $OrderList = Order::select('bnOrders.id', 'bnOrders.orderId', 'bnOrders.orderType', 'br.Rating', 'bnOrders.barId', 'bnOrders.status', 'bnOrders.tableNo', 'bnOrders.totalPrice', 'bnOrders.grandTotal', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnOrders.cgst', 'bnOrders.cgstAmount', 'bnOrders.sgst', 'bnOrders.sgstAmount', 'bnOrders.serviceCharge', 'bnOrders.serviceChargeAmount', 'bnOrders.serviceChargeTax', 'bnOrders.serviceChargeTaxAmount', 'bnOrders.createdAt as orderDate', 'bnOrders.friendId', 'bt.tokenMoney')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnOrders.barId')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
            ->leftJoin('bnBarRatings as br', 'br.barId', '=', 'b.id')
            ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
            ->leftJoin('bnBookTable as bt', 'bt.id', '=', 'v.requestId')
            ->orderBy('bnOrders.id', 'desc')
            ->where(['bnOrders.userId' => $userId, 'bnOrders.orderType' => strtolower($orderType)])
            ->where(['bnOrders.paymentStatus' => 1])
            ->offset($startFrom)->limit($limit)
            ->get();

        if (count($OrderList) > 0) {
            foreach ($OrderList as $ol) {
                if ($ol->orderType == 'preorder') 
                {
                    //get list of orders of user
                    /*$sqlPre = "SELECT id, itemId, itemName, SUM(quantity) AS quantity, unitPrice AS amount FROM bnPreOrderDetails WHERE orderId = $ol->id GROUP BY itemId, unitPrice";*/

                    $sqlPre = "SELECT id, itemId, itemName, CAST(SUM(quantity) as UNSIGNED) AS quantity, unitPrice AS amount, SUM(amount) AS totalAmount FROM bnPreOrderDetails WHERE orderId = $ol->id GROUP BY itemId, unitPrice";

                    $orderDetailsPre = DB::select(DB::raw($sqlPre));

                    $ol['items'] = $orderDetailsPre;

                    // $ol['items'] = PreOrderDetail::select('id', 'itemId', 'itemName', 'quantity', 'amount')
                    //     ->where(['orderId' => $ol->id])
                    //     ->get()->toArray();

                    $userData = array();
                    if ($ol->friendId != 0) {
                        $userData = Patron::select('firstName', 'lastName', 'ProfilePic')
                            ->where(['userId' => $ol->friendId])
                            ->get();
                    }
                    $ol['giftedTo'] = $userData;
                }

                if ($ol->orderType == 'order') {
                    $frienddata = array();
                    $userkotData = array();

                    if ($ol->friendId != 0) {
                        $friendId = explode(',', $ol->friendId);
                        foreach ($friendId as $fi) {
                            $userData = Patron::select('firstName', 'lastName', 'ProfilePic')
                                ->where(['userId' => $fi])
                                ->get();
                        }
                        $ol['giftedTo'] = $userData;
                    }

                    $sql = "SELECT i.id, i.itemId, i.itemName, CAST(SUM(i.quantity) as UNSIGNED) as quantity, i.unitPrice AS amount, SUM(i.amount) AS totalAmount FROM bnOrderKotItems i inner join bnOrderKots ok on ok.id = i.kotId WHERE i.orderId = $ol->id AND (ok.status = 2 OR ok.status = 3) GROUP BY i.itemId, i.unitPrice";

                    $orderDetail = DB::select(DB::raw($sql));
                    if ($orderDetail) {
                        /*$allItems = array();
                        $odetail = array();

                        foreach ($orderDetail as $od) {

                        if (count($allItems) > 0) {
                        $qty = 0;
                        foreach ($allItems as $key => $ai) {
                        // echo "\n" . $key;
                        if ($ai['itemId'] == $od->itemId && $ai['amount'] == $od->amount) {
                        $qty = $od->quantity + $ai['qty'];
                        // unset($allItems[$key]);
                        $itemdata = array('itemId' => $od->itemId, 'qty' => $qty, 'amount' => $od->amount);
                        $allItems[] = $itemdata;

                        } else {
                        $itemdata = array('itemId' => $od->itemId, 'qty' => $od->quantity, 'amount' => $od->amount);
                        $allData[] = $itemdata;
                        }
                        }
                        $data = array_merge($allData, $allItems);
                        } else {
                        $itemdata = array('itemId' => $od->itemId, 'qty' => $od->quantity, 'amount' => $od->amount);
                        $allItems[] = $itemdata;
                        }
                        }*/
                        // print_r($allData);

                        // exit;
                        $ol['items'] = $orderDetail;
                    }

                }

                unset($ol['friendId']);

                array_walk_recursive($ol, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $data[] = $ol;
            }
            $result = array('status' => 1, 'message' => "Orders get successfully", 'data' => $data);
        } else {
            $result = array('status' => 0, 'message' => "No record found");
        }
        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    // Update Order Payment Status Function - to close order
    public function updateOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'visitId' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }

        extract($_POST);

        $record = array('status' => 4, 'updatedAt' => $this->entryDate);
        $update = Visit::where(['id' => $visitId])->update($record);

        //$record = array('paymentStatus' => 1, 'updatedAt' => $this->entryDate);
        $record = array('paymentStatus' => 0, 'status' => 3, 'updatedAt' => $this->entryDate);
        $update = Order::where(['visitId' => $visitId])->update($record);

        //Delete entry from geo tagging
        DB::table('bnGeoTaggings')->where(['visitId' => $visitId])->delete();

        // get order Data of user
        $orderData = Order::select('id', 'orderId as uniqueId')->where(['visitId' => $visitId])->first();
        $result = array('status' => 1, 'message' => "Status has been successfully updated",
            'data'=>$orderData);

        $result['app_version'] = CommonFunction::appVersion();
        return response()->json($result);
    }

    //Update Transaction table 20-02-20120 by ravindra
    public function updateOrderPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'barId' => 'required',
            'orderId' => 'required',
            //'transactionId' => 'required',
            'paymentMode' => 'required', //|in:cash,online,card,wallet
            'amount' => 'required',
            'amountWithoutTax' => 'required',
        ]);
        if ($validator->fails()) {
            $result = array('status' => 0, 'message' => 'Validation error occurred');
            return response()->json($result);
        }
        extract($_POST);
        $response = (object) array();
        $status = false;
        //$uniqueId = CommonFunction::GetSingleField('bnOrders', 'orderId', 'id', $orderId);
        $orderData = Order::select('bnOrders.orderId', 'bnOrders.paymentStatus') 
            ->where(['bnOrders.id' => $orderId])
            ->first();

        if ($orderData) {
            if($orderData->paymentStatus == 0) {
                $uniqueId = $orderData->orderId;
                $orderDesc = 'Paid for order '.$uniqueId;

                /* === If all payment paid through BN Wallet === */
                if(isset($walletMoney) && $walletMoney > 0 && $walletMoney >= $amount) {
                    $param = array(
                        'barId' => $barId,
                        'userId' => $userId,
                        'moduleId' => $orderId,
                        'moduleType' => 'order',
                        'paymentMode' => 'bnwallet',
                        'description' => $orderDesc,
                        'amount'    => $amount,
                        'totalPrice' => $amountWithoutTax,
                        'paymentDate' => date('Y-m-d'),
                        'status' => 1,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate
                    );

                    $transaction = Transaction::create($param);
                    if($transaction){
                        // Update the order payment status
                        $param = array('paymentStatus' => 1);
                        Order::where('id', $orderId)->update($param);
                        
                        // Decrease the wallet balance
                        Wallet::where('userId',$userId)->decrement('amount', $amount);

                        /* === Transaction in wallet table === */
                        $param = array('userId' => $userId, 'description' => $orderDesc, 'amount' => $amount, 'moduleId' => $orderId, 'moduleType' => 'order', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        DB::table('bnWalletTransactions')->insertGetId($param); 

                        $status = true;
                    }
                }
                /* === If wallet money is less than to final amount === */
                else 
                {
                    /* === If wallet money is used, then make a transaction in wallet table === */
                    if(isset($walletMoney) && $walletMoney > 0) {
                        // Decrease the wallet balance
                        Wallet::where('userId',$userId)->decrement('amount', $walletMoney);

                        $param = array('userId' => $userId, 'description' => $orderDesc, 'amount' => $walletMoney, 'moduleId' => $orderId, 'moduleType' => 'order', 'type' => 'paid', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        DB::table('bnWalletTransactions')->insertGetId($param);
                        
                        // Wallet total price calculation;
                        $ratio = ($walletMoney/$amount)*100;
                        $ratioAmt = ($walletMoney/100)*$ratio;
                        $walletTotalAmount = $walletMoney-$ratioAmt;

                        $param = array(
                            'barId' => $barId,
                            'userId' => $userId,
                            'moduleId' => $orderId,
                            'moduleType' => 'order',
                            'paymentMode' => 'bnwallet',
                            'description' => $orderDesc,
                            'amount'    => $walletMoney,
                            'totalPrice' => $walletTotalAmount,
                            'paymentDate' => date('Y-m-d'),
                            'status' => 1,
                            'createdAt' => $this->entryDate,
                            'updatedAt' => $this->entryDate
                        );
                        $transaction = Transaction::create($param);

                        // Reset the Amount and Amount without Tax, to continue processing
                        $amount = $amount - $walletMoney;
                        $amountWithoutTax = $amountWithoutTax - $walletTotalAmount;
                    }

                    /* === Now transaction entry based on other payment modes === */
                    $param = array(
                        'barId' => $barId,
                        'userId' => $userId,
                        'moduleId' => $orderId,
                        'moduleType' => 'order',
                        'transactionId' => $transactionId,
                        'paymentMode' => $paymentMode,
                        'description' => $orderDesc,
                        'amount'    => $amount,
                        'totalPrice' => $amountWithoutTax,
                        'paymentDate' => date('Y-m-d'),
                        'status' => 1,
                        'createdAt' => $this->entryDate,
                        'updatedAt' => $this->entryDate
                    );
                    
                    $transaction = Transaction::create($param);
                    if($transaction){
                        // Update the order payment status
                        $param = array('paymentStatus' => 1);
                        Order::where('id', $orderId)->update($param);
                        
                        /* === Add the amount in user wallet if available === */
                        $appSettings = CommonFunction::getAppSettings();
                        $cashBackOn = $appSettings['cashBackOn'];
                        if($cashBackOn > 0 && $amount >= $cashBackOn) {

                            $thousandCount = (int)($amount/$cashBackOn);
                            $cashBackAmount = $appSettings['cashBackAmount'] * $thousandCount;

                            //Update the wallet amount or Insert
                            $walletAmt = CommonFunction::GetSingleField('bnWallets', 'amount', 'userId', $userId);
                            if(!empty($walletAmt) || $walletAmt === 0.00) { //bcz 0 can be an amount
                                // Update the amount
                                $param = array('amount' => $walletAmt + $cashBackAmount,'updatedAt' => $this->entryDate);
                                $wStatus = DB::table('bnWallets')
                                    ->where(['userId' => $userId])
                                    ->update($param);
                            }
                            else {
                                $param = array('userId' => $userId, 'amount' => $cashBackAmount, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                $wStatus = DB::table('bnWallets')->insertGetId($param);
                            }

                            // Create entry in wallet transaction
                            if(isset($wStatus)) {
                                $param = array('userId' => $userId, 'description' => "Cashback Received", 'amount' => $cashBackAmount, 'moduleId' => $orderId, 'moduleType' => 'order', 'type' => 'received', 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                                DB::table('bnWalletTransactions')->insertGetId($param);

                                $response = array(
                                    'cashBackReceived' => 1,
                                    'cashBackAmount' => $cashBackAmount,
                                );
                            }
                        } 

                        $status = true;
                    }
                }

                if($status)
                {
                    //Create an entry in notification table for the Bar
                    $order = Order::select('bnOrders.orderId', 'p.firstName', 'p.lastName', 'p.userId', 'v.sittingType', 'v.tableStandingNo')
                            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnOrders.userId')
                            ->leftJoin('bnVisits as v', 'v.id', '=', 'bnOrders.visitId')
                            ->where(['bnOrders.id' => $orderId])
                            ->first();
                    if (!empty($order)) {
                        
                        $message = 'Payment for Order ID ' .$order->orderId." paid by ". ucwords(strtolower($order->firstName . ' ' . $order->order));
                        
                        $record = array('moduleType' => 'orderPayment', 'moduleId' => $orderId, 'description' => $message, 'barId' => $barId, 'createdAt' => $this->entryDate, 'updatedAt' => $this->entryDate);
                        $not = BarNotification::create($record);
                    }

                    $result = array('status' => 1, 'message' => 'Payment Updated successfully', 'data' => $response);
                }
                else{
                    $result = array('status' => 0, 'message' => 'Somethig went wrong, Please try again.');
                }
            }
            else{
                $result = array('status' => 0, 'message' => 'Order payment is already done');
            }
        }
        else{
            $result = array('status' => 0, 'message' => 'Invalid Order');
        }
        return response()->json($result);
    }

}