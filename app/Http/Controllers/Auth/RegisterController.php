<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\VerifyUser;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    // Signup - Email verificaiton function
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->email_verified_at) {
                $verifyUser->user->email_verified_at = date('Y-m-d H:i:s');
                $verifyUser->user->status = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }

        } else {
            $status = "Sorry your email cannot be identified.";
        }

        $data['status'] = $status;
        return view('auth.emailVerified', $data);
        //return redirect('/login')->with('status', $status);
    }

    // Email Update - Email verificaiton function
    public function verifyEmailUpdate($token)
    {
        $row = DB::table('bnEmailUpdates as e')->select('e.*')
            ->where(['e.token' => $token])->first();
        
        if (!empty($row)) {
            
            //Check Email is already registerd or not
            $query = DB::table('bnUsers')
                ->select('id')
                ->where(['email' => $row->email, 'userType' => 'U'])
                ->where('id', '!=', $row->userId)
                ->first();
            
            if ($query) {
                $status = 0;
                $message = "Requested Email is already registerd with other users.";
            }
            else {
                $param = array('email' => $row->email, 'updatedAt' => date("Y-m-d H:i:s"));
                $update = User::where('id', $row->userId)->update($param);

                $message = "Your Email has been verified successfully. New email <b>".$row->email."</b> is now linked with you profile.";
                $status = 1;
            }
        } 
        else {
            $status = 0;
            $message = "The link you clicked may be broken or the page may have been removed.";
        }

        $data['status'] = $status;
        $data['message'] = $message;
        return view('auth.emailUpdateVerified', $data);
    }

}