<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarRating extends Model
{
    public $table = 'bnBarRatings';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'rating', 'totalUsers', 'createdAt', 'updatedAt',
    ];
}