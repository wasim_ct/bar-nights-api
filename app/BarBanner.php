<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BarBanner extends Model
{
    public $table = 'bnBarBanners';
    public $timestamps = false;

    protected $fillable = [
        'id', 'barId', 'mediaId', 'mediaName', 'mediaType', 'createdAt', 'updatedAt', 'displayOrder',
    ];
}