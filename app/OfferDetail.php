<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferDetail extends Model
{
    public $table = 'bnOfferDetails';
    public $timestamps = false;

    protected $fillable = [
        'id', 'offerType', 'barId', 'title', 'description', 'offerImage', 'templateId', 'status', 'createdAt', 'updatedAt',
    ];
}