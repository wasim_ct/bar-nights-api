<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotMail extends Mailable
{
    use Queueable, SerializesModels;

    public $param;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($param)
    {
        $this->data = $param;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->data['subject'])) {
            $subject = $this->data['subject'];
        } else {
            $subject = "Bar Nights";
        }

        return $this->from('noreply@barnightslive.com')->subject($subject)->view('emails.passwordForgot')->with('data', $this->data);
    }
}
