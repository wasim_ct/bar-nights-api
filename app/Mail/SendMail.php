<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->data['subject'])) {
            $subject = $this->data['subject'];
        } else {
            $subject = "BarNights";
        }

        return $this->from('wasim@coretechies.com')->subject($subject)->view('emails.dailyReport')->with('data', $this->data);
    }
}
