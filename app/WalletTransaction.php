<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    public $table = 'bnWalletTransactions';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'moduleId', 'moduleType', 'amount', 'status', 'createdAt', 'updatedAt',
    ];
}