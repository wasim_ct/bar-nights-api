<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EventBooking extends Model
{
    public $table = 'bnEventBookings';
    public $timestamps = false;

    protected $fillable = [
        'id', 'eventId', 'userId', 'totalSeats', 'paymentStatus', 'amount', 'code', 'codeAvailed', 'createdAt', 'updatedAt',
    ];

    public static function getEventListByUserId($userId){
    	$result= DB::table('bnEventBookings as eventBooking')
        ->select('bar.barName', 'event.title as eventTitle', 'event.entryFees', 'event.startDate', 'event.endDate', 'event.startTime', 'event.endTime', 'eventBooking.totalSeats', 'eventBooking.amount')
        ->leftJoin('bnSpecialEvents as event', 'event.id', '=', 'eventBooking.eventId')
        ->leftJoin('bnBars as bar', 'bar.id', '=', 'event.barId')
        ->where('eventBooking.userId', $userId)
        ->get();
        return $result;
    }
}