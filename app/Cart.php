<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $table = 'bnCarts';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'itemId', 'quantity', 'unitTypeId', 'orderType', 'createdAt', 'updatedAt', 'suggestions',
    ];
}