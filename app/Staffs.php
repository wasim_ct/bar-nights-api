<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Staffs extends Model
{
    public $table = 'bnStaffs';
    public $timestamps = false;

    protected $fillable = [
        'id', 'name', 'userId', 'profilePic',
    ];
}