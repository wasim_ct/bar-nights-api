<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportEnquire extends Model
{
    public $table = 'bnSupportEnquires';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'email', 'subject', 'message', 'createdAt', 'updatedAt',
    ];
}
