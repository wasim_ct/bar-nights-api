<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountOfferItem extends Model
{
    public $table = 'bnDiscountOfferItems';
    public $timestamps = false;

    protected $fillable = [
        'id', 'offerId', 'itemId', 'discountRate', 'createdAt', 'updatedAt',
    ];
}