<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    public $table = 'bnUserDevices';
    public $timestamps = false;
    
    protected $fillable = [
        'userId', 'deviceId', 'deviceToken', 'deviceType', 'modelName', 'osVersion', 'osType', 'ipAddress', 'loginStatus', 'latitude', 'longitude', 'createdAt', 'updatedAt'
    ];
}
