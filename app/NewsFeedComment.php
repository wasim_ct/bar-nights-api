<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsFeedComment extends Model
{
    public $table = 'bnNewsFeedComments';
    public $timestamps = false;

    protected $fillable = [
        'id', 'newsId', 'userId', 'comment', 'createdAt', 'updatedAt',
    ];
}