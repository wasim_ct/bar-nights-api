<?php
use App\BarTiming;
use App\Cart;
use App\Patron;
use App\DiscountOffer;
use App\DiscountOfferItem;
use App\HappyHourItem;
use App\HappyHoursTiming;
use App\Order;
use App\OrderKot;
use App\TopBar;
use App\UserDevice;
use App\Visit;
use App\VisitCode;
use App\BookTable;
use App\EventBooking;
use App\OrderKotItem;
use App\BarNotification;
use App\BarReview;
use App\Bar;
use App\UserNotification;
use App\User;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class CommonFunction
{
    private $entryDate;
    public function __construct()
    {
        $this->entryDate = date("Y-m-d H:i:s");
    }

    //User Version Function
    public static function appVersion()
    {
        $versions = array('android' => '1.0', 'ios' => '1.0', 'barAndroid' => '1.1', 'barIos' => '1.8');
        return $versions;
    }

    // License Types of Bar
    public static function licenseTypes()
    {
        $license = array(
            '1' => 'Pan Card',
            '2' => 'GST Registration',
            '3' => 'FSSAI License',
            '4' => 'FL3 License',
            '5' => 'Eating House',
            '6' => 'Fire NOC',
        );
        return $license;
    }
 
    // Item Categories - Parent Cats
    public static function getItemCategories()
    {
        /*$cats = array(
            '1' => 'Drink',
            '2' => 'Food',
        );*/

        $query = DB::table('bnItemCategories')
            ->select('id', 'catName')
            ->where(['parentId' => 0, 'status' => 1])
            ->get()->toArray();

        $cats = array();
        if(count($query)) {
            foreach ($query as $row) {
                $cats[$row->id] = $row->catName;
            }
        } 
        else{
            $cats = array(
                '1' => 'Alcoholic Beverages',
                '2' => 'Non-alcoholic Beverages',
                '3' => 'Appetizers',
                '4' => 'Maines',
            );
        }

        return $cats;
    }

    //Used to get all settings of the app like Commission, Wallet Limit etc.
    public static function getAppSettings($value = null, $field = null, $userType = null)
    {
        $query = DB::table('bnAppSettings')
            ->select('commission', 'cashBackOn', 'cashBackAmount', 'eventCommission', 'taxOnCommission', 'barOneCommission', 'barTwoCommission', 'barThreeCommission', 'topBarLimit', 'topSpenderMinLimit', 'bookingTokenMoney', 'cgst', 'sgst', 'serviceCharge', 'serviceChargeTax')
            ->first();
        $settings = array();
        if (!empty($query)) {
            $settings = array(
                'cashBackOn' => $query->cashBackOn,
                'cashBackAmount' => $query->cashBackAmount,
                'commission' => $query->commission,
                'taxOnCommission' => $query->taxOnCommission,
                'topper1Comm' => $query->barOneCommission,
                'topper2Comm' => $query->barTwoCommission,
                'topper3Comm' => $query->barThreeCommission,
                'eventCommission' => $query->eventCommission,
                'topBarLimit' => $query->topBarLimit,
                'topSpenderMinLimit' => $query->topSpenderMinLimit,
                'bookingTokenMoney' => $query->bookingTokenMoney,
                'cgst' => $query->cgst,
                'sgst' => $query->sgst,
                'serviceCharge' => $query->serviceCharge,
                'serviceChargeTax' => $query->serviceChargeTax,
            );
        }
        return $settings;
    }

    //Check Phone No/Email/userName is registered or not with user type
    public static function checkValueExist($value = null, $field = null, $userType = null)
    {
        $row = DB::table('bnUsers')
            ->select('id', 'status')
            ->where([$field => $value, 'userType' => $userType])
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    } 

    //This Function is used to update user device data in login & Social media login
    public static function updateUserDevice($deviceData, $userId)
    {
        if ($deviceData && $userId) {
            extract($deviceData);
            //Check deviceId is already registerd or not with the User ID
            $query = UserDevice::select('id')->where(['deviceToken' => $deviceToken])->get(); 
            //, 'userId' => $userId

            if (count($query) > 0) {
                //$deviceData['updatedAt'] = date('Y-m-d H:i:s');
                //$update = UserDevice::where(['deviceId' => $deviceId, 'userId' => $userId])->update($deviceData);

                DB::table('bnUserDevices')->where(['deviceToken' => $deviceToken])->delete();
            } 
            $deviceData['createdAt'] = date('Y-m-d H:i:s');
            $deviceData['updatedAt'] = date('Y-m-d H:i:s');
            $deviceData['userId'] = $userId;
            $deviceData['deviceType'] = 1; // 1=Phone & 2=Web Browser
            UserDevice::create($deviceData);

            // Create entry in bnAppDownloads table
            if(isset($osType)) {
            $exist = DB::table('bnAppDownloads')
                ->select('id')->where(['deviceId' => $deviceId, 'osType' => $osType])->first();
                if(empty($exist)) {
                    $param = array('deviceId' => $deviceId, 'osType' => $osType, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
                    DB::table('bnAppDownloads')->insertGetId($param);
                }
            }
        }
    }

    // Function is used to update app downloads
    public static function updateAppDownloads($osType = null, $deviceId = null)
    {        
        $exist = DB::table('bnAppDownloads')
            ->select('id')->where(['deviceId' => $deviceId, 'osType' => $osType])->first();
        if(empty($exist)) {
            $param = array('deviceId' => $deviceId, 'osType' => $osType, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            DB::table('bnAppDownloads')->insertGetId($param);
        }
    }

    //Function to get all states
    public static function getSpecialEventFeeSetup()
    {
        return $seFee = DB::table('bnSpecialEventFeeSetup')
            ->select('price', 'duration')
            ->get()->toArray();
    }

    //Function to get all states
    public static function getAllStates()
    {
        return $states = DB::table('bnStates')
            ->select('state_id', 'state_name')
            ->orderBy('state_name', 'asc')
            ->get()->toArray();
    }

    //Function to get all Cities
    public static function getAllCities($stateId)
    {
        return $cities = DB::table('bnCities')
            ->select('city_id', 'city_name')
            ->where('state_id', $stateId)
            ->orderBy('city_name', 'asc')
            ->get()->toArray();
    }

    //This funciton is used to pass column name in views blade files
    public static function GetSingleField($table, $select, $field, $value)
    {
        $result = DB::table($table)->where([$field => $value])->select($select)->first();
        if (!empty($result)) {
            return $result->$select;
        } else {
            return '';
        }
    }

    // Function is used to create login token 
    public static function createLoginToken($userId)
    {
        $length = 30;
        $token = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);

        $param = array('userId' => $userId, 'token' => $token, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
        $query = DB::table('bnLoginTokens')->insertGetId($param);
        
        return ($query) ? $token : false;
    }

    // Function is used to check user status
    public static function isUserBlocked($userId = NULL)
    {  
        if($userId != NULL) {
            $result = array();
            $block = DB::table('bnUsers')->select('id')
                ->where(['id' => $userId, 'status' => 2])->first();
            if(!empty($block)) {
                $contactEmail = self::GetSingleField('bnAppSettings', 'contactEmail', 'id', 1);
                $message = "Your session should be logged out. Please contact on ".$contactEmail." for more information.";

                $result = array('status' => 100, 'message' => $message);   
            }
            return $result;
        }
    }

    //This funciton is used to get single row
    public static function GetSingleRow($table, $field, $value)
    {
        return DB::table($table)->where([$field => $value])->first();
    }

    //This funciton is used to get images path
    public static function getMediaPath()
    {
        return array(
            'barLogoPath' => asset('uploads/bars/logo/'),
            'barBannerPath' => asset('uploads/bars/banners/'),
            'barImgPath' => asset('uploads/bars/images/'),
            'barVideoPath' => asset('uploads/bars/videos/'),
            'itemImgPath' => asset('uploads/bars/items/'),
            'catImgPath' => asset('uploads/bars/categories/'),
            'userPicPath' => asset('uploads/patrons/profile/'),
            'userImgPath' => asset('uploads/bars/images/'),
            'userVideoPath' => asset('uploads/bars/videos/'),
            'userNewFeedPath' => asset('uploads/patrons/newsfeed/'),
            'barKycDocPath' => asset('uploads/bars/kycdoc/'),
            'offerImgPath' => asset('uploads/bars/offers/'),
            'eventImgPath' => asset('uploads/events/'),
            'templateImgPath' => asset('uploads/templates/'),
            'storyImgPath' => asset('uploads/patrons/story/'),
        );
    }

    //generate unique code for Visit;
    public static function generateCode()
    {
        $value = substr(str_shuffle('1234567980'), 0, 4);
        $code = 'BN' . date('d/m/' . $value);
        //check that the code is alerady exist or not in visitCode table
        $codeExist = VisitCode::select('id')->where('code', $code)->get();
        if (count($codeExist) > 0) {
            $code = self::generateCode();
            return $code;
        } else {
            return $code;
        }
    }
    //generate Kot;
    public static function generateKot()
    {
        $value = substr(str_shuffle('1234567980'), 0, 4);
        $kotNumber = 'BN' . $value;
        //check that the code is alerady exist or not in visitCode table
        $kotExist = OrderKot::select('id')->where('kotNumber', $kotNumber)->get();
        if (count($kotExist) > 0) {
            $kotNumber = self::generateKot();
            return $kotNumber;
        } else {
            return $kotNumber;
        }
    }

    // Functin to generate unique code to table booking
    public static function generateBookingCode()
    {
        $value = substr(str_shuffle('1234567980'), 0, 3);
        $code = 'BNT'. $value;
        
        $codeExist = BookTable::select('id')->where('code', $code)->get();
        if (count($codeExist) > 0) {
            $code = self::generateBookingCode();
            return $code;
        } else {
            return $code;
        }
    }

    // Functin to generate unique code to event booking
    public static function generateEventCode()
    {
        $value = substr(str_shuffle('1234567980'), 0, 3);
        $code = 'BNE'. $value;
        
        $codeExist = EventBooking::select('id')->where('code', $code)->get();
        if (count($codeExist) > 0) {
            $code = self::generateEventCode();
            return $code;
        } else {
            return $code;
        }
    }

    //this function is used to check the discount offer exist or not with the bar
    public static function checkBarOffers($barId = null, $date = null)
    {
        $currentTime = date('H:i:s');
        $data = DiscountOffer::select('bnDiscountOffers.id', 'od.offerImage', 'bnDiscountOffers.appliedOn', 'bnDiscountOffers.discountRate', 'bnDiscountOffers.startTime', 'bnDiscountOffers.endTime')
            ->join('bnOfferDetails as od', 'od.id', '=', 'bnDiscountOffers.odId')
            ->where(['od.barId' => $barId, 'od.status' => 1])
            ->where('bnDiscountOffers.startDate', '=', $date)
            ->where('bnDiscountOffers.endDate', '=', $date)
            ->where('bnDiscountOffers.startTime', '<=', $currentTime)
            ->where('bnDiscountOffers.endTime', '>=', $currentTime)
            ->first();
        return $data;
    }

    //this function is used to check the happy hour for current time in a BAR
    public static function checkBarHappyHour($barId = null)
    {
        $today = strtolower(date('l'));
        $currentTime = date('H:i:s');

        $data = HappyHoursTiming::select('bnHappyHourTiming.id', 'od.offerImage', 'bnHappyHourTiming.startTime', 'bnHappyHourTiming.endTime', 'bnHappyHourTiming.appliedOn', 'bnHappyHourTiming.discountRate', 'bnHappyHourTiming.discountType', 'bnHappyHourTiming.free', 'bnHappyHourTiming.buy')
            ->join('bnOfferDetails as od', 'od.id', '=', 'bnHappyHourTiming.odId')
            ->where(['od.barId' => $barId, 'od.status' => 1])
            ->whereRaw("find_in_set('$today',bnHappyHourTiming.weekDay)")
            ->where('bnHappyHourTiming.startTime', '<=', $currentTime)
            ->where('bnHappyHourTiming.endTime', '>=', $currentTime)
            ->first();
        return $data;
    }

    //This funciton is used to update the calculated bar rating
    public static function updateBarRating($barId)
    {
        $reviews = DB::table('bnBarReviews')
            ->select("id", "rating")
            ->where('barId', '=', $barId)
            ->get()->toArray();

        if (!empty($reviews)) {
            $totalRating = 0;
            foreach ($reviews as $re) {
                $totalRating = $totalRating + $re->rating;
            }

            return $totalRating / count($reviews);
        }
    }

    //This funciton is used to count total seats alloted to event
    public static function countBookedSeats($eventId)
    {
        //get summ of seets bvooked for event till now
        $BookedSeats = DB::table('bnEventBookings')
            ->select("id", "totalSeats")
            ->where('eventId', '=', $eventId)
            ->sum('totalSeats');

        if (!empty($BookedSeats)) {
            return $BookedSeats;
        }
    }

    //This funciton is used to get Discounted price of an item
    public static function getDiscountedPrice($discountData = array(), $itemId = null, $price = null)
    {
        if (!empty($discountData) && !empty($itemId) && !empty($price)) {
            $returnData = array();
            $currentTime = strtotime(date('H:i:s'));
            $dd = $discountData;
            $returnData['discount'] = 0;
            $returnData['discountPrice'] = 0;

            //Comparing the current time with offer time
            if ($currentTime >= strtotime($dd->startTime) && $currentTime <= strtotime($dd->endTime)) {

                //If applied on specified items
                if ($discountData->appliedOn == 2) {
                    $sid = DiscountOfferItem::select('discountRate')
                        ->where(['offerId' => $discountData->id, 'itemId' => $itemId])->first();

                    if ($sid) {
                        $returnData['discount'] = $discountData->discountRate;
                        $discountAmt = ($price / 100) * $discountData->discountRate;
                        $returnData['discountPrice'] = $price - $discountAmt;
                    }
                }
                //if applied on all items
                else if ($discountData->appliedOn == 1) {
                    $returnData['discount'] = $discountData->discountRate;
                    $discountAmt = ($price / 100) * $discountData->discountRate;
                    $returnData['discountPrice'] = $price - $discountAmt;
                }

            }

            /*if ($currentTime >= strtotime($dd->startTime) && $currentTime <= strtotime($dd->endTime)) {
            $returnData['discount'] = $dd->discountRate;
            $discountAmt = ($price / 100) * $dd->discountRate;
            $returnData['discountPrice'] = $price - $discountAmt;
            }*/
            $returnData['discountPrice'] = floatval($returnData['discountPrice']);

            return $returnData;
        }
    }

    //This funciton is used to get happy hour price of an item
    public static function getHappyHourPrice($happyHourData = array(), $itemId = null, $price = null)
    {
        if (!empty($happyHourData) && !empty($itemId) && !empty($price)) {
            $returnData = array();
            $hh = $happyHourData;

            $currentTime = strtotime(date('H:i:s'));

            //Comparing the current time with offer time
            if ($currentTime >= strtotime($hh->startTime) && $currentTime <= strtotime($hh->endTime)) {

                //If applied on specified items
                if ($hh->appliedOn == 2) {

                    $sid = HappyHourItem::select('id')
                        ->where(['hhTimingId' => $hh->id, 'itemId' => $itemId])->first();
                    //print_r($sid);exit;

                    if ($sid) {
                        if ($hh->discountType == 1) //Discount
                        {
                            $discountAmt = ($price / 100) * $hh->discountRate;
                            $returnData['happyHourPrice'] = round(($price - $discountAmt), 2);

                        } else //Offer (buy 1 get 1 free)
                        {
                            $returnData['buy'] = $hh->buy;
                            $returnData['free'] = $hh->free;
                        }
                    }
                } else //if applied on all items
                {
                    if ($hh->discountType == 1) //Discount
                    {
                        $discountAmt = ($price / 100) * $hh->discountRate;
                        $returnData['happyHourPrice'] = round(($price - $discountAmt), 2);
                    } else //Offer (buy 1 get 1 free)
                    {
                        $returnData['buy'] = $hh->buy;
                        $returnData['free'] = $hh->free;
                    }
                }
            }

            return $returnData;
        }
    }

    //This funciton is used to calculate Offer price of an item (Buy/Free items)
    public static function getOfferPrice($hhOffer = array(), $qty = null, $price = null)
    {
        if (!empty($hhOffer) && !empty($qty) && !empty($price)) {
            extract($hhOffer);

            $finalQty = 0;
            $totalQty = $buy + $free;

            if ($qty % $totalQty == 0) {
                $avoid = $qty / $totalQty;
                $finalQty = $qty - ($avoid * $free);
            } else {
                if ($qty > $totalQty) {
                    $devidedBy = (int) ($qty / $totalQty);
                    $extraTimes = (int) ($qty % $totalQty);

                    $extra = $qty - $totalQty;

                    $finalQty = ($buy * $devidedBy) + $extraTimes;
                } else {
                    $finalQty = $qty;
                }
            }
            return $price * $finalQty;
        }
    }

    //This function is used to get users device data to send notification
    public static function getUserDeviceData($userId = null)
    {
        $deviceData = DB::table('bnUserDevices as d')
            ->join("bnUsers as u", "u.id", "=", "d.userId")
            ->select('d.deviceToken')
            ->where(["d.userId" => $userId, "u.status" => 1])
        //->first();
            ->get()->toArray();
        // $tokens = json_decode(json_encode($deviceData), true);
        $tokens = array();
        if (!empty($deviceData)) {
            foreach ($deviceData as $d) {
                if (!empty($d->deviceToken) && $d->deviceToken != '') {
                    $tokens[] = $d->deviceToken;
                }
            }
        }

        return $tokens;
    }

    // This function is used to get All users device data to send notification
    public static function getAllUserDeviceData()
    {
        $deviceData = DB::table('bnUserDevices as d')
            ->join("bnUsers as u", "u.id", "=", "d.userId")
            ->select('d.deviceToken')
            ->where(["u.userType" => 'U', "u.status" => 1])
        //->first();
            ->get()->toArray();
        // $tokens = json_decode(json_encode($deviceData), true);
        $tokens = array();
        if (!empty($deviceData)) {
            foreach ($deviceData as $d) {
                if (!empty($d->deviceToken) && $d->deviceToken != '') {
                    $tokens[] = $d->deviceToken;
                }

            }
        }

        return $tokens;
    }

    //This funciton is used to send push notification using firebase
    public static function sendGeneralAndroidNotification($message, $device_token, $payLoadData, $app_type)
    {
        //print_r($device_token); echo $app_type;exit;
        if (!empty($device_token)) {
            // Original Server
            if ($app_type == 'U') {
                //Patron App
                $application_id = 'AAAA_uLdx4I:APA91bHqR1OFlV22LDiZUhyVXciY3GneWZ0xIt7WAl9uBXW5qB3zi_iS07ivM-th3GfeiyZHgOUj_aQ2MuUbf-T3PNTA0Fj7VLoURIAdeEdCxS_KHlEqCgyOMlNqy87oVpB3p2nKldvg';
            } else {
                //Bar App
                $application_id = 'AAAAh5KGqhE:APA91bEs9HBIpVr54mydTI0RtSt8_gOzJE1AWRYUjTgOnsfLyZIP_U7G4EqCsyaiGfRwmi-mw1NkR0YF7qgRCvfFb8s9oS8KJHoMFPa3ZPAE8_hFZFN2RnUpzHbNbpYnoePPYiqccq_K';
            }

            // Testing Server
            /*if ($app_type == 'U') {
                //Patron App
                $application_id = 'AAAA6Ok9h6o:APA91bEDDyjYhDTtSaqdtXALCOuLae3EfT-0tYLZiux8fshqgcKFcE1RiJblEsYJh5urcgELoupg0K4eyb-zsh9bo1xsluvFgczK4uyVcm2B4pfyPaylhBl61DisCP37QucppcLN1VE7';
            } else {
                //Bar App
                $application_id = 'AAAALDpHC_w:APA91bEUcf3Shmv4xou_q7l5rAvdmm5UstzLxquxhBP8dZjK0OVBXUiGYRm1fsHiFFDUM3-tIfdAptRh5IkFmD_wb6I4a3HRdPtZ1j5Mjy7gbOYu5gsy2WGaMIpZOIzwFjspIeWPRXXe';
            } */

            $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
            $token = $device_token;
            $tokenList = $device_token;

            $notification = [
                "title" => $message,
                'sound' => true,
            ];

            $moredata = '';
            if (!empty($payLoadData)) {
                $moredata = $payLoadData;
            }

            $extraNotificationData = ["message" => $notification, "moredata" => $moredata];

            $fcmNotification = [
                'registration_ids' => $tokenList, //multple token array
                //'to' => $token, //single token
                'notification' => $notification,
                'data' => $extraNotificationData,
            ];

            $headers = [
                'Authorization: key=' . $application_id,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            // echo "<pre>"; print_r($result);
            curl_close($ch);
            return true;
        }
    }

    //Used to Encrypt ID
    public static function encryptId($id)
    {
        return substr(md5($id), 0, 6) . dechex($id);
    }

    //Used to Dycrypt ID
    public static function decryptId($id)
    {
        $md5_8 = substr($id, 0, 6);
        $real_id = hexdec(substr($id, 6));
        return ($md5_8 == substr(md5($real_id), 0, 6)) ? $real_id : 0;
    }

    //Used to Encrypt ID
    public static function visitInsert()
    {
        extract($_POST);
        $openHour = array();
        $visitDate = date('Y-m-d');
        $day = strtolower(date('l', strtotime($visitDate)));
        // print_r($day);exit;

        /* === Bar Timing Validation === */
        $rs = self::getBarTimingSatus($barId);
        if(!empty($result) && $result['status'] == 0) {
            return $result;
        }

        // getting bar timing on bases of visit date of user
        $openingHrs = BarTiming::select('bnBarTimings.id', 'bnBarTimings.weekDay', 'bnBarTimings.openingTime', 'bnBarTimings.closingTime', 'b.openingStatus')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnBarTimings.barId')
            ->where(['barId' => $barId, 'weekDay' => $day])->first();

        if ($openingHrs) {
            //check opening status of bar
            if ($openingHrs->openingStatus == 1) {
                $visitTime = date('H:i:s');

                //take time 30 mint less then bar closing time
                $selectedTime = $openingHrs->closingTime;
                $endTime = strtotime("-30 minutes", strtotime($selectedTime));
                $closingTime = date('H:i:s', $endTime);

                //checking visit time of user with the bar opening and closing time
                if ($visitTime >= $openingHrs->openingTime && $visitTime < $closingTime) {
                    //bookng a visit
                    $insert = array('userId' => $userId, 'barId' => $barId, 'visitDate' => $visitDate, 'status' => 1, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                    $Visit = Visit::create($insert);

                    if ($Visit['id']) {
                        //get code generated for booking visit
                        $code = self::generateCode();
                        $codeInsert = array('visitId' => $Visit['id'], 'code' => $code, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                        $VisitCode = VisitCode::create($codeInsert);
                        if ($VisitCode['id']) {
                            $visits = Visit::select('bnVisits.id', 'bnVisits.sittingType', 'bnVisits.tableStandingNo', 'bnVisits.status', 'vc.code', 'b.barName', 'b.logo', 'ad.address', 'city.city_name as cityName', 'bnVisits.createdAt')
                                ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'bnVisits.id')
                                ->leftJoin('bnBars as b', 'b.id', '=', 'bnVisits.barId')
                                ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'b.userId')
                                ->leftJoin('bnCities as city', 'city.city_id', '=', 'ad.cityId')
                                ->where(['bnVisits.id' => $Visit['id']])->first();
                            
                            if (!empty($visits)) {
                                //send PUSH Notification to BAr
                                $user = Patron::select('firstName', 'lastName')->where(['userId' => $userId])->first();

                                if (!empty($user)) {
                                    $message = 'New code generated by ' . ucwords(strtolower($user->firstName . ' ' . $user->lastName));

                                    $visitId = $Visit['id'];
                                    $record = array('moduleType' => 'visit', 'moduleId' => $visitId, 'description' => $message, 'barId' => $barId, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                                    $not = BarNotification::create($record);

                                    if ($not['id']) {
                                        $barUserId = self::GetSingleField('bnBars', 'userId', 'id', $barId);

                                        $deviceToken = self::getUserDeviceData($barUserId); //Get Device Tokens
                                        if ($deviceToken) {
                                            //Firebase Notification
                                            $payLoadData = array('moduleType' => 'visitBooked', 'moduleId' => $visitId, 'barId' => $barId,'title' => 'New visit booked', 'msg' => $message);
                                            $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'B');
                                        }
                                    }
                                }
                            }

                            //Converting NULL to "" String
                            array_walk_recursive($visits, function (&$item, $key) {
                                $item = null === $item ? '' : $item;
                            });
                            $mediaPath = self::getMediaPath();

                            //Delete the Blocked entry of the user
                            DB::table('bnVisitBlockedUsers')->where(['userId' => $userId])->delete();

                            $result = array('status' => 1, 'message' => "Visit book successfully", 'data' => $visits, 'mediaPath' => $mediaPath);
                        }
                    } else {
                        $result = array('status' => 0, 'message' => "Visit not booked");
                    }
                } else {
                    $result = array('status' => 0, 'message' => "Bar is closed for this time");
                }
            } else {
                $result = array('status' => 0, 'message' => "Bar is closed");
            }
        } else {
            $result = array('status' => 0, 'message' => "Bar is closed on selected date");
        }
        return $result;

    }

    //tax calculation
    public static function calculateTax($totalPrice = null, $taxes = null)
    {
        $cgst = 0;
        $sgst = 0;
        $serviceCharge = 0;
        $serviceChargeTax = 0;
        $serviceChargeAmount = 0;
        $cgstAmount = 0;
        $sgstAmount = 0;
        $serviceChargeTaxAmount = 0;
        $grandTotal = $totalPrice;

        if (!empty($totalPrice) && !empty($taxes)) {
            $cgst = $taxes->cgst;
            $sgst = $taxes->sgst;
            $serviceCharge = $taxes->serviceCharge;
            $serviceChargeTax = $taxes->serviceChargeTax;

            //calculate cgst
            $cgstAmount = ($totalPrice / 100) * $cgst;

            //calculate sgst
            $sgstAmount = ($totalPrice / 100) * $sgst;

            $serviceChargeAmount = 0;
            $serviceChargeTaxAmount = 0;

            if ($serviceCharge > 0) {
                //calculate serviceCharge
                $serviceChargeAmount = ($totalPrice / 100) * $serviceCharge;
                if ($serviceChargeTax > 0) {
                    if ($serviceChargeAmount > 0) {
                        //calculate serviceChargeTax
                        $serviceChargeTaxAmount = ($serviceChargeAmount / 100) * $serviceChargeTax;
                    }
                }
            }

            $grandTotal = $totalPrice + $cgstAmount + $sgstAmount + $serviceChargeAmount + $serviceChargeTaxAmount;
        }
        $data = array(
            'cgst' => $cgst,
            'sgst' => $sgst,
            'serviceCharge' => $serviceCharge,
            'serviceChargeTax' => $serviceChargeTax,
            'cgstAmount' => round($cgstAmount, 2),
            'sgstAmount' => round($sgstAmount, 2),
            'serviceChargeAmount' => round($serviceChargeAmount, 2),
            'serviceChargeTaxAmount' => round($serviceChargeTaxAmount, 2),
            'grandTotal' => round($grandTotal, 2),
            'totalprice' => $totalPrice,
        );

        return $data;
    }
    public static function barLoginUrl()
    {
        $loginUrl = 'http://104.197.28.169';
        return $loginUrl;
    }

    // Function to Add Items in the cart table
    public static function addItemsInCart()
    {
        extract($_POST);
        $cart = array();
        $cartArray = json_decode($cartItems, true);
        $currentDate = date('Y-m-d');

        $discountData = self::checkBarOffers($barId, $currentDate);
        $happyHourData = self::checkBarHappyHour($barId);

        foreach ($cartArray as $key => $val) {
            extract($val);

            $unitType = 0;
            if (isset($unitTypeId)) {
                $unitType = $unitTypeId;
            }

            $sug = isset($val['suggestions']) ? trim($val['suggestions']) : NULL;

            $insert = array(
                'userId' => $userId,
                'barId' => $barId,
                'itemId' => $itemId,
                'unitTypeId' => $unitType,
                'quantity' => $quantity,
                'suggestions' => $sug,
                //'orderType' => $orderType,
                'createdAt' => date("Y-m-d H:i:s"),
                'updatedAt' => date("Y-m-d H:i:s"),
            );
            //insert data in cart table
            $items = Cart::create($insert);
            $data[] = $items;
        }
        if (!empty($data)) {
            //get list of carts of user on bases of order type
            $userCartList = Cart::select('bnCarts.id', 'bnCarts.barId', 'bnCarts.quantity', 'bnCarts.suggestions', 'bnCarts.itemId', 'i.itemName')
                ->leftJoin('bnBarItems as i', 'i.id', '=', 'bnCarts.itemId')
                ->where(['bnCarts.userId' => $userId]) //'bnCarts.orderType' => $orderType
                ->get()->toArray();

            if (count($userCartList) > 0) {
                //Get Bar ID from Cart, so we cal call the Discount/Happy Hours
                $barId = self::GetSingleField('bnCarts', 'barId', 'userId', $userId);

                $order = array();
                foreach ($userCartList as $ucl) {

                    $ucl['label'] = '';
                    $unitPrice = 0;
                    $itemPricing = self::GetSingleField('bnBarItems', 'price', 'id', $ucl['itemId']);

                    if ($itemPricing) {
                        $price = $unitPrice = $itemPricing;

                        //If dscount is available for the day
                        if ($discountData) {
                            $disData = self::getDiscountedPrice($discountData, $ucl['itemId'], $itemPricing);
                            if (!empty($disData)) {
                                if($disData['discountPrice'] != 0) {
                                    $price = $disData['discountPrice'];
                                    $ucl['label'] = $discountData->discountRate. "  % off";
                                }
                            }
                        }
                        //If Happy Hour is available for the day
                        $offerExist = false;
                        if ($happyHourData) {
                            $hhData = self::getHappyHourPrice($happyHourData, $ucl['itemId'], $itemPricing);
                            if (!empty($hhData)) {
                                if (isset($hhData['happyHourPrice'])) {
                                    $price = $hhData['happyHourPrice'];
                                    $ucl['label'] = $happyHourData->discountRate. "  % off";
                                } else {
                                    $hhOffer = $hhData;
                                    $ucl['label'] = "buy ".$hhData['buy']." get ". $hhData['free']. " free";
                                    $offerExist = true;
                                }
                            }
                        }

                        //if Happy Hour Offer have buy-get free then calculate the price
                        if (isset($hhOffer) && $offerExist) {
                            $price = self::getOfferPrice($hhOffer, $ucl['quantity'], $itemPricing);
                            $unitPrice = $itemPricing;
                        } else {
                            $unitPrice = $price;
                            $price = $price * $ucl['quantity'];
                        }

                        $itemPricing = round($price, 2);
                    }

                    $ucl['unitPrice'] = round($unitPrice, 2);
                    $ucl['price'] = $itemPricing;

                    $cart[] = $ucl;
                }

                //Converting NULL to "" String
                array_walk_recursive($cart, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
        }
        return $cart;
    }

    //Function to calculate Cart Amount
    public static function calculateCartAmount()
    {
        extract($_POST);
        $totalPrice = array();
        $orderType = '';
        $visitStatus = Visit::select('*')->where(['userId' => $userId, 'barId' => $barId, 'status' => 2])->first();

        $cartTotalAmount = array();

        //Get total amount of the Cart Items for an user
        $cartTotalAmount = Cart::select('bnCarts.id', 'bnCarts.barId', 'i.itemName', 'bnCarts.quantity', 'bnCarts.itemId')
            ->leftJoin('bnBarItems as i', 'i.id', '=', 'bnCarts.itemId')
            ->where(['bnCarts.userId' => $userId])
            ->get();

        if (count($cartTotalAmount) > 0) {
            $barName = "";
            $logo = "";
            $cartBarId = 0;

            $order = array();
            $i = 1;
            $itemPrice = 0;

            //Get the active discount/offers, for item pricing
            $currentDate = date('Y-m-d');
            $discountData = self::checkBarOffers($barId, $currentDate);
            $happyHourData = self::checkBarHappyHour($barId);

            foreach ($cartTotalAmount as $cta) 
            {
                $cartBarId = $cta->barId;
                $price = 0;
                //get item price
                $itemPricing = self::GetSingleField('bnBarItems', 'price', 'id', $cta->itemId);

                if ($itemPricing) {
                    $price = $itemPricing;

                    //If dscount is available for the day
                    if ($discountData) {
                        $disData = self::getDiscountedPrice($discountData, $cta->itemId, $itemPricing);
                        if (!empty($disData)) {
                            if($disData['discountPrice'] != 0)
                                $price = $disData['discountPrice'];
                        }
                    }

                    //If Happy Hour is available for the day
                    $offerExist = false;
                    if ($happyHourData) {
                        $hhData = self::getHappyHourPrice($happyHourData, $cta->itemId, $itemPricing);
                        if (!empty($hhData)) {
                            if (isset($hhData['happyHourPrice'])) {
                                $price = $hhData['happyHourPrice'];
                            } else {
                                $hhOffer = $hhData;
                                $offerExist = true;
                            }
                        }
                    }

                    //if Happy Hour Offer have buy-get free then calculate the price
                    if (isset($hhOffer) && $offerExist) {
                        $price = self::getOfferPrice($hhOffer, $cta->quantity, $itemPricing);
                    } else {
                        $price = $price * $cta->quantity;
                    }
                    //echo $price."\n";
                }
                $cta['price'] = $price;
                $itemPrice = $itemPrice + $price;
            }

            //Get the Tax information of the Bar
            $taxes = self::GetSingleRow('bnBars', 'id', $barId);

            if (!empty($taxes)) {
                //take total price calculation;
                $taxesCalculation = self::calculateTax($itemPrice, $taxes);
                $barName = $taxes->barName;
                $logo = $taxes->logo;
            }

            //get cart total qty
            $quantity = Cart::select('id', 'quantity', 'itemId')->where(['userId' => $userId])
                ->sum('quantity');
                /*->where(function ($query) use ($orderType) {
                    if (!empty($orderType)) {
                        $query->where('orderType', $orderType);
                    }
                })*/
                

            $totalPrice = array(
                'price' => $itemPrice,
                'item' => $cartTotalAmount,
                'barName' => $barName,
                'logo' => $logo,
                'taxes' => $taxesCalculation,
                'itemsInCart' => (int) $quantity,
                'cartBarId' => $cartBarId,
                'orderType' => '', //$orderType,
            );
        }
        return $totalPrice;
    }
    public static function getTopBarPossition($barId)
    {
        $lastMonth = strtolower(date('F', strtotime('-1 month', time()))) . '-' . date('Y');
        $result = TopBar::select('bnTopBars.position')
            ->where(['bnTopBars.barId' => $barId, 'bnTopBars.month' => $lastMonth])->get();
        if (count($result)) {
            return $result[0]->position;
        } else {
            return 0;
        }
    }
    public static function getBarDueAmount($barId)
    {
        $result = DB::table('bnBarBalances as balance')->select('balance.earningDue')->where(['balance.barId' => $barId])->get();
        if (count($result)) {
            return $result[0]->earningDue;
        } else {
            return 0;
        }
    }
    public static function getSettlementStatus($barId, $startDate, $endDate)
    {
        //$startDate = date("Y-m-d H:i:s", strtotime($startDate));
        //$endDate = date("Y-m-d H:i:s", strtotime($endDate));
        $result = DB::table('bnBarPayments')->select('*')
            ->where([
                'barId' => $barId,
                'paymentStartDate' => $startDate,
                'paymentEndDate' => $endDate,
            ])->get();
        if (count($result)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function calculateRemainingAmount($orderId, $barId)
    {
        //Get the sum of items amount
        $allamount = OrderKotItem::select('amount')->where(['orderId' => $orderId])->sum('amount');
        /*if ($allamount == 0) {
            $updateOrder = Order::where(['id' => $orderId])->delete();
        } 
        else {*/
            //Call Tax info of the bar
            $taxes = self::GetSingleRow('bnBars', 'id', $barId);

            //take total price calculation;
            $taxesCalculation = self::calculateTax($allamount, $taxes);
            
            // Reuturn lastest amount,cgst,sgst add all taxes in order table
            $orderData = array(
                'totalPrice' => $allamount,
                'grandTotal' => $taxesCalculation['grandTotal'],
                'cgst' => $taxesCalculation['cgst'],
                'cgstAmount' => $taxesCalculation['cgstAmount'],
                'sgst' => $taxesCalculation['sgst'],
                'sgstAmount' => $taxesCalculation['sgstAmount'],
                'serviceCharge' => $taxesCalculation['serviceCharge'],
                'serviceChargeAmount' => $taxesCalculation['serviceChargeAmount'],
                'serviceChargeTax' => $taxesCalculation['serviceChargeTax'],
                'serviceChargeTaxAmount' => $taxesCalculation['serviceChargeTaxAmount'],
                'updatedAt' => date('Y-m-d H:i:s'),
            );
            return $orderData;
        // }
    }

    //get visit data for app
    public static function getVisitData($userId)
    {
        //Load User's current Visit
        $visit = Visit::select('bnVisits.*', 'vc.code', 'b.barName', 'b.openingStatus', 'a.address', 'bb.mediaName', 'rr.rating', 'b.logo')
            ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'bnVisits.id')
            ->leftJoin('bnBars as b', 'b.id', '=', 'bnVisits.barId')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'b.userId')
            ->leftJoin('bnBarBanners as bb', 'bb.barId', '=', 'b.id')
            ->leftJoin('bnOrders as o', 'o.visitId', '=', 'bnVisits.id')
            //->leftJoin('bnBarMediaFiles as m', 'm.id', '=', 'bb.mediaId')
            ->leftJoin('bnBarRatings as rr', 'rr.barId', '=', 'b.id')
            ->where('bnVisits.userId', $userId)
            // ->where('bnVisits.status', '!=', 4)
            ->where('bnVisits.status', '!=', 3)
            //->where('o.paymentStatus', '=', 0)
            ->orderBy('bnVisits.id', 'desc')
            ->limit(1)->first();

        if (!empty($visit)) {
            if ($visit->status == 1) {
                $date1 = $visit->visitDate;
                $date2 = date('Y-m-d');
                // date into dateTimestamp
                $dateTimestamp1 = strtotime($date1);
                $dateTimestamp2 = strtotime($date2);
                $currentTime = date('H:i:s');

                //get code generated time from db
                $time = $visit->createdAt;
                $codeGeneratedTime = date('H:i:s', strtotime($time));

                //get only minites by taking difference of code generated time and current time
                //Then remove points by round function
                //convert -ve value in +ve by using function abs
                $minutes = abs(round((strtotime($currentTime) - strtotime($codeGeneratedTime)) / 60));

                if ($minutes > 15) {
                    //update visit status in table
                    Visit::where(['id' => $visit->id])->update(['status' => 3]);
                    //get visit status to show
                    $visit['status'] = 3;
                } else if ($dateTimestamp2 > $dateTimestamp1) {
                    //update visit status in table
                    Visit::where(['id' => $visit->id])->update(['status' => 3]);
                    //get visit status to show
                    $visit['status'] = 3;
                }
            }

            //Getting current day opening/closing status
            $openingStatus = $visit->openingStatus;

            //Call today's timing of the bar
            $today = strtolower(date('l'));
            $currentTime = strtotime(date('H:i:s'));

            $timing = BarTiming::select('id', 'openingTime', 'closingTime')->where(['barId' => $visit->barId, 'weekDay' => $today])->first(); //'openingStatus',

            $visit['openStatus'] = 'N/A';
            if (!empty($timing)) {
                $openStatus = 'Closed Now';

                if ($currentTime >= strtotime($timing->openingTime) && $currentTime <= strtotime($timing->closingTime)) {
                    $openStatus = 'Open Now';
                }

                // If bar visiblity option from profile
                if ($openingStatus == 0) {
                    $openStatus = 'Closed Now';
                }

                /*else if ($timing->openingStatus == 1) {
                $openStatus = 'Open Today';
                }*/

                $visit['currentTime'] = date("Y-m-d H:i:s");
                $visit['openStatus'] = $openStatus;
            }
            
            // GEt the payment status of the visit, if available
            $visit['paymentStatus'] = self::GetSingleField('bnOrders', 'paymentStatus', 'visitId', $visit->id);

            if($visit['paymentStatus'] == '')
                $visit['paymentStatus'] = 0;

            //Converting NULL to "" String
            if (!empty($visit)) {
                array_walk_recursive($visit, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
        }

        if ($visit['status'] == 3)
            $visitData = null;
        else if($visit['paymentStatus'] == 1) 
        {    
            // Check user submit the rating or not
            $reviewExist = BarReview::select('id')->where(['moduleId' => $visit->id, 'moduleType' => 'visit'])->first();
            if ($reviewExist)
                $visitData = null;    
            else {
                $visit['ratingSubmit'] = 0;
                $visitData = $visit;
            }
        }
        else 
            $visitData = $visit;

        return $visitData;
    }

    // This function is used to get item price, which is applicable on order/carts/menu etc
    public static function getItemFinalPrice($discountData = array(), $happyHourData = array(), $itemPrice = NULL, $itemId = NULL, $quantity = NULL)
    {
        $price = $itemPrice;
        $data['label']='';
        $data['discountPrice'] = 0;
        $data['discount'] = 0;
        $data['happyHourPrice'] = 0;
        $data['happyHourOffer'] = 0;
        $data['buy'] = 0;
        $data['free'] = 0;

        //If dscount is available for the day
        if ($discountData) {
            $disData = self::getDiscountedPrice($discountData, $itemId, $itemPrice);
            if (!empty($disData)) {
                if($disData['discountPrice'] != 0) {
                    $price = $disData['discountPrice'];
                    $data['label'] = $discountData->discountRate. "  % off";
                    $data['discount'] = $disData['discount'];
                    $data['discountPrice'] = round($disData['discountPrice'], 2);
                    $data['type'] = 1;
                }
            }
        }

        //If Happy Hour is available for the day
        $offerExist = false;
        if ($happyHourData) {
            $hhData = self::getHappyHourPrice($happyHourData, $itemId, $itemPrice);
            if (!empty($hhData)) {
                if (isset($hhData['happyHourPrice'])) {
                    $price = $hhData['happyHourPrice'];
                    $data['label'] = $happyHourData->discountRate. "  % off";
                    $data['happyHourOffer'] = 'discount';
                    $data['happyHourPrice'] = round($hhData['happyHourPrice'], 2);
                } else {
                    $hhOffer = $hhData;
                    $data['label'] = "buy ".$hhData['buy']." get ". $hhData['free']. " free";
                    $data['happyHourOffer'] = 'offer';
                    $data['buy'] = $hhData['buy'];
                    $data['free'] = $hhData['free'];
                    $offerExist = true;
                }
            }
        }

        //if Happy Hour Offer have buy-get free then calculate the price
        if (isset($hhOffer) && $offerExist) {
            $price = self::getOfferPrice($hhOffer, $quantity, $itemPrice);
        } else {
            $price = $price * $quantity;
        }

        $data['itemPricing'] = round($price, 2);
        
        return $data;
    }

    // This function is used to get Hashtags from a string
    public static function getHashTags($string){
        /* Match hashtags */
        $keywords = [];
        preg_match_all('/#(\w+)/', $string, $matches);
            /* Add all matches to array */
            foreach ($matches[1] as $match) {
            $keywords[] = $match;
        }

        return (array) $keywords;
    }

    //This function return id of city by name
    public static function getCityIdByName($cityName){
        if(!empty($cityName)){
            $city = DB::table('bnCities as city')->select('city_id as cityId')->where('city.city_name','=',$cityName)->get();
            if($city && count($city)){
               $result =  $city[0]->cityId;
            }else{
               $result = false;
            }
        }else{
            $result = false;
        }
        return $result;

    }

    //This function is used for check abusive word 17-02-220
    public static function isAbusiveContent($string){
        if(!empty($string))
        {
            $abusiveWords = DB::table('bnAppSettings')->select('abusiveWords')->get();
            if($abusiveWords && count($abusiveWords))
            {
                if(!empty($abusiveWords[0]->abusiveWords)) 
                {
                    $abusiveArray = explode( ',', $abusiveWords[0]->abusiveWords);

                    foreach($abusiveArray as $key => $abusive) {
                        if(strpos($string, trim($abusive)) !== false){
                            return $result = true; 
                        }
                        else
                            $result = false; 
                    }
                }
                else
                    $result = false; 
            }
            else
               $result = false;
        }
        else
            $result = false;

        return $result;
    }

    // Function to send visit Cancel Notification
    public static function visitCancelNotification($visitId = NULL){
        $visit = Visit::select('bnVisits.userId', 'bnVisits.barId', 'bar.barName', 'bar.logo', 'a.address')
            ->leftJoin('bnBars as bar', 'bnVisits.barId', '=', 'bar.id')
            ->leftJoin('bnAddresses as a', 'a.userId', '=', 'bar.userId')
            ->leftJoin('bnPatrons as p', 'p.userId', '=', 'bnVisits.userId')
            ->where(['bnVisits.id' => $visitId])->first();
        if (!empty($visit)) {
            $message = 'Your visit has cancelled for '.ucwords(strtolower($visit->barName));
             
            $record = array('moduleType' => 'visitCancelled', 'moduleId' => $visitId, 'description' => $message, 'userId' => $visit->userId, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

            $not = UserNotification::create($record);

            if ($not['id']) {
                $deviceToken = self::getUserDeviceData($visit->userId);
                if ($deviceToken) {
                    //Firebase Notification
                    $payLoadData = array('moduleType' => 'visitCancelled', 'moduleId' => $visitId, 'userId' => $visit->userId, 'title' => 'Visit Cancelled', 'msg' => $message, 'barId' => $visit->barId, 'barName' => $visit->barName, 'address' => $visit->address, 'logo' => $visit->logo);
                    $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                }
            }
        }
    }

    // Function to check User is geotagged or not
    public static function isUserGeoTagged($userId = NULL){
        $visit = Visit::select('bnVisits.barId', 'b.barName')
            ->leftJoin('bnBars as b', 'bnVisits.barId', '=', 'b.id')
            ->where(['bnVisits.userId' => $userId, 'bnVisits.status' => 2])->first();
        return $visit; //(!empty($visit)) ? $visit->barId : 0; 
    }

    // Function to send visit Cancel Notification
    public static function checkUserIncognitoMode($userId = NULL){
        $query = DB::table('bnIncognitoUsers')
            ->select('*')
            ->where(['userId' => $userId])
            ->first();
        if(!empty($query)) {
            $date1 = date("Y-m-d H:i:s");
            $date2 = $query->createdAt;
            $timestamp1 = strtotime($date1);
            $timestamp2 = strtotime($date2);

            $hour = abs($timestamp2 - $timestamp1)/(60*60);
            if($hour < 24) 
                return 1;
            else
                return 0;
        }
        else return 0;
    }

    // Function to send Offers Notification from Bar Side
    public static function sendOfferNotificationToPatrons($barId, $moduleId, $moduleType){

        $bar = Bar::select('bnBars.barName', 'ad.cityId', 'ad.areaId', 'ad.latitude', 'ad.longitude')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'bnBars.userId')
            ->where(['bnBars.id' => $barId])->first();

        if (!empty($bar)) {
            if ($moduleType == 'discount') {
                $message = 'New Discount offer started at ' . ucwords(strtolower($bar->barName));
                $moduleType = 'discountCreated';
                $title = 'New Discount';
            } 
            else if ($moduleType == 'happyhour') {
                $message = 'New Happy Hour offer started at ' . ucwords(strtolower($bar->barName));
                $moduleType = 'happyHourCreated';
                $title = 'New Happy Hour';
            }
            
            // Get Patrons who are in same city of the bar, within 3 KM range
            $sql = "SELECT ucl.userId, ucl.latitude, ucl.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($bar->latitude))
                     * COS(RADIANS(ucl.latitude))
                     * COS(RADIANS(ucl.longitude) - RADIANS($bar->longitude))
                     + SIN(RADIANS($bar->latitude))
                     * SIN(RADIANS(ucl.latitude))))
                     AS distanceInKM
                    FROM bnUserCurrentLocation ucl inner join bnAddresses a on a.userId = ucl.userId
                    inner join bnUsers u on u.id = ucl.userId
                    WHERE u.status = 1 AND a.cityId = $bar->cityId
                    HAVING distanceInKM <= 3000
                    ORDER BY distanceInKM ASC";
            $userData = DB::select(DB::raw($sql));

            if (!empty($userData)) {
                foreach ($userData as $uid) {
                    /*$record = array('moduleType' => 'discount', 'moduleId' => $moduleId, 'description' => $message, 'userId' => $uid->userId, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                    $not = UserNotification::create($record);*/
                    
                    $patronIds[] = $uid->userId;
                    
                    /*if ($not['id']) {
                        $deviceToken[] = self::getUserDeviceData($uid->userId);
                    }*/
                }
            }
            
            // Now call users from Notificaiton Settings, area wise - pending
            $areas = DB::table('bnUserNotificationSettings')
                ->select('userId')->where(['areaId' => $bar->areaId])
                ->get()->toArray();
            
            if(!empty($areas)) {
                foreach ($areas as $a) {
                    $patronIds[] = $a->userId;
                }
            }
            
            if(isset($patronIds))
            {
                $patronIds = array_unique($patronIds);
                foreach ($patronIds as $uid) {
                    $record = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'description' => $message, 'userId' => $uid, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                    $not = UserNotification::create($record);
                    
                    if ($not['id']) {
                        $deviceToken[] = self::getUserDeviceData($uid);
                    }
                }
            }

            //If notification created, then send push notification also
            if (isset($deviceToken)) {
                $deviceToken = call_user_func_array('array_merge', $deviceToken);
                
                //Firebase Notification
                $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'title' => $title, 'msg' => $message);
                $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
            }
        }
    }

    // Function to send Special Events Notification from Bar Side
    public static function sendEventNotificationToPatrons($barId, $moduleId, $title){
        $bar = Bar::select('bnBars.barName', 'ad.cityId', 'ad.latitude', 'ad.longitude')
            ->leftJoin('bnAddresses as ad', 'ad.userId', '=', 'bnBars.userId')
            ->where(['bnBars.id' => $barId])->first();

        if (!empty($bar)) {
            $message = 'New event '.$title.' is going to be held at ' . ucwords(strtolower($bar->barName));
            
            // Get Patrons who are in same city of the bar, within 3 KM range
            $sql = "SELECT ucl.userId, ucl.latitude, ucl.longitude, 111.045 * DEGREES(ACOS(COS(RADIANS($bar->latitude))
                     * COS(RADIANS(ucl.latitude))
                     * COS(RADIANS(ucl.longitude) - RADIANS($bar->longitude))
                     + SIN(RADIANS($bar->latitude))
                     * SIN(RADIANS(ucl.latitude))))
                     AS distanceInKM
                    FROM bnUserCurrentLocation ucl inner join bnAddresses a on a.userId = ucl.userId
                    inner join bnUsers u on u.id = ucl.userId
                    WHERE u.status = 1 AND a.cityId = $bar->cityId
                    HAVING distanceInKM <= 3000
                    ORDER BY distanceInKM ASC";
            $userData = DB::select(DB::raw($sql));
            
            if (!empty($userData)) {
                foreach ($userData as $uid) {
                    /*$record = array('moduleType' => 'specialEventCreated', 'moduleId' => $moduleId, 'description' => $message, 'userId' => $uid->userId, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                    $not = UserNotification::create($record);
                    
                    if ($not['id']) {
                        $deviceToken[] = self::getUserDeviceData($uid->userId);
                    }*/
                    $patronIds[] = $uid->userId;
                }

                // Now call users from Notificaiton Settings, area wise - pending
                $areas = DB::table('bnUserNotificationSettings')
                    ->select('userId')->where(['areaId' => $bar->areaId])
                    ->get()->toArray();
                
                if(!empty($areas)) {
                    foreach ($areas as $a) {
                        $patronIds[] = $a->userId;
                    }
                }

                if(isset($patronIds))
                {
                    $patronIds = array_unique($patronIds);
                    foreach ($patronIds as $uid) {
                        $record = array('moduleType' => 'specialEventCreated', 'moduleId' => $moduleId, 'description' => $message, 'userId' => $uid, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                        $not = UserNotification::create($record);
                        
                        if ($not['id']) {
                            $deviceToken[] = self::getUserDeviceData($uid);
                        }
                    }
                }

                //If notification created, then send push notification also
                if (isset($deviceToken)) {
                    $deviceToken = call_user_func_array('array_merge', $deviceToken);
                    
                    //Firebase Notification
                    $payLoadData = array('moduleType' => 'specialEventCreated', 'moduleId' => $moduleId, 'title' => 'New Special Event', 'msg' => $message);
                    $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                }
            }
        }
    }

    // Function to send Notification to User on update Lat/Long API
    public static function sendRegularNotification($userId, $latitude, $longitude, $cityId){ 
        // Get Discount/Offers of all the Bars, within 3 KM range
        $sql = "SELECT od.id, od.barId, od.offerType, b.barName, b.logo, ad.address, 111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
             * COS(RADIANS(ad.latitude))
             * COS(RADIANS(ad.longitude) - RADIANS($longitude))
             + SIN(RADIANS($latitude))
             * SIN(RADIANS(ad.latitude))))
             AS distanceInKM
            FROM bnOfferDetails od INNER JOIN bnBars b on b.id = od.barId INNER JOIN bnAddresses ad on ad.userId = b.userId inner join bnUsers u on u.id = b.userId WHERE u.status = 1 AND od.status = 1 AND ad.cityId = $cityId group by od.id HAVING distanceInKM <= 3000 ORDER BY distanceInKM ASC";
        $offers = DB::select(DB::raw($sql));

        if(count($offers) > 0) 
        {
            foreach ($offers as $off) 
            {
                $barName = $off->barName;
                $barId = $off->barId;
                $address = $off->address;
                $logo = $off->logo;

                // Discount only of today
                if($off->offerType == 'discount') {
                    $today = date("Y-m-d");
                    $disData = DiscountOffer::select('id')
                        ->where(['odId' => $off->id, 'startDate' => $today, 'endDate' => $today])
                        ->first();
                    if(!empty($disData)){
                        $message = 'New Discount offer started at ' . ucwords(strtolower($barName));
                        $moduleType = 'discountCreated';
                        $title = 'New Discount';
                        $moduleId = $disData->id;
                    }
                }
                // Happy hour which relates to current day
                else if($off->offerType == 'happyhour') {
                    $today = strtolower(date('l'));
                    
                    $hhData = HappyHoursTiming::select('id')->where(['odId' => $off->id])
                        ->whereRaw("find_in_set('$today', weekDay)")
                        ->first();

                    if(!empty($hhData)){
                        $message = 'New Happy Hour offer started at ' . ucwords(strtolower($barName));
                        $moduleType = 'happyHourCreated';
                        $title = 'New Happy Hour';
                        $moduleId = $hhData->id;
                    }
                }

                // Create an entry in notification table and send PUSH Notification
                if(isset($message)) {
                    $record = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'description' => $message, 'userId' => $userId, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                    $not = UserNotification::create($record);

                    if ($not['id']) {    
                        $deviceToken = self::getUserDeviceData($userId);
                        if ($deviceToken) {
                            //Firebase Notification
                            $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'userId' => $userId, 'barId' => $barId, 'title' => $title, 'msg' => $message, 'barName' => $barName, 'address' => $address, 'logo' => $logo);
                            $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                        }
                    }
                }
            }
        }

        // Get Special Events of all the Bars, within 3 KM range
        $today = date("Y-m-d");
        $sql = "SELECT se.id, se.barId, se.title, b.barName, b.logo, a.address,
                111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
                 * COS(RADIANS(a.latitude))
                 * COS(RADIANS(a.longitude) - RADIANS($longitude))
                 + SIN(RADIANS($latitude))
                 * SIN(RADIANS(a.latitude))))
                 AS distanceInKM
                FROM bnSpecialEvents se
                INNER JOIN bnBars b on b.id = se.barId
                INNER JOIN bnAddresses a on a.userId = b.userId
                INNER JOIN bnUsers u on u.id = b.userId
                WHERE se.status = 1 AND se.startDate >= '$today' AND a.cityId = $cityId group by se.id HAVING distanceInKM <= 3000 ORDER BY distanceInKM ASC";
        $events = DB::select(DB::raw($sql));

        if(count($events) > 0) 
        {
            foreach ($events as $off)
            {
                $barName = $off->barName;
                $barId = $off->barId;
                $address = $off->address;
                $logo = $off->logo;

                $message = 'New event '.$off->title.' is going to be held at ' . ucwords(strtolower($barName));
                $moduleType = 'specialEventCreated';
                $title = 'New Special Event';
                $moduleId = $off->id;

                // Create an entry in notification table and send PUSH Notification    
                $record = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'description' => $message, 'userId' => $userId, 'createdAt' => date("Y-m-d H:i:s"), 'updatedAt' => date("Y-m-d H:i:s"));
                $not = UserNotification::create($record);

                if ($not['id']) {    
                    $deviceToken = self::getUserDeviceData($userId);
                    if ($deviceToken) {
                        //Firebase Notification
                        $payLoadData = array('moduleType' => $moduleType, 'moduleId' => $moduleId, 'title' => $title, 'msg' => $message);
                        $rs = self::sendGeneralAndroidNotification($message, $deviceToken, $payLoadData, 'U');
                    }
                }
            }
        }
    }

    // Function to send Bar Activation Request email to Admin
    public static function sendActivationEmailToAdmin($userId){
        $bar = self::GetSingleRow('bnBars', 'userId', $userId);
        if(!empty($bar)) {
            $barName = $bar->barName;
            $adminEmail = User::select('email')->where(['userType' => 'A'])->first();
            if (!empty($adminEmail)) {
                
                //create message
                $msg = '<h1 class="title">Hello Admin</h1>
                        <p><b>'.$barName.'</b> has completed their profile and sent a request to activate their account.</p>';
                $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

                $data = array(
                    'msg' => $msg,
                    'subject' => "Bar Profile Activation Request",
                );
                Mail::to($adminEmail)->send(new SendMail($data));
            }
        }
    }

    // Function to send email to Bar on Activation request approval
    public static function sendActivationEmailToBar($userId){
        $bar = Bar::select('u.email', 'bnBars.barName')->join('bnUsers as u', 'u.id', '=', 'bnBars.userId')
            ->where(['bnBars.userId' => $userId, 'u.userType' => 'B'])->first();
                
        if(!empty($bar)) {
            $barName = $bar->barName;
            $email = $bar->email;
            //create message
            $msg = '<h1 class="title">Hello '.$barName.'</h1>
                    <p>Your activation request has been accepted by us. Now your profile is visible to Bar Nights Patron App.</p>';
            $msg .= '<p>For more details, please <a href="'.config('app.front_url').'"> Login </a> to your account.</p>';

            $data = array(
                'msg' => $msg,
                'subject' => "Profile Activated",
            );
            Mail::to($email)->send(new SendMail($data));
        }
    }

    // Function to get Bar Visibility Status
    public static function getBarVisibility($barId){
        $barData = Bar::select('bnBars.openingStatus') 
            ->where(['bnBars.id' => $barId])->first();

        if (!empty($barData)) {
            if($barData->openingStatus == 0) {
                $result = array('status' => 0, 'message' => "This bar is currently not serving");
            }
            else  {
                // getting bar timing on bases of visit date of user
                $today = date('Y-m-d');
                $day = strtolower(date('l', strtotime($today)));

                $openingHrs = BarTiming::select('bnBarTimings.id', 'bnBarTimings.weekDay', 'bnBarTimings.openingTime', 'bnBarTimings.closingTime')
                    ->leftJoin('bnBars as b', 'b.id', '=', 'bnBarTimings.barId')
                    ->where(['barId' => $barId, 'weekDay' => $day])->first();

                if ($openingHrs) {
                    $currentTime = date('H:i:s');

                    //take time 30 mint less then bar closing time
                    $selectedTime = $openingHrs->closingTime;
                    $endTime = strtotime("0 minutes", strtotime($selectedTime));
                    $closingTime = date('H:i:s', $endTime);

                    // checking visit time of user with the bar opening and closing time
                    if ($currentTime >= $openingHrs->openingTime && $currentTime < $closingTime) {
                        $result = array('status' => 1, 'message' => "Bar is active");
                    }
                    else {
                        $result = array('status' => 0, 'message' => "Bar is closed for this time");
                    }
                } 
                else {
                    $result = array('status' => 0, 'message' => "Bar is closed on selected date");
                }
            }
        }
        else {
            $result = array('status' => 0, 'message' => "Invalid Bar");
        }
        return $result;
    }

    // Function to get Bar timing status - to validate on several APIs
    public static function getBarTimingSatus($barId){
        $barData = Bar::select('bnBars.openingStatus') 
            ->where(['bnBars.id' => $barId])->first();

        if (!empty($barData)) {
            if($barData->openingStatus == 0) {
                $result = array('status' => 0, 'message' => "This bar is currently not serving");
            }
            else  {
                // getting bar timing on bases of visit date of user
                $currentTime = date('H:i:s');
                $today = date('Y-m-d');
                $day = strtolower(date('l', strtotime($today)));

                // If current time is in between 12-05 AM then will take the timing of last day
                $maxTime = strtotime("5:00");
                $raatKe1259 = strtotime("23:59");
                $raatKe12Bje = strtotime("00:00");
                $crTime = strtotime($currentTime);

                if($crTime >= $raatKe12Bje && $crTime <= $maxTime) {
                    $today = date('Y-m-d', strtotime("-1 days"));
                    $day = strtolower(date('l', strtotime($today)));
                }

                $openingHrs = BarTiming::select('id', 'weekDay', 'openingTime', 'closingTime')
                    ->where(['barId' => $barId, 'weekDay' => $day])->first();

                if ($openingHrs) {
                    
                    $dbStartTime = strtotime($openingHrs->openingTime);
                    $dbEndTime = strtotime($openingHrs->closingTime);
                    
                    // If Bar is closed before 12 AM
                    if($dbStartTime <= $raatKe1259 && $dbEndTime <= $raatKe1259 && $dbEndTime >= $maxTime) 
                    {
                        //take time 30 mint less then bar closing time
                        $selectedTime = $openingHrs->closingTime;
                        $endTime = strtotime("-30 minutes", strtotime($selectedTime));
                        $closingTime = date('H:i:s', $endTime);

                        // checking visit time of user with the bar opening and closing time
                        if ($currentTime >= $openingHrs->openingTime && $currentTime < $closingTime) {
                            $result = array('status' => 1, 'message' => "Bar is active");
                        }
                        else {
                            $result = array('status' => 0, 'message' => "Bar is closed for this time");
                        }
                    }
                    // If Bar is active before-after 12 AM
                    else {
                        if($crTime <= $raatKe1259 && $crTime >= $maxTime) { 
                            //take time 30 mint less then raatKe1259
                            $endTime = strtotime("-30 minutes", $raatKe1259);
                            $closingTime = date('H:i:s', $endTime);

                            // checking visit time of user with the bar opening and closing time
                            if ($currentTime < $closingTime) {
                                $result = array('status' => 1, 'message' => "Bar is active");
                            }
                            else {
                                $result = array('status' => 0, 'message' => "Bar is closed for this time");
                            }
                        }
                        else {
                            //take time 30 mint less then bar closing time
                            $selectedTime = $openingHrs->closingTime;
                            $endTime = strtotime("-30 minutes", strtotime($selectedTime));
                            $closingTime = date('H:i:s', $endTime);

                            // checking visit time of user with the bar opening and closing time
                            if ($currentTime < $closingTime) {
                                $result = array('status' => 1, 'message' => "Bar is active");
                            }
                            else {
                                $result = array('status' => 0, 'message' => "Bar is closed for this time");
                            }
                        }
                    }
                } 
                else {
                    $result = array('status' => 0, 'message' => "Bar is closed on selected date");
                }
            }
        }
        else {
            $result = array('status' => 0, 'message' => "Invalid Bar");
        }
        return $result;
    }


    // Function to update the booking status to epxired
    public static function updateTableBookingExpireStatus($bookingId, $status, $bTime, $payStatus){
        // $time = $row->bookingDate.' '.$row->bookingTime;
        $currentTime = date('H:i:s');  //date('Y-m-d H:i:s'); 
        $bookingTime = date('H:i:s', strtotime($bTime));
        $minutes = round((strtotime($currentTime) - strtotime($bookingTime)) / 60);

        $limit = config('app.table_time_limit');

        if ($minutes > $limit) {
            if(($status == 0 || $status == 1) && $payStatus == 0) {
                DB::table('bnBookTable')->where(['id' => $bookingId])->update(['status' => 3, 'updatedAt' => date("Y-m-d H:i:s")]);
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    // Function to validate Bar timing on profile
    public static function barTimingValidation($sTime, $eTime, $day) {
        if($sTime != NULL && $eTime != NULL) {
            $result = array();
            $day = ucwords($day);

            $sTime = strtotime(($sTime));
            $eTime = strtotime(($eTime));
            $minTime = strtotime("5:00");
            $maxTime = strtotime("23:59");

            if ($eTime < $minTime)
            {
                if ($sTime > $minTime && $sTime <= $maxTime){
                    // save the time
                    $result = array('status' => 1, 'message' => 'Proceed');
                }
                else {
                    // show warning that start timer must be between 5:00 AM to 12:00 PM
                    $result = array('status' => 0, 'message' => 'Opening time must be between 5:00 AM to 12:00 PM on '.$day);
                }
            }
            else
            {
                if ($sTime > $eTime) {
                    // show warning
                    $result = array('status' => 0, 'message' => 'Opening time should be lower than closing time on '.$day);
                }
                else
                {
                    if ($sTime > $minTime && $sTime <= $maxTime) {
                        // save the time
                        $result = array('status' => 1, 'message' => 'Proceed');
                    }
                    else {
                        // show warning that start timer must be between 5:00 AM to 12:00 PM
                        $result = array('status' => 0, 'message' => 'Opening time must be between 5:00 AM to 12:00 PM on '.$day);
                    }
                }
            }

            return $result;
        }
    }

    // Function to validate Offer timing on discount
    public static function discountTimingValidation($sTime, $eTime, $barId) {
        if($sTime != NULL && $eTime != NULL) {
            $result = array();

            $startTime = $sTime;
            $endTime = $eTime;
            $to_time = strtotime(date("Y-m-d ".$startTime));
            $from_time = strtotime(date("Y-m-d ".$endTime));
            $min = round(abs($to_time - $from_time) / 60,2);

            $sTime = strtotime($sTime);
            $eTime = strtotime($eTime);
            $maxTime = strtotime("5:00");
            $raatKe12Bje = strtotime("00:00");
            $dinKe12Bje = strtotime("12:00");

            $today = date("Y-m-d");
            $day = strtolower(date('l', strtotime($today)));

            // getting bar timing for today
            $barTiming = BarTiming::select('id', 'openingTime', 'closingTime')
                ->where(['barId' => $barId, 'weekDay' => $day])->first();

            if ($barTiming) {
                $opTime =  date("h:i A", strtotime($barTiming->openingTime));
                $clsTime =  date("h:i A", strtotime($barTiming->closingTime));
                $dayStartTime = strtotime($barTiming->openingTime);
                $dayEndTime = strtotime($barTiming->closingTime);

                //If Bar is open after 12 AM in the night
                if($dayEndTime >= $raatKe12Bje && $dayEndTime <= $maxTime) {
                    if ($dayEndTime < $maxTime && $sTime < $maxTime)
                    {
                        if ($sTime < $dayEndTime && $sTime < $eTime && $eTime <= $dayEndTime)
                        {
                            if($min < 60) {
                                // Min 60 min condition
                                $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                            }
                            else {
                                // correct time
                                $result = array('status' => 1, 'message' => "Proceed First");
                            }
                        }
                        else
                        {
                            // show warning
                            $result = array('status' => 0, 'message' => "Offer timing should be in between bar timing");
                        }
                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime > $maxTime)
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 2");
                        }

                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime < $maxTime && $eTime <= $dayEndTime)
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 3");
                        }
                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime < $maxTime && $eTime <= $dayEndTime )
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 4");
                        }
                    }
                    else if ($dayEndTime > $maxTime && $sTime < $maxTime)
                    {
                        // show warning
                        $result = array('status' => 0, 'message' => "Offer timing should be in between bar timing");
                    }
                }
                else {
                    //If Bar is closed before 12 AM in the night
                    if ($eTime <= $dayEndTime && $eTime < $raatKe12Bje)
                    {
                        if ($sTime >= $dayStartTime && $sTime <= $dayEndTime){
                            if($min < 60) {
                                // Min 60 min condition
                                $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                            }
                            else {
                                // save the time
                                $result = array('status' => 1, 'message' => 'Proceed before 12 AM 1');
                            }
                        }
                        else {
                            // show warning that start timer must be between 5:00 AM to 12:00 PM
                            $result = array('status' => 0, 'message' => 'Offer timing must be between '.$opTime .' to '.$clsTime);
                        }
                    }
                    else
                    {
                        if ($sTime > $eTime) {
                            // show warning
                            $result = array('status' => 0, 'message' => 'Start time should be lower than end time');
                        }
                        else if ($eTime > $dayEndTime) {
                            // show warning
                            $result = array('status' => 0, 'message' => 'End time should be lower than bar closing time at '. $clsTime);
                        }
                        else
                        {
                            if ($sTime >= $dayStartTime && $sTime <= $dayEndTime) {
                                if($min < 60) {
                                    // Min 60 min condition
                                    $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                                }
                                else {
                                    // save the time
                                    $result = array('status' => 1, 'message' => 'Proceed before 12 AM 2');
                                }
                            }
                            else {
                                // show warning that start timer must be between 5:00 AM to 12:00 PM
                                $result = array('status' => 0, 'message' =>  'Offer timing must be between '.$opTime .' to '.$clsTime);
                            }
                        }
                    }
                }
            }
            else {
                $result = array('status' => 0, 'message' => "Bar timing is not set for today. Please update Bar timing before creating any offer.");
            }

            return $result;
        }
    }

    // Function to validate Offer timing on Happy Hours
    public static function happyHourTimingValidation($sTime, $eTime, $barId, $day) {
        if($sTime != NULL && $eTime != NULL) {
            $result = array();

            $startTime = $sTime;
            $endTime = $eTime;
            $to_time = strtotime(date("Y-m-d ".$startTime));
            $from_time = strtotime(date("Y-m-d ".$endTime));
            $min = round(abs($to_time - $from_time) / 60,2);

            $sTime = strtotime($sTime);
            $eTime = strtotime($eTime);
            $maxTime = strtotime("5:00");
            $raatKe12Bje = strtotime("00:00");
            $dinKe12Bje = strtotime("12:00");

            $today = date("Y-m-d");
            $day = strtolower($day);

            // getting bar timing for selecte weekday
            $barTiming = BarTiming::select('id', 'openingTime', 'closingTime')
                ->where(['barId' => $barId, 'weekDay' => $day])->first();

            if ($barTiming) {
                $opTime =  date("h:i A", strtotime($barTiming->openingTime));
                $clsTime =  date("h:i A", strtotime($barTiming->closingTime));
                $dayStartTime = strtotime($barTiming->openingTime);
                $dayEndTime = strtotime($barTiming->closingTime);

                //If Bar is open after 12 AM in the night
                if($dayEndTime >= $raatKe12Bje && $dayEndTime <= $maxTime) {
                    if ($dayEndTime < $maxTime && $sTime < $maxTime)
                    {
                        if ($sTime < $dayEndTime && $sTime < $eTime && $eTime <= $dayEndTime)
                        {
                            if($min < 60) {
                                // Min 60 min condition
                                $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                            }
                            else {
                                // correct time
                                $result = array('status' => 1, 'message' => "Proceed First");
                            }
                        }
                        else
                        {
                            // show warning
                            $result = array('status' => 0, 'message' => "Offer timing should be in between bar timing for ".$day);
                        }
                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime > $maxTime)
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 2");
                        }

                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime < $maxTime && $eTime <= $dayEndTime)
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 3");
                        }
                    }
                    else if ($dayEndTime < $maxTime && $sTime >= $dayStartTime && $sTime > $maxTime && $eTime < $maxTime && $eTime <= $dayEndTime )
                    {
                        if($min < 60) {
                            // Min 60 min condition
                            $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                        }
                        else {
                            // correct time
                            $result = array('status' => 1, 'message' => "Proceed 4");
                        }
                    }
                    else if ($dayEndTime > $maxTime && $sTime < $maxTime)
                    {
                        // show warning
                        $result = array('status' => 0, 'message' => "Offer timing should be in between bar timing for ".$day);
                    }
                }
                else {
                    //If Bar is closed before 12 AM in the night
                    if ($eTime <= $dayEndTime && $eTime < $raatKe12Bje)
                    {
                        if ($sTime >= $dayStartTime && $sTime <= $dayEndTime){
                            if($min < 60) {
                                // Min 60 min condition
                                $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                            }
                            else {
                                // save the time
                                $result = array('status' => 1, 'message' => 'Proceed before 12 AM 1');
                            }
                        }
                        else {
                            // show warning that start timer must be between 5:00 AM to 12:00 PM
                            $result = array('status' => 0, 'message' => 'Offer timing must be between '.$opTime .' to '.$clsTime." for ".$day);
                        }
                    }
                    else
                    {
                        if ($sTime > $eTime) {
                            // show warning
                            $result = array('status' => 0, 'message' => 'Start time should be lower than end time for '.$day);
                        }
                        else if ($eTime > $dayEndTime) {
                            // show warning
                            $result = array('status' => 0, 'message' => 'End time should be lower than bar closing time at '. $clsTime. ' for '.$day);
                        }
                        else
                        {
                            if ($sTime >= $dayStartTime && $sTime <= $dayEndTime) {
                                if($min < 60) {
                                    // Min 60 min condition
                                    $result = array('status' => 0, 'message' => 'Offer duration must be minimum 1 hour');
                                }
                                else {
                                    // save the time
                                    $result = array('status' => 1, 'message' => 'Proceed before 12 AM 2');
                                }
                            }
                            else {
                                // show warning that start timer must be between 5:00 AM to 12:00 PM
                                $result = array('status' => 0, 'message' =>  'Offer timing must be between '.$opTime .' to '.$clsTime.' for '.$day);
                            }
                        }
                    }
                }
            }
            else {
                $result = array('status' => 0, 'message' => "Bar timing is not set for ".$day.". Please update Bar timing before creating any offer.");
            }

            return $result;
        }
    }
}