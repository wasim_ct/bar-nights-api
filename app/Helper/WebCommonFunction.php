<?php
namespace App\Helper;

use DB;
class WebCommonFunction
{
    //Check Phone No/Email/userName is registered or not with user type
    public static function checkValueExist($value = null, $field = null, $userType = null)
    {
        $row = DB::table('bnUsers')
            ->select('id', 'status')
            ->where([$field => $value, 'userType' => $userType])
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    }

    //Check Phone No/Email/userName is registered or not with user type for edit action
    public static function checkEditValueExist($value = null, $field = null, $userType = null,$userId = null)
    {
        $row = DB::table('bnUsers')
            ->select('id', 'status')
            ->where([$field => $value, 'userType' => $userType])
            ->where('id','!=', $userId)
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    }
}