<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsFeed extends Model
{
    public $table = 'bnNewsFeeds';
    public $timestamps = false;

    protected $fillable = [
        'id', 'description', 'userId', 'visibility', 'createdAt', 'updatedAt', 'locationTag', 'locationId',
    ];
}