<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTable extends Model
{
    public $table = 'bnBookTable';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'code', 'message', 'bookingDate', 'bookingTime', 'tableNumber', 'tokenMoney', 'status ', 'totalPerson', 'status', 'paymentStatus', 'cancelReason', 'cancelBy', 'createdAt', 'updatedAt',
    ];
}