<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    public $table = 'bnActivityLogs';
    public $timestamps = false;

    protected $fillable = [
        'id', 'description', 'barId', 'createdAt', 'updatedAt',
    ];
}