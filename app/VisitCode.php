<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class VisitCode extends Model
{
    public $table = 'bnVisitCodes';
    public $timestamps = false;

    protected $fillable = [
        'id', 'visitId', 'code', 'codeAvailed', 'createdAt', 'updatedAt',
    ];
    //This function is used for get all visit code list
    public static function getAllCode(){
    	$result= DB::table('bnVisits as v')
        ->select('v.userId','vc.code','v.visitDate','v.visitTime','v.status')
        ->whereNotNull('vc.code')
        ->leftJoin('bnVisitCodes as vc', 'vc.visitId', '=', 'v.id')
        ->get();
        return $result;
    }
}