<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountOffer extends Model
{
    public $table = 'bnDiscountOffers';
    public $timestamps = false;

    protected $fillable = [
        'id', 'odId', 'barId', 'appliedOn', 'discountRate', 'startDate', 'endDate', 'startTime', 'endTime', 'status', 'createdAt', 'updatedAt',
    ];
}