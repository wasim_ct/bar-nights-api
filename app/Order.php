<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'bnOrders';
    public $timestamps = false;

    protected $fillable = [
        'id', 'userId', 'barId', 'friendId', 'tableNo', 'orderType', 'orderId', 'orderDate', 'status', 'totalPrice', 'cgst', 'cgstAmount', 'sgst', 'sgstAmount', 'serviceCharge', 'serviceChargeAmount', 'serviceChargeTax', 'serviceChargeTaxAmount', 'grandTotal', 'visitId', 'paymentStatus', 'createdAt', 'updatedAt',
    ];

    //This function is used for get order history by user id(Admin Panel)
    public static function getOrderByUserId($userId, $orderType = null)
    {
        $query = DB::table('bnOrders as order')->select('order.id', 'order.orderId as uniqueId', 'order.orderDate', 'order.createdAt', 'order.orderType', 'order.grandTotal', 'order.status', 'order.paymentStatus', 'bar.barName');
        $query->where('order.userId', $userId);
        /*if(!empty($orderType)){
        $query->where('order.orderType', $orderType);
        }*/
        $query->leftJoin('bnBars as bar', 'bar.id', '=', 'order.barId');
        $result = $query->get();
        return $result;
    }
    //This function is used for get admin commisiion list of completed order
    public static function adminCommissionOrderWise()
    {
        $result = DB::table('bnOrders as order')
            ->select('order.id as orderId', 'order.barId', 'orderType', 'order.totalPrice', DB::raw('(order.totalPrice*10/100) + (order.totalPrice*18/100) as commission'), DB::raw('CASE WHEN (order.status = 3) THEN "completed" ELSE "running" END as status'))
            ->where('order.status', 3)
            ->leftJoin('bnBars as bar', 'bar.id', '=', 'order.barId')
            ->get();
        return $result;
    }
    //This function is used for getbar income month wise
    public static function barIncomeMonthWise()
    {
        $result = DB::table('bnOrders as order')
            ->select('order.barId', DB::raw('sum(order.totalPrice) as totalAmount'), DB::raw('sum((order.totalPrice*10/100) + (order.totalPrice*18/100)) as totalCommission'), DB::raw('sum(order.totalPrice-((order.totalPrice*10/100) + (order.totalPrice*18/100))) as totalIncome'), DB::raw('date_format(order.orderDate, "%M %Y") as month'))
            ->where('order.status', 3)
            ->leftJoin('bnBars as bar', 'bar.id', '=', 'order.barId')
            ->groupBy(['order.barId', 'month'])
            ->get();
        return $result;
    }
    //This function is used for calculated bar income statements
    public static function barIncomeStatement($barId, $startDate, $endDate)
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $result = DB::table('bnOrders as order')
            ->select('order.barId', DB::raw('sum(order.totalPrice) as totalAmount'), DB::raw('sum((order.totalPrice*10/100) + (order.totalPrice*18/100)) as totalCommission'), DB::raw('sum(order.totalPrice-((order.totalPrice*10/100) + (order.totalPrice*18/100))) as totalIncome'))
            ->where('order.status', 3)
            ->where('order.barId', $barId)
            ->where('order.orderDate', '>=', $startDate)
            ->where('order.orderDate', '<=', $endDate)
            ->leftJoin('bnBars as bar', 'bar.id', '=', 'order.barId')
            ->groupBy(['order.barId'])
            ->get();
        return $result;
    }
}