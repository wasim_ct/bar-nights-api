<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnVisitCodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visitId')->default(0);
            $table->string('code', 50)->nullable();
            $table->boolean('codeAvailed', 1)->default(0)->comment = '0:not availed, 1:availed';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnVisitCodes');
    }
}
