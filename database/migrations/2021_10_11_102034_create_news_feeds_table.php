<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnNewsFeeds', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->integer('userId')->default(0);
            $table->boolean('visibility', 1)->default(0)->comment = '1:public,2:contacts,3:private';
            $table->integer('locationId')->default(0);
            $table->boolean('isReported',4)->default(0);
            $table->integer('isDeleted')->default(0);
            $table->boolean('isIgnore',1)->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnNewsFeeds');
    }
}