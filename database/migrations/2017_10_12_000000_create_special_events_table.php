<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnSpecialEvents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->string('eventImage', 255)->nullable();
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->time('startTime')->nullable();
            $table->time('endTime')->nullable();
            $table->boolean('entryType', 1)->default(0)->comment = '1:free,2:paid';
            $table->float('entryfees', 10, 2)->default(0);
            $table->date('bookingStartDate')->nullable();
            $table->date('bookingendDate')->nullable();
            $table->boolean('passApplicable', 1)->default(0)->comment = '1:yes,0:no';
            $table->boolean('status', 1)->default(0)->comment = '1:active,0:inactive';
            $table->integer('totalTickets')->default(0);
            $table->integer('totalStandingTickets')->default(0);
            $table->date('publishedOn')->nullable();
            $table->integer('duration')->default(0)->comment="1:-one week,2:-two week";
            $table->float('price', 10, 2)->default(0)->comment="ad price";
            $table->string('venue', 255)->nullable();
            $table->float('coverCharges', 10, 2)->default(0);
            $table->float('discountRate', 10, 2)->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnSpecialEvents');
    }
}
