<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnVisitInvitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toUserId')->default(0)->comment='invitation receiver id';
            $table->integer('visitId')->default(0);
            $table->boolean('status',1)->default(0)->comment='1:pending, 2:accept,3:reject';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnVisitInvitations');
    }
}
