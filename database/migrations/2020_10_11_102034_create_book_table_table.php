<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBookTable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->integer('barId')->default(0);
            $table->string('code',255)->nullable();
            $table->text('message')->nullable();
            $table->date('bookingDate')->nullable();
            $table->time('bookingTime')->nullable();
            $table->integer('totalPerson')->default(0);
            $table->string('tableNumber',255)->nullable();
            $table->float('tokenMoney', 10, 2)->default(0);
            $table->boolean('status', 1)->default(0)->comment = '0:Pending, 1:Accept, 2:Reject';
            $table->boolean('codeAvailed', 1)->default(0)->comment = '0:not availed,1:availed';
            $table->boolean('paymentStatus', 4)->default(0)->comment = '0:not paid, & 1: paid'; 
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBookTable');
    }
}