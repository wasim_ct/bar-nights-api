<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarNotifications', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->integer('barId')->default(0);
            $table->integer('moduleId')->default(0);
            $table->string('moduleType', 20)->nullable()->comment = 'orders/visits/table';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    } 
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarNotifications');
    }
}