<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnEventBookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eventId')->default(0);
            $table->integer('userId')->default(0);
            $table->integer('totalSeats')->default(0);
            $table->boolean('PaymentStatus', 1)->default(0)->comment = '0:not paid, & 1: paid';
            $table->float('amount', 10, 2)->default(0);
            $table->string('code',255)->nullable();
            $table->boolean('codeAvailed', 1)->default(0)->comment = '0:not availed,1:availed';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnEventBookings');
    }
}
