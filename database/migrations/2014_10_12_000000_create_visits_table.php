<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnVisits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->integer('barId')->default(0);
            $table->date('visitDate')->nullable();
            $table->time('visitTime')->nullable();
            $table->string('sittingType', 100)->nullable()->comment = 'table/standing';
            $table->string('tableStandingNo', 255)->nullable();
            $table->integer('requestId')->default(0)->comment='table booking/event booking';
            $table->string('requestType',100)->nullable()->comment="table/event";
            $table->boolean('status', 1)->default(0)->comment = '1:pending, 2:confirmed,3:cancelled,4:completed';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    } 
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnVisits');
    }
}
