<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderKotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnOrderKots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId')->default(0);
            $table->integer('friendId')->default(0)->comment = 'Gifted userId';
            $table->string('kotNumber', 100)->nullable();
            $table->boolean('status', 1)->default(0)->comment = '1:pending, 2:accept, 3:collected, 4:cancelled';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnOrderKots');
    }
}
