<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnAppSettings', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('aboutUs')->nullable();
            $table->string('contactEmail', 255)->nullable();
            $table->longText('faq')->nullable();
            $table->longText('privacyPolicy')->nullable();
            $table->longText('termsAndConditions')->nullable();
            $table->float('commission', 10, 2)->default(0);
            $table->float('eventCommission', 10, 2)->default(0);
            $table->float('taxOnCommission', 10, 2)->default(0);
            $table->float('barOneCommission', 10, 2)->default(0);
            $table->float('barTwoCommission', 10, 2)->default(0);
            $table->float('barThreeCommission', 10, 2)->default(0);
            $table->float('topBarLimit', 10, 2)->default(0);
            $table->float('topSpenderMinLimit', 10, 2)->default(0); 
            $table->float('cashBackOn', 10, 2)->default(0);
            $table->float('cashBackAmount', 10, 2)->default(0);
            $table->float('bookingTokenMoney', 10, 2)->default(0)->comment="Table booking token money";
            $table->text('abusiveWords')->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnAppSettings');
    }
}