<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnTransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->integer('barId')->default(0)->comment = 'On behalf of';
            $table->text('description')->nullable();
            $table->integer('moduleId')->default(0);
            $table->string('moduleType', 20)->nullable()->comment = '1:order,2:special event,3:ticket Purchase'; 
            $table->float('totalPrice', 10, 2)->default(0)->comment = 'price without tax';
            $table->float('amount', 10, 2)->default(0);
            $table->string('paymentMode', 100)->nullable()->comment="cash/online/card/wallet";             
            $table->text('transactionId')->nullable();
            $table->text('responseData')->nullable()->comment = 'payment gateway response';
            $table->text('remark')->nullable()->comment="custom message";
            $table->boolean('status', 1)->default(0)->comment = ' 1: success & 2: fail';
            $table->date('paymentDate')->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
     
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnTransactions');
    }
}
