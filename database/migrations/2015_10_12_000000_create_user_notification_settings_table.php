<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnUserNotificationSettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->string('latitude', 100)->nullable();
            $table->string('longitude', 100)->nullable();
            $table->boolean('status', 1)->default(0)->comment = '1:Active, 0:Inactive';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    } 
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnUserNotificationSettings');
    }
}