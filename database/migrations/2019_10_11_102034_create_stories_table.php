<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnStories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->string('mediaName', 255)->nullable();
            $table->boolean('mediaType', 1)->default(0)->comment="1:image,2:video";
            $table->longtext('jsonData')->nullable();
            $table->boolean('visibility', 1)->default(0)->comment="1:public,2:contacts,3:private";
            $table->boolean('isDeleted', 1)->default(0)->comment="1:Deleted, 0:Not";
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }	 	  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnStories');
    }
}
