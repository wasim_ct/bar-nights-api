<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnCommissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moduleId')->default(0);
            $table->string('moduleType', 20)->default(0)->comment = '1:event,2:orders,3:tickets';
            $table->float('amount', 10, 2)->default(0);
            $table->float('percentage', 10, 2)->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnCommissions');
    }
}