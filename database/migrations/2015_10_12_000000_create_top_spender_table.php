<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopSpenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnTopSpenders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('month', 100)->nullable();
            $table->integer('position')->default(0)->comment = '1st/2nd/3rd';
            $table->integer('userId')->default(0);
            $table->float('amountSpent', 10, 2)->default(0);
            $table->integer('cityId')->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnTopSpenders');
    }
}