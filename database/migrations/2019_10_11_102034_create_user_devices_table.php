<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnUserDevices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->string('platform', 50)->nullable()->comment="bn/facebook/gmail/instagram";
            $table->text('deviceId')->nullable();
            $table->text('deviceToken')->nullable();
            $table->boolean('deviceType', 1)->default(0)->comment = '1:phone,2:browser';
            $table->string('osVersion',255)->nullable()->comment = '1:android,2:ios';
            $table->string('osType',50)->nullable()->comment="android/ios";
            $table->text('modelName',255)->nullable();
            $table->boolean('loginStatus', 1)->default(0)->comment = '1:login, 0:logout';
            $table->string('latitude', 100)->nullable();
            $table->string('longitude', 100)->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
        
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnUserDevices');
    }
}
