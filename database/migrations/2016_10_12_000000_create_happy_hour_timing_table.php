<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappyHourTimingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnHappyHourTiming', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('odId')->default(0)->comment = 'Primary key id from bnOrderDetails table';
            $table->integer('barId')->default(0);
            $table->string('weekDay', 255)->nullable();
            $table->integer('appliedOnDay')->default(1)->comment = '1:-All,2:-Specific Day';
            $table->time('startTime')->nullable();
            $table->time('endTime')->nullable();
            $table->boolean('status', 1)->default(0)->comment = '1:active, 0:inactive';
            $table->boolean('appliedOn', 1)->default(0)->comment = '1:all,2:specific items';
            $table->boolean('discountType', 1)->default(0)->comment = '1: Discount & 2: Offer';
            $table->integer('buy')->default(0);
            $table->integer('free')->default(0);
            $table->float('discountRate', 10, 2)->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
      
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnHappyHourTiming');
    }
}
