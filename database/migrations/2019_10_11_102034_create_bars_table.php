<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->string('type', 50)->nullable()->comment = '1:bar,2:pub';
            $table->string('barName', 255)->nullable();
            $table->text('about')->nullable();
            $table->string('logo', 255)->nullable();
            $table->float('cgst', 10, 2)->default(0)->comment = '%';
            $table->float('sgst', 10, 2)->default(0)->comment = '%';
            $table->float('serviceCharge', 10, 2)->default(0)->comment = '%';
            $table->float('serviceChargeTax', 10, 2)->default(0)->comment = '%';
            $table->boolean('dayOnOffStatus', 1)->default(0)->comment = '1:on, 0:off';
            $table->boolean('openingStatus', 1)->default(0)->comment = '1:open,0:close';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBars');
    }
}