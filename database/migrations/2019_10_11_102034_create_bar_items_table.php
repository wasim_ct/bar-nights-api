<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarItems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->integer('catId')->default(0);
            $table->integer('subCatId')->default(0);
            $table->string('itemName', 255)->nullable();
            $table->string('itemImage', 255)->nullable();
            $table->text('description')->nullable();
            $table->boolean('itemType', 1)->default(0)->comment = '1:veg,2:non-veg';
            $table->boolean('status', 1)->default(0)->comment = '1:Active, 0:Inactive';
            $table->float('price', 10, 2)->default(0);
            $table->boolean('isCustomized', 1)->default(0)->comment = '1:yes, 0:no';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarItems');
    }
}