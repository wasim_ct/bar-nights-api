<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnOrders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->integer('barId')->default(0);
            $table->string('orderId', 100)->nullable()->comment="For display purpose";
            $table->string('tableNo', 100)->nullable();
            $table->date('orderDate')->nullable();
            $table->boolean('status', 1)->default(0)->comment = '1:running/placed,2:cancel,3:completed';
            $table->float('totalPrice', 10, 2)->default(0);
            $table->float('cgst', 10, 2)->default(0);
            $table->float('cgstAmount', 10, 2)->default(0);
            $table->float('sgst', 10, 2)->default(0);
            $table->float('sgstAmount', 10, 2)->default(0);
            $table->float('serviceCharge', 10, 2)->default(0);
            $table->float('serviceChargeAmount', 10, 2)->default(0);
            $table->float('serviceChargeTax', 10, 2)->default(0);
            $table->float('serviceChargeTaxAmount', 10, 2)->default(0);
            $table->float('grandTotal', 10, 2)->default(0);
            $table->integer('visitId')->default(0);
            $table->integer('friendId')->default(0);
            $table->string('orderType', 50)->nullable()->comment="order/preorder";
            $table->boolean('paymentStatus', 1)->default(0)->comment = '1:paid,2:due';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnOrders');
    }
}
