<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnUsers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userName',255)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('password')->nullable();
            $table->string('userType', 20)->nullable()->comment='A:Admin, B:Bar, U:User';
            $table->string('otp', 10)->nullable();
            $table->boolean('status',1)->default(0)->comment='1:Active, 0:Inactive, 2:Block';
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->rememberToken();
            $table->string('platform', 50)->nullable()->comment='bn/facebook/gmail/instagram';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	      
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnUsers');
    }
}
