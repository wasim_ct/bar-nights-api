<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarBalances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->float('totalEarning',10,2)->default(0);
            $table->float('earningReceived',10,2)->default(0);
            $table->float('earningDue',10,2)->default(0);
            $table->float('commissionPaid',10,2)->default(0); 
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarBalances');
    }
}