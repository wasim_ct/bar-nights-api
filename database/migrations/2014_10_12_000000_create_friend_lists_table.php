<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFriendListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnFriendLists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0)->comment="MyId";
            $table->integer('friendId')->default(0);
            $table->boolean('status',1)->default(0)->comment="0:Pending, 1:Accept, 2:Reject";
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }	  	
	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnFriendLists');
    }
}
