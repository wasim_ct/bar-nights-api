<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountOfferItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnDiscountOfferItems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offerId')->default(0);
            $table->integer('itemId')->default(0)->comment="used if discount is for specific items";
            $table->float('discountRate', 10, 2)->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnDiscountOfferItems');
    }
}
