<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnCarts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->integer('barId')->default(0);
            $table->integer('itemId')->default(0);
            $table->integer('unitTypeId')->default(0);
            $table->integer('quantity')->default(0);
            $table->string('orderType', 50)->nullable()->comment = 'preorder/order';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }	  	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnCarts');
    }
}