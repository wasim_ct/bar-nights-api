<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnDiscountOffers', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('odId')->default(0)->comment = 'Primary key id from bnOfferDetails table';
            $table->integer('barId')->default(0);
            $table->boolean('appliedOn', 1)->default(0)->comment = '1:all,2:specific items';
            $table->float('discountRate', 10, 2)->default(0);
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->time('startTime')->nullable();
            $table->time('endTime')->nullable();
            $table->boolean('status', 1)->default(0)->comment = '1:active, 0:inactive';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnDiscountOffers');
    }
}
