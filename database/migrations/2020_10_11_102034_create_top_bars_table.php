<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnTopBars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('month', 255)->nullable()->comment="august-2019";
            $table->integer('position')->default(0)->comment = '1st/2nd/3rd';
            $table->integer('barId')->default(0);
            $table->float('commissionRate', 10, 2)->default(0);
            $table->integer('cityId')->default(0);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnTopBars');
    }
}