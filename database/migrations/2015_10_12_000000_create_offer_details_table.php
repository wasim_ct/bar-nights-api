<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnOfferDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offerType', 50)->nullable()->comment = 'discount/happyhour';
            $table->integer('barId')->default(0);
            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('offerImage', 255)->nullable();
            $table->integer('templateId')->default(0)->comment="from bnTemplates";
            $table->boolean('status', 1)->default(0)->comment = '0:inactive,1:active';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnOfferDetails');
    }
}
