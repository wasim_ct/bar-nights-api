<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarMediaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarMediaFiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->string('mediaName', 255)->nullable();
            $table->boolean('mediaType', 1)->nullable()->comment = '1:image,2:video';
            $table->string('mediaSize', 100)->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarMediaFiles');
    }
}