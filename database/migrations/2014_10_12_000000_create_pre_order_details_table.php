<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnPreOrderDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId')->default(0);
            $table->integer('itemId')->default(0);
            $table->string('itemName', 255)->nullable();
            $table->integer('quantity')->default(0);
            $table->integer('unitTypeId')->default(0);
            $table->string('unitTypeName', 100)->nullable();
            $table->float('amount', 10, 2)->default(0);
            $table->float('unitPrice', 10, 2)->default(0)->comment="price without quantity";
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
  	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnPreOrderDetails');
    }
}
