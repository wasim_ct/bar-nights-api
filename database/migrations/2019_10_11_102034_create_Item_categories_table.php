<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnItemCategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->string('catName', 255)->nullable();
            $table->string('catImage', 255)->nullable();
            $table->text('catDescription')->nullable();
            $table->integer('parentId')->default(0)->comment = 'For Sub Cat';
            $table->boolean('status', 1)->default(0)->comment = '1:Active, 0:Inactive';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnItemCategories');
    }
}