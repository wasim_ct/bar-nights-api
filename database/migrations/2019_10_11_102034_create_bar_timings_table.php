<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarTimings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->string('weekDay', 50)->nullable();
            $table->boolean('openingStatus',1)->default(0)->comment="1:open,0:close";
            $table->time('openingTime')->nullable();
            $table->time('closingTime')->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
	 	 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarTimings');
    }
}