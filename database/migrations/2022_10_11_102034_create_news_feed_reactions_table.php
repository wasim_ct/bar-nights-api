<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsFeedReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnNewsFeedReactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('newsId')->default(0);
            $table->integer('userId')->default(0);
            $table->integer('reactionId')->default(0)->comment = '1:like,2:dislike,3:sad,4:heart,etc';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnNewsFeedReactions');
    }
}
