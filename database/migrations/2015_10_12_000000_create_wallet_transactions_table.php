<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnWalletTransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0);
            $table->text('description')->nullable();
            $table->integer('moduleId')->default(0);
            $table->string('moduleType', 20)->nullable()->comment = 'order/event';
            $table->float('amount', 10, 2)->default(0)->comment="Can be negative";
            $table->boolean('status', 1)->default(0)->comment = '1: success / 2: fail';
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnWalletTransactions');
    }
}
