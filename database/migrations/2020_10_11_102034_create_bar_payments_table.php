<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnBarPayments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barId')->default(0);
            $table->float('amount', 10, 2)->default(0);
            $table->date('paymentStartDate')->nullable();
            $table->date('paymentEndDate')->nullable();
            $table->string('paymentFor', 100)->default(0)->comment = 'order/event';
            $table->boolean('EntryType', 1)->default(0)->comment = '1:paid,2:free';
            $table->text('remarks')->nullable();
            $table->string('paymentMode', 100)->nullable()->comment="cash/online";
            $table->boolean('paymentStatus', 1)->default(0)->comment = '1:paid,2:due';
            $table->float('paidAmount', 10, 2)->default(0);
            $table->float('commission', 10, 2)->default(0);
            $table->boolean('commissionPaid', 1)->default(0)->comment = '1:yes,2:no';
            $table->text('settlementData')->nullable();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }
       
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bnBarPayments');
    }
}