
$(document).ready(function () {
    //$('body').load(function(){
    var graph = $('#graph').val();
    var time = $('.active .nav-link').html();
    loadGraph(graph, time); //Function calling
    //});
});

$("a.nav-link").click(function () {
    var graph = $('#graph').val();
    var time = $(this).html();
    loadGraph(graph, time); //Function calling					
});

$("select#graph").change(function () {
    var graph = $('#graph').val();
    var time = $('.active .nav-link').html();
    loadGraph(graph, time); //Function calling					
});


//Graph method defination
function loadGraph(graph, time) {
    if (graph !== "" && time !== "") {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Stapages/load_graph",
            data: { graph: graph, time: time },
            dataType: "json",
            success: function (response) {
                /**** active user ****/

                if (response['status'] === true && response['time'] === 'week') {
                    var data = response['data'];

                    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    var d = new Date(data[0]['create_date']);
                    var one = days[d.getDay()];

                    var d = new Date(data[1]['create_date']);
                    var two = days[d.getDay()];

                    var d = new Date(data[2]['create_date']);
                    var three = days[d.getDay()];

                    var d = new Date(data[3]['create_date']);
                    var four = days[d.getDay()];

                    var d = new Date(data[4]['create_date']);
                    var five = days[d.getDay()];

                    var d = new Date(data[5]['create_date']);
                    var six = days[d.getDay()];

                    var d = new Date(data[6]['create_date']);
                    var seven = days[d.getDay()];

                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: response['heading']
                        },
                        xAxis: {
                            categories: [one, two, three, four, five, six, seven],

                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: response['side']
                            }
                        },
                        exporting: { enabled: false },

                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        colors: [
                            '#91cc34'
                        ],
                        series: [{
                            name: response['down'],
                            data: [Number(data[0]['users']), Number(data[1]['users']), Number(data[2]['users']), Number(data[3]['users']), Number(data[4]['users']), Number(data[5]['users']), Number(data[6]['users'])],

                        }]
                    });



                }
                if (response['status'] === true && response['time'] === 'month') {
                    data = response['data'][0];

                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: response['heading']
                        },

                        xAxis: {
                            categories: [
                                'Jan',
                                'Feb',
                                'Mar',
                                'Apr',
                                'May',
                                'Jun',
                                'Jul',
                                'Aug',
                                'Sep',
                                'Oct',
                                'Nov',
                                'Dec'
                            ],

                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: response['side']
                            }
                        },
                        exporting: { enabled: false },

                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        colors: [
                            '#91cc34'
                        ],
                        series: [{
                            name: response['down'],
                            data: [data['Jan'], data['Feb'], data['Mar'], data['Apr'], data['May'], data['Jun'], data['Jul'], data['Aug'], data['Sept'], data['Oct'], data['Nov'], data['Dec']],

                        }]
                    });

                }
                if (response['status'] === true && response['time'] === 'year') {
                    data = response['data'];


                    Highcharts.chart('container', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: response['heading']
                        },
                        xAxis: {
                            labels: [2019],

                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: response['side']
                            }
                        },
                        exporting: { enabled: false },

                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        colors: [
                            '#91cc34'
                        ],
                        series: [{
                            name: response['down'],
                            data: [data['2019']]

                        }]
                    });

                }

            }
        });
    } else {
        alert('Please select graph and time ')
    }
}